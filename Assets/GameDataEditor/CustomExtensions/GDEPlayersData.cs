// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by the Game Data Editor.
//
//      Changes to this file will be lost if the code is regenerated.
//
//      This file was generated from this data file:
//      C:\Users\12345\Documents\Manito\Assets/GameDataEditor/Resources/gde_data.txt
//  </autogenerated>
// ------------------------------------------------------------------------------
using UnityEngine;
using System;
using System.Collections.Generic;

using GameDataEditor;

namespace GameDataEditor
{
    public class GDEPlayersData : IGDEData
    {
        static string PlayersKey = "Players";
		public List<GDEPlayerData>      Players;
		public void Set_Players()
        {
	        GDEDataManager.SetCustomList(_key, PlayersKey, Players);
		}
		

        public GDEPlayersData(string key) : base(key)
        {
            GDEDataManager.RegisterItem(this.SchemaName(), key);
        }
        public override Dictionary<string, object> SaveToDict()
		{
			var dict = new Dictionary<string, object>();
			dict.Add(GDMConstants.SchemaKey, "Players");
			
            dict.Merge(true, Players.ToGDEDict(PlayersKey));
            return dict;
		}

        public override void UpdateCustomItems(bool rebuildKeyList)
        {
            if (Players != null)
            {
                for(int x=0;  x<Players.Count;  x++)
                {
                    GDEDataManager.UpdateItem(Players[x], rebuildKeyList);
                    Players[x].UpdateCustomItems(rebuildKeyList);
                }
            }
        }

        public override void LoadFromDict(string dataKey, Dictionary<string, object> dict)
        {
            _key = dataKey;

			if (dict == null)
				LoadFromSavedData(dataKey);
			else
			{
                dict.TryGetCustomList(PlayersKey, out Players);
                LoadFromSavedData(dataKey);
			}
		}

        public override void LoadFromSavedData(string dataKey)
		{
			_key = dataKey;
			
            Players = GDEDataManager.GetCustomList(_key, PlayersKey, Players);
        }

        public GDEPlayersData ShallowClone()
		{
			string newKey = Guid.NewGuid().ToString();
			GDEPlayersData newClone = new GDEPlayersData(newKey);

            newClone.Players = new List<GDEPlayerData>(Players);
			newClone.Set_Players();

            return newClone;
		}

        public GDEPlayersData DeepClone()
		{
			GDEPlayersData newClone = ShallowClone();
            newClone.Players = new List<GDEPlayerData>();
			if (Players != null)
			{
				foreach(var val in Players)
					newClone.Players.Add(val.DeepClone());
			}
			newClone.Set_Players();
            return newClone;
		}

        public void Reset_Players()
		{
			GDEDataManager.ResetToDefault(_key, PlayersKey);

			Dictionary<string, object> dict;
			GDEDataManager.Get(_key, out dict);

			dict.TryGetCustomList(PlayersKey, out Players);
			Players = GDEDataManager.GetCustomList(_key, PlayersKey, Players);

			Players.ForEach(x => x.ResetAll());
		}

        public void ResetAll()
        {
            GDEDataManager.ResetToDefault(_key, PlayersKey);

            Reset_Players();

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            LoadFromDict(_key, dict);
        }
    }
}
