﻿using Photon;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using ExitGames.Client.Photon;

public class RpsDemoConnect : PunBehaviour
{
    public InputField InputField;
    public string UserId;

	string previousRoomPlayerPrefKey = "PUN:Demo:RPS:PreviousRoom";
	public string previousRoom;

    private const string MainSceneName = "DemoRPS-Scene";

	const string NickNamePlayerPrefsKey = "NickName";


	void Start()
	{
		InputField.text = PlayerPrefs.HasKey(NickNamePlayerPrefsKey)?PlayerPrefs.GetString(NickNamePlayerPrefsKey):"";
	}

	// Connect UI 버튼을 누르면 호출됨
    public void ApplyUserIdAndConnect()
    {
        // 닉네임이 중복되어도 상관없으려나 
        // InputField에 글자가 없으면 DemoNick으로 기본 설정된다
		string nickName = "DemoNick";
        if (this.InputField != null && !string.IsNullOrEmpty(this.InputField.text))
        {
            nickName = this.InputField.text;
			PlayerPrefs.SetString(NickNamePlayerPrefsKey,nickName);
        }
        //if (string.IsNullOrEmpty(UserId))
        //{
        //    this.UserId = nickName + "ID";
        //}
    
        // 이게 무슨 역할을 하는지 찾아볼 필요있음
        if (PhotonNetwork.AuthValues == null)
        {
            PhotonNetwork.AuthValues = new AuthenticationValues();
        }
        //else
        //{
        //    Debug.Log("Re-using AuthValues. UserId: " + PhotonNetwork.AuthValues.UserId);
        //}

		// To be able to ReJoin any room, you need to use UserIDs!
        // 방에 재입장하려면 UserId를 사용한다는군
        // 닉네임을 UserId와 같게 처리하기 때문에 중복될 수 있다
        // UserId 중복이 일어나면 중복나는 사람들을 하나로 인식한다 
		PhotonNetwork.AuthValues.UserId = nickName;

		Debug.Log("Nickname: " + nickName + " userID: " + this.UserId,this);
		


        PhotonNetwork.playerName = nickName;
        PhotonNetwork.ConnectUsingSettings("0.5");
        
        // this way we can force timeouts by pausing the client (in editor)
        // 에디터 일시정지로 타임아웃을 만들 수 있다  
        // ㅇㅎ 백그라운드 스레드가 돌아가면 얼마동안은 온라인 상태를 유지한다 
        // 이 스레드를 미리 꺼놔서 일시정지 버튼 누르면 바로 타임아웃 나도록 한다는거군
        // 포톤 내부 코드를 보면 Connect를 하면 반드시 이 스레드를 실행하도록 되어있음 
        PhotonHandler.StopFallbackSendAckThread();
    }


    public override void OnConnectedToMaster()
    {
        // after connect 
        this.UserId = PhotonNetwork.player.UserId;
        ////Debug.Log("UserID " + this.UserId);

        // 서버 접속을 했는데 이전 룸 기록이 있다면, 룸 이름 가져오기
		if (PlayerPrefs.HasKey(previousRoomPlayerPrefKey))
		{
			Debug.Log("getting previous room from prefs: ");
			this.previousRoom = PlayerPrefs.GetString(previousRoomPlayerPrefKey);
			PlayerPrefs.DeleteKey(previousRoomPlayerPrefKey); // we don't keep this, it was only for initial recovery
		}


        // after timeout: re-join "old" room (if one is known)
        // 룸 이름이 Null이 아니라면 재접속하기 
        if (!string.IsNullOrEmpty(this.previousRoom))
        {
            Debug.Log("ReJoining previous room: " + this.previousRoom);
            PhotonNetwork.ReJoinRoom(this.previousRoom);
            // we only will try to re-join once. if this fails, we will get into a random/new room
            // 딱 한번만 시도하고, 안되면 새로운 방을 만들거나 랜덤방에 접속한다 
            this.previousRoom = null;       
        }
        else
        {
            // else: join a random room
            PhotonNetwork.JoinRandomRoom();
        }
    }

    public override void OnJoinedLobby()
    {
        OnConnectedToMaster(); // this way, it does not matter if we join a lobby or not
    }

    public override void OnPhotonRandomJoinFailed(object[] codeAndMsg)
    {
		Debug.Log("OnPhotonRandomJoinFailed");

		// important! MAX Player 2, TTL 20 sec 
        PhotonNetwork.CreateRoom(null, new RoomOptions() { MaxPlayers = 2, PlayerTtl = 0000 }, null);
    }

    public override void OnJoinedRoom()
    {
		Debug.Log("Joined room: " + PhotonNetwork.room.Name);

		// save room name for that use when next connection 
        this.previousRoom = PhotonNetwork.room.Name;
		PlayerPrefs.SetString(previousRoomPlayerPrefKey,this.previousRoom);

    }

    public override void OnPhotonJoinRoomFailed(object[] codeAndMsg)
	{
		Debug.Log ("OnPhotonJoinRoomFailed");
		this.previousRoom = null;
		PlayerPrefs.DeleteKey (previousRoomPlayerPrefKey);

        // rejoin에 실패를 하면 자동으로 랜덤방에 접속을 하더라 
        // 딱히 그걸 호출하는 코드가 이 함수안에는 전개되지 않아서 의아했다 
        // 알고보니 이렇게 방 접속 실패를 하면, 서버 연결을 강제로 끊고 다시 재접속을 하더라 
        // (GameEnteredOnGameServer -> DisconnectToReconnect)
        // 다시 재연결이 되면서 이벤트 함수인 OnConnectedToMaster가 재호출된다 
    }

    public override void OnConnectionFail(DisconnectCause cause)
    {
        Debug.Log("Disconnected due to: " + cause + ". this.previousRoom: " + this.previousRoom);
    }
	
	public override void OnPhotonPlayerActivityChanged(PhotonPlayer otherPlayer)
	{
		Debug.Log("OnPhotonPlayerActivityChanged() for "+otherPlayer.NickName+" IsInactive: "+otherPlayer.IsInactive);
	}

}
