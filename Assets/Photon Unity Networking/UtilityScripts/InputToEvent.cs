using UnityEngine;

/// <summary>
/// Utility component to forward mouse or touch input to clicked gameobjects.
/// Calls OnPress, OnClick and OnRelease methods on "first" game object.
/// </summary>
public class InputToEvent : MonoBehaviour
{
    private GameObject lastGo;
    public static Vector3 inputHitPos;
    public bool DetectPointedAtGameObject;
    public static GameObject goPointedAt { get; private set; }
    public UICamera nguiCamera;
    private Vector2 pressedPosition = Vector2.zero;
    private Vector2 currentPos = Vector2.zero;
    [System.NonSerialized] public bool Dragging;
    public static bool canMove;
    private Camera m_Camera;

    public Vector2 DragVector
    {
        get { return this.Dragging ? this.currentPos - this.pressedPosition : Vector2.zero; }
    }

    private void Start()
    {
        this.m_Camera = GetComponent<Camera>();
        canMove = true;
    }

    // Update is called once per frame
    private void Update()
    {
        if (canMove == false)
            return;
        // 되게 간단하게 구현되어있네 아주 간편한 부분
        if (this.DetectPointedAtGameObject)
        {
            goPointedAt = RaycastObject(Input.mousePosition);
        }

        // 쭈우욱 드래그를 하고 있는 상태 
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            this.currentPos = touch.position;

            if (touch.phase == TouchPhase.Began)
            {
                Press(touch.position);
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                Release(touch.position);
            }

            return;
        }

        this.currentPos = Input.mousePosition;
        if (Input.GetMouseButtonDown(0))
        {
            Press(Input.mousePosition);
        }
        if (Dragging)
        {
            Drag(Input.mousePosition);
        }
        if (Input.GetMouseButtonUp(0))
        {
            Release(Input.mousePosition);
        }

        if (Input.GetMouseButtonDown(1))
        {
            this.pressedPosition = Input.mousePosition;
            this.lastGo = RaycastObject(this.pressedPosition);
            if (this.lastGo != null)
            {
                this.lastGo.SendMessage("OnPressRight", SendMessageOptions.DontRequireReceiver);
            }
        }
    }


    private void Press(Vector2 screenPos)
    {
        if (UICamera.hoveredObject != null && UICamera.hoveredObject.layer == 5)
            return;

        this.pressedPosition = screenPos;
        this.Dragging = true;

        this.lastGo = RaycastObject(screenPos);
        if (this.lastGo != null)
        {
            this.lastGo.SendMessage("OnPress", inputHitPos, SendMessageOptions.DontRequireReceiver);
        }
    }

    private void Drag(Vector2 screenPos)
    {
        GameObject currentGo = RaycastObject(screenPos);
        if (currentGo != null)
        {
            currentGo.SendMessage("OnDrag", inputHitPos, SendMessageOptions.DontRequireReceiver);
        }
    }

    private void Release(Vector2 screenPos)
    {
        if (this.lastGo != null)
        {
            GameObject currentGo = RaycastObject(screenPos);
            if (currentGo == this.lastGo && DragVector.sqrMagnitude < 20)
            {
                this.lastGo.SendMessage("OnClick", inputHitPos, SendMessageOptions.DontRequireReceiver);
            }

            this.lastGo.SendMessage("OnRelease", SendMessageOptions.DontRequireReceiver);
            this.lastGo = null;
        }

        this.pressedPosition = Vector2.zero;
        this.Dragging = false;
    }

    private GameObject RaycastObject(Vector2 screenPos)
    {
        RaycastHit info;
        if (Physics.Raycast(this.m_Camera.ScreenPointToRay(screenPos), out info, 500))
        {
            inputHitPos = info.point;
            return info.collider.gameObject;
        }

        return null;
    }
}