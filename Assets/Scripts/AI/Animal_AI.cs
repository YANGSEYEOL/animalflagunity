using System.Collections;
using System.Collections.Generic;
using GameDataEditor;
using Vectrosity;
using UnityEngine;

public class Animal_AI 
{
    public Animal animal;

    public List<AttackCommand.AttackTargetInfo> attackableAnimals = new List<AttackCommand.AttackTargetInfo>();

    public AttackCommand.AttackTargetInfo targetAnimalInfo;

    

    public IEnumerator DoAI()
    {
        if (!animal.canAction)
            yield break;

        /// 카메라 패닝
        CameraMovement.instance.followingTarget = animal.isVisible ? animal.transform : null;

        /// HPBar on
        animal.SetHPBarVisible(animal.ownedTile.isVisible);
        if (animal.isVisible) yield return new WaitForSeconds(1.0f);

        /// AI Core
        yield return DoAICore();

        CameraMovement.instance.followingTarget = null;

        /// HPBar off
        animal.SetHPBarVisible(false);

        GameManager.instance.OnAnimalDidAction(animal);
    }

    
    protected virtual IEnumerator DoAICore()
    {
        // 운송중인 동물이 본인인 경우
        if (GameManager.instance.flags[0].carryingAnimal == animal)
        {
            /// 깃발을 가지고 있다면, 선 공격이 아닌 선 이동을 택한다 
            yield return MoveToOurFlagPosition();

            /// 공격하거나 경계하거나
            yield return AttackOrOverwatch();
        }
        else
        {
            // 공격범위 내에 들어오는 동물 리스트를 갱신한다
            AttackCommand.instance.GetAllAttackableTargetInfoList(animal, ref attackableAnimals);
        
            if (attackableAnimals.Count > 0)
            {
                yield return DirectingAttackInMyRange();
            }
            else
            {
                /// 시야 내에 있는 적을 찾아내고, 접근한다 
                /// 시야 내에 없으면 적 기지로 이동시킨다 
                yield return SearchEnemyAndApproach();

                /// 공격하거나 경계하거나
                yield return AttackOrOverwatch();
            }
        } 
    }

    
    protected IEnumerator MoveToOurFlagPosition()
    {
        if (GameManager.instance.flags[0].carryingAnimal != animal || !animal.canMove) yield break;

        // 우리 깃발과 가장 가까운 path 경로 찾기 
        TileChunk.instance.dijkstra.FindPaths(animal, animal.curremainMovement);
        List<int[]> moveableTileListForLeap = TileChunk.instance.dijkstra.LoadValidTiles();

        Tile closePathTileToOurFlag = null;
        float curTileDistance = 9999f;
        foreach (int[] path in moveableTileListForLeap)
        {
            Tile tile = TileChunk.instance.tiles[path[1] % TileChunk.instance.tileSizeWidth, path[1] / TileChunk.instance.tileSizeHeight];

            float distance = Vector3.Distance(tile.position, GameManager.instance.flags[1].ownedTile.position);
            if (distance < curTileDistance)
            {
                curTileDistance = distance;
                closePathTileToOurFlag = tile;
            }
        }

        yield return MoveToTargetTile(closePathTileToOurFlag);

        if (animal.isVisible) yield return new WaitForSeconds(0.5f);
    }


    protected virtual IEnumerator AttackOrOverwatch()
    {
        /// 공격가능한 동물이 있는지 서칭해서 공격 시도해본다 
        yield return DirectingAttackInMyRange();

        /// 공격할 대상없어서 공격 못했으면, 경계 커맨드로 마무리 
        yield return Overwatch();
    }


    protected IEnumerator Overwatch()
    {
        if (animal.canAction)  
        {
            animal.Overwatch();

            if (animal.isVisible) yield return new WaitForSeconds(1.0f);
        }
    }


    protected IEnumerator DirectingAttackInMyRange()
    {
        if (!animal.canAction) yield break;

        // 공격범위 내에 들어오는 동물 리스트를 갱신한다
        AttackCommand.instance.GetAllAttackableTargetInfoList(animal, ref attackableAnimals);
        
        // 헌터가 아닌 동물만 사냥한다.
        if (attackableAnimals.Count <= 0) yield break;
        
        targetAnimalInfo = attackableAnimals[Random.Range(0, attackableAnimals.Count)];

        /// 공격할 때는 동물 시야에 안 보이더라도 무조건 보여지게 한다 
        animal.SetVisible(true);
        animal.SetHPBarVisible(true);
        animal.ownedTile.isVisible = true;
        TileChunk.instance.fogOfWar.RefreshTexture();

        // 공격대상을 표시
        //TileChunk.instance.DisplayTileHighlight(targetAnimalInfo.animal.ownedTile, "TARGET_" + targetAnimalInfo.animal.photonView.viewID, Color.yellow);
        targetAnimalInfo.animal.SetHPBarVisible(true);

        /// 타겟과 헌터 사이로 카메라를 위치시켜 둘다 보이게 만든다 
        CameraMovement.instance.followingTarget = null; /// 카메라 팔로잉 풀기 
        Camera.main.MoveTo((animal.ownedTile.position + targetAnimalInfo.animal.ownedTile.position) / 2f);

        yield return AttackCoroutine();
        
        if (animal.isVisible) yield return new WaitForSeconds(1.5f);

        /// 공격대상표시를 제거
        //TileChunk.instance.UndisplayLine("TARGET_" + targetAnimalInfo.animal.photonView.viewID);
        targetAnimalInfo.animal.SetHPBarVisible(false);

        /// 강제로 보여지게 했던거 해제
        TileChunk.instance.RefreshFieldOfView();
        animal.SetVisible(animal.ownedTile != null && animal.ownedTile.isVisible);
        animal.SetHPBarVisible(animal.ownedTile != null && animal.ownedTile.isVisible);
    }


    protected virtual IEnumerator AttackCoroutine()
    {
        /// 공격 판정 주사위 돌리기 
        AttackCommand.HitResult hitResult = AttackCommand.instance.RollForHit(targetAnimalInfo.hitResultTable, targetAnimalInfo.animal);
        int damage = AttackCommand.instance.FinalizeDamage(animal, targetAnimalInfo.animal, hitResult);

        yield return animal.Attack(targetAnimalInfo.animal.photonView.viewID, (int)hitResult, damage);
    }


    protected IEnumerator SearchEnemyAndApproach()
    {
        if (!animal.canMove) yield break;

        /// 이동은 공격할 대상이 없을 때, 동물들을 찾기 위해서 한다 
        TileChunk.instance.dijkstra.FindPaths(animal, animal.curremainMovement);
        List<int[]> moveableTileList = TileChunk.instance.dijkstra.LoadValidTiles();

        /// 시야 범위 안에 있는 동물이 있는지 체크 
        List<Animal> enemyListInMySight = GameManager.instance.enemyListInMySight;

        /// 시야안에 동물이 있는 경우
        Tile targetTile = null;
        if (enemyListInMySight.Count > 0)
        {
            /// 가장 가까운 녀석 골라내기 
            Animal closeAnimal = enemyListInMySight[0];
            float curDistance = 9999f; 
            foreach (Animal userAnimal in enemyListInMySight)
            {
                float distance = Vector3.Distance(closeAnimal.ownedTile.position, userAnimal.ownedTile.position);
                if (distance < curDistance)
                {
                    curDistance = distance;
                    closeAnimal = userAnimal;
                }
            }

            /// 그 녀석과 가장 가까운 이동 가능한 타일 골라내기 
            float curTileDistance = 9999f;
            foreach (int[] path in moveableTileList)
            {
                Tile tile = TileChunk.instance.tiles[path[1] % TileChunk.instance.tileSizeWidth, path[1] / TileChunk.instance.tileSizeHeight];

                float distance = Vector3.Distance(tile.position, closeAnimal.ownedTile.position);
                if (distance < curTileDistance)
                {
                    curTileDistance = distance;
                    targetTile = tile;
                }
            }
        }
        /// 시야 범위안에도 동물이 없다면 
        else
        {
            /// 상대 진영 쪽으로 이동시키기 
            /// 우리 깃발이 빼았기더라도 깃발을 선점하려면 진영쪽으로 와야하니, 상대진영쪽으로 계속 이동 시키는게 공격적인 행동이다 
            List<Tile> advanceTileList = new List<Tile>();
            foreach (int[] path in moveableTileList)
            {
                int tileIndex_X = path[1] % TileChunk.instance.tileSizeWidth;
                int tileIndex_Y = path[1] / TileChunk.instance.tileSizeHeight;

                /// Y 인덱스가 내가 위치한 타일의 Y 인덱스보다 크면 아래에 위치한 타일이다
                /// 동시에, 적의 깃발 위치를 넘어서게 하지 않는다 
                if (tileIndex_Y > animal.ownedTile.TileIndex_Y && tileIndex_Y <= GameManager.instance.flags[0].originPositionTile.TileIndex_Y)
                {
                    advanceTileList.Add(TileChunk.instance.tiles[tileIndex_X, tileIndex_Y]);
                }
            }

            /// 아래에 있는 타일들 중 하나 랜덤으로 골라서 선택 
            if (advanceTileList.Count > 0) 
            {
                targetTile = advanceTileList[Random.Range(0, advanceTileList.Count)];
            }
            /// 아래로 갈 수 있는 타일이 하나도 없으면 path 타일 중 그냥 하나 랜덤으로 지정해서 이동 
            else
            {
                int[] randomPath = moveableTileList[Random.Range(0, moveableTileList.Count)];
                int tileIndex_X = randomPath[1] % TileChunk.instance.tileSizeWidth;
                int tileIndex_Y = randomPath[1] / TileChunk.instance.tileSizeHeight;
                targetTile = TileChunk.instance.tiles[tileIndex_X, tileIndex_Y];
            }
        }

        // 이동
        yield return MoveToTargetTile(targetTile);
    }

    
    protected IEnumerator MoveToTargetTile(Tile targetTile)
    {
        if (targetTile == null) yield break;
      /*  
        int[] shortestPath = TileChunk.instance.GetShortestPathForMovement(animal, targetTile._tileIndex, animal.curremainMovement);

        /// 이동 path들이 전부 안개에 가려진다면 순간이동시킨다
        /// 하지만, path 중 하나라도 시야에 보여지는 타일이 있으면 무빙 연출을 보여준다 
        bool foundVisibleTileInPath = animal.isVisible;
        if (!animal.isVisible)
        {
            for (int i = shortestPath[0] - 1; i >= 1; --i)
            {
                int pathTileIndex = shortestPath[i];

                int tileX = pathTileIndex % TileMaker.instance.tileSize;
                int tileY = pathTileIndex / TileMaker.instance.tileSize;
                Tile tile = TileChunk.instance.tiles[tileX, tileY];
                
                if (tile.isVisible) 
                {
                    foundVisibleTileInPath = true;
                    break;
                }
            }
        }

        if (!GameManager.instance.showSightOfAI && !foundVisibleTileInPath)
        {
            animal.ownedTile = targetTile;
            animal.transform.position = animal.GetRealPositionByTile(targetTile);
        }
        else */
        {
            IEnumerator moveCoroutine = animal.MoveIfCanMove(targetTile);
            while (animal.IsTrackedCoroutineRunning(moveCoroutine))
            {
                CameraMovement.instance.followingTarget = animal.movingTile != null && animal.movingTile.isVisible ? animal.transform : null;
                animal.SetHPBarVisible(animal.movingTile != null && animal.movingTile.isVisible);
                
                yield return null;
            }   
        }

        if (animal.isVisible) yield return new WaitForSeconds(0.5f);
    }
}