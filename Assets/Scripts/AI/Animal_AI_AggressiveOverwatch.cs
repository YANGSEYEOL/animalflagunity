﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Animal_AI_AggressiveOverwatch : Animal_AI 
{
    protected override IEnumerator AttackOrOverwatch()
    {
        /// 공격가능한 동물이 있는지 서칭해서 공격 시도해본다 
        yield return DirectingAttackInMyRange();

        List<Animal> enemyListInMySight = GameManager.instance.enemyListInMySight;

        /// 공격은 할 수 없지만, 내 시야 범위안에 동물이 들어와있는 상태라면 포악한 경계 커맨드를 발동한다 
        if (animal.canAction && enemyListInMySight.Count > 0 && animal.curRemainSkillCoolTime <= 0)
        {
            animal.AggressiveOverwatch();

            if (animal.isVisible) yield return new WaitForSeconds(1.0f);
        }

        /// 공격할 대상없어서 공격 못했으면, 경계 커맨드로 마무리 
        if (animal.canAction)  
        {
            animal.Overwatch();

            if (animal.isVisible) yield return new WaitForSeconds(1.0f);
        }
    }
}
