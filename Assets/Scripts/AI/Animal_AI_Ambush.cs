﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Animal_AI_Ambush : Animal_AI
{

    List<Animal> animalsInRange;
    List<Tile> tilesToJump;

    const int FOREST_MIN_DISTANCE = 3;

    protected override IEnumerator DoAICore()
    {
        // 운송중인 동물이 본인인 경우
        if (GameManager.instance.flags[0].carryingAnimal == animal)
        {
            /// 깃발을 가지고 있다면, 선 공격이 아닌 선 이동을 택한다 
            yield return MoveToOurFlagPosition();

            /// 공격하거나 경계하거나
            yield return AttackOrOverwatch();
        }
        else
        { 
            // ambush 사정거리에 들어오는 동물들을 리스트에 저장한다.
            AmbushCommand.instance.GetEnemiesWithinRange(animal, ref animalsInRange, ref tilesToJump); 
            // 평타 범위 내에 들어오는 동물 리스트를 갱신한다
            AttackCommand.instance.GetAllAttackableTargetInfoList(animal, ref attackableAnimals);

            // validThrustTiles에 있는 타일중 하나 랜덤하게 thrust
            // thrust할 수 없는 상황이라면 평타 사거리 내에 있는 동물이 있다면 평타를 친다.
            if (animalsInRange.Count > 0 && animal.ownedTile.type == TileMaker.TileType.FOREST)
            {
                int targetIndex = Random.Range(0, animalsInRange.Count);
                var ambushCoroutine = AmbushCommand.instance.Ambush(animal, animalsInRange[targetIndex], tilesToJump[targetIndex]);
                while (ambushCoroutine.MoveNext()) yield return ambushCoroutine.Current;
            }
            else if (attackableAnimals.Count > 0)
            {
                var attackCoroutine = DirectingAttackInMyRange();
                while (attackCoroutine.MoveNext()) yield return attackCoroutine.Current;
            }
            // 평타 사거리에 동물이 없다면 이동한다.
            else
            { 
                // 마지막에 ambush나 평타를 쓸 수 있는 상황인지 체크하고 ㄱㄱ
                if (animal.curHealth > 0)
                {
                    /// 시야 내에 있는 적을 찾아내고, 접근한다 
                    /// 시야 내에 없으면 적 기지로 이동시킨다 
                    yield return SearchEnemyAndApproach();

                    AmbushCommand.instance.GetEnemiesWithinRange(animal, ref animalsInRange, ref tilesToJump);
                    if (animalsInRange.Count > 0 && animal.ownedTile.type == TileMaker.TileType.FOREST)
                    {
                        int targetIndex = Random.Range(0, animalsInRange.Count);
                        var ambushCoroutine = AmbushCommand.instance.Ambush(animal, animalsInRange[targetIndex], tilesToJump[targetIndex]);
                        while (ambushCoroutine.MoveNext()) yield return ambushCoroutine.Current;
                    }
                    else
                    {
                        /// 공격하거나 경계하거나
                        yield return AttackOrOverwatch();
                    }
                }
            }
        }
    }
}
