﻿using UnityEngine;
using System.Collections;


public class Animal_AI_DoubleAttack : Animal_AI 
{
    protected override IEnumerator AttackCoroutine()
    {
        AttackCommand.HitResult[] hitResults;
        int[] damages;

        /// 판정 주사위 굴리기
        DoubleAttackCommand.instance.RollForHitTwice(animal, targetAnimalInfo, out hitResults, out damages);

        int[] hitResultInt = new int[2];
        hitResultInt[0] = (int)hitResults[0];
        hitResultInt[1] = (int)hitResults[1];

        if (animal.canAction && animal.curRemainSkillCoolTime <= 0)
        {
            yield return animal.DoubleAttack(targetAnimalInfo.animal.photonView.viewID, hitResultInt, damages);
        }
        else
        {
            yield return animal.Attack(targetAnimalInfo.animal.photonView.viewID, hitResultInt[0], damages[0]);
        }
    }
}
