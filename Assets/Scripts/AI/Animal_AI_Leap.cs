﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Animal_AI_Leap : Animal_AI 
{
    bool canLeap { get { return animal.curHealth > 0 && animal.canAction && animal.curRemainSkillCoolTime <= 0; } }

	protected override IEnumerator DoAICore()
    {
        /// 상대 깃발 위치로 한번에 갈 수 있는지 계산
        bool foundPathToGoEnemyFlag = false;
        TileChunk.instance.dijkstra.FindPaths(animal, animal.curremainMovement);
        List<int[]> moveableTileList = TileChunk.instance.dijkstra.LoadValidTiles();
        foreach (int[] path in moveableTileList)
        {
            int tileIndex_X = path[1] % TileChunk.instance.tileSizeWidth;
            int tileIndex_Y = path[1] / TileChunk.instance.tileSizeHeight;
            Tile tile = TileChunk.instance.tiles[tileIndex_X, tileIndex_Y];

            if (GameManager.instance.flags[0].ownedTile == tile)
            {
                foundPathToGoEnemyFlag = true;

                break;
            }
        }

        if (GameManager.instance.flags[0].carryingAnimal == null && canLeap && foundPathToGoEnemyFlag)
        {
            // 이동
            yield return MoveToTargetTile(GameManager.instance.flags[0].ownedTile);

            yield return LeapIfCanLeap();
        }
        // 운송중인 동물이 본인인 경우
        else if (GameManager.instance.flags[0].carryingAnimal == animal)
        {
            /// 깃발을 가지고 있다면, 선 공격이 아닌 선 이동을 택한다 
            yield return MoveToOurFlagPosition();

            yield return LeapIfCanLeap();
        }
        // 깃발로 한번에 갈 수 있는 경로가 없거나 우리 팀 중 깃발을 운송중인 녀석이 있다면, 일반 AI처럼 행동한다 
        else
        {
            yield return base.DoAICore();
        }
    }


    IEnumerator LeapIfCanLeap()
    {
        /// 온전하게 깃발을 쟁취했다면
        if (GameManager.instance.flags[0].carryingAnimal == animal && canLeap)
        {
            // 도약
            animal.Leap();

            if (animal.isVisible) yield return new WaitForSeconds(0.5f);

            // 우리 깃발로 이동                
            yield return MoveToOurFlagPosition();
        }
        /// 이동 중에 방해받았다면 (혹은 죽었거나)
        else
        {
            /// 공격하거나 경계하거나
            yield return AttackOrOverwatch();
        }
    }
}
