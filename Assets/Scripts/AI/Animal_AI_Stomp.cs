﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Animal_AI_Stomp : Animal_AI
{
    List<Tile> validTilesForStomp = new List<Tile>();

    protected override IEnumerator DoAICore()
    {
        // 운송중인 동물이 본인인 경우
        if (GameManager.instance.flags[0].carryingAnimal == animal)
        {
            /// 깃발을 가지고 있다면, 선 공격이 아닌 선 이동을 택한다 
            yield return MoveToOurFlagPosition();

            /// 공격하거나 경계하거나
            yield return AttackOrOverwatch();
        }
        else
        {
            // 현재 이동 가능 타일에서 최고의 점수를 낼 수있는 타일을 불러온다.
            StompCommand.instance.GetBestTileToStomp(animal, validTilesForStomp);
            // 평타 범위 내에 들어오는 동물 리스트를 갱신한다
            AttackCommand.instance.GetAllAttackableTargetInfoList(animal, ref attackableAnimals);

            // stomp할만한 타일이 있다면 
            if (validTilesForStomp.Count > 0)
            {
                Tile targetTile = validTilesForStomp[Random.Range(0, validTilesForStomp.Count)];
                int[] shortestPath = TileChunk.instance.GetShortestPathForMovement(animal, targetTile._tileIndex, animal.curremainMovement);

                /// 이동 path들이 전부 안개에 가려진다면 순간이동시킨다
                /// 하지만, path 중 하나라도 시야에 보여지는 타일이 있으면 무빙 연출을 보여준다 
                bool foundVisibleTileInPath = animal.isVisible;
                if (!animal.isVisible)
                {
                    for (int i = shortestPath[0] - 1; i >= 1; --i)
                    {
                        int pathTileIndex = shortestPath[i];

                        int tileX = pathTileIndex % TileMaker.instance.tileSize;
                        int tileY = pathTileIndex / TileMaker.instance.tileSize;
                        Tile tile = TileChunk.instance.tiles[tileX, tileY];

                        if (tile.isVisible)
                        {
                            foundVisibleTileInPath = true;
                            break;
                        }
                    }
                }

                if (!GameManager.instance.showSightOfAI && !foundVisibleTileInPath)
                {
                    animal.ownedTile = targetTile;
                    animal.transform.position = animal.GetRealPositionByTile(targetTile);
                }
                else
                {
                    IEnumerator moveCoroutine = animal.MoveIfCanMove(targetTile);
                    while (animal.IsTrackedCoroutineRunning(moveCoroutine))
                    {
                        CameraMovement.instance.followingTarget = animal.movingTile != null && animal.movingTile.isVisible ? animal.transform : null;
                        animal.SetHPBarVisible(animal.movingTile != null && animal.movingTile.isVisible);

                        yield return moveCoroutine.Current;
                    }
                }

                var stompCoroutine = StompCommand.instance.Stomp(animal);
                while (stompCoroutine.MoveNext()) yield return stompCoroutine.Current;
            }
            else if (attackableAnimals.Count > 0)
            {
                var attackCoroutine = DirectingAttackInMyRange();
                while (attackCoroutine.MoveNext()) yield return attackCoroutine.Current;
            }
            // 평타 사거리에 동물이 없다면 이동한다.
            else
            {
                /// 시야 내에 있는 적을 찾아내고, 접근한다 
                /// 시야 내에 없으면 적 기지로 이동시킨다 
                yield return SearchEnemyAndApproach();
                /// 공격하거나 경계하거나
                yield return AttackOrOverwatch();
            }
        }
    }
}
