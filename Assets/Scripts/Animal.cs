﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using GameDataEditor;
using Vectrosity;
using I2.Loc;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public delegate void OnAnimalAttackedDelegate(Animal animal, Animal targetAnimal);
public delegate void OnMovedDelegate(Animal animal, Tile tile);
public delegate void OnStartMoveDelegate(Animal animal, Tile tile);
public delegate void OnFinishedMoveDelegate(Animal animal);
public delegate void OnFinishedAttackDelegate(Animal animal);
public delegate void OnChangedCanActionDelegate(bool canAction);
public delegate void OnChangedCanRetrieveFlagDelegate(bool canRetrieveFlag);
public delegate void OnChangedOwnedTileDelegate(Tile tile);
public delegate void OnDeathDelegate(Animal animal);

public partial class Animal : CacheMonobehavior
{
    [System.NonSerialized]
    public GameObject UIPositionTransform;
    GDEAnimalData _animalData;

    //자기가 속한 동물리스트(GameManager내의 animals1ForSimulation, animals2ForSimulation...)
    public List<Animal> animalListBelongTo = null;

    [System.NonSerialized]
    public HPBar hpBar;
   
    public OnMovedDelegate OnMoved = delegate { };
    public OnStartMoveDelegate OnStartMove = delegate { };
    public OnFinishedMoveDelegate OnFinishedMove = delegate { };
    public OnFinishedAttackDelegate OnFinishedAttack = delegate { };
    public OnChangedCanActionDelegate OnChangedCanAction = delegate { };
    public OnChangedCanRetrieveFlagDelegate OnChangedCanRetrieveFlag = delegate { };
    public OnChangedOwnedTileDelegate OnChangedOwnedTile = delegate { };
    public OnDeathDelegate OnDeath = delegate { };

    /// 동물 안보이게 만들려고 쓰는 변수
    [System.NonSerialized]
    public MeshRenderer meshRenderer;

    /*앞 좌 우 뒤*/
    [System.NonSerialized]
    public bool[] biteDirections;

    GameObject targetHighlight;

    /// 현재 상주하고 있는 타일 
    Tile _ownedTile;
    public Tile ownedTile
    {
        get { return _ownedTile; }
        set
        {
            if (_ownedTile != null)
            {
                TileChunk.instance.ChnangeTileOccupation(_ownedTile._tileIndex, false);
                _ownedTile.owningAnimal = null;
                // 아니 왜 한번하면 해제가 안되고 두번하면 되냐?   
                _ownedTile.OnChangedTileVisible -= OnChangedTileVisible;
                _ownedTile.OnChangedTileVisible -= OnChangedTileVisible;
            }

            _ownedTile = value;

            if (value != null)
            {
                TileChunk.instance.ChnangeTileOccupation(value._tileIndex, true);
                value.owningAnimal = this;
                value.OnChangedTileVisible += OnChangedTileVisible;
            }

            OnChangedOwnedTile(value);
        }
    }

    // ownedTile이 아니라 실제 모델이 지나가고있는 타일
    Tile currentTile;

    private Material originalMaterial;
    private Color originalColor;
    private bool isFlashing;
    [System.NonSerialized]
    public Transform modelTransform;

    public GDEAnimalData animalData
    {
        get { return _animalData; }
        set { _animalData = value; Init(); }
    }

    [System.NonSerialized]
    public string species;
    [System.NonSerialized]
    public int tier = 0;

    [System.NonSerialized]
    public int curremainMovement;

    int _curHealth;
    public int curHealth
    {
        get { return _curHealth; }
        set
        {
            if (_curHealth == value || value > animalData.health) return;

            _curHealth = value;
        }
    }

    [System.NonSerialized]
    public int curRange;
    [System.NonSerialized]
    public int curSight;
    [System.NonSerialized]
    public int curNightSight;

    Vector3 lookVector;

    [System.NonSerialized]
    public int curHearingRange;
    [System.NonSerialized]
    public int curSmellRange;

    [System.NonSerialized]
    public bool isMoving;

    bool appearThisTurn;

    public float movingAnimSpeed
    {
        get { return animator != null ? animator.GetFloat("movingAnimSpeed") : 1f; }
        set { if (animator != null) animator.SetFloat("movingAnimSpeed", value); }
    }

    // canAction이 false가 되지않는 행동을 하고있는 상태
    // 이 변수는 이번턴에 행동을 시작했는지 알기 위해 만들어놓음
    public bool isDoingAction = false;

    /// Move를 시작할 때 바로 ownedTile을 도착 지점 타일로 바꾸기 때문에 시야 갱신을 하거나 할 때, 현재 움직이고 있는 타일 위치를 알려줄 또 하나의 타일 변수가 필요했다
    public Tile movingTile;
    [System.NonSerialized]
    public float moveSpeed = 10f;
    
    bool _canAction = true;
    public bool canAction
    {
        get { return _canAction && curHealth > 0; }
        set { _canAction = value; OnChangedCanAction(this); }
    }

    public bool isFinishedTurnOrder
    {
        get
        {
            bool isFinishedTurnOrder = !canAction;

            /// 도약 스킬을 사용했다면 행동하나를 소모해서 이동 한 번을 더하기 때문에, 이동과 행동 둘다 소모되어야 턴 행동이 끝난걸로 처리한다
            if (isLeaped)
            {
                isFinishedTurnOrder = !canAction && curremainMovement == 0;
            }

            return isFinishedTurnOrder;
        }
    }

    /// 행동 후 이동을 불가능하게 한 이유는 행동 하나의 하나의 리스크를 주는 것이다
    /// 잘못된 행동 이후, 이동이 가능하다면 행동을 신중하게 선택하지 않을 것이기 때문
    /// 플레이어의 신중한 행동 선택을 유도한다
    public bool canMove
    {
        get
        {
            bool canMove = curremainMovement > 0 && canAction;

			/// 도약 스킬을 사용했다면, 행동 사용 유무가 이동의 영향을 미치지 않게 한다 
            if (isLeaped)
            {
                canMove = curremainMovement > 0;
            }

            return canMove && curHealth > 0;
        }
    }

    public enum Direction {NONE, FRONT, BEHIND, SIDE }


    /// owner였던 플레이어가 재접속 할 때, owner 였는지 아니였는지를 확인하기 위한 변수 
    /// photonView.owner는 owner가 나가면 null이 되어버림 
    [System.NonSerialized]
    public string ownerUserId;

    /// 사운드
    [SerializeField]
    public AudioClip movingSound;
    [SerializeField]
    public AudioClip[] clickSound;
    [SerializeField]
    public AudioClip deathSound;


    /// 물에 얼마나 빠질지에 대한 y 값
    [SerializeField]
    public int yFactor_fallingIntoWater = -10;
    /// 물에서 얼마나 뒤로 기울일지 X angle값
    [SerializeField]
    public int xAngle_fallingIntoWater = -22;

    /// 모델 메쉬(default)를 y축 0 위에 올리기 위한 보정값
    /// 애니메이션에서 메쉬의 position을 수정하면 프리팹에 설정해놨던 보정값이 날라가기 때문에
    /// 애니메이션 하기전에 부모 transform에다 보정값을 더해주고, 애니메이션이 끝나면 다시 빼줘야 한다
    Vector3 meshOffset;

    // 메시 렌더러
    //[System.NonSerialized]
    public bool isVisible
    {
        get { return meshRenderer.enabled; }
        set
        {
            if (meshRenderer.enabled != value)
            {
                meshRenderer.enabled = value;

                // 만약 동물이 보이게 된다면 
                if (value == true)
                {
                    TileChunk.instance.SetAlphaTexture(ownedTile, Color.red * 0.3f);
                }
                // 동물이 안보이게 된다면
                else
                {
                    TileChunk.instance.SetAlphaTexture(ownedTile, Color.white);
                }
                
                StartCoroutine(AdjustAlphaBlendingForConceal(0.5f));
            }
            RefreshEnemyHighlightLine();
            if (ownedTile != null) TileChunk.instance.ChnangeTileOccupation(ownedTile._tileIndex, value);
        }
    }

    /// AI
    public Animal_AI ai;

    /// 텍스트 이펙트 보정 위치 
    readonly public static Vector3 DAMAGE_TEXT_POS_FACTOR = new Vector3(10, 20, 0);
    readonly public static Vector3 SKILL_TEXT_POS_FACTOR = new Vector3(10, 30, 0);

    /// 동기화를 위한 Hashtable key 
    public static readonly string ANIMAL_DATA_KEY_PROP_KEY = "dataKey";
    public static readonly string ANIMAL_OWNER_USER_ID_PROP_KEY = "ownerUserId";
    public static readonly string ANIMAL_OWNED_TILE_INDEX_PROP_KEY = "ownedTileIndex";
    public static readonly string ANIMAL_HP_PROP_KEY = "Hp";
    public static readonly string ANIMAL_VIEW_ID_PROP_KEY = "viewID";
    public static readonly string ANIMAL_REMAIN_MOVEMENT_PROP_KEY = "remainMovement";
    public static readonly string ANIMAL_CAN_ACTION_PROP_KEY = "canAction";
    public static readonly string ANIMAL_TIER_PROP_KEY = "tier";
    public static readonly string ANIMAL_FIRST_SKILL_PROP_KEY = "firstSkill";
    public static readonly string ANIMAL_SECOND_SKILL_PROP_KEY = "secondSkill";
    public static readonly string ANIMAL_THIRD_SKILL_PROP_KEY = "thirdSkill";

    // Use this for initialization
    protected virtual void Awake()
    {
        meshRenderer = GetComponentInChildren<MeshRenderer>();

        shader_basic = Shader.Find("Legacy Shaders/VertexLit");
        shader_transparent = Shader.Find("Legacy Shaders/Transparent/VertexLit");
    }


    void OnDestroy()
    {
    }


    protected virtual void Start()
    {
        GameManager.instance.OnChangedPlayerTurn += OnChangedPlayerTurn;
        GameManager.instance.OnSelectedAnimal += OnSelectedAnimal;
        GameManager.instance.OnSetTargetAnimal += OnSetTargetAnimal;
        OnFinishedMove += OnMoveCompleted;

        RefreshEnemyHighlightLine();
        
        foreach (var flag in GameManager.instance.flags)
        {
            OnMoved += flag.OnMoved;
        }
        OnDeath += OnDeathMe;

        StartCoroutine(IdleAnimationCoroutine());

        meshOffset = GetComponentInChildren<MeshFilter>().transform.localPosition;
    }


    void OnDeathMe(Animal self)
    {
        GameManager.instance.OnChangedPlayerTurn -= OnChangedPlayerTurn;
        GameManager.instance.OnSelectedAnimal -= OnSelectedAnimal;
        GameManager.instance.OnSetTargetAnimal -= OnSetTargetAnimal;
    }


    IEnumerator IdleAnimationCoroutine()
    {
        int idleAnimationCount = 4;
        AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
        while (true)
        {
            if (stateInfo.IsName("idle"))
            {
                int randomIdleAnimationIndex = Random.Range(1, idleAnimationCount);

                string randomIdleAnimationName = "idle" + randomIdleAnimationIndex;
                if(this is Hunter)
                {
                    randomIdleAnimationName = "hunterIdle" + randomIdleAnimationIndex;
                }

                /// 랜덤 idle 애니메이션의 시간을 알아오기 위한 검색
                AnimationClip randomIdleAnimationClip = GetAnimationClip(randomIdleAnimationName);


                /// 랜덤 idle 애니메이션 반영
                animator.SetInteger("idle", randomIdleAnimationIndex);

                /// 해당 랜덤 idle 애니메이션 시간만큼 코루틴을 대기시킨다
                yield return new WaitForSeconds(randomIdleAnimationClip.length);

                /// 랜덤 idle 애니메이션 초기화
                /// WaitForSeconds 도중에 다른 애니메이션으로 전환되었다가 다시 idle State가 되었다면 
                /// 이전에 세팅된 idle Integer 값에 의해 idle 애니메이션이 자동으로 실행된다 
                /// 하지만 이전에 걸어놓은 WaitForSeconds가 걸려버리면 idle 애니메이션을 하다가 다른 idle 애니메이션으로 바뀌게 된다 
                /// 도중에 다른 애니메이션 하다가 돌아온 idle 애니메이션일 때 끝나는 시간만큼 기다려줘야 한다 
                AnimatorStateInfo animationState = animator.GetCurrentAnimatorStateInfo(0);
                AnimatorClipInfo[] myAnimatorClip = animator.GetCurrentAnimatorClipInfo(0);
                if (animationState.normalizedTime < 0.99 && myAnimatorClip.Length != 0)
                {
                    float currentTimeOfAnimation = myAnimatorClip[0].clip.length * animationState.normalizedTime;

                    yield return new WaitForSeconds(myAnimatorClip[0].clip.length - currentTimeOfAnimation);
                }

                animator.SetInteger("idle", 0);

                /// 끝나고 한 프레임 쉬는 작업 매우 중요
                /// 안 해주면 SetInteger("idle", 0)이 반영되지 않는다 
                yield return null;
            }
            else
            {
                yield return null;
            }
        }
    }


    // 호출되는 곳 : 새로 동물을 생성했을때(재접속포함)
    // 이 동물의 프로퍼티가 업데이트 되었을때
    public void Init(Hashtable syncTable)
    {
        if (syncTable == null)
        {
            return;
        }

        if (syncTable.ContainsKey(ANIMAL_DATA_KEY_PROP_KEY))
        {
            animalData = new GDEAnimalData((string)syncTable[ANIMAL_DATA_KEY_PROP_KEY]);
        }

        if (syncTable.ContainsKey(ANIMAL_OWNER_USER_ID_PROP_KEY))
        {
            ownerUserId = (string)syncTable[ANIMAL_OWNER_USER_ID_PROP_KEY];

            /// AI 생성
            if (ownerUserId == GameManager.instance.userIDForSimulation_another)
            {
                ai = FindAnimal_AI_Skill();
            }
        }

        if (syncTable.ContainsKey(ANIMAL_OWNED_TILE_INDEX_PROP_KEY))
        {
            int tileIndex = (int)syncTable[ANIMAL_OWNED_TILE_INDEX_PROP_KEY];

            // 소유타일 결정
            ownedTile = TileChunk.instance.tiles[tileIndex % TileMaker.instance.tileSize, tileIndex / TileMaker.instance.tileSize];
            ownedTile.owningAnimal = this;
            currentTile = ownedTile;

            // 동물이 위치한 지형에 알파값을 넣기위한 텍스처 생성(지형셰이더에서 이용됨)
            TileChunk.instance.SetAlphaTexture(ownedTile, Color.red * 0.3f);

            //동물의 transform 초기설정
            transform.position = GetRealPositionByTile(ownedTile);
            var lookRotation = transform.rotation;
            if (ownedTile.type == TileMaker.TileType.RIVER)
            {               
                lookRotation.eulerAngles += Quaternion.Euler(xAngle_fallingIntoWater, 0, 0).eulerAngles;
            }
            transform.rotation = lookRotation;

            // hp바 생성
            UIPositionTransform = new GameObject("hpBarPositionTransform");
            UIPositionTransform.transform.SetParent(gameObject.transform);
            UIPositionTransform.transform.localPosition = new Vector3(0, GetComponentInChildren<Renderer>().bounds.extents.y, 0);
            hpBar = UIManager.instance.hpBarContainer.AddChild(UIManager.instance.hpBarPrefab).GetComponent<HPBar>();
            hpBar.Init(this);
            hpBar.SetActive(false);

            fxPoint = transform.Find("default/Fx Point");
            //// 타겟 하이라이트 생성
            //targetHighlight = TileChunk.instance.CreateTileTargetHighlight();
            
            
            /// 은신류 동물들 첫 배치 타일일 때 처리 
            if (canConceal || canSubmerge || canClimbTree)
            {
                /// 은신할 수 있는 타일로 이동하면 은신시키고, 은신할 수 있는 타일이 아니면 은신이 풀린다 
                bool conceal = false;
                if (canConceal && ownedTile.type == TileMaker.TileType.GRASS) conceal = true;
                else if (canSubmerge && ownedTile.type == TileMaker.TileType.RIVER) conceal = true;
                else if (canClimbTree && ownedTile.type == TileMaker.TileType.FOREST) conceal = true;
                
                StartCoroutine(Conceal(conceal, 0, 0, true));
            }
        }

        // 아래 값들은 반드시 애니멀 SYNC_DATA_KEY 보다 늦게 해야 한다 
        // 애니멀 데이타가 초기화 되면서 모든 멤버 변수를 초기값으로 변경시키기 떄문 
        if (syncTable.ContainsKey(ANIMAL_HP_PROP_KEY))
        {
            curHealth = (int)syncTable[ANIMAL_HP_PROP_KEY];
        }

        if (syncTable.ContainsKey(ANIMAL_CAN_ACTION_PROP_KEY))
        {
            canAction = (bool)syncTable[ANIMAL_CAN_ACTION_PROP_KEY];
        }

        if (syncTable.ContainsKey(ANIMAL_REMAIN_MOVEMENT_PROP_KEY))
        {
            curremainMovement = (int)syncTable[ANIMAL_REMAIN_MOVEMENT_PROP_KEY];
        }

        if (syncTable.ContainsKey(ANIMAL_TIER_PROP_KEY))
        {
            tier = (int)syncTable[ANIMAL_TIER_PROP_KEY];
        }

        gameObject.name = animalData.Key + photonView.viewID;
    }

    void InitSelectionHighlight()
    {
        //selectionHighlight_red = gameObject.AddChild(GameManager.instance.selectionHighlight_red);
        //selectionHighlight_blue = gameObject.AddChild(GameManager.instance.selectionHighlight_blue);

        //// 동물에 맞게 크기를 조절해줌
        ////Renderer rend = GetComponentInChildren<Renderer>();
        //////Vector3 center = rend.bounds.center;
        ////Vector3 extends = rend.bounds.extents;
        ////float radius = 0;
        ////if (extends.x > extends.z) radius = extends.x;
        ////else radius = extends.z;
        //selectionHighlight_red.transform.localScale = new Vector3(35, 1, 35);
        //selectionHighlight_red.transform.localPosition = Vector3.up * 5;
        //selectionHighlight_blue.transform.localScale = new Vector3(35, 1, 35);
        //selectionHighlight_blue.transform.localPosition = Vector3.up * 5;

        //// 빨강 파랑 하이라이트를 모두 꺼준다
        //TrailRenderer[] trailRenderers = selectionHighlight_blue.GetComponentsInChildren<TrailRenderer>();
        //for (int i = 0; i < trailRenderers.Length; i++)
        //{
        //    trailRenderers[i].time = 0;
        //}
        //trailRenderers = selectionHighlight_red.GetComponentsInChildren<TrailRenderer>();
        //for (int i = 0; i < trailRenderers.Length; i++)
        //{
        //    trailRenderers[i].time = 0;
        //}
    }

    Animal_AI FindAnimal_AI_Skill()
    {
        Animal_AI ai = null;
        string skillName = animalData.skills[0];
        
        string aiName = string.Format("Animal_AI_{0}", skillName);
        var type = System.Type.GetType(aiName);
        if (type == null)
            ai = new Animal_AI();
        else
        {
            ai = (Animal_AI)System.Activator.CreateInstance(type);
        }

        ai.animal = this;
        return ai;
    }



    /// 애니멀 클릭뿐만 아니라, 타일 클릭으로도 동물이 선택될 수 있다. 사실 그냥 타일 클릭하나만으로 선택처리를 하는게 심플하다
    protected void OnSelectedAnimal(Animal selectedAnimal)
    {
        if (selectedAnimal == this)
        {
            /// start to flash
            StartCoroutine(Flash());

            /// 클릭 사운드 
            AudioClip sound = clickSound[Random.Range(0, clickSound.Length)];
            audioSource.PlayOneShot(sound, 0.3f);

        }

        SetHPBarVisible(selectedAnimal == this);
    }


    void OnSetTargetAnimal(Animal targetAnimal)
    {
        if (targetAnimal == null)
            return;

        // 내 동물이 아닐때 키기
        if (!photonView.isMine)
        {
            SetHPBarVisible(targetAnimal == this && ownedTile.isVisible);
        }
    }

 
    /// 턴이 끝났을 때 자동으로 들어오게끔 만드는 함수를 받아서 처리시키는게 나아보임
    void Init()
    {
        curremainMovement = animalData.mobility;
        curHealth = animalData.health;
        curSight = animalData.sight;
        isDoingAction = false;
        //curNightSight = animalData.nightSight;
        //curHearingRange = animalData.hearingRange;
        //curSmellRange = animalData.smellRange;
        species = animalData.Key.Split('_')[0];
        
        biteDirections = new bool[4];
        _canAction = true;
        _canRetrieveFlag = false;
        isRetrievingFlag = false;
        modelTransform = transform.Find("default");
        if (modelTransform == null)
            modelTransform = transform.Find("Model");
    }


    /// 애니멀 기본 셰이더가 Standard/Opaque 였는데 Emission 칼라 변경 적용이 안되네; EmissonColor도 안됨
    /// 그래서 셰이더를 Legacy Shaders/VertexLit으로 변경했음
    private IEnumerator Flash()
    {
        if (isFlashing)
        {
            yield break;
        }
        isFlashing = true;

        this.originalMaterial = GetComponentInChildren<Renderer>().material;
        if (!this.originalMaterial.HasProperty("_Emission"))
        {
            Debug.LogWarning("Doesnt have emission, can't flash " + gameObject);
            yield break;
        }

        this.originalColor = this.originalMaterial.GetColor("_Emission");
        this.originalMaterial.SetColor("_Emission", Color.white);

        for (float f = 0.0f; f <= 1.0f; f += 0.08f)
        {
            Color lerped = Color.Lerp(Color.white, this.originalColor, f);
            this.originalMaterial.SetColor("_Emission", lerped);
            yield return null;
        }

        this.originalMaterial.SetColor("_Emission", this.originalColor);
        isFlashing = false;
    }


    /// 지형에 대한 이동 비용 
    public int GetMovementCostBy(Tile tile)
    {
        int cost = 1;

        // 동물 데이터에 따라 다른 값이 반환되도록 수정해야 한다 
        switch (tile.type)
        {
            case TileMaker.TileType.NA_TILE:    // NA_TILE 일때 암것도안하고 로그만 띄운다.
                Debug.Log("[WARNING] NA_TILE HAS BEEN DETECTED DURING DIJKSTRA PATH FINDING.");
                break;
            case TileMaker.TileType.FLAT:
                cost = animalData.tileCost_flat;
                break;
            case TileMaker.TileType.GRASS:
                cost = animalData.tileCost_grass;
                break;
            case TileMaker.TileType.RIVER:
                cost = isTracing ? 99999 : animalData.tileCost_river;
                break;
            case TileMaker.TileType.DESSERT:
                cost = animalData.tileCost_dessert;
                break;
            case TileMaker.TileType.FOREST:
                cost = animalData.tileCost_forest;
                break;
            default:
                break;
        }

        return cost;
    }


    public IEnumerator MoveIfCanMove(Tile tile)
    {
        if (!canMove) return null;

        /// 0 = 경로 갯수
        int[] shortestPath = TileChunk.instance.GetShortestPathForMovement(this, tile._tileIndex, curremainMovement);
        if (shortestPath == null || shortestPath[0] < 1) return null;

        /// 경로에 적 동물의 대기 공격 스킬이 있는지 체크 
        bool areThereEnemiesAttackSkills;
        Hashtable attackedQueue;
        int changedDestinationPathIndex;

        CheckEnemiesAttackSkillsInPath(ref shortestPath, out areThereEnemiesAttackSkills, out attackedQueue, out changedDestinationPathIndex);
        
        /// 체크된게 있으면 돌리고, 체크되는게 없다면 일반 Move를 호출한다 
        IEnumerator moveCoroutine = null;
        if (areThereEnemiesAttackSkills)
        {
            moveCoroutine = StartAttackedQueue(attackedQueue);
        }
        else
        { 
            moveCoroutine = Move(shortestPath, changedDestinationPathIndex);
        }
        StartTrackedCoroutine(moveCoroutine);

        return moveCoroutine;
    }


    [PunRPC]
    public IEnumerator Move(int[] path, int destinationPathIndex)
    {
        if (path == null)
        {
            yield break;
        }

        if (isMoving)
        {
            yield break;
        }

        /// 이동 연출 시작 
        var moveCoroutine = DirectingMove(path, destinationPathIndex);
        while (moveCoroutine.MoveNext()) yield return moveCoroutine.Current;


		// 내턴에는 내 동물의 시야만 업데이트해야한다
        if (ownerUserId == PhotonNetwork.player.UserId)
        {
            /// UI 갱신
            UIManager.instance.RefreshUIViews();

            /// 알리기
            OnFinishedMove(this);
        }
    }


    public IEnumerator DirectingMove(int[] path, int destinationPathIndex, string forceMovingAnim = "hop", float movingSpeedFactor = 1f)
    {
        /// RefreshFieldOfView에서 참조
        isMoving = true;

        /// ownedTile 처리 미리 하기
        /// 동기화 이슈 및 해당 동물 이동 중일 때, 다른 동물들도 선택해서 움직일 수 있도록 하기 위함
        int destinationTileIndex = path[destinationPathIndex];

        /// 가려는 곳에 이미 있는 상태라면 바로 함수 코루틴 종료
        if (ownedTile._tileIndex == destinationTileIndex)
        {
            yield break;
        }

        int destinationTileIndexX = destinationTileIndex % TileMaker.instance.tileSize;
        int destinationTileIndexY = destinationTileIndex / TileMaker.instance.tileSize;
        Tile destinationTile = TileChunk.instance.tiles[destinationTileIndexX, destinationTileIndexY];
        ownedTile = destinationTile;

        /// 한번 움직이면 그 다음 움직일 수 없도록 한다 
        /// 플레이어가 이동 선택에 신중함을 가지도록 하기 위함이다 
        curremainMovement = 0;

        /// 내 위치 타일을 건너뛰기 위해 -1
        for (int i = path[0] - 1; i >= destinationPathIndex; --i)
        {
            /// 0 = 경로 갯수
            /// path[경로 갯수] = 동물 현 위치 
            /// 1 = 도착 지점 
            int pathTileIndex = path[i];

            int tileX = pathTileIndex % TileMaker.instance.tileSize;
            int tileY = pathTileIndex / TileMaker.instance.tileSize;
            Tile tile = TileChunk.instance.tiles[tileX, tileY];

            int currentTileIndex = path[i + 1];
            int currentTileX = currentTileIndex % TileMaker.instance.tileSize;
            int currentTileY = currentTileIndex / TileMaker.instance.tileSize;
            currentTile = TileChunk.instance.tiles[currentTileX, currentTileY];
            /// 무빙 사운드 처리
            if (tile.isVisible)
            {
                /// 이전 타일이 물 타일이고, 가려는 타일도 물 타일이면 movingSound_water
                /// 이전 타일이 물 타일이고, 가려는 타일이 물 타일이 아니더라도 movingSound_water
                if (currentTile.type == TileMaker.TileType.RIVER)
                {
                    audioSource.PlayOneShot(GameManager.instance.movingSound_water, 0.2f);
                }
                /// 이전 타일이 물 타일이 아니고, 가려는 타일이 물 타일이면 waterSplashSound가 PlayWaterSplashSound에서 애니메이션 이벤트로 재생된다 
                /// 그 때를 제외하고 기본 movingSound로 재생
                else if (!(currentTile.type != TileMaker.TileType.RIVER && tile.type == TileMaker.TileType.RIVER ))
                {
                    audioSource.PlayOneShot(movingSound, 0.2f);
                }
            }

            /// 시야 갱신을 위한 무빙 타일 반영
            /// 이동 연출보다 빠르게 호출해줘야 한다 
            /// 연출하기 전에 movingTile을 참조하게 되면 null을 맛보게 된다 
            if (movingTile != null) movingTile.OnChangedTileVisible -= OnChangedTileVisible;
            movingTile = tile;
            if (movingTile != null) movingTile.OnChangedTileVisible += OnChangedTileVisible;

            /// 회전 
            lookVector = tile.position - currentTile.position;
            var lookRotation = Quaternion.LookRotation(lookVector);
            if (movingTile.type == TileMaker.TileType.RIVER)
            {
              
                lookRotation.eulerAngles += Quaternion.Euler(xAngle_fallingIntoWater, 0, 0).eulerAngles;
            }

            iTween.Stop(gameObject);
            transform.rotation = lookRotation;

            if (movingTile.type == TileMaker.TileType.RIVER)
            {
                Swim();
            }
            /// 이동 애니메이션 결정
            string movingAnim = animalData.movingAnim;
            if (GetType() == typeof(Animal)) /// Hunter 때문에 해야하는 더러운 코드 
            {
                if (currentTile.type == TileMaker.TileType.RIVER)
                {
                    movingAnim = "waddle";
                }
                else
                {
                    movingAnim = forceMovingAnim; 
                }
            }
            // 물로 뛰어들때 dive
            if (currentTile.type != TileMaker.TileType.RIVER && tile.type == TileMaker.TileType.RIVER)
            {
                //yield return new WaitForSeconds(0.1f);
                movingAnim = "dive";
            }

            /// 은신 패시브 스킬 
            if (canConceal || canSubmerge || canClimbTree)
            {
                /// 은신할 수 있는 타일로 이동하면 은신시키고, 은신할 수 있는 타일이 아니면 은신이 풀린다 
                bool conceal = false;
                if (canConceal && movingTile.type == TileMaker.TileType.GRASS) conceal = true;
                else if (canSubmerge && movingTile.type == TileMaker.TileType.RIVER) conceal = true;
                else if (canClimbTree && movingTile.type == TileMaker.TileType.FOREST) conceal = true;
                    
                /// 해당 동물의 무빙 애니메이션 찾기 
                AnimationClip movingAnimationClip = GetAnimationClip(movingAnim);
                /// 무빙 애니메이션 시간
                float movingAnimationTime = movingAnimationClip.length / (movingAnimSpeed * movingSpeedFactor);

                StartCoroutine(Conceal(conceal, 0, movingAnimationTime));
            }


            /// 애니메이터 트리거를 쓰는 이유는 AnyState 때문
            /// AnyState는 어떤 State에서도 해당 조건을 만족시키면 State를 변경시킨다 
            /// 문제는 트리거를 쓰지 않으면 State 변경 조건이 맞을 시 계속 State를 변경시킨다 
            /// 그 결과 애니메이션이 계속 재시작되는 현상이 발생한다
            /// 트리거는 State 변경을 '딱 한번만' 해줄 것을 약속한다
            animator.SetBool(movingAnim, true);
            animator.SetTrigger(movingAnim + "Trigger");

            /// 움직일꺼라는 이벤트 알려주기 
            OnStartMove(this, tile);

            // 움직일 타일의 isVisible이면 동물의 visible을 켜준다
            // 근데 안개에서 안개가없는곳으로 나올때는 움직이기 전에 메쉬렌더러를 켜줘야 자연스러움
            if (tile.isVisible)
                OnChangedTileVisible(tile, true);

            /// 이동할 위치 결정
            Vector3 tilePos = GetRealPositionByTile(tile);

            // 이 동물이 시야에 보이는 타일에 한번이라도 있으면 느낌표가 안뜨도록
            if (currentTile.isVisible == true && appearThisTurn == false)
                appearThisTurn = true;
            // 시야밖에서 들어올때 느낌표연출
            if (currentTile.isVisible == false && tile.isVisible == true && appearThisTurn == false)
            {
                // 한턴에 안개로 들어갔다 나왔다를 여러번할경우 느낌표가 중복으로 나타나지 않도록
                appearThisTurn = true;

                // 애니메이션이 멈춤
                animator.speed = 0;

                audioSource.PlayOneShot(GameManager.instance.surprisedSound2, 0.4f);
                UIManager.instance.ShowHUDText("!", Color.yellow, transform.position + DAMAGE_TEXT_POS_FACTOR);
                Camera.main.MoveTo(transform.position);

                yield return new WaitForSeconds(0.5f);

                // 애니메이션 재개
                animator.speed = 1;
            }
            
            yield return MoveTo(tilePos, movingAnim, movingSpeedFactor);
   
            /// 이동 애니메이션 초기화
            animator.SetBool(movingAnim, false);
            animator.ResetTrigger(movingAnim + "Trigger");

            currentTile = tile;

            /// 타일 시야 갱신(상대방 턴에 움지이는동물은 갱신이 되면안된다)
            if (ownerUserId == PhotonNetwork.player.UserId)
                TileChunk.instance.RefreshFieldOfView(this);

            // 움직일 타일의 isVisible이 아닐때, 동물의 isVisible을꺼준다 안개가없는곳에서 안개로 들어갈때
            // 움직인다음에 메쉬렌더러를 꺼줘야 자연스러움
            if (!tile.isVisible)
                OnChangedTileVisible(tile, false);

            /// 이벤트 알리기
            OnMoved(this, tile);
            
            // 오브젝트들의 애니메이션 셰이더를 텍스처로 조절
            TileChunk.instance.SetAlphaTexture(currentTile, Color.white);
            TileChunk.instance.SetAlphaTexture(ownedTile, Color.red * 0.3f);
        }

        // 이동경로에 어떤 동물이 있었을때 가로막히는 연출, 도착경로를 조작하는 포경스킬에서는 발동되지않도록해놓음
        if (destinationPathIndex != 1 && !isAggressiveOverwatching)
            yield return BlockedByOtherAnimal(path[destinationPathIndex - 1]);

        // 도착했을때 물이면 기울여서 idle
        if (movingTile.type == TileMaker.TileType.RIVER)
        {
            iTween.RotateTo(gameObject, iTween.Hash("x", xAngle_fallingIntoWater, "easeType", iTween.EaseType.linear, "time", 1f));
        }
        else if (movingTile.type != TileMaker.TileType.RIVER && currentTile.type == TileMaker.TileType.RIVER)
        {
            iTween.RotateTo(gameObject, iTween.Hash("x", 0, "easeType", iTween.EaseType.linear, "time", 0.2f));
        }

        isMoving = false;
    }
    
    // 이동 경로에 어떤 동물이 가로막고있어서, destinationPathIndex가 1이 아닐경우 가다가 깜짝놀래야함 
    IEnumerator BlockedByOtherAnimal(int pathTileIndex)
    {
        int tileX = pathTileIndex % TileMaker.instance.tileSize;
        int tileY = pathTileIndex / TileMaker.instance.tileSize;
        Tile tile = TileChunk.instance.tiles[tileX, tileY];
        
        // 이 동물이 이 동물에게 막혔다는 이벤트
        GameManager.instance.OnAnimalBlocked(this, tile.owningAnimal);

        /// 회전 
        lookVector = tile.position - currentTile.position;
        var lookRotation = Quaternion.LookRotation(lookVector);

        iTween.Stop(gameObject);
        transform.rotation = lookRotation;

        animator.SetTrigger(animalData.movingAnim + "Trigger");

        // 중간지점까지 갔다가
        AnimationClip animClip = GetAnimationClip(animalData.movingAnim);
        yield return MoveToAnimDuration((currentTile.position + tile.position) / 2, animalData.movingAnim, animClip.length / 2);  
        
        // 애니메이션이 멈춤
        animator.speed = 0;
        
        audioSource.PlayOneShot(GameManager.instance.surprisedSound, 0.4f);
        UIManager.instance.ShowHUDText("!", Color.yellow, transform.position + DAMAGE_TEXT_POS_FACTOR);

        yield return new WaitForSeconds(0.5f);
        
        // 애니메이션 재개
        animator.speed = 1; 

        // 다시 제자리로
        yield return MoveToRapidly(currentTile.position);
    }

    public Direction GetDirectionOfTarget(Animal target)
    {
        Direction targetDirection = Direction.SIDE;

        /// y축 값이 1이라도 차이나면 SIDE 판정이 나온다 
        /// y축이 같은 같으로 비교해야 한다 
        Vector3 toOther = target.ownedTile.position - ownedTile.position;
        toOther.Normalize();

        float forwardDot = Vector3.Dot(transform.forward, toOther);
        if (forwardDot.NearlyEqual(1.0f))
        {
            targetDirection = Direction.FRONT;
        }
        else if (forwardDot.NearlyEqual(-1.0f))
        {
            targetDirection = Direction.BEHIND;
        }

        return targetDirection;
    }


    public Direction GetDirectionOfTargetByTile(Vector3 position)
    {
        Direction targetDirection = Direction.SIDE;

        /// y축 값이 1이라도 차이나면 SIDE 판정이 나온다 
        /// y축이 같은 같으로 비교해야 한다 
        Vector3 toOther = position - ownedTile.position;
        toOther.Normalize();

        float forwardDot = Vector3.Dot(transform.forward, toOther);
        if (forwardDot == 1)
        {
            targetDirection = Direction.FRONT;
        }
        else if (forwardDot == -1)
        {
            targetDirection = Direction.BEHIND;
        }

        return targetDirection;
    }


    public bool CheckTheTileIsOwnedByEnemy(int tileIndexFactor_x, int tileIndexFactor_y, out Animal owningAnimal)
    {
        int tileX = ownedTile.TileIndex_X + tileIndexFactor_x;
        int tileY = ownedTile.TileIndex_Y + tileIndexFactor_y;
        Tile tile = TileChunk.instance.GetTile(tileX, tileY);

        owningAnimal = tile.owningAnimal;

        return canAttackableTarget(tile.owningAnimal);
    }


    public bool canAttackableTarget(Animal target)
    {
        return target != null 
        && target.ownerUserId != ownerUserId /// 나와 다른 owner여야하고
        && target.curHealth > 0 /// 살아있어야 하고
        && !target.isConcealed && !target.isClimbedTree && !target.isSubmerged; /// 은신 상태가 아니어야 한다
    }

    bool isMovingRapidly = false;
    bool rapidMoveCanceled = false;
    public IEnumerator MoveToRapidly(Vector3 destination, bool cancelAnotherRapidMove = false)
    {
        // 다른 moveToRapidly를 캔슬시킴
        if (cancelAnotherRapidMove)
        {
            rapidMoveCanceled = true;
        }

        // 다른 movetoRapidly 코루틴이 끝날때까지 기다림
        while (isMovingRapidly) yield return null;
        
        // 다른 moveToRapidly 코루틴이 대기하도록 전역변수 조절
        isMovingRapidly = true;
        rapidMoveCanceled = false;
        
        float distance = Vector3.Distance(destination, transform.position);
        // rapidMoveCanceled 
        while (distance > 0.1f && rapidMoveCanceled == false)
        {
            transform.position = Vector3.Lerp(transform.position, destination, Time.deltaTime * moveSpeed);

            distance = Vector3.Distance(destination, transform.position);

            yield return null;
        }

        isMovingRapidly = false;
    }

    // 느낌표 효과, 죽이고 
    bool isFalling = false;
    public IEnumerator FallDownOutOfMap(Vector3 fallDownPos)
    {
        Camera.main.GetComponent<CameraMovement>().followingTarget = this.transform;

        /// 타겟 동물 놀라는 연출 
        audioSource.PlayOneShot(GameManager.instance.surprisedSound, 0.4f);
        UIManager.instance.ShowHUDText("!", Color.yellow, transform.position + DAMAGE_TEXT_POS_FACTOR);
        
        isFalling = true;

        int damage = curHealth;
        curHealth = 0;

        yield return new WaitForSeconds(1f);

        // gore 애니메이션을 취소시키기 위한 변수
        animator.SetBool("fallDown", true);
        animator.SetTrigger("fallDownTrigger");

        /// 소리 
        audioSource.PlayOneShot(GameManager.instance.fallDownSound, 0.3f);

        // 뒤로기울이기
        transform.Rotate(Vector3.right * -70);
        fallDownPos.y = -90;
        // 타겟을 땅으로 푹 꺼지게한다 
        yield return MoveToDuring(fallDownPos, 0.4f);
        yield return ShrinkDuring(1.0f);

        yield return CheckHealthAndDie();
        //yield return new WaitForSeconds(1);

        Camera.main.GetComponent<CameraMovement>().followingTarget = null;

        yield return hpBar.ReduceCurHp(damage, 0.1f);
        yield return new WaitForSeconds(1f);
        hpBar.SetActive(false);
    }

    IEnumerator ShrinkDuring(float during)
    {
        Vector3 speed = transform.localScale / during;

        while(transform.localScale.x > 0)
        {
            transform.localScale -= speed * Time.deltaTime;
            yield return null;
        }

        yield break;
    }

    public IEnumerator LocalMoveToRapidly(Vector3 destination)
    {
        float distance = Vector3.Distance(destination, transform.localPosition);
        while (distance > 0.1f)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, destination, Time.deltaTime * moveSpeed);

            distance = Vector3.Distance(destination, transform.localPosition);

            yield return null;
        }
    }

    // 이동을 movingAnimationName의 에니메이션 속도로 진행하는데 animDuration만큼만 진행한다.(중간에 이동을 멈춘다.)
    public IEnumerator MoveToAnimDuration(Vector3 destination, string movingAnimationName, float animDuration)
    {
        /// 해당 동물의 무빙 애니메이션 찾기 
        AnimationClip movingAnimationClip = GetAnimationClip(movingAnimationName);
        float movingAnimationTime = movingAnimationClip.length / movingAnimSpeed;
        Vector3 look = destination - transform.position;
        look.Normalize();
        
        float distance = Vector3.Distance(destination, transform.position);
        
        float speed = distance / movingAnimationTime;
        
        float t = 0;
        while (t <= animDuration)
        {
            t += Time.deltaTime;
            
            float speedByFrame = speed * Time.deltaTime;
            
            transform.position += look * speedByFrame;

            yield return null;
        }
    }

    public IEnumerator MoveToDuring(Vector3 destination, float during)
    {        
        /// 방향벡터 
        Vector3 look = destination - transform.position;
        look.Normalize();

        /// 이동해야 할 거리
        float distance = Vector3.Distance(destination, transform.position);

        /// 무빙 애니메이션 시간에 맞춰서 초당 얼마에 속도를 가져야 하는지 계산
        float speed = distance / during;

        /// 무빙 애니메이션 시간까지만 while문을 돌린다
        /// 애니메이션이 끝날 때 동물에 위치는, 계산식에 의해 정확히 목적지에 도착해 있다 
        float t = 0;
        while (t <= during)
        {
            /// deltaTime은 1초를 기준으로 하나의 프레임 연산처리가 얼마나 걸리는 지를 의미한다 (초 단위)
            /// 1초에 60프레임이라면 deltaTime은 0.016이 나온다 0.016 * 60 = 0.96 (sec)
            /// 1초에 30프레임이라면 deltaTime은 0.03이 나온다 0.03 * 30 = 0.9 (sec)
            /// 즉, 환경이 구리든 좋든 while문이 끝나는 시간은 동일하다 
            t += Time.deltaTime;

            /// 1 프레임당 얼마에 속도를 가져야 하는지 계산 
            float speedByFrame = speed * Time.deltaTime;

            /// 이동
            transform.position += look * speedByFrame;

            yield return null;
        }
    }

    public IEnumerator LocalMoveTo(Vector3 destination, string movingAnimationName, float movingSpeedFactor = 1.0f)
    {
        /// 해당 동물의 무빙 애니메이션 찾기 
        AnimationClip movingAnimationClip = GetAnimationClip(movingAnimationName);
        /// 무빙 애니메이션 시간
        float movingAnimationTime = movingAnimationClip.length / (movingAnimSpeed * movingSpeedFactor);

        /// 방향벡터 
        Vector3 look = destination - transform.localPosition;
        look.Normalize();

        /// 이동해야 할 거리
        float distance = Vector3.Distance(destination, transform.localPosition);

        /// 무빙 애니메이션 시간에 맞춰서 초당 얼마에 속도를 가져야 하는지 계산
        float speed = distance / movingAnimationTime;

        /// 무빙 애니메이션 시간까지만 while문을 돌린다
        /// 애니메이션이 끝날 때 동물에 위치는, 계산식에 의해 정확히 목적지에 도착해 있다 
        float t = 0;
        while (t <= movingAnimationTime)
        {
            /// deltaTime은 1초를 기준으로 하나의 프레임 연산처리가 얼마나 걸리는 지를 의미한다 (초 단위)
            /// 1초에 60프레임이라면 deltaTime은 0.016이 나온다 0.016 * 60 = 0.96 (sec)
            /// 1초에 30프레임이라면 deltaTime은 0.03이 나온다 0.03 * 30 = 0.9 (sec)
            /// 즉, 환경이 구리든 좋든 while문이 끝나는 시간은 동일하다 
            t += Time.deltaTime;

            /// 1 프레임당 얼마에 속도를 가져야 하는지 계산 
            float speedByFrame = speed * Time.deltaTime;

            /// 이동
            transform.localPosition += look * speedByFrame;

            yield return null;
        }
    }

    public IEnumerator MoveTo(Vector3 destination, string movingAnimationName, float movingSpeedFactor = 1.0f)
    {
        /// 해당 동물의 무빙 애니메이션 찾기 
        AnimationClip movingAnimationClip = GetAnimationClip(movingAnimationName);
        /// 무빙 애니메이션 시간
        float movingAnimationTime = movingAnimationClip.length / (movingAnimSpeed * movingSpeedFactor);

        /// 방향벡터 
        Vector3 look = destination - transform.position;
        look.Normalize();

        /// 이동해야 할 거리
        float distance = Vector3.Distance(destination, transform.position);

        /// 무빙 애니메이션 시간에 맞춰서 초당 얼마에 속도를 가져야 하는지 계산
        float speed = distance / movingAnimationTime;

        /// 무빙 애니메이션 시간까지만 while문을 돌린다
        /// 애니메이션이 끝날 때 동물에 위치는, 계산식에 의해 정확히 목적지에 도착해 있다 
        float t = 0;
        while (t <= movingAnimationTime)
        {
            /// deltaTime은 1초를 기준으로 하나의 프레임 연산처리가 얼마나 걸리는 지를 의미한다 (초 단위)
            /// 1초에 60프레임이라면 deltaTime은 0.016이 나온다 0.016 * 60 = 0.96 (sec)
            /// 1초에 30프레임이라면 deltaTime은 0.03이 나온다 0.03 * 30 = 0.9 (sec)
            /// 즉, 환경이 구리든 좋든 while문이 끝나는 시간은 동일하다 
            t += Time.deltaTime;

            /// 1 프레임당 얼마에 속도를 가져야 하는지 계산 
            float speedByFrame = speed * Time.deltaTime;

            /// 이동
            transform.position += look * speedByFrame;

            yield return null;
        }
    }

    bool swimUp = false;
    void Swim()
    {
        float angle = xAngle_fallingIntoWater;
        if (swimUp)
            angle = 0;
        swimUp = !swimUp;
        iTween.Stop(gameObject);
        iTween.RotateTo(gameObject, iTween.Hash("x", angle,  "easeType", iTween.EaseType.linear, "time", 0.5f));
    }

    public void PlayMovingSound()
    {
        if (movingTile != null && movingTile.type != TileMaker.TileType.RIVER)
        {
            audioSource.PlayOneShot(movingSound, 0.2f);
        }
    }

    int rippleIndex = 0;
    public void PlayRippleFX()
    {
        if(TileChunk.instance.GetTileWithPosition(transform.position).type != TileMaker.TileType.RIVER)
        {
            return;
        }
        GameManager.instance.rippleEffects[rippleIndex].Stop();
        // 렌더링이 제대로 안되는 원인은 y좌표가 아니다.
        // 특정 타일위에서는 물결파티클이 보이지않는다. 특정 타일에서만 발생하는 문제이다.
        // 근데 또 씬의 카메라에서는 잘보인다.
        Vector3 pos = this.transform.position;
        GameManager.instance.rippleEffects[rippleIndex].transform.localPosition = new Vector3(modelTransform.position.x, -5, modelTransform.position.z);
        var lookRotation = Quaternion.LookRotation(lookVector);
        lookRotation.eulerAngles += Vector3.right * 90; 
        GameManager.instance.rippleEffects[rippleIndex].transform.rotation = lookRotation;
        GameManager.instance.rippleEffects[rippleIndex].Play();
        rippleIndex++;
        if (rippleIndex >= GameManager.instance.rippleEffects.Length )
            rippleIndex = 0;
    }

    int sandWalkingDustIndex = 0;
    public void PlaySandWalkingSoundAndParticle()
    {
        if(TileChunk.instance.GetTileWithPosition(transform.position).type != TileMaker.TileType.DESSERT)
        {
            return;
        }
        GameManager.instance.sandWalkingDust[sandWalkingDustIndex].Stop();

        Vector3 POS = this.transform.position;
        GameManager.instance.sandWalkingDust[sandWalkingDustIndex].transform.localPosition = new Vector3(modelTransform.position.x, 0, modelTransform.position.z);
        var lookRotation = Quaternion.LookRotation(lookVector);
        lookRotation.eulerAngles +=  Vector3.right * 90;
        GameManager.instance.sandWalkingDust[sandWalkingDustIndex].transform.rotation = lookRotation;
        GameManager.instance.sandWalkingDust[sandWalkingDustIndex].Play();
        sandWalkingDustIndex++;
        if (sandWalkingDustIndex >= GameManager.instance.sandWalkingDust.Length)
            sandWalkingDustIndex = 0;
    }

    public void PlayWaterSplashSoundAndParticle()
    {
        if (movingTile != null && movingTile.type == TileMaker.TileType.RIVER && movingTile.isVisible)
        {
            audioSource.PlayOneShot(GameManager.instance.waterSplashSound, 0.2f);
            GameManager.instance.splashParticle.transform.position = new Vector3(movingTile.position.x, -5, movingTile.position.z);
            GameManager.instance.splashParticle.Play();
        }
    }

    public void PlayFlagStealOrGoalSound()
    {
        foreach (var flag in GameManager.instance.flags)
            flag.PlayFlagStealOrGoalSound(this);
    }

    public void PlayWaddleInGrassSound()
    {
        if (movingTile != null && movingTile.type == TileMaker.TileType.GRASS && movingTile.isVisible)
        {
            audioSource.PlayOneShot(GameManager.instance.waddleInGrassSound, 0.2f);
        }
    }

    public void PlayHopInGrassSound()
    {
        if (movingTile != null && movingTile.type == TileMaker.TileType.GRASS && movingTile.isVisible)
        {
            audioSource.PlayOneShot(GameManager.instance.hopInGrassSound, 0.2f);
        }
    }

    public void PlayHopInForestSound()
    {
        if (movingTile != null && movingTile.type == TileMaker.TileType.FOREST && movingTile.isVisible)
        {
            audioSource.PlayOneShot(GameManager.instance.movingSound_forest, 0.2f);
        }
    }

    public void PlayFallSound()
    {
        Tile deathPosTile = TileChunk.instance.GetTileWithPosition(transform.position);
        if (deathPosTile.type == TileMaker.TileType.RIVER)
        {
            audioSource.PlayOneShot(GameManager.instance.waterSplashSound, 0.2f);
        }
        else
        {
            audioSource.PlayOneShot(GameManager.instance.fallSound, 0.2f);
        }
    }

    public Vector3 GetRealPositionByTile(Tile tile)
    {
        Vector3 position = transform.position;

        if (tile != null)
        {
            position = tile.position_AdjustedTileYFactor;
            if (tile.type == TileMaker.TileType.RIVER)
            {                
                position.y += yFactor_fallingIntoWater;
            }
        }

        return position;
    }


    public AnimationClip GetAnimationClip(string clipName)
    {
        AnimationClip unknownAnimationClip = null;
        foreach (var animationClip in animator.runtimeAnimatorController.animationClips)
        {
            if (animationClip.name == clipName)
            {
                unknownAnimationClip = animationClip;

                break;
            }
        }

        return unknownAnimationClip;
    }
    

    public void OnMoveCompleted(Animal self)
    {
        TileChunk.instance.RefreshFieldOfView();
        RefreshEnemyHighlightLine();
        /// 도약 스킬 사용 후, 스킬 쿨타임 적용
        if (isLeaped)
        {
            curRemainSkillCoolTime = LeapCommand.instance.skillCoolTime;

            isLeaped = false;
        }
    }


    /// 반드시 OnTurnCompleted에서 하는 대부분의 초기화 코드는 OnChangedPlayerTurn에서도 할 수 있다 
    /// 두 함수 사용 용도를 명확히 나누기 위해 반드시 OnTurnCompleted에서 해야하는 코드가 아니라면 OnChangedPlayerTurn에서 한다 
    public void OnTurnCompleted()
    {  
    }

    const string LINE_FOR_IDENTIFICATION = "ANIMAL_";
    public void OnChangedPlayerTurn(string turnUserId)
    {
        /// owner 턴으로 바뀌었을 때 
        if (turnUserId == ownerUserId)
        {
            /// 은신 상태일 때 행동력 절반으로 감소
            curremainMovement = animalData.mobility;
            isDoingAction = false;

            canAction = !isStunned && !isBiting;

            isOverwatched = false;
            isLeaped = false;   /// 도약 스킬을 썼지만, 사용하지 않았을 때를 대비한 코드
            isAggressiveOverwatching = false;

            /// 쿨타임을 빼주는 타이밍을 OnTurnCompleted에서 하면 플레이어가 선제턴으로 시작했냐, 후속턴으로 시작했냐에 따라 쿨타임 표시가 달라질 수 있다 
            /// 후속턴으로 시작했을 시, 스킬이 상대턴에 발동되서 내 턴으로 오면 쿨타임이 --가 되지 않는다 
            /// 하지만 선제턴으로 시작했을 시, 스킬이 상대턴이 발동되서 내 턴으로 오면 턴 하나가 끝나고 내턴으로 오기 때문에 --처리가 되어 있다 
            curRemainSkillCoolTime--;

            if (isRetrievingFlag)
            {
                isRetrievingFlag = false;

                /// 깃발 회수
                Flag myFlag = GameManager.instance.GetFlagWithUserId(ownerUserId);
                myFlag.ResetPosition();
            }
        }
        /// 상대 턴으로 바뀌었을 때 
        else
        {
            appearThisTurn = false;
        }

        //// ai모드가 아니면 
        //if(!GameManager.instance.isOnAI)
        //    hpBar.ChangeHPBarColor();

        RefreshEnemyHighlightLine();

        if (isConcealed || isClimbedTree || isSubmerged)
        { 
            StartCoroutine(AdjustAlphaBlendingForConceal());
        }
    }

    
    void RefreshEnemyHighlightLine()
    {
        /// 아군, 적군 식별용 타일 하이라이트 
        if (ownerUserId != GameManager.instance.userIDForSimulation_origin 
        && ownedTile != null && ownedTile.isVisible 
        && curHealth > 0 
        && !isMoving
        && isVisible) 
            TileChunk.instance.DisplayTileHighlight(ownedTile, LINE_FOR_IDENTIFICATION + photonView.viewID, Color.red, 0.8f);
        else
            TileChunk.instance.UndisplayLine(LINE_FOR_IDENTIFICATION + photonView.viewID);
    }

    
    // Tilechunk의 refreshfieldofview나 directingMove에서 호출됨
    public void OnChangedTileVisible(Tile tile, bool visible)
    {
        // 타일이 visible하더라도 내가 은신중일때는 동물의 isVisible을 켜주지않는다.
        // 근데 타일이 절대시야에 있다면 켜야한다.
        bool isDetected = CheckDetected(tile);
        if (visible == true)
        {
            if ((isConcealed || isSubmerged || isClimbedTree) && !isDetected)
            {
                visible = false;
            }
            else if (isDetected)
            {
                visible = true;
            }
        }
        else
        {
            if (isDetected)
            {
                visible = true;
            }
        }
        SetVisible(visible);
    }
        
    public virtual void SetVisible(bool visible)
    {        
        bool original_visible = visible;
        // 플레이어의 동물은 그냥보이도록한다.
        if (ownerUserId == GameManager.instance.userIDForSimulation_origin)
        {
            visible = true;
        }

        // ai와 시야를 번갈아가며 하는 경우, ai 시야를 보고있을때는 내 동물이 나오면 안된다.
        if (GameManager.instance.showSightOfAI && PhotonNetwork.player.UserId == GameManager.instance.userIDForSimulation_another)
        {
            // 1p의 동물이 무조건보이면 안되고 원래 동물처럼 보여야 한다.
            if (ownerUserId == GameManager.instance.userIDForSimulation_origin)
                visible = original_visible;
            // 자기동물(ai)는 true로 보여준다.
            else
                visible = true;
        }

        isVisible = visible;        
    }

    private bool CheckDetected(Tile tile)
    {
        if (tile == null)
            return false;
        // 게임의 모든 유저의 DETECTED를 참조한다. 아마 두명일것이다. 
        // 자기 동물은 자기의 isDetected와는 관련이 없으므로 넘긴다.
        if (ownerUserId == GameManager.instance.userIDForSimulation_origin)
        {
            if(tile.isDetected[GameManager.instance.userIDForSimulation_another])
            {
                return true;
            }
        }
        else if(ownerUserId == GameManager.instance.userIDForSimulation_another)
        {
            if (tile.isDetected[GameManager.instance.userIDForSimulation_origin])
            {
                return true;
            }
        }     
    
        return false;
    }
    
    public void SetHPBarVisible(bool visible)
    {
        if (hpBar == null) return;
        
        /// 은신 중인 적의 동물이라면 체력바를 표시하지 않는다
        bool hpBarActive = visible;
        if (visible && !photonView.isMine && (isConcealed || isClimbedTree || isSubmerged) && !isVisible)
        {
            hpBarActive = false;
        }
        
        hpBar.SetActive(hpBarActive && curHealth > 0);
        //hpBar.SetActive(true);
    }

    //public void SetTargetHighLightVisible(bool visible)
    //{
    //    if (!isVisible)
    //        visible = false;
        
    //    if (visible)
    //    {
    //        // 아군의 동물 
    //        if (PhotonNetwork.player.UserId == ownerUserId)
    //        {
    //            targetHighlight.GetComponent<Renderer>().material.color = Color.cyan;// - Color.black * 0.7f;
    //        }
    //        else
    //        {
    //            targetHighlight.GetComponent<Renderer>().material.color = Color.red;// - Color.black * 0.7f;
    //        }

    //        targetHighlight.transform.position = ownedTile.position;
    //    }
    //    targetHighlight.SetActive(visible);
    //}

    // 타겟 하이라이트에서 물결이 흐르는 효과를 내려면 물결의 중심좌표를 매터리얼의 프로퍼티에 설정해주어야함
    //public void SetHighlightMat(Vector3 ChunkPos, float ChunkSize)
    //{
    //    ChunkPos.z *= -1;
    //    targetHighlight.GetComponent<Renderer>().material.SetVector("_ChunkSize", new Vector3(ChunkSize, 1, ChunkSize));
    //    targetHighlight.GetComponent<Renderer>().material.SetVector("_ChunkPos", ChunkPos);
    //}


    /// 처음에는 커스텀 프로퍼티에 Animals -> playersAnimals -> Animal 이런 식으로 데이터를 저장했음 
    /// 하지만, 동물 하나 검색해내는데 너무 많은 뎁스가 필요해서 수정하게 됨 
    /// OnCustomProperties에서 해당 동물만 바꼈는지 안 바꼈는지 알고 싶은데, 그걸 바로 알수가 없게됨 
    public static string GetAnimalPropKey(string playerUserId, int animalViewID = -1)
    {
        const string ANIMAL_PROP_KEY_OFFSET = "'s Animals:";
        return playerUserId + ANIMAL_PROP_KEY_OFFSET + (animalViewID == -1 ? "" : animalViewID.ToString());
    }


    /// 어떤 행동을 하거나, 행동을 당해서 애니멀의 상태가 바뀌었다면, 이 함수를 호출해서 동기화 정보를 포톤에 입력시켜야 한다 
    protected void SetCustomPropertiesByMe(bool remove = false)
    {/*
        /// 자기 동물의 정보만 셋업하도록 한다 
        if(!photonView.isMine)
        {
            return;
        }

        /// 먼저 커스텀 프로퍼티에 동물 데이터가 있는지 체크 
        string animalPropKey = GetAnimalPropKey(ownerUserId, photonView.viewID);
        if(!PhotonNetwork.room.CustomProperties.ContainsKey(animalPropKey))
        {
            return;
        }

        Hashtable animalNewProp = null;
        if(!remove)
        {
            animalNewProp = (Hashtable)PhotonNetwork.room.CustomProperties[animalPropKey];
            animalNewProp[ANIMAL_CAN_ACTION_PROP_KEY] = canAction;
            animalNewProp[ANIMAL_DATA_KEY_PROP_KEY] = _animalData.Key;
            animalNewProp[ANIMAL_HP_PROP_KEY] = curHealth;
            animalNewProp[ANIMAL_OWNED_TILE_INDEX_PROP_KEY] = ownedTile._tileIndex;
            animalNewProp[ANIMAL_REMAIN_MOVEMENT_PROP_KEY] = curremainMovement;
            animalNewProp[ANIMAL_TIER_PROP_KEY] = tier;
            if (tier > 0)
                animalNewProp[ANIMAL_FIRST_SKILL_PROP_KEY] = learnedSkill[0].skillName;
            if (tier > 1)
                animalNewProp[ANIMAL_SECOND_SKILL_PROP_KEY] = learnedSkill[1].skillName;
            if (tier > 2)
                animalNewProp[ANIMAL_THIRD_SKILL_PROP_KEY] = learnedSkill[2].skillName;
        }

        Hashtable animalProp = new Hashtable();
        animalProp[animalPropKey] = animalNewProp;
        PhotonNetwork.room.SetCustomProperties(animalProp);*/
    }


    public override void OnPhotonCustomRoomPropertiesChanged(Hashtable propertiesThatChanged)
    {
        string animalPropKey = GetAnimalPropKey(ownerUserId, photonView.viewID);
        if (propertiesThatChanged.ContainsKey(animalPropKey))
        {
            Hashtable animalNewProp = (Hashtable)propertiesThatChanged[animalPropKey];
			Init(animalNewProp);
        }
    }


    public override void OnPhotonPlayerConnected(PhotonPlayer otherPlayer)
    {
		if(otherPlayer.UserId == ownerUserId && photonView.ownerId != otherPlayer.ID)
        {
            photonView.TransferOwnership(otherPlayer);
        }
    }
}