﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vectrosity;
using Photon;
using Hashtable = ExitGames.Client.Photon.Hashtable;


/// <summary>
/// 코루틴 쓰려고 MonoBehaviour 써버리기
/// </summary>
class AnimalTrail : PunBehaviour
{
    /// <summary>
    /// 동물 생성을 위한 프리팹 이름 
    /// </summary>
    string animalName;

    /// <summary>
    /// 여기서 animal은 단지 모델로서만 작동한다. Direct 하라고 할 때까지는 만들지 않는다 
    /// </summary>
    [NonSerialized]
    Animal animal;

    /// <summary>
    /// 저장해놓은 동물 이동 경로 히스토리 
    /// </summary>
    [NonSerialized]
    public int[] path;

    /// <summary>
    /// 0 이하로 떨어지면 이 클래스 객체는 삭제된다 
    /// </summary>
    [NonSerialized]
    public int remainingTurn = 2;

    /// <summary>
    /// 이 흔적을 남긴 동물의 소유 플레이어 UserId
    /// </summary>
    [NonSerialized]
    public string ownerUserId;

    /// <summary>
    /// path 그리기용
    /// </summary>
    VectorLine line = null;
    public Material lineMaterial = null;
    VectorPoints dot = null;
    public Material dotMaterial = null;

    /// <summary>
    /// AnimalTrails을 관리하기 위한 index
    /// </summary>
    public int index;

    TileChunk tileChunk;
    GameManager gameManager;

    /// <summary>
    /// 데이타에 들어있는 Animal Trails의 총 갯수
    /// </summary>
    public static readonly string TOTAL_INDEX_PROP_KEY = "animalTrailTotalIndex";

    /// <summary>
    /// AnimalTrail은 정해진 lifeTime이 있다. 그 lifeTime마다 데이타에서 꺼내와 삭제해야하는데
    /// 얼만큼 삭제되었는지 알려주기 위한 값이다
    /// </summary>
    public static readonly string START_INDEX_PROP_KEY = "animalTrailStartIndex";    

    public static readonly string PATH_PROP_KEY = "animalTrailPath";
    public static readonly string ANIMAL_NAME_PROP_KEY = "animalTrailAnimalName";
    public static readonly string OWNER_USER_ID_PROP_KEY = "animalTrailOwnerUserId";
    public static readonly string REMAINING_TURN_PROP_KEY = "animalTrailRemainingTurn";

    void Start()
    {        
        tileChunk = FindObjectOfType<TileChunk>();
        lineMaterial = tileChunk.lineMaterial;
        dotMaterial = tileChunk.dotMaterial;

        gameManager = FindObjectOfType<GameManager>();
        gameManager.OnChangedPlayerTurn += OnChangedPlayerTurn;
    }

    public void Init(int index)
    {
        this.index = index;
        
        if (PhotonNetwork.room.CustomProperties.ContainsKey(PATH_PROP_KEY + index))
        {
            path = (int[])PhotonNetwork.room.CustomProperties[PATH_PROP_KEY + index];
        }

        if (PhotonNetwork.room.CustomProperties.ContainsKey(ANIMAL_NAME_PROP_KEY + index))
        {
            animalName = (string)PhotonNetwork.room.CustomProperties[ANIMAL_NAME_PROP_KEY + index];
        }

        if (PhotonNetwork.room.CustomProperties.ContainsKey(OWNER_USER_ID_PROP_KEY + index))
        {
            ownerUserId = (string)PhotonNetwork.room.CustomProperties[OWNER_USER_ID_PROP_KEY + index];
        }
    }


    void OnDestroy()
    {
        if(animal != null)
        {
            Destroy(animal.gameObject);
        }
        
        VectorLine.Destroy(ref line);
        VectorPoints.Destroy(ref dot);

        gameManager.OnChangedPlayerTurn -= OnChangedPlayerTurn;
    }


    public bool IsInPath(Tile tile)
    {
        if(path != null)
        {
            for (int i = path[0] - 1; i >= 1; --i)
            {
                /// 0 = 경로 갯수
                /// path[경로 갯수] = 동물 현 위치 
                /// 1 = 도착 지점 
                int pathTileIndex = path[i];

                int tileX = pathTileIndex % tileChunk.tileSizeWidth;
                int tileY = pathTileIndex / tileChunk.tileSizeHeight;
                Tile pathTile = tileChunk.tiles[tileX, tileY];

                if(tile == pathTile)
                {
                    return true;
                }
            }
        }

        return false;
    }


    /// <summary>
    /// static 함수
    /// </summary>
    public static void SaveAnimalTrail(int[] path, string ownerUserId, string animalName)
    {
        if (!PhotonNetwork.isMasterClient)
        {
            return;
        }

        Hashtable animalTrailProp = new Hashtable();

        /// CustomProperties에서 AnimalTrails의 totalIndex를 알아온다 
        int newAnimalTrailIndex = 0;
        if (PhotonNetwork.room.CustomProperties.ContainsKey(TOTAL_INDEX_PROP_KEY))
        {
            newAnimalTrailIndex = (int)PhotonNetwork.room.CustomProperties[TOTAL_INDEX_PROP_KEY];
        }
        else
        {
            animalTrailProp[START_INDEX_PROP_KEY] = 0;
        }

        /// index와 AnimalTrailPropKey를 조합해서 1 depth Key에 데이터들을 넣어준다 
        animalTrailProp[ANIMAL_NAME_PROP_KEY + newAnimalTrailIndex] = animalName;
        animalTrailProp[PATH_PROP_KEY + newAnimalTrailIndex] = path;
        animalTrailProp[OWNER_USER_ID_PROP_KEY + newAnimalTrailIndex] = ownerUserId;

        /// totalIndex를 하나 증가하고 넣어준다 
        animalTrailProp[TOTAL_INDEX_PROP_KEY] = newAnimalTrailIndex + 1;

        PhotonNetwork.room.SetCustomProperties(animalTrailProp);
    }

    
    /// <summary>
    /// OnTurnCompleted에서 이 처리를 하면 나중 순서와 먼저 한 유저의 trail이 활용이 불공평해진다. 
    /// 나중에 한 유저는 먼저 한 유저의 trail을 바로 볼 수 있지만, 먼저한 유저는 나중에 한 유저의 trail을 다음 턴에 볼수 있기 때문이다. 
    /// remainingTurn 차감은 trail 주인의 차례가 끝날 때 해줘야한다. 
    /// </summary>
    public void OnChangedPlayerTurn(string turnUserId)
    {
        UndisplayTrail();

        if (!PhotonNetwork.isMasterClient || turnUserId != ownerUserId)
        {
            return;
        }

        Hashtable animalTrailProp = new Hashtable();
        if (remainingTurn - 1 > 0)
        {
            /// remainingTurn - 1 된 것을 동기화
            animalTrailProp[REMAINING_TURN_PROP_KEY + index] = remainingTurn - 1;
        }
        else
        {
            /// CustomProperties에서 삭제한다 
            animalTrailProp[ANIMAL_NAME_PROP_KEY + index] = null;
            animalTrailProp[PATH_PROP_KEY + index] = null;
            animalTrailProp[OWNER_USER_ID_PROP_KEY + index] = null;
            animalTrailProp[REMAINING_TURN_PROP_KEY + index] = null;

            /// startIndex의 규칙상 한 턴에 여러개의 trail들이 생성되고, 먼저 생성된 것들이 index 숫자가 언제나 낮게 배정되기 때문에 가능한 코드 
            /// 삭제되는 trail에 접근하지 못하도록 startIndex를 수정해준다
            int animalTrailsStartIndex = (int)PhotonNetwork.room.CustomProperties[AnimalTrail.START_INDEX_PROP_KEY];
            if (index + 1 > animalTrailsStartIndex)
            {
                animalTrailProp[AnimalTrail.START_INDEX_PROP_KEY] = index + 1;
            }
        }
        PhotonNetwork.room.SetCustomProperties(animalTrailProp);
    }


    public void DisplayTrail()
    {
        StartCoroutine(Direct());
    }


    public void UndisplayTrail()
    {
        /// 알파 완전히 끄기
        ChangeAlphaForDirect(0f, 0f);

        /// Direct 코루틴 꺼버리기
        isDirecting = false;
    }


    SpriteRenderer spriteRenderer;
    Color spriteRendererColor;
    Color lineColor = Color.magenta;
    float moveSpeed = 10f;
    const float alphaColorVariation = 0.01f;
    public bool isDirecting = false;
    IEnumerator Direct()
    {
        if (path == null || animalName == "" || isDirecting)
        {
            yield break;
        }
        isDirecting = true;

        Vector3 pathStartPos = tileChunk.tiles[path[path[0]] % tileChunk.tileSizeWidth, path[path[0]] / tileChunk.tileSizeHeight].position;

        /// 퍼포먼스를 위해 진짜 Direct를 할 때만 Animal을 만들고, 라인을 그린다 
        if (animal == null)
        {
            animal = Instantiate(Resources.Load("Animals/" + animalName, typeof(Animal)), new Vector3(1000,1000,1000), Quaternion.identity) as Animal;

            /// aniaml 생성까지 기다린다 
            yield return new WaitForEndOfFrame();

            /// 클릭못하게 막기
            animal.GetComponent<BoxCollider>().enabled = false;

            spriteRenderer = animal.GetComponentInChildren<SpriteRenderer>();
            spriteRendererColor = spriteRenderer.color;
            
            line = new VectorLine("AnimalTrail", new List<Vector3>(), lineMaterial, 20.0f, LineType.Continuous, Joins.Weld);
            
            for (int i = 1; i <= path[0]; i++)
            {
                line.points3.Add(tileChunk.tiles[path[i] % tileChunk.tileSizeWidth, path[i] / tileChunk.tileSizeHeight].position);
            }
            line.Draw3D();

            dot = new VectorPoints("AnimalTrailDot", new List<Vector3>(), dotMaterial, 100.0f);
            dot.points3.Add(pathStartPos);
            dot.points3.Add(tileChunk.tiles[path[1] % tileChunk.tileSizeWidth, path[1] / tileChunk.tileSizeHeight].position);
            dot.Draw3D();
        }

        ChangeAlphaForDirect(0f, 0f);

        while (isDirecting)
        {
            animal.transform.position = pathStartPos;

            /// 알파 1이 될 때까지 돌려주기 
            while (/*spriteRendererColor.a < 1.0f ||*/ lineColor.a < 1.0f && isDirecting)
            {
                yield return null;

                float nextSpriteRendererColor = spriteRendererColor.a + alphaColorVariation;
                if (nextSpriteRendererColor > 0.8f)
                {
                    nextSpriteRendererColor = 0.8f;
                }

                ChangeAlphaForDirect(lineColor.a + alphaColorVariation, nextSpriteRendererColor);
            }

            /// 동물 라인 처음부터 마지막까지 가기 
            for (int i = path[0] - 1; i >= 1; --i)
            {
                /// 0 = 경로 갯수
                /// path[경로 갯수] = 동물 현 위치 
                /// 1 = 도착 지점 
                int pathTileIndex = path[i];

                int tileX = pathTileIndex % TileMaker.instance.tileSize;
                int tileY = pathTileIndex / TileMaker.instance.tileSize;
                Tile tile = tileChunk.tiles[tileX, tileY];

                /// 이동 
                float distance = Vector3.Distance(tile.position, animal.transform.position);
                while (distance > 0.1f && isDirecting)
                {
                    animal.transform.position = Vector3.Lerp(animal.transform.position, tile.position, Time.deltaTime * moveSpeed);

                    distance = Vector3.Distance(tile.position, animal.transform.position);

                    yield return null;
                }
            }

            /// 라인 끝까지 도달했다면 이번엔 알파를 빼버리기 
            while (spriteRendererColor.a > 0f || lineColor.a > 0f && isDirecting)
            {
                yield return null;

                ChangeAlphaForDirect(lineColor.a - alphaColorVariation, spriteRendererColor.a - alphaColorVariation);
            }

            /// 다시 시작하기 전에 약간의 여유를 주기 
            yield return new WaitForSeconds(1f);
        }
    }


    void ChangeAlphaForDirect(float lineAlpha, float animalAlpha)
    {
        spriteRendererColor.a = animalAlpha;
        lineColor.a = lineAlpha;

        /// 최적화를 위해 애니멀 생성을 뒤로 미뤘다
        /// 진짜 렌더링이 시작되는 경우에만 아래 변수들이 null이 아니다
        if(animal != null)
        {
            spriteRenderer.color = spriteRendererColor;
            line.color = lineColor;
            dot.color = lineColor;
        }
    }


    public override void OnPhotonCustomRoomPropertiesChanged(Hashtable propertiesThatChanged)
    {
        if (propertiesThatChanged.ContainsKey(REMAINING_TURN_PROP_KEY + index) && propertiesThatChanged[REMAINING_TURN_PROP_KEY + index] != null)
        {
            remainingTurn = (int)propertiesThatChanged[REMAINING_TURN_PROP_KEY + index];
        }
    }
}
