﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using GameDataEditor;
using Vectrosity;
using I2.Loc;
using Hashtable = ExitGames.Client.Photon.Hashtable;



public partial class Animal : CacheMonobehavior 
{
    public static float attakcedShakeDuration = 0.8f;
    

    public void Attack(Animal target, AttackCommand.HitResult hitResult, int damage)
    {
        /// viewId가 모두가 공유하는 고유ID이기 때문에 이걸 식별자로 쓴다  
        photonView.RPC("Attack", PhotonTargets.All, target.photonView.viewID, (int)hitResult, damage);
    }


    public void DoubleAttack(Animal target, AttackCommand.HitResult[] hitResult, int[] damage)
    {
        int[] hitResultInt = new int[2];
        hitResultInt[0] = (int)hitResult[0];
        hitResultInt[1] = (int)hitResult[1];

        photonView.RPC("DoubleAttack", PhotonTargets.All, target.photonView.viewID, hitResultInt, damage);
    }


    [System.NonSerialized]
    public bool isAttacking = false;

    [PunRPC]
    public virtual IEnumerator Attack(int targetAnimalViewId, int hitResultInt, int damage)
    {
        if (isAttacking)
        {
            yield break;
        }

        isAttacking = true;


        PhotonView targetAnimalView = PhotonView.Find(targetAnimalViewId);
        if (targetAnimalView != null)
        {
            Animal targetAnimal = targetAnimalView.GetComponent<Animal>();

            canAction = false;

            AttackCommand.HitResult hitResult = (AttackCommand.HitResult)hitResultInt;

            /// 공격 연출 시작 
            var directingAttackCoroutine = DirectingAttack(targetAnimal, hitResult, damage);
            while (directingAttackCoroutine.MoveNext()) yield return directingAttackCoroutine.Current;
            
            /// 동기화 
            SetCustomPropertiesByMe();

            /// 끝났다고 알려주기
            OnFinishedAttack(this);
        }

        isAttacking = false;
    }


    [PunRPC]
    public virtual IEnumerator DoubleAttack(int targetAnimalViewId, int[] hitResultInt, int[] damage)
    {
        if (isAttacking)
        {
            yield break;
        }

        isAttacking = true;


        PhotonView targetAnimalView = PhotonView.Find(targetAnimalViewId);
        if (targetAnimalView != null)
        {
            Animal targetAnimal = targetAnimalView.GetComponent<Animal>();

            canAction = false;

            curRemainSkillCoolTime = DoubleAttackCommand.instance.skillCoolTime;

            /// 사운드
            AudioClip sound = clickSound[Random.Range(0, clickSound.Length)];
            audioSource.PlayOneShot(sound, 0.3f);

            /// 텍스트 이펙트
            string text = ScriptLocalization.Get(typeof(DoubleAttackCommand).Name + "_name");
            UIManager.instance.ShowHUDText(text, Color.yellow, transform.position + SKILL_TEXT_POS_FACTOR);

            /// 텍스트 이펙트가 사라질 때까지 기다린다. 공격할 때 뜨는 텍스트 이펙트랑 겹치니까 눈에 거슬린다 
            yield return new WaitForSeconds(0.5f);

            /// 공격 연출 시작 
            var directingAttackCoroutine = DirectingAttack(targetAnimal, (AttackCommand.HitResult)hitResultInt[0], damage[0]);
            while (directingAttackCoroutine.MoveNext()) yield return directingAttackCoroutine.Current;

            /// 프로토타입 코드니까 이렇게 하는거다
            /// 서버를 붙인다면 이미 더블어택 연출을 시작하기 전에 체력이 반영되어 있어야 하고, 한번만 공격할지 두번만 공격할지 이미 결정되어있어야 한다
            if (curHealth > 0 && targetAnimal.curHealth > 0)
            {
                directingAttackCoroutine = DirectingAttack(targetAnimal, (AttackCommand.HitResult)hitResultInt[1], damage[1]);
                while (directingAttackCoroutine.MoveNext()) yield return directingAttackCoroutine.Current;
            }


            /// 동기화 
            SetCustomPropertiesByMe();

            /// 끝났다고 알려주기
            OnFinishedAttack(this);
        }

        isAttacking = false;
    }


     public IEnumerator DirectingAttack(Animal targetAnimal, AttackCommand.HitResult hitResult, int damage)
    {
        /// 은신이었다가 맞는 녀석도 은신을 풀어야 한다 
        yield return targetAnimal.Unconceal();

        /// 은신 중인 상태였다면 은신 해제하면서 공격, 근데 이미 디텍티드되고있는상황에서는 느낌표안뜨게
        if ((isConcealed || isSubmerged || isClimbedTree) && !CheckDetected(currentTile))
        {
            /// 타겟 동물 놀라는 연출 
            targetAnimal.audioSource.PlayOneShot(GameManager.instance.surprisedSound, 0.4f);
            UIManager.instance.ShowHUDText("!", Color.yellow, targetAnimal.transform.position + DAMAGE_TEXT_POS_FACTOR);
        }
        if (isConcealed || isSubmerged || isClimbedTree)
        {
            /// 은신 해제
            yield return Conceal(false, 0.5f);
        }

        /// 경계 해제 
        targetAnimal.isOverwatched = false;

        /// 연출하기 전 체력 차감을 먼저 해놓는다 
        switch (hitResult)
        {
            case AttackCommand.HitResult.COUNTER:
                {
                    curHealth -= damage;
                }
                break;
            case AttackCommand.HitResult.HIT:
            case AttackCommand.HitResult.GRAZE:
            case AttackCommand.HitResult.CRITICAL:
                {
                    targetAnimal.curHealth -= damage;
                }
                break;
        }

        /// 때리는 연출 
        yield return DoAttackAnimation(targetAnimal, hitResult);

        Vector3 destination = transform.position + (targetAnimal.transform.position - transform.position) / 2f;

        switch (hitResult)
        {
            case AttackCommand.HitResult.COUNTER:
                {
                    if (targetAnimal.animalData.skills.Contains("RearKick"))
                    {
                        var rearKickCoroutine = DoRearKick(targetAnimal);
                        while (rearKickCoroutine.MoveNext()) yield return rearKickCoroutine.Current;
                    }
                    else
                    {
                        /// 타겟 회전 
                        var targetLookVector = ownedTile.position - targetAnimal.ownedTile.position;
                        var targetLookRotation = Quaternion.LookRotation(targetLookVector);
                        targetAnimal.transform.rotation = targetLookRotation;

                        /// 카운터 연출
                        //targetAnimal.animator.SetBool(counterAnimationName, true);
                        targetAnimal.animator.SetTrigger(targetAnimal.animalData.attackAnim + "Trigger");

                        /// 타격 이펙트 시작하라는 애니메이션 키프레임 이벤트가 올 때까지 대기 
                        while (!targetAnimal.startAttackEffect) yield return null;

                        /// 카메라 셰이크
                        CameraShake.instance.Shake(attakcedShakeDuration, 1.5f);

                        /// 타격 사운드 
                        AudioClip atkSound = GameManager.instance.attackSound_critical[Random.Range(0, GameManager.instance.attackSound_critical.Length)];
                        audioSource.PlayOneShot(atkSound, 0.4f);

                        /// 이펙트
                        AttackEffect.instance.Stop();
                        AttackEffect.instance.Play(destination, hitResult);

                        /// 텍스트 이펙트
                        UIManager.instance.ShowHUDText(ScriptLocalization.Get("COUNTER"), Color.yellow, destination + DAMAGE_TEXT_POS_FACTOR, false);
                        UIManager.instance.ShowHUDText(damage.ToString(), Color.yellow, destination + DAMAGE_TEXT_POS_FACTOR);

                        /// 내가 맞는 연출
                        yield return Damaged(damage, hitResult);

                        //targetAnimal.animator.SetBool(counterAnimationName, false);
                        targetAnimal.animator.ResetTrigger(targetAnimal.animalData.attackAnim + "Trigger");

                        /// 다시 내 제자리로 돌아온다 
                        yield return MoveToRapidly(GetRealPositionByTile(ownedTile));

                        /// 반드시 책임지고 false로 만들어줘야 다음 공격 연출에도 사용할 수 있다 
                        targetAnimal.startAttackEffect = false;
                    }
                }
                break;

            case AttackCommand.HitResult.HIT:
            case AttackCommand.HitResult.GRAZE:
            case AttackCommand.HitResult.CRITICAL:
                {
                    /// 타격 이펙트 시작하라는 애니메이션 키프레임 이벤트가 올 때까지 대기 
                    /// (이 루틴 들어오기 전에 공격 애니메이션 시간과 다가가는 것을 맞춰놨다 
                    ///  이 곳으로 들어오면 공격 애니메이션 반드시 끝나있기 때문에 startAttackEffect가 항상 true라 의미없는 조건문이다)
                    //while (!startAttackEffect) yield return null;

                    /// 카메라 셰이크
                    float cameraShakeMagnitude = 0.8f;
                    if (hitResult == AttackCommand.HitResult.GRAZE)
                    {
                        cameraShakeMagnitude = 0.3f;
                    }
                    else if (hitResult == AttackCommand.HitResult.CRITICAL)
                    {
                        cameraShakeMagnitude = 1.5f;
                    }
                    CameraShake.instance.Shake(attakcedShakeDuration, cameraShakeMagnitude);

                    /// 타격 사운드 
                    AudioClip atkSound = GameManager.instance.attackSound_hit[Random.Range(0, GameManager.instance.attackSound_hit.Length)];
                    float volume = 0.3f;
                    if (hitResult == AttackCommand.HitResult.GRAZE)
                    {
                        atkSound = GameManager.instance.attackSound_graze[Random.Range(0, GameManager.instance.attackSound_graze.Length)];
                        volume = 0.2f;
                    }
                    else if (hitResult == AttackCommand.HitResult.CRITICAL)
                    {
                        atkSound = GameManager.instance.attackSound_critical[Random.Range(0, GameManager.instance.attackSound_critical.Length)];
                        volume = 0.4f;
                    }
                    audioSource.PlayOneShot(atkSound, volume);

                    /// 이펙트
                    AttackEffect.instance.Stop();
                    AttackEffect.instance.Play(targetAnimal.ownedTile.position, hitResult);

                    /// 텍스트 이펙트
                    Color textColor = Color.red;
                    if (hitResult == AttackCommand.HitResult.GRAZE)
                    {
                        textColor = Color.white;
                        UIManager.instance.ShowHUDText(ScriptLocalization.Get("GRAZE"), textColor, targetAnimal.transform.position + DAMAGE_TEXT_POS_FACTOR, false);
                    }
                    else if (hitResult == AttackCommand.HitResult.CRITICAL)
                    {
                        textColor = Color.yellow;
                        UIManager.instance.ShowHUDText(ScriptLocalization.Get("CRITICAL"), textColor, targetAnimal.transform.position + DAMAGE_TEXT_POS_FACTOR, false);
                    }
                    UIManager.instance.ShowHUDText(damage.ToString(), textColor, targetAnimal.transform.position + DAMAGE_TEXT_POS_FACTOR);

                    /// 타겟 동물 맞는 연출
                    targetAnimal.StartCoroutine(targetAnimal.Damaged(damage, hitResult));    

                    /// 다시 내 제자리로 돌아온다 
                    yield return MoveToRapidly(GetRealPositionByTile(ownedTile));

                    /// 타겟 동물의 데미징 코루틴이 종료되어야만 빠져나간다
                    while (targetAnimal.isDamaging) yield return null;
                }
                break;

            case AttackCommand.HitResult.MISS:
                {
                    // 맞는 동물이 Escape 스킬을 가지고 있는 경우, 해당동물이 도망갈 경로를 미리 정해놓은 상태
                    if (EscapeCommand.instance.pathsToTargetTiles.ContainsKey(targetAnimal))
                    {
                        /// 피하는 소리 
                        audioSource.PlayOneShot(GameManager.instance.missSound, 0.3f);

                        /// 피하는 연출 
                        yield return targetAnimal.DirectingDodge(ownedTile.position);
                        

                        /// 때릴려고 했던 녀석 다시 내 제자리로 돌아온다 
                        yield return MoveToRapidly(GetRealPositionByTile(ownedTile));
                        
                        /// 피하는 연출 
                        if (!targetAnimal.isBiting)
                        {
                            /// 피한 녀석 다시 돌아오기 
                            yield return targetAnimal.MoveToRapidly(targetAnimal.GetRealPositionByTile(targetAnimal.ownedTile));
                        }

                        // 타겟이 도망가는 모션
                        yield return EscapeCommand.instance.Escape(targetAnimal);
                    }
					// 그냥 일반적인 miss처리
                    else
                    {
                        /// 피하는 소리 
                        audioSource.PlayOneShot(GameManager.instance.missSound, 0.3f);

                        /// 텍스트 이펙트
                        UIManager.instance.ShowHUDText(ScriptLocalization.Get("MISS"), Color.white, targetAnimal.transform.position + DAMAGE_TEXT_POS_FACTOR);

                        /// 피하는 연출 

                        yield return targetAnimal.DirectingDodge(ownedTile.position);
 
                        /// 때릴려고 했던 녀석 다시 내 제자리로 돌아온다 
                        yield return MoveToRapidly(GetRealPositionByTile(ownedTile));

                        if (!targetAnimal.isBiting)
                        {
                            /// 피한 녀석 다시 돌아오기 
                            yield return targetAnimal.MoveToRapidly(targetAnimal.GetRealPositionByTile(targetAnimal.ownedTile));
                        }
                    }
                }
                break;
        }

        // 공격한 후에 물타일이라면 다시 돌아온다.
        if(curHealth > 0)
        if (ownedTile.type == TileMaker.TileType.RIVER)
        {
            //iTween.Stop(gameObject);
            iTween.RotateTo(gameObject, iTween.Hash("x", xAngle_fallingIntoWater, "easeType", iTween.EaseType.linear, "time", 0.6f));
        }
        // 공격한 후에 타겟동물이 물에있었다면 다시 기울이는 각도로 변경해준다.(miss나 counter의 경우에 각도가 변경됨).
        if (targetAnimal.ownedTile != null && targetAnimal.ownedTile.type == TileMaker.TileType.RIVER)
        {
            iTween.RotateTo(targetAnimal.gameObject, iTween.Hash("x", targetAnimal.xAngle_fallingIntoWater, "easeType", iTween.EaseType.linear, "time", 0.6f));
            {
                iTween.RotateTo(targetAnimal.gameObject, iTween.Hash("x", targetAnimal.xAngle_fallingIntoWater, "easeType", iTween.EaseType.linear, "time", 0.6f));
            }
        }

        /// 이 변수를 사용하지 않았더라도 false로 만들어줘야 한다 
        /// 공격 애니메이션이 실행되면 StartAttackEffect() 키프레임 이벤트가 호출되서 startAttackEffect가 true로 강제되기 때문이다 
        startAttackEffect = false;
     }

    
    [System.NonSerialized] public bool isDodging = false;
    public IEnumerator DirectingDodge(Vector3 enemyPosition, bool autoReturn = false, bool onlyDodgeWithSide = false)
    {
        if (isDodging || isBiting)
        {
            yield break;
        }
        isDodging = true;

        /// 먼저 때리는 녀석의 방향으로 회전
        var targetLookVector = enemyPosition - ownedTile.position;
        var targetLookRotation = Quaternion.LookRotation(targetLookVector);
        transform.rotation = targetLookRotation;

        /// 어느 방향으로 피할 것인지 결정
        Vector3 dodgeDestination = Vector3.zero;

        int dodgeDirection = Random.Range(0, onlyDodgeWithSide ? 2 : 3);
        switch (dodgeDirection)
        {
            case 0: /// 왼쪽
                dodgeDestination = transform.position + ((transform.right * -1f) * (Tile.TILE_WORLD_WIDTH / 2f));
                break;
            case 1: /// 오른쪽
                dodgeDestination = transform.position + (transform.right * (Tile.TILE_WORLD_WIDTH / 2.0f));
                break;
            case 2: /// 뒤
                dodgeDestination = transform.position + ((transform.forward * -1f) * (Tile.TILE_WORLD_WIDTH / 2.0f));
                break;
        }

        /// 피하는 연출 
        yield return MoveToRapidly(dodgeDestination, true);

        /// 피한 녀석 다시 돌아오기 
        if (autoReturn)
        {
            yield return new WaitForSeconds(0.5f);

            yield return MoveToRapidly(GetRealPositionByTile(ownedTile));
        }

        isDodging = false;
    }
    
    IEnumerator DoAttackAnimation(Animal targetAnimal, AttackCommand.HitResult hitResult)
    {
        // 진행중인 itween을 다 중지한다. 현재는 swim일때만 존재함
        iTween.Stop(gameObject);
        /// 회전 
        lookVector = targetAnimal.ownedTile.position - ownedTile.position;
        var lookRotation = Quaternion.LookRotation(lookVector);
        transform.rotation = lookRotation;

        // 상대가 리어킥을 가지고있는 상태에서 카운터에 걸리면
        if (hitResult == AttackCommand.HitResult.COUNTER && targetAnimal.animalData.skills.Contains("RearKick"))
        {
            animator.SetTrigger(animalData.attackAnim + "Trigger");

            AnimationClip animClip = GetAnimationClip(animalData.attackAnim);
            yield return MoveToAnimDuration((transform.position + targetAnimal.transform.position) / 2, animalData.attackAnim, animClip.length / 2);  // 중간지점까지 갔다가
            animator.speed = 0; // 애니메이션이 멈춤
        }
        // 원거리 평타스킬을 가지고 있고 근접해있지않다면
        else if (animalData.skills.Contains("DistantAttack") && Mathf.Abs(targetAnimal.ownedTile._tileIndex - ownedTile._tileIndex) != 1 &&
                    Mathf.Abs(targetAnimal.ownedTile._tileIndex - ownedTile._tileIndex) != TileMaker.instance.tileSize)
        {
            /// 해당 동물의 무빙 애니메이션 찾기 
            AnimationClip movingAnimationClip = GetAnimationClip("distantAttack");
            animator.SetTrigger("distantAttack" + "Trigger");
            /// 무빙 애니메이션 시간
            float movingAnimationTime = movingAnimationClip.length / movingAnimSpeed;
            yield return new WaitForSeconds(movingAnimationTime);
        }
        else //보통 평타 애니메이션시작
        {
            //animator.SetBool(animalData.attackAnim, true);
            animator.SetTrigger(animalData.attackAnim + "Trigger");

            Vector3 destination = transform.position + (targetAnimal.transform.position - transform.position) / 2f;

            yield return MoveTo(destination, animalData.attackAnim);
        }

        //animator.SetBool(animalData.attackAnim, false);
        animator.ResetTrigger(animalData.attackAnim + "Trigger");
    }

    enum DeathAnimation { LEFT, RIGHT, BACK_LEFT, BACK_RIGHT }
    [System.NonSerialized] public bool isDamaging = false;
    public IEnumerator Damaged(int damage, AttackCommand.HitResult hitResult, bool shake = true)
    {
        if (isDamaging)
        {
            yield break;
        }
        isDamaging = true;

        /// 맞는 녀석 은신 바로 해제
        StartCoroutine(Unconceal());

        /// Shake 연출
        float shakeSize = 1f;
        float shakeSpeed = 40f;
        switch (hitResult)
        {
            case AttackCommand.HitResult.COUNTER:
            case AttackCommand.HitResult.CRITICAL:
                shakeSize = 5f;
                break;

            case AttackCommand.HitResult.HIT:
                shakeSize = 2f;
                break;
        }

        Vector3 positionBeforeHit = transform.position;

        if (shake)
        {
            yield return Shake(shakeSize, shakeSpeed);
        }

        /// 맞기전 위치를 기억해놓았던 것을 다시 복구
        /// 비중있는 이동이 아니므로 그냥 이동
        transform.position = positionBeforeHit;

        GameManager.instance.OnAnimalDamaged(this);

        yield return hpBar.ReduceCurHp(damage, 0.25f);

        /// 죽어서 사라지는 연출
        yield return CheckHealthAndDie();
        
        isDamaging = false;

        yield return new WaitForSeconds(2.0f);
        hpBar.SetActive(false);
    }


    [System.NonSerialized]
    public bool isShaking = false;
    public IEnumerator Shake(float shakeSize, float shakeSpeed)
    {
        isShaking = true;

        float direction = -1f;
        float elapsed = 0f;

        // 맞아서 흔들리는 과정
        while (elapsed < attakcedShakeDuration)
        {
            Vector3 destination = transform.position;

            /// 동물이 바라보는 기준으로 양옆 흔들기 
            Vector3 transformRight = transform.right * direction * shakeSize;
            destination += transformRight;

            float distance = Vector3.Distance(destination, transform.position);
            while (distance > 0.1f && elapsed < attakcedShakeDuration)
            {
                transform.position = Vector3.Lerp(transform.position, destination, Time.deltaTime * shakeSpeed);

                distance = Vector3.Distance(destination, transform.position);

                elapsed += Time.deltaTime;

                yield return null;
            }

            direction *= -1f;
        }

        isShaking = false;
    }


    /// Hunter AI에서 Death 애니메이션이 끝날 때까지 기다려주기 위한 변수
    [System.NonSerialized]
    public bool isDisappearing = false;
    public IEnumerator CheckHealthAndDie()
    {
        if (curHealth <= 0)
        {
            GameManager.instance.OnAnimalDeath(this);
            Tile currentTile = ownedTile;
            ownedTile = null;

            /// 선택되있는 동물이었다면 선택해제
            if (GameManager.instance.SelectedAnimal == this)
            {
                GameManager.instance.SelectedAnimal = null;
            }

            TileChunk.instance.UndisplayLine(LINE_FOR_IDENTIFICATION + photonView.viewID);

            OnDeath(this);

            if (isFalling == false)
            {
                /// 애니메이션  
                int randomDeathAnim = Random.Range(1, 3);   /// BACK_LEFT, BACK_RIGHT는 자연스럽게 철푸덕 쓰러지는 것 같지 않아서 뺀다

                animator.SetTrigger("deathTrigger");
                animator.SetInteger("death", randomDeathAnim);

                /// 데스 사운드 
                audioSource.PlayOneShot(deathSound, 0.3f);

                /// 동물마다 Z축으로 엎어질 때 내려가는 y축이 제각각이다
                /// 그 y축을 타일과 맞추기 위한 작업이다 
                MeshFilter meshFilter = GetComponentInChildren<MeshFilter>();
                Bounds meshBounds = meshFilter.sharedMesh.bounds;
                Vector3 meshSize = Vector3.Scale(meshBounds.size, meshFilter.transform.lossyScale);

                Vector3 deathPos = GetRealPositionByTile(currentTile);
                deathPos.y += meshSize.x / 2f;

                var moveToDeathPosCoroutine = MoveToRapidly(deathPos);
                while (moveToDeathPosCoroutine.MoveNext()) yield return moveToDeathPosCoroutine.Current;

                /// 애니메이션 끝날 때까지 기다리기 "death_left"
                AnimationClip deathAnimationClip = GetAnimationClip("death_left");

                float deathAnimationTime = deathAnimationClip.length;

                yield return new WaitForSeconds(deathAnimationTime);

                /// 사라지는 연출은 절차적으로 해줄 필요없다 
                /// 굉장히 오래 걸리기도 해서 혹여나 밖에서 Wait하는 While문이 있다면 문제가 된다 
                StartCoroutine(Disappear());
            }
        }
    }
    // 내가공격했을때 targetAnimal에게 뒷발차기를 맞는 함수
    IEnumerator DoRearKick(Animal targetAnimal)
    {
        /// 느낌표를 띄운다. 뒷발차기 hud를 띄운다.
        /// 동물 놀라는 연출 
        audioSource.PlayOneShot(GameManager.instance.surprisedSound, 0.4f);
        UIManager.instance.ShowHUDText("!", Color.yellow, targetAnimal.transform.position + DAMAGE_TEXT_POS_FACTOR);

        yield return new WaitForSeconds(1.0f);

        /// 타겟 회전 
        var targetLookVector = targetAnimal.ownedTile.position - ownedTile.position;
        var targetLookRotation = Quaternion.LookRotation(targetLookVector);
        targetAnimal.transform.rotation = targetLookRotation;

        /// 공격하는 애니메이션 실행
        targetAnimal.animator.SetTrigger("kickingWithHeelTrigger");
        yield return new WaitForSeconds(GetAnimationClip("kickingWithHeel").length);

        AudioClip atkSound = GameManager.instance.attackSound_critical[Random.Range(0, GameManager.instance.attackSound_critical.Length)];
        audioSource.PlayOneShot(atkSound, 0.4f);
        AttackEffect.instance.Play(transform.position, AttackCommand.HitResult.HIT);
        UIManager.instance.ShowHUDText("GRAZE", Color.white, targetAnimal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);
        UIManager.instance.ShowHUDText(animalData.damage.ToString(), Color.grey, transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);

        yield return Damaged(targetAnimal.animalData.damage, AttackCommand.HitResult.GRAZE);

        Tile tileToFly = ownedTile;
        int flyRange = 4;

        int indexX = ownedTile.TileIndex_X;
        int indexZ = ownedTile.TileIndex_Y;

        bool validFlyRange = false;
        // 1칸부터 ~ flyRange이내에 날라갈 수 있는 타일이 있는지 확인해본다.
        for(int i = 1; i < flyRange; i++)
        {
            if (targetLookVector.x != 0)
            {
                indexX = ownedTile.TileIndex_X + i * -(int)(targetLookVector.x / Mathf.Abs(targetLookVector.x));
            }
            if (targetLookVector.z != 0)
            {
                indexZ = ownedTile.TileIndex_Y + i * (int)(targetLookVector.z / Mathf.Abs(targetLookVector.z));
            }

            // 맵 내부라면
            if(TileChunk.instance.IsValidTileIndex(indexX, indexZ))
            { 
                // 빈타일이라면 flyRange내부에 날릴 수 있는 타일이 존재한다는 소리이므로  
                if(TileChunk.instance.tiles[indexX, indexZ].owningAnimal == null && 
                  !TileChunk.instance.tiles[indexX, indexZ].isRock)
                {
                    validFlyRange = true;
                    break;
                }
            }
            // 맵밖으로 넘어가면 날려버릴수있단소리이다. 
            else
            {
                validFlyRange = true;
                break;
            }
        }

        // flyRange내에 날릴 수 있는 타일이 없다면 flyRange를 1씩늘려가면서 그타일로 날릴 수 있는지 찾아봐야한다.
        if(validFlyRange == false)
        {
            // flyRange를 넘어간 타일을 하나씩 확인하면서 그 타일이 빈타일이거나 맵밖으로 나갈때까지 계속돌린다.
            while (TileChunk.instance.IsValidTileIndex(indexX, indexZ) &&
                (TileChunk.instance.tiles[indexX, indexZ].owningAnimal != null || TileChunk.instance.tiles[indexX, indexZ].isRock))
            {             
                if (targetLookVector.x != 0)
                {
                    indexX = ownedTile.TileIndex_X + flyRange * -(int)(targetLookVector.x / Mathf.Abs(targetLookVector.x));
                }
                if (targetLookVector.z != 0)
                {
                    indexZ = ownedTile.TileIndex_Y + flyRange * (int)(targetLookVector.z / Mathf.Abs(targetLookVector.z));
                }
                flyRange++;
            }
        }
        // flyrange 내부에 날릴 만한 칸이 존재한다.
        else
        {
            indexX = ownedTile.TileIndex_X;
            indexZ = ownedTile.TileIndex_Y;

            // 이제 랜덤하게 돌린다.
            while (TileChunk.instance.IsValidTileIndex(indexX, indexZ) &&
                  (TileChunk.instance.tiles[indexX, indexZ].owningAnimal != null || TileChunk.instance.tiles[indexX, indexZ].isRock))
            {                
                if (targetLookVector.x != 0)
                {
                    indexX = ownedTile.TileIndex_X + Random.Range(1, flyRange) * -(int)(targetLookVector.x / Mathf.Abs(targetLookVector.x));
                }
                if (targetLookVector.z != 0)
                {
                    indexZ = ownedTile.TileIndex_Y + Random.Range(1, flyRange) * (int)(targetLookVector.z / Mathf.Abs(targetLookVector.z));
                }
            }
        }

        // 맵 내부로 떨어진다면
        if (TileChunk.instance.IsValidTileIndex(indexX, indexZ))
        {
            tileToFly = TileChunk.instance.tiles[indexX, indexZ];
            ownedTile = tileToFly;

            ///  나를 날려버린다.
            animator.speed = 1;
            animator.SetTrigger("ambushTrigger");
            var moveCoroutine = MoveTo(tileToFly.position, "ambush");
            while (moveCoroutine.MoveNext()) yield return moveCoroutine.Current;

            // 물에 떨어졌다면 각도를 바꺼야겠지
            if (curHealth > 0)
            {
                // 물에떨어졌을때 각도 바꿔주고 떨어지는 소리, 물결표시
                if (tileToFly.type == TileMaker.TileType.RIVER)
                {
                    audioSource.PlayOneShot(GameManager.instance.waterSplashSound, 0.2f);
                    GameManager.instance.splashParticle.transform.position = new Vector3(transform.position.x, -5, transform.position.z);
                    GameManager.instance.splashParticle.Play();

                    transform.Rotate(Vector3.right, xAngle_fallingIntoWater);

                    yield return MoveToRapidly(tileToFly.position_AdjustedTileYFactor + Vector3.up * yFactor_fallingIntoWater);
                }
                else
                {
                    yield return MoveToRapidly(tileToFly.position_AdjustedTileYFactor);
                }

                targetAnimal.SetVisible(targetAnimal.ownedTile.isVisible);
            }

            SetVisible(ownedTile.isVisible);

            // 타일이 바뀐뒤 시야를 갱신
            TileChunk.instance.RefreshFieldOfView();
        }
        else   // 맵밖으로 벗어난 인덱스인경우 -> 낙사
        {
            Vector3 flyPosition = new Vector3((indexX + 0.5f) * Tile.TILE_WORLD_WIDTH, 0,  - (indexZ + 0.5f)* Tile.TILE_WORLD_HEIGHT);

            ///  나를 날려버린다.
            animator.speed = 1;
            animator.SetTrigger("flyTrigger");

            yield return MoveTo(flyPosition, "fly");            
            
            yield return FallDownOutOfMap(flyPosition);
        }     
    }

    public IEnumerator Disappear()
    {
        if (isDisappearing) yield break;

        isDisappearing = true;

        if (TileChunk.instance.GetTileWithPosition(transform.position).type != TileMaker.TileType.RIVER)
        {
            /// 사라지는 연출 전 딜레이
            yield return new WaitForSeconds(5);
        }
        /// 밑으로 스르륵 사리지게 한다 
        Vector3 disappearPos = transform.position;
        disappearPos.y -= 50f;

        float distance = Vector3.Distance(disappearPos, transform.position);
        while (distance > 0.1f)
        {
            transform.position = Vector3.Lerp(transform.position, disappearPos, Time.deltaTime * 0.1f);

            distance = Vector3.Distance(disappearPos, transform.position);

            yield return null;
        }

        isDisappearing = false;
    }


    /// 타격감의 정확한 타이밍을 위해 있는 공격 애니메이션에서 호출되는 이벤트 함수 
    [System.NonSerialized] public bool startAttackEffect = false;
    public void StartAttackEffect()
    {
        startAttackEffect = true;
    }
}
