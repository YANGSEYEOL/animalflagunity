﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using GameDataEditor;
using Vectrosity;
using I2.Loc;
using Hashtable = ExitGames.Client.Photon.Hashtable;



public partial class Animal : CacheMonobehavior
{
    [System.NonSerialized]
    public Skill[] skills;
    [System.NonSerialized]
    public Skill[] learnedSkill;

    Transform fxPoint;

    /// 경계 사용 상태
    [System.NonSerialized]
    string overwatchingAnimationName = "snuff";
    bool _isOverwatched = false;
    public bool isOverwatched
    {
        get { return _isOverwatched; }
        set
        {
            _isOverwatched = value;

            if (value)
            {
                animator.SetTrigger(overwatchingAnimationName + "Trigger");
            }
            else
            {
                animator.ResetTrigger(overwatchingAnimationName + "Trigger");
            }

            animator.SetBool(overwatchingAnimationName, value);
        }
    }

    bool _canRetrieveFlag = true;
    public bool canRetrieveFlag
    {
        get { return _canRetrieveFlag; }
        set { _canRetrieveFlag = value; OnChangedCanRetrieveFlag(this); }
    }
    [System.NonSerialized]
    public bool isRetrievingFlag = false;

    [System.NonSerialized]
    int _curRemainSkillCoolTime;
    public int curRemainSkillCoolTime
    {
        get { return _curRemainSkillCoolTime; }
        set
        {
            _curRemainSkillCoolTime = value;
            if (_curRemainSkillCoolTime < 0)
            {
                _curRemainSkillCoolTime = 0;
            }
        }
    }

    /// 메쉬 알파블렌딩을 위한 셰이더 변수 
    /// 알파블렌딩을 하려면 Transparent가 들어간 셰이더를 써야 한다  
    /// 하지만 Transparent가 들어간 셰이더를 기본 셰이더로 지정해서 렌더링하면 메쉬 렌더링이 건드리지 않아도 투명하게 되는 현상이 생김 
    /// 가령 코끼리를 렌더링한다면 뒤에서 카메라로 볼 때 상아가 귀 때문에 안 보여야 하는데, 상아가 보이는 현상이 생긴다 
    /// 그래서 알파블렌딩을 사용하지 않을 때는 기본 셰이더를 쓰고, 알파블렌딩을 사용할 때만 Transparent가 들어간 셰이더를 쓴다 
    public static Shader shader_basic;
    public static Shader shader_transparent;

    GameObject stunFx;
    // 스턴 상태(이번턴 행동 불가 )
    bool _isStunned = false;
    public bool isStunned
    {
        get { return _isStunned; }
        set {
            if (_isStunned == value)
                return;

            _isStunned = value;
            if (value == true)
            {
                stunFx = GameManager.instance.GetStunFx();
                stunFx.SetActive(true);
                stunFx.transform.position = fxPoint.position;
                stunFx.transform.localScale = Vector3.one * 10;
                EffectCamera.instance.FollowMainCamera(4f);
            }
            else
            {
                stunFx.SetActive(false);
            }
        }
    } 

    // 현재 biting중 -> 타일의 제자리에있지않고 뭔가 계속하고있는상태, 앞으로 추가될수도있는데 일단 biting
    public bool isBiting
    {
        get { return animator.GetBool("biteIdle") || animator.GetBool("bitten"); }
    }

    /// 은신 스킬 사용 상태
    private bool _isConcealed = false;
    public bool isConcealed
    {
        get { return _isConcealed; }
        set
        {
            _isConcealed = value;
            // conceal여부에따라 hp바 하단에 은신아이콘 업데이트
            if (_isConcealed)
                hpBar.AddStatus("icon_conceal");
            else
                hpBar.RemoveStatus("icon_conceal");
        }
    }

    /// 나무타기 스킬 사용 상태
    private bool _isClimbedTree = false;
    public bool isClimbedTree
    {
        get { return _isClimbedTree; }
        set
        {
            _isClimbedTree = value;
            // conceal여부에따라 hp바 하단에 은신아이콘 업데이트
            if (_isClimbedTree)
                hpBar.AddStatus("icon_conceal");
            else
                hpBar.RemoveStatus("icon_conceal");
        }
    }
    /// 잠수 스킬 사용 상태
    private bool _isSubmerged = false;
    public bool isSubmerged
    {
        get { return _isSubmerged; }
        set
        {
            _isSubmerged = value;
            // conceal여부에따라 hp바 하단에 은신아이콘 업데이트
            if (_isSubmerged)
                hpBar.AddStatus("icon_conceal");
            else
                hpBar.RemoveStatus("icon_conceal");
        }
    }
    /// tracing 스킬 사용 상태
    [System.NonSerialized]
    public bool isTracing = false;

    public bool canConceal { get { return animalData.skills.Contains("Conceal"); } }
    public bool canClimbTree { get { return animalData.skills.Contains("ClimbTree"); } }
    public bool canSubmerge { get { return animalData.skills.Contains("Submerge"); } }

    /// 도약 스킬 사용 상태
    [System.NonSerialized]
    public bool isLeaped = false;

    /// 카운터 스킬 사용 상태
    public bool canCounter
    {
        get
        {
            return animalData.skills.Contains("Counter") ||
                     animalData.skills.Contains("RearKick");
        }
    }

    /// 공격적인 경계 스킬 사용 상태
    [System.NonSerialized]
    public bool isAggressiveOverwatching = false;


    void CheckAggressiveWatchingAnimalInPath(ref int[] path, ref bool areThereEnemiesAttackSkills, ref Hashtable enemiesAttackSkillQueue, 
                                ref int estimatedHealth, ref int estimatedDestinationPathIndex)
    {
        List<Animal> aggressiveWatchedAnimals = new List<Animal>();

        if (PhotonNetwork.offlineMode)
        {
            // 
            // 여기도 현재 PhotonNetwork.player.UserId 쓰는게 아니라 이 동물의 ownerUserID 를 쓰는게 동작을 따라가는데 더 쉬운듯
            if (ownerUserId == GameManager.instance.userIDForSimulation_origin)
            {
                foreach (var enemyAnimal in GameManager.instance.animals2ForSimulation)
                {
                    if (enemyAnimal.isAggressiveOverwatching) aggressiveWatchedAnimals.Add(enemyAnimal);
                }
                foreach (var enemyAnimal in GameManager.instance.huntersForSimulation)
                {
                    if (enemyAnimal.isAggressiveOverwatching) aggressiveWatchedAnimals.Add(enemyAnimal);
                }
            }
            else
            {
                foreach (var enemyAnimal in GameManager.instance.animals1ForSimulation)
                {
                    if (enemyAnimal.isAggressiveOverwatching) aggressiveWatchedAnimals.Add(enemyAnimal);
                }
            }
        }

        /// 체크하기 전에 aggressive overwatched 동물들이 자기 친구들의 타일을 path로 사용하지 못하도록 잠깐 tileOccupation을 걸어둔다 
        foreach (Animal enemy in GameManager.instance.enemyAnimals)
        {
            TileChunk.instance.ChnangeTileOccupation(enemy.ownedTile._tileIndex, true);
        }

        /// 좀더 정확한 path 계산을 위해 지금 있는 위치 타일에 Occupation을 false로 바꿔둔다
        /// 내가 이동했을 때, 내가 있었던 타일을 포악한 경계에 경로로 사용할 수 있게 하기 위함
        TileChunk.instance.ChnangeTileOccupation(ownedTile._tileIndex, false);

        bool stopSearching = false;
        int attackedByAggressiveOverwatchedAnimalCount = 0;
        for (int i = path[0] - 1; i >= estimatedDestinationPathIndex; --i)
        {
            /// 포악한 경계가 path를 구하는 규칙은 '내가 그 타일에 갈 수 있는가'이다
            /// 예상되는 이동 타일에 occupation을 true로 바꾸면, 그 타일은 막힌게 되어, 포악한 경계 path가 나오지 않게 된다 
            //TileChunk.instance.ChnangeTileOccupation(shortestPath[i], true);

            /// 동물의 시야 범위에도 들어와야 하며, 이동 가능 범위에도 들어와야 한다 
            foreach (Animal enemyAnimal in aggressiveWatchedAnimals)
            {
                /// 이미 이전 공격들을 맞았을 때 체력이 0 이하로 떨어졌다면 이후 체크는 하지 않는다
                if (estimatedHealth <= 0) break;

                /// 동물에게 접근할 수 있는가
                int pathTileIndex = path[i];
                int[] enemyAttackPath = TileChunk.instance.GetShortestPathForMovement(enemyAnimal, pathTileIndex, enemyAnimal.animalData.mobility);
                if (enemyAttackPath == null) break; /// 이동력에 닿지 않는 타일이라면, 패스

                /// 시야에 들어오는가
                /// 포악한 경계는 반경범위 안에 들어오는 적을 자동으로 추적하여 공격하는 스킬이다 
                /// 근데 시야 체크를 넣게 되면 반경 범위 안에 들어와도 시야에 보이지 않으면 공격하지 않는다 
                /// 여기서 혼동이 생긴다 
                /// 분명 반경 범위안에 들어와있는데 추적 공격을 하지 않았으니 말이다 
                /// 시야는 언제든지 꺼졌다 켜지는 변수이기 때문에 시야에 따라 유동적으로 변경되는 반경 표시하는 무리다 
                /// 혼동을 없애려면 아예 시야체크를 하지 않으면 된다 
                /// 허나 이러면 포악한 경계는 보이지 않는 것도 공격하는 S티어 스킬이 된다 
                /// 공격을 당하는 입장에서는 그렇다쳐도, 공격하는 입장에서 시야에 안 보이는데 발동되면 되게 이상하게 느껴진다 
                /// 설명을 잘 써주던가 해야겠다 
                bool isTargetInMySight = false;
                foreach (Tile tileInEnemySight in GameManager.instance.sightOfUser[enemyAnimal.ownerUserId])
                {
                    if (tileInEnemySight._tileIndex == pathTileIndex)
                    {
                        isTargetInMySight = true;

                        break;
                    }
                }
                if (!isTargetInMySight) break;  /// 시야에 들어오지 않는다면, 패스

                /// 접근할 수 있다면 공격 가능 
                areThereEnemiesAttackSkills = true;

                /// 다음 체크하려는 동물이 이동하려는 타일을 path로 쓰지 못하도록 미리 occupation 해놓음
                int enemyDestinationTileIndex = enemyAttackPath[2];
                TileChunk.instance.ChnangeTileOccupation(enemyDestinationTileIndex, true);

                /// 원래 있었던 타일에 occupation은 false 시켜서 다음 동물이 path로 쓸 수 있도록 함
                TileChunk.instance.ChnangeTileOccupation(enemyAnimal.ownedTile._tileIndex, false);

                //enemyAnimal.isAggressiveOverwatching = false;

                /// 공격 판정 주사위 돌리기 
                int[] hitResultTable = AttackCommand.instance.GetHitChance(enemyAnimal, this);
                AttackCommand.HitResult hitResult = AttackCommand.instance.RollForHit(hitResultTable, this);
                int damage = AttackCommand.instance.FinalizeDamage(enemyAnimal, this, hitResult);

                Hashtable AttackedByAggressiveOverwatchedAnimalTable = new Hashtable();
                AttackedByAggressiveOverwatchedAnimalTable["path"] = path;
                AttackedByAggressiveOverwatchedAnimalTable["destinationPathIndex"] = i;
                AttackedByAggressiveOverwatchedAnimalTable["enemyPath"] = enemyAttackPath;
                AttackedByAggressiveOverwatchedAnimalTable["attackAnimalViewId"] = enemyAnimal.photonView.viewID;
                AttackedByAggressiveOverwatchedAnimalTable["hitResultInt"] = (int)hitResult;
                AttackedByAggressiveOverwatchedAnimalTable["damage"] = damage;

                enemiesAttackSkillQueue["AttackedByAggressiveOverwatchedAnimalTable_" + attackedByAggressiveOverwatchedAnimalCount.ToString()] = AttackedByAggressiveOverwatchedAnimalTable;
                attackedByAggressiveOverwatchedAnimalCount++;

                /// 더 조금 움직여서 발동되는 공격이 있다면 서칭을 더 할 필요 없다 
                if (i > estimatedDestinationPathIndex)
                {
                    estimatedDestinationPathIndex = i;
                    stopSearching = true;
                }

                estimatedHealth -= damage;
            }

            if (stopSearching) break;
        }

        /// 체크 다 하고 tileOccupation을 다시 해제 한다 
        TileChunk.instance.RefreshTileOccupation();
    }

    public void CheckEnemiesAttackSkillsInPath(ref int[] path, out bool areThereEnemiesAttackSkills, out Hashtable enemiesAttackSkillQueue, out int changedDestinationPathIndex)
    {
        areThereEnemiesAttackSkills = false;

        int estimatedHealth = curHealth;
        int estimatedDestinationPathIndex = 1;

        enemiesAttackSkillQueue = new Hashtable();

        /// 1. path 가는 도중에 적의 동물이 있는지 체크
        for (int i = path[0] - 1; i >= estimatedDestinationPathIndex; --i)
        {
            /// 0 = 경로 갯수
            /// path[경로 갯수] = 동물 현 위치 
            /// 1 = 도착 지점 
            int pathTileIndex = path[i];

            int nextTileX = pathTileIndex % TileMaker.instance.tileSize;
            int nextTileY = pathTileIndex / TileMaker.instance.tileSize;
            Tile nextTile = TileChunk.instance.tiles[nextTileX, nextTileY];

            /// isMine 체크는 혹시나 해서 하는 것이다 
            /// 내 동물이 path 경로에 있다면 그건 버그다 
            Animal owningAnimal = nextTile.owningAnimal;
            // isMine사용보다는 ownerUserId를 비교해주는게 상대턴에 움직이는(타조의 escape같이) 경우에도
            // 상대방의 경로차단에 반응할 수 있음
            if (owningAnimal == null || owningAnimal.ownerUserId == ownerUserId) continue;   

            /// 은신동물이라면 
            if (owningAnimal.isConcealed || owningAnimal.isClimbedTree || owningAnimal.isSubmerged)
            {
                Animal concealedAnimal = owningAnimal;
                
                /// 공격 판정 주사위 돌리기 
                int[] hitResultTable = AttackCommand.instance.GetHitChance(concealedAnimal, this);
                AttackCommand.HitResult hitResult = AttackCommand.instance.RollForHit(hitResultTable, this);
                int damage = AttackCommand.instance.FinalizeDamage(concealedAnimal, this, hitResult);

                Hashtable attackedByConcealedAnimalTable = new Hashtable();
                attackedByConcealedAnimalTable["path"] = path;
                attackedByConcealedAnimalTable["destinationPathIndex"] = i + 1;
                attackedByConcealedAnimalTable["concealedAnimalViewId"] = concealedAnimal.photonView.viewID;
                attackedByConcealedAnimalTable["hitResultInt"] = (int)hitResult;
                attackedByConcealedAnimalTable["damage"] = damage;

                enemiesAttackSkillQueue["AttackedByConcealedAnimal"] = attackedByConcealedAnimalTable;

                areThereEnemiesAttackSkills = true;

                /// i 번째 타일에 은신된 유닛이 있으니 그 전 타일로 이동한다 
                estimatedDestinationPathIndex = i + 1;

                estimatedHealth -= damage;
            }
            /// 은신동물은 아니었지만, 시야에 가려서 안 보였던 동물 
            else
            {
                /// 그 동물이 있었던 타일의 전 타일까지만 이동 
                estimatedDestinationPathIndex = i + 1;
            }

            break;
        }


        /// 2. path 가는 도중에 포악한 경계 중인 동물이 있는지 체크 
        if (isTracing == false)
        {
            CheckAggressiveWatchingAnimalInPath(ref path, ref areThereEnemiesAttackSkills, ref enemiesAttackSkillQueue, ref estimatedHealth, ref estimatedDestinationPathIndex);
        }

        /// 3. 최종적으로 결정된 도착 타일보다 더 많이 이동시켜야하는 공격이라면 그 공격은 취소한다 
        if (enemiesAttackSkillQueue.ContainsKey("AttackedByConcealedAnimal"))
        {
            Hashtable attackedByConcealedAnimalTable = (Hashtable)enemiesAttackSkillQueue["AttackedByConcealedAnimal"];
            int destinationPathIndex = (int)attackedByConcealedAnimalTable["destinationPathIndex"];
            if (destinationPathIndex < estimatedDestinationPathIndex)
            {
                enemiesAttackSkillQueue.Remove("AttackedByConcealedAnimal");
            }
        }

        /// 위의 체크순서상으로 볼 때, 포악한 경계 체크가 은신동물 공격 체크보다 나중에 한다 
        /// 포악한 경계가 은신공격동물과 동시에 공격하는 destinationPath라면, 둘다 발동된다
        /// 은신동물공격보다 더 빨리 공격하는 destinationPath라면, 은신동물공격이 취소돼야 한다
        /// 은신동물공격보다 더 늦게 공격하는 destinationPath라면, 애초에 2번 검사에서 걸러지기 때문에 이 체크는 필요없다 
        /*int attackedAggressiveOverwatchAnimalCount = 0;
        while (enemiesAttackSkillQueue.ContainsKey("AttackedByAggressiveOverwatchedAnimalTable_" + attackedAggressiveOverwatchAnimalCount.ToString()))
        {
            Hashtable AttackedByAggressiveOverwatchedAnimalTable = (Hashtable)enemiesAttackSkillQueue["AttackedByAggressiveOverwatchedAnimalTable_" + attackedAggressiveOverwatchAnimalCount.ToString()];
            int destinationPathIndex = (int)AttackedByAggressiveOverwatchedAnimalTable["destinationPathIndex"];
            if (destinationPathIndex < estimatedDestinationPathIndex)
            {
                enemiesAttackSkillQueue.Remove("AttackedByAggressiveOverwatchedAnimalTable_" + attackedAggressiveOverwatchAnimalCount.ToString());
            }

            attackedAggressiveOverwatchAnimalCount++;
        } */

        changedDestinationPathIndex = estimatedDestinationPathIndex;
    }

    
    public IEnumerator StartAttackedQueue(Hashtable attackedQueue)
    {
        if (attackedQueue.ContainsKey("AttackedByConcealedAnimal"))
        {
            Hashtable attackedByConcealedAnimalTable = (Hashtable)attackedQueue["AttackedByConcealedAnimal"];
            int[] path = (int[])attackedByConcealedAnimalTable["path"];
            int destinationPathIndex = (int)attackedByConcealedAnimalTable["destinationPathIndex"];
            int concealedAnimalViewId = (int)attackedByConcealedAnimalTable["concealedAnimalViewId"];
            int hitResultInt = (int)attackedByConcealedAnimalTable["hitResultInt"];
            int damage = (int)attackedByConcealedAnimalTable["damage"];

            var coroutine = AttackedByConcealedAnimal(path, destinationPathIndex, concealedAnimalViewId, hitResultInt, damage);
            while (coroutine.MoveNext()) yield return coroutine.Current;

            /// 연출의 자연스러움을 위해 약간의 딜레이
            yield return new WaitForSeconds(0.3f);
        }

        int attackedAggressiveOverwatchAnimalCount = 0;
        while (attackedQueue.ContainsKey("AttackedByAggressiveOverwatchedAnimalTable_" + attackedAggressiveOverwatchAnimalCount.ToString()))
        {
            Hashtable AttackedByAggressiveOverwatchedAnimalTable = (Hashtable)attackedQueue["AttackedByAggressiveOverwatchedAnimalTable_" + attackedAggressiveOverwatchAnimalCount.ToString()];
            int[] path = (int[])AttackedByAggressiveOverwatchedAnimalTable["path"];
            int destinationPathIndex = (int)AttackedByAggressiveOverwatchedAnimalTable["destinationPathIndex"];
            int[] enemyPath = (int[])AttackedByAggressiveOverwatchedAnimalTable["enemyPath"];
            int attackAnimalViewId = (int)AttackedByAggressiveOverwatchedAnimalTable["attackAnimalViewId"];
            int hitResultInt = (int)AttackedByAggressiveOverwatchedAnimalTable["hitResultInt"];
            int damage = (int)AttackedByAggressiveOverwatchedAnimalTable["damage"];

            var coroutine = AttackedByAggressiveOverwatchedAnimal(path, destinationPathIndex, enemyPath, attackAnimalViewId, hitResultInt, damage);
            while (coroutine.MoveNext()) yield return coroutine.Current;

            attackedAggressiveOverwatchAnimalCount++;

            /// 연출의 자연스러움을 위해 약간의 딜레이
            yield return new WaitForSeconds(0.3f);
        }
    }


    public IEnumerator Conceal(bool conceal, float delatAfterTextEffect = 0f, float fadingTime = 1f, bool ignoreSound = false)
    {
        bool prevConcealState = isConcealed || isSubmerged || isClimbedTree;    /// 셋 중 하나라도 true였다면 true

        /// 이전 은신 상태와 같은 처리는 다시 하지 않는다 
        /// 은신 중인데 은신 가능 타일이라고 또 은신 소리 나거나 할 수 있다 
        if (prevConcealState == conceal) yield break;
        

        /// 상대턴에 은신이 풀렸다면, 은신된 동물 타일로 다시 이동 불가능하게 막기
        if (!conceal)
        {
            TileChunk.instance.ChnangeTileOccupation(_ownedTile._tileIndex, true);
        }

        string textEffectText = "";
        AudioClip concealSound = null;
        if (canConceal)
        {
            isConcealed = conceal;
            textEffectText = conceal ? ScriptLocalization.Get("ConcealCommand_name") : "";
            concealSound = conceal ? GameManager.instance.concealSound : GameManager.instance.unconcealSound;
        }
        else if (canSubmerge)
        {
            isSubmerged = conceal;
            textEffectText = conceal ? ScriptLocalization.Get("SubmergeCommand_name") : "";
            concealSound = conceal ? GameManager.instance.submergeSound : GameManager.instance.unsubmergedSound;
        }
        else if (canClimbTree)
        {
            isClimbedTree = conceal;
            textEffectText = conceal ? ScriptLocalization.Get("ClimbTreeCommand_name") : "";
            concealSound = conceal ? GameManager.instance.climbTreeSound : GameManager.instance.unclimbTreeSound;
        }

        /// 사운드
        audioSource.PlayOneShot(concealSound, ignoreSound ? 0.0f : 0.3f);

        /// 텍스트 이펙트 
        /// 패시브로 바뀌고 나서부터 이동할 때마다 떠서 거슬림
        //UIManager.instance.ShowHUDText(textEffectText, Color.yellow, transform.position + SKILL_TEXT_POS_FACTOR);

        /// 연출 자연스러움을 위해 텍스트 이펙트 나오고 딜레이 주는 시간 
        yield return new WaitForSeconds(delatAfterTextEffect);

        /// 알파블렌딩
        var adjustAlphaBlendingForConcealoroutine = AdjustAlphaBlendingForConceal(fadingTime);
        while (adjustAlphaBlendingForConcealoroutine.MoveNext()) yield return adjustAlphaBlendingForConcealoroutine.Current;
    }


    // isConcealed ... 이 3 변수들에 따라 alpha값을 1, 0.5, 0 이렇게 조절하는 함수
    public IEnumerator AdjustAlphaBlendingForConceal(float fadingTime = 1f)
    {
        Color origin = meshRenderer.material.color;
        float adjustAlpha = 1.0f;
        if (isConcealed || isSubmerged || isClimbedTree)
        {
            meshRenderer.material.shader = shader_transparent;

            // 내 동물이면 0.5 , ai를 보이게해놨으면 모든동물이 0.5, 해당타일이 디텍트 상태면 0.5
            adjustAlpha = photonView.isMine || GameManager.instance.showSightOfAI || CheckDetected(currentTile)
                ? 0.5f : 0.0f;
        }

        /// 이메쉬의 알파값이 바꾸려고 하는 알파값과 다르다면 duration 동안 자연스럽게 알파블렌딩 시킨다 
        if (origin.a != adjustAlpha)
        {
            float elapsed = 0f;

            while (elapsed < fadingTime)
            {
                float alpha = Mathf.Lerp(origin.a, adjustAlpha, elapsed / fadingTime);

                meshRenderer.material.color = new Color(origin.r, origin.g, origin.b, alpha);

                elapsed += Time.deltaTime;

                yield return null;
            }

            meshRenderer.material.color = new Color(origin.r, origin.g, origin.b, adjustAlpha);
        }

        /// 알파블렌딩이 없는 상태로 돌아간다고 하더라도 기존 메쉬 상태가 알파블렌딩이 적용되어있을 수 있으니 
        /// 먼저 알파를 1로 자연스럽게 돌아오게 한 다음 알파를 쓰지 않는 셰이더로 바꿔준다 
        if (!isConcealed && !isSubmerged && !isClimbedTree)
        {
            meshRenderer.material.shader = shader_basic;
        }
    }


    public IEnumerator Unconceal()
    {
        if (isConcealed || isSubmerged || isClimbedTree) yield return Conceal(false, 0.5f);
    }
    

    IEnumerator AttackedByConcealedAnimal(int[] path, int destinationPathIndex, int concealedAnimalViewId, int hitResultInt, int damage)
    {
        PhotonView concealedAnimalView = PhotonView.Find(concealedAnimalViewId);
        Animal concealedAnimal = concealedAnimalView.GetComponent<Animal>();

        AttackCommand.HitResult hitResult = (AttackCommand.HitResult)hitResultInt;

        /// 은신된 동물이 있는 곳까지 이동
        var directingMoveCoroutine = DirectingMove(path, destinationPathIndex);
        while (directingMoveCoroutine.MoveNext()) yield return directingMoveCoroutine.Current;

        /// 은신 동물 강제로 visible
        concealedAnimal.SetVisible(true);

        /// 은신했던 동물이 동물 공격 
        var directingAttackCoroutine = concealedAnimal.DirectingAttack(this, hitResult, damage);
        while (directingAttackCoroutine.MoveNext()) yield return directingAttackCoroutine.Current;

        if (photonView.isMine)
        {
            /// UI 갱신
            UIManager.instance.RefreshUIViews();

            /// 알리기
            OnFinishedMove(this);
        }
    }


    public void Leap()
    {
        isLeaped = true;

        canAction = false;

        /// 이동력의 1/2으로 다시 이동할 수 있다 
        curremainMovement = (int)(animalData.mobility * 0.5f);

        /// 사운드
        AudioClip sound = clickSound[Random.Range(0, clickSound.Length)];
        audioSource.PlayOneShot(sound, 0.3f);

        /// 텍스트 이펙트
        string text = ScriptLocalization.Get(typeof(LeapCommand).Name + "_name");
        UIManager.instance.ShowHUDText(text, Color.yellow, transform.position + SKILL_TEXT_POS_FACTOR);
    }


    public void AggressiveOverwatch()
    {
        canAction = false;

        isAggressiveOverwatching = true;

        curRemainSkillCoolTime = AggressiveOverwatchCommand.instance.skillCoolTime;

        /// 사운드. 그나마 가장 공격적인 것 같은 소리로 튼다 
        audioSource.PlayOneShot(clickSound[0], 0.3f);

        /// 텍스트 이펙트
        string text = ScriptLocalization.Get(typeof(AggressiveOverwatchCommand).Name + "_name");
        UIManager.instance.ShowHUDText(text, Color.yellow, transform.position + SKILL_TEXT_POS_FACTOR);
    }

    
    public void Overwatch()
    {
        canAction = false;

        isOverwatched = true;

        /// 사운드
        AudioClip sound = clickSound[Random.Range(0, clickSound.Length)];
        audioSource.PlayOneShot(sound, 0.3f);

        /// 텍스트 이펙트
        string text = I2.Loc.ScriptLocalization.Get(typeof(OverwatchCommand).Name + "_name");
        UIManager.instance.ShowHUDText(text, Color.yellow, transform.position + SKILL_TEXT_POS_FACTOR);
    }


    public void RetrieveFlag()
    {
        isRetrievingFlag = true;

        canAction = false;

        curremainMovement = 0;

        canRetrieveFlag = false;
    }


    IEnumerator AttackedByAggressiveOverwatchedAnimal(int[] path, int destinationPathIndex, int[] enemyPath, int attackAnimalViewId, int hitResultInt, int damage)
    {
        PhotonView aggressiveOverwatchedAnimalView = PhotonView.Find(attackAnimalViewId);
        Animal aggressiveOverwatchedAnimal = aggressiveOverwatchedAnimalView.GetComponent<Animal>();

        AttackCommand.HitResult hitResult = (AttackCommand.HitResult)hitResultInt;

        /// 포악한 경계중인 동물이 있는 곳까지 이동
        var directingMoveCoroutine = DirectingMove(path, destinationPathIndex);
        while (directingMoveCoroutine.MoveNext()) yield return directingMoveCoroutine.Current;

        /// 은신이었으면 풀린다
        if (isConcealed || isSubmerged || isClimbedTree) yield return Conceal(false, 0.5f);

        /// 동물 놀라는 연출 
        audioSource.PlayOneShot(GameManager.instance.surprisedSound, 0.4f);
        UIManager.instance.ShowHUDText("!", Color.yellow, transform.position + DAMAGE_TEXT_POS_FACTOR);

        /// 텍스트 이펙트 약간 사라질 때까지 대기 
        yield return new WaitForSeconds(0.5f);

        /// 텍스트 이펙트
        string text = ScriptLocalization.Get(typeof(AggressiveOverwatchCommand).Name + "_name");
        UIManager.instance.ShowHUDText(text, Color.yellow, aggressiveOverwatchedAnimal.transform.position + SKILL_TEXT_POS_FACTOR);

        /// 경계중이던 동물 강제 Visible
        aggressiveOverwatchedAnimal.SetVisible(true);

        /// 사운드. 그나마 가장 공격적인 것 같은 소리로 튼다 
        aggressiveOverwatchedAnimal.audioSource.PlayOneShot(aggressiveOverwatchedAnimal.clickSound[0], 0.3f);

        /// 텍스트 이펙트 약간 사라질 때까지 대기 
        yield return new WaitForSeconds(0.5f);

        /// 포악한 경계중인 동물이 공격할 대상에게 접근. 2를 넣는 이유는 대상 타겟 타일이 목적지 타일로 지정되어있기 때문에 겹치지 않게 딱 그 전타일로 이동시키기 위함 
        aggressiveOverwatchedAnimal.movingAnimSpeed = 1.5f;	/// 포악하게 달려드는 느낌이 나도록 일반 이동속도보다 빠르게 설정 
        directingMoveCoroutine = aggressiveOverwatchedAnimal.DirectingMove(enemyPath, 2);
        while (directingMoveCoroutine.MoveNext()) yield return directingMoveCoroutine.Current;
        aggressiveOverwatchedAnimal.movingAnimSpeed = 1f;

        /// 포악한 경계중인 동물이 공격 
        var directingAttackCoroutine = aggressiveOverwatchedAnimal.DirectingAttack(this, hitResult, damage);
        while (directingAttackCoroutine.MoveNext()) yield return directingAttackCoroutine.Current;

        aggressiveOverwatchedAnimal.isAggressiveOverwatching = false;

        if (photonView.isMine)
        {
            /// UI 갱신
            UIManager.instance.RefreshUIViews();

            /// 알리기
            OnFinishedMove(this);
        }
    }
}
