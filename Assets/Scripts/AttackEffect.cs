﻿using UnityEngine;
using System.Collections;

public class AttackEffect : CacheMonobehavior 
{
    public static AttackEffect instance;


    [System.NonSerialized]
    ParticleSystem partsEffect_burst;
    [SerializeField]
    ParticleSystem partsEffect_star;
    [SerializeField]
    ParticleSystem partsEffect_glow;
    [SerializeField]
    ParticleSystem partsEffect_ray;

    ///Transform _target;
    ParticleSystemRenderer partsEffectRenderer_burst;
    ParticleSystemRenderer partsEffectRenderer_ray;

    /// 0 : Graze, 1 : HIT, 2: COUNTER or CRITICAL
    [SerializeField]
    Material[] materials_burst;
    [SerializeField]
    Material[] materials_ray;


    // unity functions ---------------------------------------------------
    void Awake()
	{
        instance = this;
    }

    void Start()
    {
        partsEffect_burst = GetComponent<ParticleSystem>();

        partsEffectRenderer_burst = partsEffect_burst.GetComponent<ParticleSystemRenderer>();
        partsEffectRenderer_ray = partsEffect_ray.GetComponent<ParticleSystemRenderer>();
    }


    // public functions ---------------------------------------------------
    public void RotateParticle (float angle)
	{
        ParticleSystem[] systems = GetComponentsInChildren<ParticleSystem>();

        foreach (ParticleSystem system in systems)
        {
            system.startRotation += angle * Mathf.Deg2Rad;
        }
	}


    public void Play(Vector3 position, AttackCommand.HitResult hitResult)
    {
        switch (hitResult)
        {
            case AttackCommand.HitResult.COUNTER:
            case AttackCommand.HitResult.CRITICAL:
                partsEffect_star.gameObject.SetActive(true);
                partsEffect_ray.gameObject.SetActive(true);
                partsEffect_glow.gameObject.SetActive(true);
                partsEffectRenderer_burst.material = materials_burst[2];
                partsEffectRenderer_ray.material = materials_ray[2];
                break;

            case AttackCommand.HitResult.HIT:
                partsEffect_star.gameObject.SetActive(false);
                partsEffect_ray.gameObject.SetActive(true);
                partsEffect_glow.gameObject.SetActive(true);
                partsEffectRenderer_burst.material = materials_burst[1];
                partsEffectRenderer_ray.material = materials_ray[1];
                break;

            case AttackCommand.HitResult.GRAZE:
                partsEffect_star.gameObject.SetActive(false);
                partsEffect_ray.gameObject.SetActive(false);
                partsEffect_glow.gameObject.SetActive(false);
                partsEffectRenderer_burst.material = materials_burst[0];
                partsEffectRenderer_ray.material = materials_ray[0];
                break;

            case AttackCommand.HitResult.MISS:
                return; /// MISS 일때는 공격 이펙트 연출하지 않는다. 바로 함수 종료
        }
        
        transform.position = position;

        EffectCamera.instance.FollowMainCamera(4f);


        ParticleSystem[] systems = GetComponentsInChildren<ParticleSystem>();

        foreach (ParticleSystem system in systems)
        {
            system.Play();
        }
    }


    public void Stop()
    {
        ParticleSystem[] systems = GetComponentsInChildren<ParticleSystem>();

        foreach (ParticleSystem system in systems)
        {
            system.Stop();
        }
    }
}
