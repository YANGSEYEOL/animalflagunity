﻿using UnityEngine;
using System.Collections;


public class Background : MonoBehaviour 
{
    bool isPress = false;
    Vector3 firstPressHitPos = Vector3.zero;

    [System.NonSerialized]
    float confinedCamera_minX = -70f;
    [System.NonSerialized]
    float confinedCamera_maxX = 550f;
    [System.NonSerialized]
    float confinedCamera_minZ = -770f;
    [System.NonSerialized]
    float confinedCamera_maxZ = -140f;


	void Start()
	{
		confinedCamera_minX = TileChunk.instance.confinedCamera_minX;
		confinedCamera_maxX = TileChunk.instance.confinedCamera_maxX;
		confinedCamera_minZ = TileChunk.instance.confinedCamera_minZ;
		confinedCamera_maxZ = TileChunk.instance.confinedCamera_maxZ;
	}


	///  타일청크에 처음 눌린 위치와 드래그하는 위치의 방향을 기반으로 카메라 패닝을 구현 
    ///  기존 방식은 카메라의 Y축이 45도 방향으로 회전되어있을 때, 2D 처럼 상하좌우로만 패닝되기 때문에 이 방식으로 수정함 
    void OnPress(Vector3 hitPos)
    {
        isPress = true;

        firstPressHitPos = hitPos;

        /// 카메라 무빙 중이라면 무빙 캔슬시키기
        /// 유저에 의지대로 화면을 조종하지 못하면 답답함을 느끼더라
        iTween.Stop(Camera.main.gameObject);
    }


    void OnDrag(Vector3 hitPos)
    {
        if (isPress)
        {
            Vector3 diff = firstPressHitPos - hitPos;

            Camera.main.transform.Translate(diff, Space.World);
        }
    }


    void OnRelease()
    {
        isPress = false;

        /// 화면 가두기 범위를 벗어나는지 체크해서 화면 가둠 코루틴 실행 
        Vector3 position = Camera.main.transform.position;
        if (!(confinedCamera_minX <= position.x && position.x <= confinedCamera_maxX && confinedCamera_minZ <= position.z && position.z <= confinedCamera_maxZ))
        {
            StartCoroutine(ConfinedCamera());
        }
    }

    IEnumerator ConfinedCamera()
    {
        Vector3 confinedPosition = Camera.main.transform.position;

        bool isConfinedCamera = false;
        while (!isPress && !isConfinedCamera)
        {
            float confiningSpeed = 10f * Time.deltaTime;

            isConfinedCamera = true;
            
            Vector3 cameraPosition = Camera.main.transform.position;
            if (cameraPosition.x > confinedCamera_maxX)
            {
                confinedPosition.x = confinedCamera_maxX;

                isConfinedCamera = false;
            }
            else if (cameraPosition.x < confinedCamera_minX)
            {
                confinedPosition.x = confinedCamera_minX;

                isConfinedCamera = false;
            }

            if (cameraPosition.z > confinedCamera_maxZ)
            {
                confinedPosition.z = confinedCamera_maxZ;

                isConfinedCamera = false;
            }
            else if (cameraPosition.z < confinedCamera_minZ)
            {
                confinedPosition.z = confinedCamera_minZ;

                isConfinedCamera = false;
            }

            Camera.main.transform.position = Vector3.Lerp(cameraPosition, confinedPosition, confiningSpeed);

            yield return null;
        }       
    }
}
