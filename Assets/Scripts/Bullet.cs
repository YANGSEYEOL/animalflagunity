﻿using UnityEngine;
using System.Collections;
using Vectrosity;
using System.Collections.Generic;

public delegate void OnFinishedShootDelegate(Bullet bullet);

public class Bullet : CacheMonobehavior
{
    VectorLine bulletLine;

    [System.NonSerialized]
    public bool isUsing = false;


    public OnFinishedShootDelegate OnFinishedShoot = delegate {};


    void Start ()
    {
        /// 총알 만들기
        VectorManager.useDraw3D = true;

        List<Vector3> points = new List<Vector3>();
        points.Add(new Vector3(0, 0, -5));
        points.Add(new Vector3(0, 0, 5));

        bulletLine = new VectorLine("bullet", points, TileChunk.instance.lineMaterial, 20.0f);
        bulletLine.UseIndependentCanvas3D();
        bulletLine.color = Color.yellow;

        VectorManager.ObjectSetup(gameObject, bulletLine, Visibility.Dynamic, Brightness.None, false);
    }


    public IEnumerator Shoot(Vector3 startPos, Quaternion startRot, Vector3 destination)
    {
        isUsing = true;

        transform.position = startPos;
        transform.rotation = startRot;

        /// 방향벡터 
        Vector3 look = destination - transform.position;
        look.Normalize();

        /// 이동해야 할 거리
        float distance = Vector3.Distance(destination, transform.position);

        /// 1 초당 속도기준으로 목표 지점에 도착하기까지의 시간 계산
        float estimatedTime = distance / GameManager.instance.bulletSpeed;

        /// 도착 예상되는 시간까지만 while문을 돌린다 
        /// 이런 정확한 처리를 안하고 transform.poistion += look * speed * deltaTime을 하게 되면, 만약 더한 값이 도착 지점을 넘어서게 됐을 때 distance를 처리하기가 어려워진다 
        /// 그리고 Vector3.Lerp를 이용하는 방식은 일정한 속도를 유지시키는 Move 처리가 아니다. t값은 항상 고정이기 때문에 도착 지점에 가까워질수록 움직여지는 양이 줄어든다 
        /// 일정한 속도를 유지하면서 정확한 지점에 도착하는 처리를 하기 위해서는 이렇게 해줘야 한다 
        float t = 0;
        while (t <= estimatedTime)
        {
            /// deltaTime은 1초를 기준으로 하나의 프레임 연산처리가 얼마나 걸리는 지를 의미한다 (초 단위)
            /// 1초에 60프레임이라면 deltaTime은 0.016이 나온다 0.016 * 60 = 0.96 (sec)
            /// 1초에 30프레임이라면 deltaTime은 0.03이 나온다 0.03 * 30 = 0.9 (sec)
            /// 즉, 환경이 구리든 좋든 while문이 끝나는 시간은 동일하다 
            t += Time.deltaTime;

            /// 1 프레임당 속도
            float speedByFrame = GameManager.instance.bulletSpeed * Time.deltaTime;

            /// 이동
            transform.position += look * speedByFrame;

            yield return null;
        }

        isUsing = false;
        transform.position = new Vector3(9999, 9999, 9999);

        OnFinishedShoot(this);
    }
}
