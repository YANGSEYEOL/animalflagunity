﻿using UnityEngine;
using System.Collections;

public class AggressiveOverwatchCommand : Command
{
    public static AggressiveOverwatchCommand instance;

    protected Animal _animal;
    protected Animal animal
    {
        get { return _animal; }
        set
        {
            if (_animal != null)
            {
                _animal.OnFinishedMove -= OnFinishedMove;
                _animal.OnFinishedAttack -= OnFinishedAttack;
                _animal.OnChangedCanAction -= OnChangedCanAction;
            }

            _animal = value;

            if (value != null)
            {
                value.OnFinishedMove += OnFinishedMove;
                value.OnFinishedAttack += OnFinishedAttack;
                value.OnChangedCanAction += OnChangedCanAction;
            }
        }
    }

    [SerializeField]
    public int skillCoolTime = 1;

    bool canDoSkill { get { return animal != null && animal.animalData.skills.Contains("AggressiveOverwatch") && animal.canAction; } }


    protected virtual void Awake()
    {
        instance = this;
    }


    private void Start()
    {
        GameManager.instance.OnSelectedAnimal += OnSelectedAnimal;
        GameManager.instance.OnChangedPlayerTurn += OnChangedPlayerTurn;

        state = CommandState.NONE;

        /// 쿨타임은 지정된 턴 수보다 +1해서 넣어줘야 한다. 
        /// 즉시 발동되는 스킬들은 쿨타임 반영이 곧바로 되서, 그 스킬을 썼던 턴을 끝낼 때도 -1 된다
        /// 쿨타임은 스킬을 사용하고 그 다음턴부터 적용되어야 한다 
        skillCoolTime++;
    }


    private void OnChangedPlayerTurn(string turnUserId)
    {
        animal = null;
    }


    public void OnChangedCanAction(bool canAction)
    {
        SwitchState();
    }


    private void OnSelectedAnimal(Animal selectedAnimal)
    {
        animal = selectedAnimal;

        SwitchState();
    }


    protected virtual void SwitchState()
    {
        if (canDoSkill)
        {
            state = animal.curRemainSkillCoolTime <= 0 /*&& animal.ownedTile.type == TileMaker.TileType.RIVER*/ ? CommandState.ACTIVE : CommandState.DISABLE;   
        }
        else
        {
            state = CommandState.NONE;
        }
    }


    public override void OnClickButton()
    {
        UIManager.instance.OpenConfirmUI(this);

        /// state 변경을 OpenConfirmUI보다 늦게 해야 UIManager에 MoveCommand가 Cancel이 먼저 호출되고 경로 표시가 표시된다
        state = CommandState.READY;
    }


    private void OnFinishedMove(Animal movedAnimal)
    {
        SwitchState();
    }


    private void OnFinishedAttack(Animal animal)
    {
        SwitchState();
    }


    protected override void OnStateChanged(CommandState changedState)
    {
        base.OnStateChanged(changedState);
        
        switch (changedState)
        {
            case CommandState.NONE:
                {
					/// MoveCommand와 같은 라인을 쓰기 때문에 일부러 끄지 않는다 
                    //TileChunk.instance.UndisplayAllPathTiles();
                }
                break;
            case CommandState.DISABLE:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(animal.curRemainSkillCoolTime > 0);
                    commandButton.cooltimeLabel.text = animal.curRemainSkillCoolTime.ToString();
                }
                break;
            case CommandState.READY:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);

                    TileChunk.instance.DisplayAllPathTilesOf(animal, animal.animalData.mobility, Color.cyan, 0.9f);
                }
                break;
            case CommandState.ACTIVE:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
        }
    }


    public override void Ready()
    {
    }


    public override void Do()
    {
        state = CommandState.NONE;

        GameManager.instance.OnAnimalDidAction(animal);
        animal.AggressiveOverwatch();
    }


    public override void Cancel()
    {
        SwitchState();
    }
}
