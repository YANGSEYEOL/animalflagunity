﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using I2.Loc;

public class AmbushCommand : Command
{
    readonly int DAMAGE = 5;
    readonly int RANGE = 2;

    static public AmbushCommand instance;

    [SerializeField]
    ParticleSystem partsEffect_dust;

    List<int> x;
    List<int> y;

    Animal _ownAnimal;
    Animal ownAnimal
    {
        get { return _ownAnimal; }
        set
        {
            if(_ownAnimal != null)
            {
                _ownAnimal.OnFinishedMove -= OnFinishedMove;
                _ownAnimal.OnChangedCanAction -= OnChangedCanAction;
            }

            _ownAnimal = value;

            if (value != null)
            {
                _ownAnimal.OnFinishedMove += OnFinishedMove;
                _ownAnimal.OnChangedCanAction += OnChangedCanAction;
            }
        }
    }

    Animal _targetAnimal;
    Animal targetAnimal
    {
        get { return _targetAnimal; }
        set
        {
            _targetAnimal = value;
            if (value == null)
                UIManager.instance.ResetTargetUI();
            else
                UIManager.instance.SetTargetUi(value);
        }
    }

    int targetIndex = 0;
    List<Animal> validTargetAnimals;
    List<Tile> validTargetTiles;
    Tile tileToJump;

    bool canAmbush { get { return ownAnimal != null && ownAnimal.animalData.skills.Contains("Ambush") && ownAnimal.canAction; } }

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        GameManager.instance.OnSelectedAnimal += OnSelectedAnimal;
        Init();
    }

    public void Init()
    {
        validTargetAnimals = new List<Animal>();
        validTargetTiles = new List<Tile>();
    }
    
    // 내가 선택할 수 있는 타일을 모두 구해서 Vectorline으로 보여준다.
    void GetAvailableTargets(Animal ownAnimal)
    {
        if (ownAnimal == null)
            return;
        validTargetAnimals.Clear();
        validTargetTiles.Clear();

        Tile ownAnimalTile = ownAnimal.ownedTile;
        int tileX = ownAnimalTile.TileIndex_X;
        int tileY = ownAnimalTile.TileIndex_Y;
        TileChunk.instance.lineCyan.Resize(0);
        for (int i = -RANGE; i <= RANGE; i++)
        {
            for (int j = -RANGE; j <= RANGE; j++)
            {
                if (TileChunk.instance.IsValidTileIndex(tileX + i, tileY + j) && 
                    TileChunk.instance.tiles[tileX + i, tileY + j].owningAnimal != null &&
                    TileChunk.instance.tiles[tileX + i, tileY + j].isVisible &&
                    TileChunk.instance.tiles[tileX + i, tileY + j].owningAnimal.photonView.isMine == false )
                {
                    if (GetEmptyNeighborTile(TileChunk.instance.tiles[tileX + i, tileY + j]) != null)
                    {
                        validTargetAnimals.Add(TileChunk.instance.tiles[tileX + i, tileY + j].owningAnimal);
                        validTargetTiles.Add(GetEmptyNeighborTile(TileChunk.instance.tiles[tileX + i, tileY + j]));
                    }
                }  
            }
        }
    }

    // 내가 선택할 수 있는 타일을 모두 구해서 Vectorline으로 보여준다.
    void GetAvailableTargetsForAi(Animal ownAnimal)
    {
        if (ownAnimal == null)
            return;
        validTargetAnimals.Clear();
        validTargetTiles.Clear();

        Tile ownAnimalTile = ownAnimal.ownedTile;
        int tileX = ownAnimalTile.TileIndex_X;
        int tileY = ownAnimalTile.TileIndex_Y;
        TileChunk.instance.lineCyan.Resize(0);
        for (int i = -RANGE; i <= RANGE; i++)
        {
            for (int j = -RANGE; j <= RANGE; j++)
            {
                if (TileChunk.instance.IsValidTileIndex(tileX + i, tileY + j) &&
                    TileChunk.instance.tiles[tileX + i, tileY + j].owningAnimal != null &&
                    TileChunk.instance.tiles[tileX + i, tileY + j].isVisible &&
                    GameManager.instance.animals1ForSimulation.Contains(TileChunk.instance.tiles[tileX + i, tileY + j].owningAnimal))
                {
                    if (GetEmptyNeighborTile(TileChunk.instance.tiles[tileX + i, tileY + j]) != null)
                    {
                        validTargetAnimals.Add(TileChunk.instance.tiles[tileX + i, tileY + j].owningAnimal);
                        validTargetTiles.Add(GetEmptyNeighborTile(TileChunk.instance.tiles[tileX + i, tileY + j]));
                    }
                }
            }
        }
    }

    Tile GetEmptyNeighborTile(Tile tile)
    {
        int tileX = tile.TileIndex_X;
        int tileY = tile.TileIndex_Y;

        int[] x = { 1, 0, 0, -1 };
        int[] y = { 0, 1, -1, 0 };
        for (int i = 0; i < 4; i++)
        {
            if(TileChunk.instance.IsValidTileIndex(tileX + x[i], tileY + y[i]))
            if(TileChunk.instance.tiles[tileX + x[i], tileY + y[i]].owningAnimal == null &&
                TileChunk.instance.tiles[tileX + x[i], tileY + y[i]].isVisible &&
                TileChunk.instance.tiles[tileX + x[i], tileY + y[i]].isRock == false)
            {
                return TileChunk.instance.tiles[tileX + x[i], tileY + y[i]];
            }
        }
        return null;
    }

    void OnFinishedMove(Animal animal)
    {
        SwitchState();
    }

    void OnChangedCanAction(bool canAction)
    {
        SwitchState();
    }

    // 이 동물이 선택되면 스킬버튼을 띄워야 한다.
    void OnSelectedAnimal(Animal animal)
    {
        ownAnimal = animal;

        SwitchState();
    }

    void ShowTargetsVectorline()
    {
        TileChunk.instance.lineCyan.Resize(0);
        TileChunk.instance.lineMagenta.Resize(0);
        foreach (var target in validTargetAnimals)
        {
            TileChunk.instance.AddVerticesIntoLinePointsByScale(target.ownedTile, 1f, TileChunk.instance.lineCyan);
        }
        TileChunk.instance.AddVerticesIntoLinePointsByScale(tileToJump, 1f, TileChunk.instance.lineMagenta);

        TileChunk.instance.lineCyan.Draw3D();
        TileChunk.instance.lineMagenta.Draw3D();
    }

    void HideTargetsVectoreline()
    {
        TileChunk.instance.lineCyan.Resize(0);
        TileChunk.instance.lineMagenta.Resize(0);
        TileChunk.instance.lineCyan.Draw3D();
        TileChunk.instance.lineMagenta.Draw3D();
    }

    public override void OnClickButton()
    {
        GetAvailableTargets(ownAnimal);

        targetIndex++;  // 타겟 리스트에서 자동으로 타겟설정
        if (validTargetAnimals.Count <= targetIndex)
        {
            targetIndex = 0;
        }
        targetAnimal = validTargetAnimals[targetIndex];
        tileToJump = validTargetTiles[targetIndex];

        ShowTargetsVectorline();

        if (validTargetAnimals.Count > 1)
        {
            state = CommandState.ACTIVATED;
        }
        else
        {
            state = CommandState.READY;
        }
        UIManager.instance.OpenConfirmUI(this);
    }

    public override void Cancel()
    {
        SwitchState();
        HideTargetsVectoreline();

        ownAnimal = null;
        targetAnimal = null;
    }

    public override void Do()
    {
        ownAnimal.canAction = false;

        GameManager.instance.OnAnimalDidAction(ownAnimal);
        StartCoroutine(Ambush(ownAnimal, targetAnimal, tileToJump));

        state = CommandState.NONE;
    }

    public override void Ready()
    {

    }

    protected virtual void SwitchState()
    {
        if (canAmbush)
        {
            GetAvailableTargets(ownAnimal);

            state = ownAnimal.curRemainSkillCoolTime <= 0 && ownAnimal.ownedTile.type == TileMaker.TileType.FOREST && validTargetAnimals.Count > 0 
                ? CommandState.ACTIVE : CommandState.DISABLE;
        }
        else
        {
            state = CommandState.NONE;
        }
    }

    protected override void OnStateChanged(CommandState changedState)
    {
        base.OnStateChanged(changedState);

        switch (changedState)
        {
            case CommandState.NONE:
                {
                }
                break;
            case CommandState.DISABLE:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(ownAnimal.curRemainSkillCoolTime > 0);
                    commandButton.cooltimeLabel.text = ownAnimal.curRemainSkillCoolTime.ToString();
                }
                break;
            case CommandState.READY:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVATED:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVE:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
        }
    }


    public IEnumerator Ambush(Animal ownAnimal, Animal targetAnimal, Tile tileToJump)
    {
        UIManager.instance.ShowHUDText(I2.Loc.ScriptLocalization.Get(GetType().Name + "_name"), Color.yellow, ownAnimal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);
        yield return new WaitForSeconds(0.5f);

        AudioClip sound = ownAnimal.clickSound[Random.Range(0, ownAnimal.clickSound.Length)];
        ownAnimal.audioSource.PlayOneShot(sound, 0.5f);

        AnimationClip ambushAnimationClip = ownAnimal.GetAnimationClip("ambush");

        ownAnimal.animator.SetTrigger("ambushTrigger");
        var lookVector = targetAnimal.ownedTile.position - ownAnimal.ownedTile.position;
        var lookRotation = Quaternion.LookRotation(lookVector);
        ownAnimal.transform.rotation = lookRotation;
        StartCoroutine(ownAnimal.MoveTo(targetAnimal.transform.position, "ambush"));
        yield return new WaitForSeconds(ambushAnimationClip.length);

        AudioClip atkSound = GameManager.instance.attackSound_critical[Random.Range(0, GameManager.instance.attackSound_critical.Length)];
        ownAnimal.audioSource.PlayOneShot(atkSound, 0.4f);
        AttackEffect.instance.Play(ownAnimal.transform.position, AttackCommand.HitResult.HIT);
        UIManager.instance.ShowHUDText("GRAZE", Color.white, targetAnimal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);
        UIManager.instance.ShowHUDText(DAMAGE.ToString(), Color.grey, targetAnimal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);

        targetAnimal.curHealth -= DAMAGE;
        StartCoroutine(targetAnimal.Damaged(DAMAGE, AttackCommand.HitResult.GRAZE));

        ownAnimal.ownedTile = tileToJump;
        lookVector = targetAnimal.ownedTile.position - tileToJump.position;

        lookRotation = Quaternion.LookRotation(lookVector);
        ownAnimal.transform.rotation = lookRotation;

        if (tileToJump.type == TileMaker.TileType.RIVER)
        {
            ownAnimal.transform.Rotate(new Vector3(ownAnimal.xAngle_fallingIntoWater, 0, 0));
            iTween.RotateTo(gameObject, iTween.Hash("x", ownAnimal.xAngle_fallingIntoWater, "easeType", iTween.EaseType.linear, "time", 1f));
            var comebackCoroutine = ownAnimal.MoveToRapidly(tileToJump.position_AdjustedTileYFactor + Vector3.up * ownAnimal.yFactor_fallingIntoWater);
            while (comebackCoroutine.MoveNext()) yield return comebackCoroutine.Current;

        }
        else
        {
            var comebackCoroutine = ownAnimal.MoveToRapidly(tileToJump.position_AdjustedTileYFactor);
            while (comebackCoroutine.MoveNext()) yield return comebackCoroutine.Current;
        }

        validTargetAnimals.Clear();
    }

    // animal위치에서 ambush할수 있는 동물의 리스트를 반환
    public void GetEnemiesWithinRange(Animal animal, ref List<Animal> animalsInRange, ref List<Tile> tilesToJump)
    {
        GetAvailableTargetsForAi(animal);
        animalsInRange = validTargetAnimals;
        tilesToJump = validTargetTiles;
    }
}
