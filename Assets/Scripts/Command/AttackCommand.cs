﻿using UnityEngine;
using System.Collections.Generic;

public class AttackCommand : Command
{
    public static AttackCommand instance;

    protected Animal _animal;
    protected Animal animal
    {
        get { return _animal; }
        set
        {
            if (_animal != null)
            {
                _animal.OnFinishedAttack -= OnFinishedAttack;
                _animal.OnFinishedMove -= OnFinishedMove;
                _animal.OnChangedCanAction -= OnChangedCanAction;
            }

            _animal = value;

            if (value != null)
            {
                value.OnFinishedAttack += OnFinishedAttack;
                value.OnFinishedMove += OnFinishedMove;
                value.OnChangedCanAction += OnChangedCanAction;
            }
        }
    }

    public enum HitResult { HIT, CRITICAL, GRAZE, MISS, COUNTER }


    protected const string LINE_NAME = "TARGET_";

    public struct AttackTargetInfo
    {
        public Animal animal;
        public int[] hitResultTable;
        public int calculatedHitChance;

        public AttackTargetInfo(Animal animal, int[] hitResultTable)
        {
            this.animal = animal;
            this.hitResultTable = hitResultTable;
            this.calculatedHitChance = hitResultTable[(int)HitResult.HIT] + hitResultTable[(int)HitResult.CRITICAL] + hitResultTable[(int)HitResult.GRAZE];
        }
    }

    protected AttackTargetInfo _targetAnimalInfo;
    protected AttackTargetInfo targetAnimalInfo
    {
        get { return _targetAnimalInfo; }
        set 
        {  
            if (_targetAnimalInfo.animal != null)
            {
                UIManager.instance.ResetTargetUI();
                //_targetAnimalInfo.animal.SetLineCircleVisible(false);
                _targetAnimalInfo.animal.SetHPBarVisible(false);
            }

            _targetAnimalInfo = value;

            if (value.animal != null)
            {
                UIManager.instance.SetTargetUi(value.animal);
                //value.animal.SetLineCircleVisible(true);
                value.animal.SetHPBarVisible(true);
            }
        }
    }
    protected List<AttackTargetInfo> targetAnimalsInfoList = new List<AttackTargetInfo>();
    protected int currentTargetIndex = 0;



    private void Awake()
    {
        instance = this;
    }


    private void Start()
    {
        GameManager.instance.OnSelectedAnimal += OnSelectedAnimal;
        GameManager.instance.OnSetTargetAnimal += OnSetTargetAnimal;
        GameManager.instance.OnChangedPlayerTurn += OnChangedPlayerTurn;

        state = CommandState.NONE;
    }


    private void OnChangedPlayerTurn(string turnUserId)
    {
        animal = null;
    }


    protected virtual void OnSelectedAnimal(Animal selectedAnimal)
    {
        animal = selectedAnimal;

        /// 이전에 선택된 동물에 커맨드 초기화
        state = CommandState.NONE;

        if (animal != null && animal.canAction)
        {
            currentTargetIndex = 0;

            /// 공격할 대상이 있는지 서칭 
            GetAllAttackableTargetInfoList(animal, ref targetAnimalsInfoList);

            state = targetAnimalsInfoList.Count > 0 ? CommandState.ACTIVE : CommandState.DISABLE;
        }
        else
        {
            state = CommandState.NONE;
        }
    }


    private void OnSetTargetAnimal(Animal setTargetAnimal)
    {
        if (setTargetAnimal == null) return;

        if (state == CommandState.READY || state == CommandState.ACTIVE)
        {
            for (int i = 0; i < targetAnimalsInfoList.Count; ++i)
            {
                AttackTargetInfo targetInfo = targetAnimalsInfoList[i];
                if (targetInfo.animal == setTargetAnimal)
                {
                    currentTargetIndex = i;

                    /// 타겟 변경
                    SwitchTarget(targetInfo);

                    if (state == CommandState.ACTIVE)
                    {
                        state = CommandState.READY;

                        UIManager.instance.OpenConfirmUI(this);

                        RefershConfirmUIDesc();
                    }

                    /// 함수 바로 종료
                    return;
                }
            }

            /// 공격 가능 리스트에 없는 녀석을 클릭한 것이라면 공격 커맨드를 취소시킨다 
            UIManager.instance.CloseConfirmUI();
        }
    }


    protected void SwitchTarget(AttackTargetInfo targetInfo)
    {
        targetAnimalInfo = targetInfo;
 
        /*Color lineColor = Color.cyan;
        foreach (var info in targetAnimalsInfoList)
        {
            lineColor = info.animal == targetAnimalInfo.animal ? Color.yellow : Color.cyan;
            TileChunk.instance.DisplayTileHighlight(info.animal.ownedTile, LINE_NAME + info.animal.photonView.viewID, lineColor);
        }*/

        /// UI Refresh
        RefershConfirmUIDesc();
    }


    private void OnFinishedMove(Animal movedAnimal)
    {
        /// 이동이 완료됐을 때, 주위에 공격 가능한 적이 있는지 확인한다 
        OnSelectedAnimal(movedAnimal);
    }


    public override void OnClickButton()
    {
        if (state == CommandState.READY)
        {
            /// 타겟 다른 걸로 바꿔주기 
            ++currentTargetIndex;
            if (currentTargetIndex >= targetAnimalsInfoList.Count)
            {
                currentTargetIndex = 0;
            }

            SwitchTarget(targetAnimalsInfoList[currentTargetIndex]);
        }
        else
        {
            targetAnimalInfo = targetAnimalsInfoList[0];

            /// 가장 명중률이 높게 나오는 타겟을 선타겟으로 지정한다
            for (int i = 0; i < targetAnimalsInfoList.Count; ++i)
            {
                AttackTargetInfo targetInfo = targetAnimalsInfoList[i];

                if (targetInfo.calculatedHitChance > targetAnimalInfo.calculatedHitChance)
                {
                    targetAnimalInfo = targetInfo;

                    currentTargetIndex = i;

                    break;
                }
            }

            state = CommandState.READY;

            UIManager.instance.OpenConfirmUI(this);

            RefershConfirmUIDesc();
        }
    }


    void RefershConfirmUIDesc()
    {
        /// 최대 데미지, 명중률, 치명타율 UI 표시 
        int maxDamage = animal.animalData.damage + animal.animalData.damageSpread;
        maxDamage += (int)((float)maxDamage * 0.5f);    /// 치명타가 반영된 최대 데미지

        UIManager.instance.confirmUI_desc.text 
            = string.Format(I2.Loc.ScriptLocalization.Get("AttackCommand_desc"), maxDamage, targetAnimalInfo.calculatedHitChance, targetAnimalInfo.hitResultTable[(int)HitResult.CRITICAL]);
    }


    public void OnChangedCanAction(bool canAction)
    {
        state = animal != null && animal.canAction && targetAnimalsInfoList.Count > 0 ? CommandState.ACTIVE : CommandState.NONE;
    }


    private void OnFinishedAttack(Animal animal)
    {
    }


    protected override void OnStateChanged(CommandState changedState)
    {
        base.OnStateChanged(changedState);

        switch (changedState)
        {
            case CommandState.NONE:
                {
                    foreach (var targetInfo in targetAnimalsInfoList)
                    {
                        TileChunk.instance.UndisplayLine(LINE_NAME + targetInfo.animal.photonView.viewID);
                    }

                    if (targetAnimalInfo.animal != null)
                    {
                        UIManager.instance.ResetTargetUI();
                        //targetAnimalInfo.animal.SetLineCircleVisible(false);
                        targetAnimalInfo.animal.SetHPBarVisible(animal != null && animal.isAttacking);	/// 공격 중이라면 끄지 않게 한다 
                    }
                }
                break;
            case CommandState.DISABLE:
                {
                    foreach (var targetInfo in targetAnimalsInfoList)
                    {
                        TileChunk.instance.UndisplayLine(LINE_NAME + targetInfo.animal.photonView.viewID);
                    }

                    if (targetAnimalInfo.animal != null)
                    {
                        UIManager.instance.ResetTargetUI();
                        //targetAnimalInfo.animal.SetLineCircleVisible(false);
                        targetAnimalInfo.animal.SetHPBarVisible(false);
                    }
                }
                break;
            case CommandState.ACTIVE:
                {
                    foreach (var targetInfo in targetAnimalsInfoList)
                    {
                        TileChunk.instance.UndisplayLine(LINE_NAME + targetInfo.animal.photonView.viewID);
                    }

                    if (targetAnimalInfo.animal != null)
                    {
                        UIManager.instance.ResetTargetUI();
                        //targetAnimalInfo.animal.SetLineCircleVisible(false);
                        targetAnimalInfo.animal.SetHPBarVisible(false);
                    }
                }
                break;
            case CommandState.READY:
                {
                    /*Color lineColor = Color.cyan;
                    foreach (var targetInfo in targetAnimalsInfoList)
                    {
                        lineColor = targetInfo.animal == targetAnimalInfo.animal ? Color.yellow : Color.cyan;
                        TileChunk.instance.DisplayTileHighlight(targetInfo.animal.ownedTile, LINE_NAME + targetInfo.animal.photonView.viewID, lineColor);
                    }*/

                    if (targetAnimalInfo.animal != null)
                    {
                        UIManager.instance.SetTargetUi(targetAnimalInfo.animal);
                        //targetAnimalInfo.animal.SetLineCircleVisible(true);
                        targetAnimalInfo.animal.SetHPBarVisible(true);
                    }
                }
                break;
        }
    }


    public override void Ready()
    {
    }


    public override void Do()
    {
        /// 판정 주사위 굴리기
        HitResult hitResult = RollForHit(targetAnimalInfo.hitResultTable, targetAnimalInfo.animal);

        /// 데미지 판정
        int damage = FinalizeDamage(animal, targetAnimalInfo.animal, hitResult);

        /// 공격 실행
        Debug.LogFormat("hitResult : {0}, damage : {1}", hitResult, damage);

        animal.Attack(targetAnimalInfo.animal, hitResult, damage);
        GameManager.instance.OnAnimalDidAction(animal);
        state = CommandState.NONE;
    }


    public override void Cancel()
    {
        if(animal == null)
        {
            return;
        }
        if (animal.canAction)
        {
            state = targetAnimalsInfoList.Count > 0 ? CommandState.ACTIVE : CommandState.DISABLE;
        }
        else
        {
            state = CommandState.NONE;
        }
    }


    public void GetAllAttackableTargetInfoList(Animal animal, ref List<AttackTargetInfo> attackableTargetList)
    {
        if (animal == null)
        {
            return;
        }

        attackableTargetList.Clear();
        

        /// 원거리 타입
        if (animal is Hunter && ((Hunter)animal).attackType != Hunter.AttackType.AXE)
        {
            /// 이 범위 체크는 정확한 범위 체크가 아니다. 대각선까지 범위1로 보기 때문. 정확한 공격범위가 되려면 마름모 형태의 범위가 돼야 한다 
            /*
            /// 공격 범위 안에 들어오는 녀석인지 체크 
            Vec start = new Vec(animal.ownedTile.TileIndex_X, animal.ownedTile.TileIndex_Y);

            for (var octant = 0; octant < 8; octant++)
            {
                for (var row = 1; row <= animal.animalData.attackRange; row++)
                {
                    Vec rowPos = start + Octant.TransformOctant(row, 0, octant);
                    if (!TileChunk.instance.IsValidTileIndex(rowPos.x, rowPos.y)) break;

                    for (var col = 0; col <= row; col++)
                    {
                        Animal attackableAnimal;
                        var indexFactor = Octant.TransformOctant(row, col, octant);
                        if(animal.CheckTheTileIsOwnedByEnemy(indexFactor.x, indexFactor.y, out attackableAnimal) &&
                            animal.IsTargetInMySight(attackableAnimal))	/// 시야 범위 체크 
                        {
                            attackableTargetList.Add(new AttackTargetInfo(attackableAnimal, GetHitChance(animal, attackableAnimal)));
                        }
                    }
                }
            } */
        }
        /// 근거리 타입
        else if (animal.animalData.skills.Contains("DistantAttack") && animal.ownedTile.type != TileMaker.TileType.RIVER)
        {
            Animal tileOwningAnimal = null;
            int[] x = { 0,   0, 0, 0, -1, -2, 1, 2};
            int[] y = { -1, -2, 1, 2,  0,  0, 0, 0};
            for (int i = 0; i < 8; i++)
            {
                if (animal.CheckTheTileIsOwnedByEnemy(x[i], y[i], out tileOwningAnimal))
                {
                    if(tileOwningAnimal.isVisible)
                        attackableTargetList.Add(new AttackTargetInfo(tileOwningAnimal, GetHitChance(animal, tileOwningAnimal)));
                }
            }
        }
        else
        {
            Animal tileOwningAnimal = null;
            int[] x = { 0, 0, -1, 1 };
            int[] y = { 1, -1, 0, 0 };
            if (CheckAttackableInRiver(animal))
            {
                for (int i = 0; i < 4; i++)
                {                 
                    if (animal.CheckTheTileIsOwnedByEnemy(x[i], y[i], out tileOwningAnimal))
                    {
                        attackableTargetList.Add(new AttackTargetInfo(tileOwningAnimal, GetHitChance(animal, tileOwningAnimal)));
                    }
                }
            }
        }    
    }

    bool CheckAttackableInRiver(Animal animal)
    {
        if (animal.ownedTile.type != TileMaker.TileType.RIVER)
        {
            return true;
        }
        else
        {
            if (animal.animalData.prefabName == "Hippo" || animal.animalData.prefabName == "Crocodile" || animal.animalData.prefabName == "Giraffe")
            {
                return true;

            }
            else
            {
                return false;
            }
        }
    }

    public int FinalizeDamage(Animal animal, Animal targetAnimal, HitResult hitResult)
    {
        int damage = animal.animalData.damage + Random.Range(0, animal.animalData.damageSpread+1);

        switch (hitResult)
        {
            case HitResult.CRITICAL:
                damage += (int)((float)damage * 0.5f);

                /// 엑스컴2 에서는 치명타 데미지 계산을 일반 데미지 공식에서 +50% 데미지를 추가하는 식으로 이뤄진다 
                /// 이 때 만약 1~3 범위의 데미지를 가지는 유닛일 때, 데미지 공식이 1이 떠버리면, +50%해도 1이 뜬다 (올림처리하면 2겠지만)
                /// 이래 되버리면 치명타인데도 불구하고 데미지는 일반 데미지처럼 보이는 현상이 생김
                /// 유저들은 치명타는 항상 최대 데미지를 넘는 데미지가 나올거라 생각한다
                /// +50%을 해도 최대 데미지를 넘지 않으면 최대 데미지에서 +1을 강제시킨다
                int DAMAGE_MAX = animal.animalData.damage + animal.animalData.damageSpread;
                if (damage <= DAMAGE_MAX)
                {
                    damage = DAMAGE_MAX + 1;
                }
                break;

            case HitResult.GRAZE:
                /// 50% 하락
                damage -= (int)((float)damage * 0.5f);
                break;

            case HitResult.COUNTER:
                damage = targetAnimal.animalData.damage + Random.Range(0, targetAnimal.animalData.damageSpread+1);
                break;
        }

        return damage;
    }


    int GetCriticalChanceFactor(Animal animal, Animal targetAnimal)
    {
        int criticalChanceFactor = 0;

        /// 타겟의 시점에서 내 동물이 어떤 방향에 있는지 확인
        Animal.Direction direction = targetAnimal.GetDirectionOfTarget(animal);
        switch (direction)
        {
            case Animal.Direction.BEHIND:
                criticalChanceFactor += 40;
                break;
            case Animal.Direction.SIDE:
                criticalChanceFactor += 20;
                break;
        }

		/// 은신 중인 상태일 때 공격하면 치명타율 상승 
        if (animal.isConcealed || animal.isSubmerged || animal.isClimbedTree)
        {
            criticalChanceFactor += 40;
        }

        return criticalChanceFactor;
    }


    protected virtual int GetHitChanceFactor(Animal animal, Animal targetAnimal)
    {
        int hitChanceFactor = 0;

        /// 포악한 경계 스킬 사용 시 명중률 패널티 
        if(animal.isAggressiveOverwatching)
        {
            hitChanceFactor -= 20;
        }

        return hitChanceFactor;
    }


    int GetTargetDodgeChanceFactor(Animal animal, Animal targetAnimal)
    {
        int dodgeChanceFactor = 0;

        /*/// 타겟이 카운터 스킬 사용 상태일 때
        if (targetAnimal.isReadiedToCounter)
        {
            dodgeChanceFactor += 50;
        }*/

        /// 경계중이라면 회피율 상승 
        if (targetAnimal.isOverwatched)
        {
            dodgeChanceFactor += 10;
        }

        return dodgeChanceFactor;
    }


    public int[] GetHitChance(Animal animal, Animal targetAnimal)
    {
        float hitChance = animal.animalData.hitChance + GetHitChanceFactor(animal, targetAnimal); /// 나의 명중률
        float criticalChance = animal.animalData.criticalChance + GetCriticalChanceFactor(animal, targetAnimal); /// 나의 치명타율
        float targetDodgeChance = targetAnimal.animalData.dodgeChance + GetTargetDodgeChanceFactor(animal, targetAnimal); /// 타겟의 회피율

        Debug.LogFormat("hitChance : {0}, criticalChance : {1}, targetDodgeChance : {2}", hitChance, criticalChance, targetDodgeChance);

        /// 엑스컴 2 롱워에서 수정한 찰과성 수식은 확실히 오리지날보다 더 나은 판정이다.
        /// 그러나, 다른 문제점이 발생되었다.
        /// 빗나감율도 줄이지만, 명중률도 줄이기 때문에 생긴 문제다.
        /// 100% 명중률이 발생되었다 했을 때, 일반적으로 사람들은 100% 확률로 
        /// 일반 데미지 혹은 치명타가 들어갈꺼라 생각하지만, 
        /// 100% 명중률이라도 10%의 찰과상율이 반영되서, 90% 명중률, 10% 찰과상율로 반영된다.
        /// 나는 이점이 마음이 들지 않아서, 찰과상율이 명중률을 전혀 건드리지 않게 하고
        /// 오직 빗나감율만 줄이도록 수정했다.
        float GRAZE_CHANCE_DEFAULT = 10f;     /// 최소  찰과상율

        /// 1단계
        /// 명중률, 찰과상 범위만을 이용해서 초기값을 계산해놓는다
        float criticalRate = 0f;
        float hitRate = hitChance;

        /// 전체 확률에서  hitChance를 뺀 값이 기본 찰과상율보다 높으면 하던데로 기본 찰과상율을 반영한다
        /// 근데 그 값이 기본 찰과상율보다 낮으면 기본 찰과상율을 줄인다
        float grazeRate = 100 - hitChance;
        if (grazeRate > GRAZE_CHANCE_DEFAULT)
        {
            grazeRate = GRAZE_CHANCE_DEFAULT;
        }
        if (hitRate < grazeRate)
        {
            /// 단, 명중률이 찰과상율보다 이상이라는 전제가 있어야 한다
            /// 명중률이 1%인데 찰과상율을 무조건 10%을 주는것도 모순적이다
            grazeRate = hitRate;
        }
        float missRate = 100f - hitRate - grazeRate;

        /// 치명타율은 해당 데미지종류를 "승급"시켜줄 확률이 된다.
        if (criticalChance > 0f)
        {
            float criticalScale = criticalChance * 0.01f;
            criticalRate = hitRate * criticalScale;
            hitRate = hitRate - (hitRate * criticalScale) + (grazeRate * criticalScale);
            grazeRate = grazeRate - (grazeRate * criticalScale) + (missRate * criticalScale);
            missRate = missRate - (missRate * criticalScale);
        }

        /// 회피율은 해당 데미지종류를 "강등"시켜줄 확률로 해석된다
        if (targetDodgeChance > 0f)
        {
            /// 치명타율에서 계산된 확률값들을 기준으로 확률을 강등시킬 것이기 때문에 
            /// 계산 순서를 역순으로 바꾼것에 유의.
            float dodgeScale = targetDodgeChance * 0.01f;
            missRate = missRate + (grazeRate * dodgeScale);
            grazeRate = grazeRate - (grazeRate * dodgeScale) + (hitRate * dodgeScale);
            hitRate = hitRate - (hitRate * dodgeScale) + (criticalRate * dodgeScale);
            criticalRate = criticalRate - (criticalRate * dodgeScale);
        }

        /// '치명타율 계산 -> 찰과상율 계산' 이렇게 순차적으로 곱하는 이유는
        /// 주사위 한번만으로 '치명타율 주사위', '찰과상율 주사위' 두개를 
        /// 던지는 것과 같은 계산이 나오도록 하기 위함이다. (조건부 확률)

        /// chance 값에 따라 rate의 값이 -로 결정될 수도 있다.
        Debug.LogFormat("criticalRate : {0}, hitRate : {1}, grazeRate : {2}, missRate : {3}, total : {4}", criticalRate, hitRate, grazeRate, missRate, criticalRate + hitRate + grazeRate + missRate);

        int[] hitResultTable = new int[4];
        hitResultTable[(int)HitResult.HIT] = (int)hitRate;
        hitResultTable[(int)HitResult.CRITICAL] = (int)criticalRate;
        hitResultTable[(int)HitResult.GRAZE] = (int)grazeRate;
        hitResultTable[(int)HitResult.MISS] = (int)missRate;

        return hitResultTable;
    }


    public HitResult RollForHit(int[] hitResultTable, Animal targetAnimal, bool forceCantCounter = false)
    {
        HitResult hitResult = HitResult.MISS;

        int randRoll = Random.Range(0, 100);

        int current = 0;
        for (int i = 0; i < (int)HitResult.MISS; ++i)     //  If we don't match a result before miss, then it's a miss.
        {
            current += hitResultTable[i];

            if (randRoll < current)
            {
                hitResult = (HitResult)i;

                break;
            }
        }

        
        /// 카운터 여부 체크
        /// MISS 판정이 뜨면, 카운터 판정으로 바뀐다 
        hitResult = hitResult == HitResult.MISS && targetAnimal.isBiting ? HitResult.GRAZE : hitResult;
        hitResult = hitResult == HitResult.MISS && targetAnimal.canCounter && targetAnimal.isOverwatched && !forceCantCounter? HitResult.COUNTER : hitResult;

        return hitResult;
    }
}