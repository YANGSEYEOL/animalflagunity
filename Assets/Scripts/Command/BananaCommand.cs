﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using I2.Loc;

// 사정거리가 엄청 길어진다. 시야에 들어오는 동물을 하나 좇아 물을 수 있다. 녹턴 궁 같이 
public class BananaCommand : Command
{
    readonly int RANGE = 3;
    readonly float BANANA_SPEED = 300;
    readonly float BANANA_ROTATION_SPEED = 20;
    public static BananaCommand instance;

    Animal _ownAnimal;
    Animal ownAnimal
    {
        get { return _ownAnimal; }
        set
        {
            if (_ownAnimal != null)
            {
                _ownAnimal.OnFinishedMove -= OnFinishedMove;
                _ownAnimal.OnChangedCanAction -= OnChangedCanAction;
            }
            _ownAnimal = value;
            if (_ownAnimal != null)
            {
                _ownAnimal.OnFinishedMove += OnFinishedMove;
                _ownAnimal.OnChangedCanAction += OnChangedCanAction;
            }
        }
    }

    Animal _targetAnimal;
    Animal targetAnimal
    {
        get { return _targetAnimal; }
        set
        {
            _targetAnimal = value;
            if (value == null)
                UIManager.instance.ResetTargetUI();
            else
                UIManager.instance.SetTargetUi(value);
        }
    }

    List<Tile> tilesInRange;
    List<Animal> animalsInRange;

    int targetIndex = 0;

    [SerializeField]
    GameObject banana;

    Tile targetTile;

    bool canThrow { get { return ownAnimal != null && ownAnimal.animalData.skills.Contains("Banana") && ownAnimal.canAction; } }

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        GameManager.instance.OnSelectedAnimal += OnSelectedAnimal;
        GameManager.instance.OnSetTargetAnimal += OnSetTargetAnimal;
        //TileChunk.instance.OnClickedTile += OnClickedTile;
        Init();
    }

    public void Init()
    {
        tilesInRange = new List<Tile>();
        animalsInRange = new List<Animal>();
    }
    
    public void GetTilesInRange()
    {
        int indexX = ownAnimal.ownedTile.TileIndex_X;
        int indexY = ownAnimal.ownedTile.TileIndex_Y;

        tilesInRange = TileChunk.instance.GetTilesInRange(indexX, indexY, RANGE);
        animalsInRange.Clear();
        if (ownAnimal.ownedTile.type == TileMaker.TileType.RIVER)
            return;

        for (int i = 0; i < tilesInRange.Count; i++)
        {
            if (tilesInRange[i].isVisible)
            {
                if (tilesInRange[i].owningAnimal != null && (!tilesInRange[i].owningAnimal.isConcealed ||
                                                            !tilesInRange[i].owningAnimal.isSubmerged ||
                                                            !tilesInRange[i].owningAnimal.isClimbedTree))
                {
                    if (GameManager.instance.enemyAnimals.Contains(tilesInRange[i].owningAnimal))
                        animalsInRange.Add(tilesInRange[i].owningAnimal);
                }
            }
        }
    }
    
    protected virtual void SwitchState()
    {
        if (canThrow)
        {
            state = ownAnimal.curRemainSkillCoolTime <= 0  ? CommandState.ACTIVE : CommandState.DISABLE;
        }
        else
        {
            state = CommandState.NONE;
        }
    }

    void OnFinishedMove(Animal animal)
    {
        SwitchState();
    }

    void OnChangedCanAction(bool canAction)
    {
        SwitchState();
    }

    // 이 동물이 선택되면 스킬버튼을 띄워야 한다.
    void OnSelectedAnimal(Animal animal)
    {
        ownAnimal = animal;

        SwitchState();
    }

    void OnSetTargetAnimal(Animal animal)
    {
        if (animal == null)
            return;

        SwitchState();
        targetAnimal = null;
        if (state == CommandState.READY || state == CommandState.ACTIVATED)
        {
            if (animalsInRange.Contains(animal))
            {
                targetAnimal = animal;
            }
        }

        if (targetAnimal != null)
            UIManager.instance.OpenConfirmUI(this);
        else
            UIManager.instance.CloseConfirmUI();
    }

    public override void OnClickButton()
    {
        UIManager.instance.curCommand = this;

        if(state == CommandState.READY)
        {
            targetIndex++;

            if (animalsInRange.Count <= targetIndex)
            {
                targetIndex = 0;
            }

            // 타겟설정
            targetAnimal = animalsInRange[targetIndex];
        }
        else
        {
            GetTilesInRange();
            if (animalsInRange.Count > 0)
            {
                targetAnimal = animalsInRange[0];
                UIManager.instance.OpenConfirmUI(this);

                state = CommandState.READY;
            }

            // 경계만 라인을 그림
            TileChunk.instance.DisplayTilesBoundary(tilesInRange, TileChunk.instance.lineCyan);
        }
    }

    public override void Cancel()
    {
        TileChunk.instance.lineCyan.Resize(0);
        TileChunk.instance.lineCyan.Draw3D();
        SwitchState();
        
        targetAnimal = null;
    }  

    public override void Do()
    {
        ownAnimal.canAction = false;

        TileChunk.instance.lineCyan.Resize(0);
        TileChunk.instance.lineCyan.Draw3D();

        GameManager.instance.OnAnimalDidAction(ownAnimal);
        StartCoroutine(Throw(ownAnimal, targetAnimal));

        state = CommandState.NONE;
        ownAnimal = null;
        targetAnimal = null;
    }

    public override void Ready()
    {

    }

    protected override void OnStateChanged(CommandState changedState)
    {
        base.OnStateChanged(changedState);

        switch (changedState)
        {
            case CommandState.NONE:
                {

                }
                break;
            case CommandState.DISABLE:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(ownAnimal.curRemainSkillCoolTime > 0);
                    commandButton.cooltimeLabel.text = ownAnimal.curRemainSkillCoolTime.ToString();
                }
                break;
            case CommandState.READY:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVATED:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVE:
                {
                    targetAnimal = null;
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
        }
    }

    public IEnumerator Throw(Animal animal, Animal targetAnimal)
    {
        Quaternion prevRotation = animal.transform.rotation;

        /// 타겟 회전 
        var targetLookVector = targetAnimal.ownedTile.position - animal.ownedTile.position;
        var targetLookRotation = Quaternion.LookRotation(targetLookVector);
        animal.transform.rotation = targetLookRotation;

        UIManager.instance.ShowHUDText(I2.Loc.ScriptLocalization.Get(GetType().Name + "_name"), Color.yellow, animal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);
        yield return new WaitForSeconds(0.5f);

        animal.animator.SetTrigger(animal.animalData.attackAnim + "Trigger");
        yield return new WaitForSeconds(animal.GetAnimationClip(animal.animalData.attackAnim).length);

        animal.animator.ResetTrigger(animal.animalData.attackAnim + "Trigger");

        /// 바나나 던지는 소리 
        animal.audioSource.PlayOneShot(GameManager.instance.missSound, 0.3f);

        /// 바나나 던지기
        var bananaCoroutine = Shoot(banana, animal.transform.position + Vector3.up * 20, targetAnimal.ownedTile.position);
        while (bananaCoroutine.MoveNext()) yield return bananaCoroutine.Current;

        /// 타격 사운드 
        AudioClip atkSound = GameManager.instance.attackSound_critical[Random.Range(0, GameManager.instance.attackSound_critical.Length)];
        targetAnimal.audioSource.PlayOneShot(atkSound, 0.4f);

        /// 이펙트
        AttackEffect.instance.Stop();
        AttackEffect.instance.Play(targetAnimal.transform.position, AttackCommand.HitResult.HIT);

        UIManager.instance.ShowHUDText(animal.animalData.damage.ToString(), Color.red, targetAnimal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);
        yield return targetAnimal.Damaged(animal.animalData.damage, AttackCommand.HitResult.HIT);

        animal.transform.rotation = prevRotation;
    }

    public IEnumerator Shoot(GameObject banana, Vector3 startPos, Vector3 destination)
    {
        banana.transform.position = startPos;
        
        /// 방향벡터 
        Vector3 look = destination - banana.transform.position;
        look.Normalize();

        /// 이동해야 할 거리
        float distance = Vector3.Distance(destination, banana.transform.position);

        /// 1 초당 속도기준으로 목표 지점에 도착하기까지의 시간 계산
        float estimatedTime = distance / BANANA_SPEED;
        
        float t = 0;
        while (t <= estimatedTime)
        {
            t += Time.deltaTime;

            float rotationSpeed = BANANA_ROTATION_SPEED * Time.deltaTime;
            float speed = BANANA_SPEED * Time.deltaTime;

            banana.transform.position += speed * look;
            banana.transform.RotateAroundLocal(Vector3.up, rotationSpeed);
            yield return null;
        }        
        banana.transform.position = new Vector3(9999, 9999, 9999);        
    }
}
