﻿using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiteCommand : Command {
    readonly int range = 1;
    readonly int DAMAGE = 2;

    public static BiteCommand instance;

    [SerializeField]
    AudioClip[] biteGrowlingSound;
    Animal _ownAnimal;
    Animal ownAnimal
    {
        get { return _ownAnimal; }
        set
        {
            if (_ownAnimal != null)
            {
                // _ownAnimal.OnMoved -= OnMoved;
                _ownAnimal.OnFinishedMove -= OnFinishedMove;
            }
            _ownAnimal = value;
            if (_ownAnimal != null)
            {
                // _ownAnimal.OnMoved += OnMoved;
                _ownAnimal.OnFinishedMove += OnFinishedMove;
            }
        }
    }

    [SerializeField]
    AudioClip biteSound;

    bool canBite { get { return ownAnimal != null && ownAnimal.animalData.skills.Contains("Bite") && ownAnimal.canAction; } }

    GameObject ownAnimalsParent;
    float[] animationClipLength;
    int[] shortestPath;
    List<Animal> validTargetAnimals;

    Animal _targetAnimal;
    Animal targetAnimal
    {
        get { return _targetAnimal; }
        set
        {
            _targetAnimal = value;
            if (value == null)
                UIManager.instance.ResetTargetUI();
            else
                UIManager.instance.SetTargetUi(value);
        }
    }

    Vectrosity.VectorLine _vectorline;
    Vectrosity.VectorLine vectorline
    {
        get
        {
            if (_vectorline == null)
                _vectorline = TileChunk.instance.lineCyan;
            return _vectorline;
        }
    }

    int targetIndex = 0;

    // 이번에 
    List<Animal> biteAnimalsInThisTurn;

    List<BiteStruct> biteStructList;

    struct BiteStruct {
        public Animal bitingAnimal;
        public Animal bittenAnimal;

        public BiteStruct(Animal _bitingAnimal, Animal _bittenAnimal)
        {
            bitingAnimal = _bitingAnimal;
            bittenAnimal = _bittenAnimal;
        }
    };

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        GameManager.instance.OnSelectedAnimal += OnSelectedAnimal;
        GameManager.instance.OnChangedPlayerTurn += OnChangedPlayerTurn;
        GameManager.instance.OnAnimalLevitated += OnAnimalLevitated;
        GameManager.instance.OnAnimalGored += OnAnimalGored;
        GameManager.instance.OnAnimalBitten += OnAnimalBitten;
        GameManager.instance.OnAnimalDiedDel += OnAnimalDied;
        Init();
    }

    public void Init()
    {
        validTargetAnimals = new List<Animal>();
        biteStructList = new List<BiteStruct>();
        biteAnimalsInThisTurn = new List<Animal>();
        animationClipLength = new float[4];
        coroutineDict = new Dictionary<Animal, Coroutine>();
    }

    int[] x = { 1, -1, 0, 0 };
    int[] y = { 0, 0, 1, -1 };
    bool IsProperTarget(Animal targetAnimal)
    {
        Tile targetAnimalTile = targetAnimal.ownedTile;
        int tileX = targetAnimalTile.TileIndex_X;
        int tileY = targetAnimalTile.TileIndex_Y;

        if (targetAnimalTile.type != TileMaker.TileType.RIVER && targetAnimal.isVisible)
        {
            return true;
        }  
        else
            return false;
    }

    // 내가 선택할 수 있는 타일을 모두 구해서 Vectorline으로 보여준다.
    void GetAvailableTargets(Animal ownAnimal)
    {
        if (ownAnimal == null)
            return;

        int tileX = ownAnimal.ownedTile.TileIndex_X;
        int tileY = ownAnimal.ownedTile.TileIndex_Y;

        validTargetAnimals.Clear();

        if (ownAnimal.ownedTile.type != TileMaker.TileType.RIVER)
        {
            for (int i = 0; i < 4; i++)
            {
                if (!TileChunk.instance.IsValidTileIndex(tileX + x[i], tileY + y[i]))
                {
                    continue;
                }
                Animal targetAnimal = TileChunk.instance.tiles[tileX + x[i], tileY + y[i]].owningAnimal;
                if (targetAnimal != null)
                {
                    if (GameManager.instance.enemyAnimals.Contains(targetAnimal) && IsProperTarget(targetAnimal))
                    {
                        validTargetAnimals.Add(targetAnimal);
                    }
                }
            }
        }
    }

    List<BiteStruct> structToRemove = new List<BiteStruct>();
    List<Animal> bitingAnimalList = new List<Animal>();
    Animal bittenAnimal;


    // 해당 동물이 물고있는 동물들도 취소하고 
    void StopBitingAnimalAndAnimalsBittenBy(Animal animalToStop)
    {
        RemoveBitingAnimalFromBiteStructList(animalToStop, ref bittenAnimal);
        RemoveBittenAnimalFromBiteStructList(animalToStop);
        
        animalToStop.canAction = true;
        animalToStop.isStunned = false;
        if(IsBiting(animalToStop))      StartCoroutine(StopBitingMotion(animalToStop));
        if(IsBeingBitten(animalToStop)) StartCoroutine(StopBittenMotion(animalToStop));
        
        animalToStop.biteDirections[0] = false;
        animalToStop.biteDirections[1] = false;
        animalToStop.biteDirections[2] = false;
        animalToStop.biteDirections[3] = false;

        // 공중에 뜬 동물이 물고있던 동물의 biteDirection을 false로 전환해주고 stopbiting해줄지 확인해보고 실행
        if (bittenAnimal != null)
        {
            ResetBiteDirectionBetween(animalToStop, bittenAnimal);

            // 물린 동물의ㅣ 네 방향이 모두 false라면 이제 물린 애니메이션을 취소해도됨
            if (!bittenAnimal.biteDirections[0] 
                && !bittenAnimal.biteDirections[1]
                && !bittenAnimal.biteDirections[2]
                && !bittenAnimal.biteDirections[3])
            {
                animalToStop.canAction = true;
                animalToStop.isStunned = false;
                StartCoroutine(StopBittenMotion(bittenAnimal));
            }
        }

        // 공중에 뜬 동물을 물고있는 애들도 다 풀어준다
        for (int i = 0; i < bitingAnimalList.Count; i++)
        {
            bitingAnimalList[i].canAction = true;
            bitingAnimalList[i].isStunned = false;
            StartCoroutine(StopBitingMotion(bitingAnimalList[i]));
        }
    }

    void OnAnimalBitten(Animal bittenAnimal)
    {
        // 이 동물이 다른 동물을 깨물고있는 상황에서는 이걸 취소해야한다
        if (IsBiting(bittenAnimal))
        {
            Animal bittenAnimalBittenByBIttenAnimal = null;
            RemoveBitingAnimalFromBiteStructList(bittenAnimal, ref bittenAnimalBittenByBIttenAnimal);

            StartCoroutine(StopBitingMotion(bittenAnimal));

            // 물린 동물의ㅣ 네 방향이 모두 false라면 이제 물린 애니메이션을 취소해도됨
            if (bittenAnimalBittenByBIttenAnimal != null)
            {
                ResetBiteDirectionBetween(bittenAnimal, bittenAnimalBittenByBIttenAnimal);

                if (!bittenAnimalBittenByBIttenAnimal.biteDirections[0]
                    && !bittenAnimalBittenByBIttenAnimal.biteDirections[1]
                    && !bittenAnimalBittenByBIttenAnimal.biteDirections[2]
                    && !bittenAnimalBittenByBIttenAnimal.biteDirections[3])
                {
                    bittenAnimalBittenByBIttenAnimal.canAction = true;
                    bittenAnimalBittenByBIttenAnimal.isStunned = false;
                    StartCoroutine(StopBittenMotion(bittenAnimalBittenByBIttenAnimal));
                }
            }
        } }

    void OnAnimalGored(Animal goredAnimal)
    {
        StopBitingAnimalAndAnimalsBittenBy(goredAnimal);
    }

    void OnAnimalLevitated(Animal levitatedAnimal)
    {
        StopBitingAnimalAndAnimalsBittenBy(levitatedAnimal);
    }

    // 물고있는, 물린동물 둘중에 하나가 죽으면 물린게 풀려야한다.
    void OnAnimalDied(Animal deadAnimal)
    {
        StopBitingAnimalAndAnimalsBittenBy(deadAnimal);
    }

    void ResetBiteDirectionBetween(Animal bitingAnimal, Animal bittenAnimal)
    {
        Animal.Direction direction = bittenAnimal.GetDirectionOfTarget(bitingAnimal);
        Vector3 biteToTargetVector = bittenAnimal.ownedTile.position - bitingAnimal.ownedTile.position;
        if (direction == Animal.Direction.FRONT)
        {
            bittenAnimal.biteDirections[0] = false;
        }
        else if (direction == Animal.Direction.SIDE)
        {
            Vector3 cross = Vector3.Cross(biteToTargetVector, bittenAnimal.transform.forward);
            if (cross.y < 0) // 대상의 왼쪽
            {
                bittenAnimal.biteDirections[1] = false;
            }
            else
            {
                bittenAnimal.biteDirections[2] = false;
            }
        }
        else
        {
            bittenAnimal.biteDirections[3] = false;
        }
    }

    // parent관계때문에 바뀐 rotation을 다시 잡아준다.
    void GetRotationRight(Animal animal)
    {
        if (animal.transform.parent.parent != null)
        {
            Animal bitingAnimal = animal;
            Animal targetAnimal = animal.transform.parent.parent.GetComponent<Animal>();
            /// 회전 
            Vector3 lookVector = targetAnimal.ownedTile.position - bitingAnimal.ownedTile.position;
            var lookRotation = Quaternion.LookRotation(lookVector);
            bitingAnimal.transform.rotation = lookRotation;
        }
        else if (animal.modelTransform.childCount > 0)
        {
            for (int i = 0; i < animal.modelTransform.childCount; i++)
            {
                Animal targetAnimal = animal;
                Animal bitingAnimal = animal.modelTransform.GetChild(i).GetComponent<Animal>();
                // 애니멀이 아닌 자식들도 있으니까
                if (bitingAnimal == null)
                    continue;

                /// 회전 
                Vector3 lookVector = targetAnimal.ownedTile.position - bitingAnimal.ownedTile.position;
                var lookRotation = Quaternion.LookRotation(lookVector);
                bitingAnimal.transform.rotation = lookRotation;
            }
        }
    }

    // 무는 애니메이션을 취소하고
    IEnumerator StopBitingMotion(Animal animal)
    {
        // bitinganimal의 로테이션을 바로 맞춰준다
        GetRotationRight(animal);

        // 동물을 원래 부모에게 돌려준다.
        animal.transform.SetParent(ownAnimalsParent.transform);

        animal.animator.SetBool("biteIdle", false);
        animal.animator.SetInteger("bite", 0);

        yield return animal.MoveToRapidly(animal.GetRealPositionByTile(animal.ownedTile));
        yield break;
    }

    IEnumerator StopBittenMotion(Animal animal)
    {
        // animal을 물고있던 동물들의 로테이션을 정리해주고, 원래 부모에게 다시 돌려준다
        for (int i = 0; i < animal.modelTransform.childCount; i++)
        {
            GetRotationRight(animal);
            animal.modelTransform.GetChild(i).SetParent(ownAnimalsParent.transform);
        }
        animal.animator.SetBool("bitten", false);
        animal.animator.SetInteger("struggle", 0);

        yield return animal.MoveToRapidly(animal.GetRealPositionByTile(animal.ownedTile));

        yield break;
    }

    void OnFinishedMove(Animal animal)
    {
        SwitchState();
    }

    void FindAnimationClip(Animal animal)
    {
        for (int i = 1; i < animationClipLength.Length; i++)
        {
            animationClipLength[i] = animal.GetAnimationClip("bite" + i.ToString()).length;
        }
    }
    
    // 이 동물이 선택되면 스킬버튼을 띄워야 한다.
    void OnSelectedAnimal(Animal animal)
    {
        if (animal == null)
            return;

        ownAnimal = animal;

        SwitchState();
    }

    public bool IsBiting(Animal animal)
    {
        return animal.animator.GetBool("biteIdle");        
    }

    bool IsBeingBitten(Animal animal)
    {
        return animal.animator.GetBool("bitten");
    }
 
    // 데미지는 이미 깎여있고, 그 이후 보여지는 처리
    IEnumerator DamagedDuringBit(Animal biteAnimal, Animal targetAnimal)
    {
        // 카메라이동
        /// AI턴일 때 아예 카메라 이동을 막아놓기 때문에 의미없어서 뺀다 
        /// 그리고 다른 곳에서 canMove false 해놓고 true로 안 바꿔주고 턴을 넘기면 플레이어 동물 자동 선택이 막힌다 
        //InputToEvent.canMove = false; 
        Camera.main.MoveTo(targetAnimal.ownedTile.position);
        
        biteAnimal.audioSource.PlayOneShot(biteGrowlingSound[Random.Range(0, GameManager.instance.attackSound_critical.Length)],0.3f);
        UIManager.instance.ShowHUDText(ScriptLocalization.Get("BLEEDING"), Color.red, targetAnimal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR, false);
        UIManager.instance.ShowHUDText((DAMAGE).ToString(), Color.red, targetAnimal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);

       // StartCoroutine(Shake(targetAnimal));

        yield return new WaitForSeconds(1.0f);
        yield return targetAnimal.Damaged(DAMAGE, AttackCommand.HitResult.HIT);

        yield return null;
    }

    void OnChangedPlayerTurn(string turnUserId)
    {
        StartCoroutine(OnChangedPlayerTurnCoroutine(turnUserId));
    }


    IEnumerator OnChangedPlayerTurnCoroutine(string turnUserId)
    {
        //턴이 시작되었으므로 이 리스트를 클리어
        biteAnimalsInThisTurn.Clear();

        for (int i = 0; i < biteStructList.Count; i++)
        {
            // 현재 차례의 동물이 물린상태라면
            if (biteStructList[i].bittenAnimal.ownerUserId == turnUserId)
            {
                biteStructList[i].bittenAnimal.curHealth -= DAMAGE;
            }
        }
        for (int i = 0; i < biteStructList.Count; i++)
        {
            // 현재 차례의 동물이 물린상태라면
            if (biteStructList[i].bittenAnimal.ownerUserId == turnUserId)
            {
                yield return DamagedDuringBit(biteStructList[i].bitingAnimal, biteStructList[i].bittenAnimal);
            }
        }   
    }

    public override void OnClickButton()
    {
        if (IsBiting(ownAnimal))            // 스킬을시전하는동물이 깨물고있다면.
        {
            UIManager.instance.OpenConfirmUI(this);
        }
        else                                // 스킬을 사용하는 동물이 깨물고있지않다면,
        {
            GetAvailableTargets(ownAnimal);
            ShowTargetsVectorline();

            targetIndex++;
            if (validTargetAnimals.Count <= targetIndex)
            {
                targetIndex = 0;
            }
            if (validTargetAnimals.Count > 0)
            {
                targetAnimal = validTargetAnimals[targetIndex];
                UIManager.instance.OpenConfirmUI(this);
            }
        }
    }

    void HideTargetsVectorline()
    {
        vectorline.Resize(0);
        vectorline.Draw3D();
    }

    void ShowTargetsVectorline()
    {
        vectorline.Resize(0);
        for(int i = 0; i < validTargetAnimals.Count; i++)
        {
            TileChunk.instance.AddVerticesIntoLinePointsByScale(validTargetAnimals[i].ownedTile, 1f, vectorline);
        }
        vectorline.Draw3D();
    }

    
    public override void Cancel()
    {
        HideTargetsVectorline();
        targetAnimal = null;
        SwitchState();
    }

    protected virtual void SwitchState()
    {
        if (canBite)
        {
            GetAvailableTargets(ownAnimal);
            state = ownAnimal.curRemainSkillCoolTime <= 0 && validTargetAnimals.Count > 0 ? CommandState.ACTIVE : CommandState.DISABLE;
        }
        else if (IsBiting(ownAnimal) && !biteAnimalsInThisTurn.Contains(ownAnimal))
        {
            state = CommandState.ACTIVATED;
        }
        else
        {
            state = CommandState.NONE;
        }
    }
    
    IEnumerator StartBittenStrugglingAnimation(Animal bittenAnimal)
    {
        // 자연스럽게 물리기 위해서 7개의 애니메이션이있다
        // 4가지 방향 + 3개의 idle모션이 있다
        int idleAnimationCount = 3;
        int directionalAnimationCount = 4;

        // 이미 물리고 있는 상황은 그냥 반환한다.
        if (bittenAnimal.animator.GetBool("bitten"))
        {
            yield break;
        }

        bittenAnimal.animator.SetTrigger("bittenTrigger");
        bittenAnimal.animator.SetBool("bitten", true);

        while (true)
        {
            // 스턴이 풀리면, 애니매이션을 초기화하고 빠져나옴
            if (!bittenAnimal.animator.GetBool("bitten"))
            {
                bittenAnimal.animator.SetInteger("struggle", 0);
                yield break;
            }
            // 
            else if(bittenAnimal.animator.GetInteger("struggle") == 0)
            {   
                int randomIdleAnimationIndex = Random.Range(1, idleAnimationCount + directionalAnimationCount + 1);
                
                // idle모션이 아니라 방향으로 흔드는 모션일때
                if (randomIdleAnimationIndex > idleAnimationCount)
                {
                    // 내가 물린 방향으로 몸을 흔든다. -> 왼쪽을 물렸으면 왼쪽으로만 몸을 기울인다. 내가 물린 방향으로만 랜덤하게 돌린다
                    while (!bittenAnimal.biteDirections[randomIdleAnimationIndex - idleAnimationCount-1])     
                    {
                        randomIdleAnimationIndex = Random.Range(idleAnimationCount + 1, idleAnimationCount + directionalAnimationCount +1);
                    }
                }

                string randomIdleAnimationName = null;

                randomIdleAnimationName = "struggle" + randomIdleAnimationIndex;

                /// 랜덤 idle 애니메이션의 시간을 알아오기 위한 검색
                AnimationClip randomIdleAnimationClip = bittenAnimal.GetAnimationClip(randomIdleAnimationName);

                /// 랜덤 idle 애니메이션 반영
                bittenAnimal.animator.SetInteger("struggle", randomIdleAnimationIndex);

                /// 해당 랜덤 idle 애니메이션 시간만큼 코루틴을 대기시킨다
                yield return new WaitForSeconds(randomIdleAnimationClip.length);
                
                bittenAnimal.animator.SetInteger("struggle", 0);

                yield return null;
            }
        }
    }
    
    IEnumerator StartBiteAnimation(Animal biteAnimal)
    {
        int idleAnimationCount = 3;
        int randomBiteAnimationIndex = 0;
        int newRandomBiteAnimationIndex = 0;
        
        FindAnimationClip(biteAnimal);

        biteAnimal.animator.SetTrigger("biteTrigger");
        biteAnimal.animator.SetBool("biteIdle", true);

        while (true)
        {
            // 스턴에 풀리면 종료시킴
            if (biteAnimal.animator.GetBool("biteIdle") == false)
            {                
                biteAnimal.animator.SetInteger("bite", 0);
                yield break;
            }
            else if (biteAnimal.animator.GetInteger("bite") == 0)
            {                
                // 무는 모션이 반복되지 않도록 중복되면 한번 더 돌림
                while (randomBiteAnimationIndex == newRandomBiteAnimationIndex)
                {
                    newRandomBiteAnimationIndex = Random.Range(1, idleAnimationCount+1);
                }
                
                randomBiteAnimationIndex = newRandomBiteAnimationIndex;

                string randomIdleAnimationName = null;

                randomIdleAnimationName = "bite" + randomBiteAnimationIndex;

                /// 랜덤 idle 애니메이션 반영
                biteAnimal.animator.SetInteger("bite", randomBiteAnimationIndex);

                /// 해당 랜덤 idle 애니메이션 시간만큼 코루틴을 대기시킨다
                yield return new WaitForSeconds(animationClipLength[randomBiteAnimationIndex]);
                biteAnimal.animator.SetInteger("bite", 0);
                yield return null;
            }
        }
    }


    // 구조체 리스트에서 이 동물이 물고있는 구조체를 제거, 그리고 이 동물에게 물렸던 동물을 반환
    void RemoveBitingAnimalFromBiteStructList(Animal animalToRemove, ref Animal bittenAnimal)
    {
        structToRemove.Clear();
        bittenAnimal = null;

        for (int i = 0; i < biteStructList.Count; i++)
        {
            if (biteStructList[i].bitingAnimal == animalToRemove)
            {
                bittenAnimal = biteStructList[i].bittenAnimal;
                structToRemove.Add(biteStructList[i]);
            }
        }
        
        for (int i = 0; i < structToRemove.Count; i++)
        {
            biteStructList.Remove(structToRemove[i]);
        }
    }

    // 구조체 리스트에서 이 동물이 물리고있는 구조체를 제거
    void RemoveBittenAnimalFromBiteStructList(Animal animalToRemove)
    {
        structToRemove.Clear();
        bitingAnimalList.Clear();

        for (int i = 0; i < biteStructList.Count; i++)
        {
            if (biteStructList[i].bittenAnimal == animalToRemove)
            {
                bitingAnimalList.Add(biteStructList[i].bitingAnimal);
                structToRemove.Add(biteStructList[i]);
            }
        }

        // 2. 이 동물이 포함된 biteRelation 구조체를 리스트에서 다 제거해준다.
        for (int i = 0; i < structToRemove.Count; i++)
        {
            biteStructList.Remove(structToRemove[i]);
        }
    }
    
    public void AddBiteRelation(Animal bitingAnimal, Animal bittenAnimal)
    {
        // 리스트에 추가해야 한다
        biteStructList.Add(new BiteStruct(bitingAnimal, bittenAnimal));
    }

    public override void Do()
    {
        // 물기 해제
        if (IsBiting(ownAnimal))
        {
            // 관계 리스트에서 제거
            RemoveBitingAnimalFromBiteStructList(ownAnimal,ref bittenAnimal);
            
            ownAnimal.canAction = true;
            StartCoroutine(StopBitingMotion(ownAnimal));
            
            // biteDirection 해제
            ResetBiteDirectionBetween(ownAnimal, bittenAnimal);
            
            // 물린동물의 biteDirection이 모두 fasle이면 애니메이션 취소
            if (!bittenAnimal.biteDirections[0] && 
                !bittenAnimal.biteDirections[1] && 
                !bittenAnimal.biteDirections[2] && 
                !bittenAnimal.biteDirections[3]) {
                bittenAnimal.canAction = true;
                bittenAnimal.isStunned = false;
                StartCoroutine(StopBittenMotion(bittenAnimal));
            }   
        }
        else   // 아직 안물고있는 하이에나를 클릭했을경우
        {
            // 리스트에 추가해야 한다
            AddBiteRelation(ownAnimal, targetAnimal);

            StartCoroutine(Bite(ownAnimal, targetAnimal));

            GameManager.instance.OnAnimalDidAction(ownAnimal);
                
            state = CommandState.NONE;
        }
    }
    
    public override void Ready()
    {

    }

    // ui조정을 위한 state
    protected override void OnStateChanged(CommandState changedState)
    {
        base.OnStateChanged(changedState);

        switch (changedState)
        {
            case CommandState.NONE:
                {
                }
                break;
            case CommandState.DISABLE:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(ownAnimal.curRemainSkillCoolTime > 0);
                    commandButton.cooltimeLabel.text = ownAnimal.curRemainSkillCoolTime.ToString();
                }
                break;
            case CommandState.READY:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVATED:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVE:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
        }
    }
    
    public IEnumerator Bite(Animal biteAnimal, Animal targetAnimal)
    {
        biteAnimal.canAction = false;

        UIManager.instance.ShowHUDText(I2.Loc.ScriptLocalization.Get(GetType().Name + "_name"), Color.yellow, biteAnimal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);
        yield return new WaitForSeconds(1f);

        // 이번에 무는 동물은 바로 풀 수 없어야 한다. 그래서 이 리스트에 등록함
        biteAnimalsInThisTurn.Add(biteAnimal);
        biteAnimal.audioSource.PlayOneShot(biteSound, 0.4f);

        /// 회전 
        var lookVector = targetAnimal.ownedTile.position - biteAnimal.ownedTile.position;
        var lookRotation = Quaternion.LookRotation(lookVector);
        biteAnimal.transform.rotation = lookRotation;

        // 물린 방향을 기록해서 물리는 애니메이션방향 결정
        Vector3 biteToTargetVector = targetAnimal.ownedTile.position - biteAnimal.ownedTile.position;
        Animal.Direction direction = targetAnimal.GetDirectionOfTarget(biteAnimal);
        int directionIndex = 0;
        if (direction == Animal.Direction.FRONT)
        {
            targetAnimal.biteDirections[0] = true;
            directionIndex = 4;
        }
        else if(direction == Animal.Direction.SIDE)
        {
            Vector3 cross = Vector3.Cross(biteToTargetVector, targetAnimal.transform.forward);
            if (cross.y < 0) // 대상의 왼쪽
            {
                targetAnimal.biteDirections[1] = true;
                directionIndex = 3;
            }
            else
            {
                targetAnimal.biteDirections[2] = true;
                directionIndex = 2;
            }
        }
        else
        {
            targetAnimal.biteDirections[3] = true;
            directionIndex = 1;
        }

        // 타겟의 neck(biteAnimal이 물고싶은 장소를 기준으로하는 곳)의 자식 오브젝트로 들어간다.
        if (ownAnimalsParent == null)
        {
            ownAnimalsParent = GameObject.Find(biteAnimal.ownerUserId + "'s Animals");
        }
        biteAnimal.transform.SetParent(targetAnimal.modelTransform);

        // 타겟의 몸체로 접근한다.
        Vector3 bitePosition = GetBiteAnimalPositionFromNeck(biteAnimal, targetAnimal);

        // 물기 위해 달려든다
        biteAnimal.animator.SetTrigger("rushToBiteTrigger");
        AnimationClip clip = biteAnimal.GetAnimationClip("rushToBite1");
        yield return biteAnimal.LocalMoveTo(bitePosition, "rushToBite1", 1);

        targetAnimal.isStunned = true;

        // 물어 뜯기 시작하는 모션을 실감나게 하기위해서 물자마자 한번 동물들의 애니메이션을 시작해준다.
        // random하게 모션을 돌리면 7개의 모션중 3가지는 그냥 idle모션이기 때문에,
        // 랜덤으로 돌리지 않고 자기가 물린 방향쪽으로 흔들리는 애니메이션을  지정해서 시작해준다
        biteAnimal.animator.SetTrigger("biteTrigger");
        biteAnimal.animator.SetBool("biteIdle", true);
        targetAnimal.animator.SetTrigger("bittenTrigger");
        targetAnimal.animator.SetBool("bitten", true);

        // 달려들자마자 한 번 흔드는 모션
        int newRandomBiteAnimationIndex = Random.Range(1, 3 + 1);
        biteAnimal.animator.SetInteger("bite", newRandomBiteAnimationIndex);
        
        GameManager.instance.OnAnimalBitten(targetAnimal);
        
        // 이번에 bite하려고 달려드는 방향으로 물린동물 휘청거리는 애니메이션
        int struggleIndex = 3 + directionIndex;
        targetAnimal.animator.SetInteger("struggle" , struggleIndex);
        AnimationClip struggleClip = targetAnimal.GetAnimationClip("struggle" + struggleIndex.ToString());

        // 기존의 타겟에니멀 물리는 애니메이션 코루틴을 중지시킨다.
        // -> 기존에 다른 동물이 깨물고있는경우, 새로 동물이 깨물려고 달려들때 휘청거리는 움직임과 기존의 깨물고있는 애니메이션과 겹침
        // -> 기존의 애니메이션 코루틴을 취소하고 새로 코루틴을 돌림
        if(coroutineDict.ContainsKey(targetAnimal) &&
            coroutineDict[targetAnimal] != null)
            StopCoroutine(coroutineDict[targetAnimal]);
        
        yield return new WaitForSeconds(struggleClip.length);

        // 첫 애니메이션때 설정했던 parameter 초기화
        biteAnimal.animator.SetInteger("bite", 0);
        targetAnimal.animator.SetInteger("struggle", 0);
        biteAnimal.animator.SetBool("biteIdle", false);
        targetAnimal.animator.SetBool("bitten", false);

        // 이제 bite애니메이션 루프 돌림
        StartCoroutine(StartBiteAnimation(biteAnimal));

        // 기존의 물리는 애니메이션코루틴을 중지시켰으므로 이제 새로운 물리는 코루틴을 생성한다
        coroutineDict[targetAnimal] = StartCoroutine(StartBittenStrugglingAnimation(targetAnimal));
    }
    Dictionary<Animal, Coroutine> coroutineDict;

    public void GetTargets(Animal animal, ref List<Animal> targetAnimals)
    { 
        GetAvailableTargets(animal);
        targetAnimals = validTargetAnimals; 
    }

    Vector3 GetBiteAnimalPositionFromNeck(Animal biteAnimal, Animal targetAnimal)
    {
        Vector3 biteToTargetVector = targetAnimal.ownedTile.position - biteAnimal.ownedTile.position;
        Animal.Direction direction = targetAnimal.GetDirectionOfTarget(biteAnimal);
        Vector3 returnVector = Vector3.zero;
        switch (targetAnimal.animalData.prefabName)
        {
            case "Lion":
                if (direction == Animal.Direction.FRONT)
                {
                    returnVector = new Vector3(-5.1f, -0.5f, 28.9f);
                }
                else if (direction == Animal.Direction.SIDE)
                {
                    Vector3 cross = Vector3.Cross(biteToTargetVector, targetAnimal.transform.forward);
                    if (cross.y < 0) // 대상의 왼쪽
                        returnVector = new Vector3(-21.8f, 0f, -1.4f);
                    else             // 대상의 오른쪽
                        returnVector = new Vector3(18.8f, 0.089f, 1.2f);
                }
                else if (direction == Animal.Direction.BEHIND)
                {
                    returnVector = new Vector3(2.5f, 0.65f, -25.9f);
                }
                break;
            case "Zebra":
                if (direction == Animal.Direction.FRONT)
                {
                    returnVector = new Vector3(1.9f, 1.83f, 30f);
                }
                else if (direction == Animal.Direction.SIDE)
                {
                    Vector3 cross = Vector3.Cross(biteToTargetVector, targetAnimal.transform.forward);
                    if (cross.y < 0) // 대상의 왼쪽
                        returnVector = new Vector3(-20.9f, 1.83f, 7.39f);
                    else             // 대상의 오른쪽
                        returnVector = new Vector3(20f, 1.83f, -7f);
                }
                else if (direction == Animal.Direction.BEHIND)
                {
                    returnVector = new Vector3(1.9f, 0.2f, -37.2f);
                }
                break;
            case "Elephant":
                if (direction == Animal.Direction.FRONT)
                {
                    returnVector = new Vector3(5.2f, 1.14f, 16.4f);
                }
                else if (direction == Animal.Direction.SIDE)
                {
                    Vector3 cross = Vector3.Cross(biteToTargetVector, targetAnimal.transform.forward);
                    if (cross.y < 0) // 대상의 왼쪽
                        returnVector = new Vector3(-19.9f, 1.14f, -10.8f);
                    else             // 대상의 오른쪽
                        returnVector = new Vector3(16.8f, 1.14f, -12f);
                }
                else if (direction == Animal.Direction.BEHIND)
                {
                    returnVector = new Vector3(-9.22f, 1.14f, -24.41f);
                }
                break;
            case "Hippo":
                if (direction == Animal.Direction.FRONT)
                {
                    returnVector = new Vector3(-2.8f, -1.22f, 14.4f);
                }
                else if (direction == Animal.Direction.SIDE)
                {
                    Vector3 cross = Vector3.Cross(biteToTargetVector, targetAnimal.transform.forward);
                    if (cross.y < 0) // 대상의 왼쪽
                        returnVector = new Vector3(-11.25f, -1.22f, 2.56f);
                    else             // 대상의 오른쪽
                        returnVector = new Vector3(11.69f, -1.22f, 2.56f);
                }
                else if (direction == Animal.Direction.BEHIND)
                {
                    returnVector = new Vector3(-0.4f, -0.4f, -12.9f);
                }
                break;                
            case "Camel":
                if (direction == Animal.Direction.FRONT)
                {
                    returnVector = new Vector3(3f, 1.3f, 19.3f);
                }
                else if (direction == Animal.Direction.SIDE)
                {
                    Vector3 cross = Vector3.Cross(biteToTargetVector, targetAnimal.transform.forward);
                    if (cross.y < 0) // 대상의 왼쪽
                        returnVector = new Vector3(-12.7f, 1.3f, 2.5f);
                    else             // 대상의 오른쪽
                        returnVector = new Vector3(19.21f, 1.3f, 2.5f);
                }
                else if (direction == Animal.Direction.BEHIND)
                {
                    returnVector = new Vector3(3.2f, 1.3f, -23.44f);
                }
                break;
            case "Giraffe":
                if (direction == Animal.Direction.FRONT)
                {
                    returnVector = new Vector3(0.76f, 1.77f, 5.19f);
                }
                else if (direction == Animal.Direction.SIDE)
                {
                    Vector3 cross = Vector3.Cross(biteToTargetVector, targetAnimal.transform.forward);
                    if (cross.y < 0) // 대상의 왼쪽
                        returnVector = new Vector3(-4.7f, 2.66f, -0.77f);
                    else             // 대상의 오른쪽
                        returnVector = new Vector3(8.03f, 2.73f, -0.77f);
                }
                else if (direction == Animal.Direction.BEHIND)
                {
                    returnVector = new Vector3(0.77f, 1.77f, -8.92f);
                }
                break;
            case "Gorilla":
                if (direction == Animal.Direction.FRONT)
                {
                    returnVector = new Vector3(3.15f, 0, 16.06f);
                }
                else if (direction == Animal.Direction.SIDE)
                {
                    Vector3 cross = Vector3.Cross(biteToTargetVector, targetAnimal.transform.forward);
                    if (cross.y < 0) // 대상의 왼쪽
                        returnVector = new Vector3(-11.69f, 0, 8.13f);
                    else             // 대상의 오른쪽
                        returnVector = new Vector3(12.35f, 0, 1.16f);
                }
                else if (direction == Animal.Direction.BEHIND)
                {
                    returnVector = new Vector3(-4.48f, 0f, -9.72f);
                }
                break;
            case "TompsonGazell":
                if (direction == Animal.Direction.FRONT)
                {
                   returnVector = new Vector3(2.8f, -4.4f, 29.5f);
                }
                else if (direction == Animal.Direction.SIDE)
                {
                    Vector3 cross = Vector3.Cross(biteAnimal.transform.forward, targetAnimal.transform.forward);
                    if (cross.y < 0) // 대상의 왼쪽
                        returnVector = new Vector3(-21.9f, -4.4f, 5.5f);
                    else             // 대상의 오른쪽
                        returnVector = new Vector3(22f, -4.4f, 3.9f);
                }
                else if (direction == Animal.Direction.BEHIND)
                {
                    returnVector = new Vector3(2.8f, -4.4f, -30.0f);
                }
                break;
            case "Hunter":
                if (direction == Animal.Direction.FRONT)
                {
                    returnVector = new Vector3(-4.3f, 0, 0);
                }
                else if (direction == Animal.Direction.SIDE)
                {
                    Vector3 cross = Vector3.Cross(biteToTargetVector, targetAnimal.transform.forward);
                    if (cross.y < 0) // 대상의 왼쪽
                        returnVector = new Vector3(0.03f, 0, -5.05f);
                    else             // 대상의 오른쪽
                        returnVector = new Vector3(-0.03f, 0, 4.9f);
                }
                else if (direction == Animal.Direction.BEHIND)
                {
                    returnVector = new Vector3(4.36f, 0, 0);
                }
                break;
            case "Rhino":
                if (direction == Animal.Direction.FRONT)
                {
                    returnVector = new Vector3(-3.73f, 0, 12.65f);
                }
                else if (direction == Animal.Direction.SIDE)
                {
                    Vector3 cross = Vector3.Cross(biteToTargetVector, targetAnimal.transform.forward);
                    if (cross.y < 0) // 대상의 왼쪽
                        returnVector = new Vector3(-12.4f, 0, 3.48f);
                    else             // 대상의 오른쪽
                        returnVector = new Vector3(12.93f, 0, 3.48f);
                }
                else if (direction == Animal.Direction.BEHIND)
                {
                    returnVector = new Vector3(-4.38f, 0, -17.83f);
                }
                break;
            case "Crocodile":
                if (direction == Animal.Direction.FRONT)
                {
                    returnVector = new Vector3(3.11f, -7.76f, 23.05f);
                }
                else if (direction == Animal.Direction.SIDE)
                {
                    Vector3 cross = Vector3.Cross(biteToTargetVector, targetAnimal.transform.forward);
                    if (cross.y < 0) // 대상의 왼쪽
                        returnVector = new Vector3(-10.6f, -7.76f, -0.4f);
                    else             // 대상의 오른쪽
                        returnVector = new Vector3(12.9f, -7.76f, -2f);
                }
                else if (direction == Animal.Direction.BEHIND)
                {
                    returnVector = new Vector3(0f, -7.76f, -27.1f);
                }
                break;
            case "Buffalo":
                if (direction == Animal.Direction.FRONT)
                {
                    returnVector = new Vector3(-4f, 0.92f, 23.3f);
                }
                else if (direction == Animal.Direction.SIDE)
                {
                    Vector3 cross = Vector3.Cross(biteToTargetVector, targetAnimal.transform.forward);
                    if (cross.y < 0) // 대상의 왼쪽
                        returnVector = new Vector3(-16.8f, 0.92f, -1f);
                    else             // 대상의 오른쪽
                        returnVector = new Vector3(17.1f, 0.92f, 2.6f);
                }
                else if (direction == Animal.Direction.BEHIND)
                {
                    returnVector = new Vector3(5.7f, 0.92f, -20.32f);
                }
                break;
            case "Chimpanzee":
                if (direction == Animal.Direction.FRONT)
                {
                    returnVector = new Vector3(2.8f, 0f, 35.5f);
                }
                else if (direction == Animal.Direction.SIDE)
                {
                    Vector3 cross = Vector3.Cross(biteToTargetVector, targetAnimal.transform.forward);
                    if (cross.y < 0) // 대상의 왼쪽
                        returnVector = new Vector3(-31.5f, 0f, -7.7f);
                    else             // 대상의 오른쪽
                        returnVector = new Vector3(29.9f, 0f, -4.2f);
                }
                else if (direction == Animal.Direction.BEHIND)
                {
                    returnVector = new Vector3(-2.8f, 0f, -39.1f);
                }
                break;
            case "Baboon":
                if (direction == Animal.Direction.FRONT)
                {
                    returnVector = new Vector3(4.6f, 2f, 37.6f);
                }
                else if (direction == Animal.Direction.SIDE)
                {
                    Vector3 cross = Vector3.Cross(biteToTargetVector, targetAnimal.transform.forward);
                    if (cross.y < 0) // 대상의 왼쪽
                        returnVector = new Vector3(-28.7f, 2f, 13f);
                    else             // 대상의 오른쪽
                        returnVector = new Vector3(28.5f, 2f, 14f);
                }
                else if (direction == Animal.Direction.BEHIND)
                {
                    returnVector = new Vector3(0f, 0f, -39.5f);
                }
                break;
            case "Cheetah":
                if (direction == Animal.Direction.FRONT)
                {
                    returnVector = new Vector3(-4.3f, -3.4f, 39.4f);
                }
                else if (direction == Animal.Direction.SIDE)
                {
                    Vector3 cross = Vector3.Cross(biteToTargetVector, targetAnimal.transform.forward);
                    if (cross.y < 0) // 대상의 왼쪽
                        returnVector = new Vector3(-27.1f, -3.4f, -8.7f);
                    else             // 대상의 오른쪽
                        returnVector = new Vector3(25.6f, 0.2f, 12.4f);
                }
                else if (direction == Animal.Direction.BEHIND)
                {
                    returnVector = new Vector3(-4.3f, -3.4f, -41.5f);
                }
                break;
            case "Hyena":
                if (direction == Animal.Direction.FRONT)
                {
                    returnVector = new Vector3(-4.6f, 3.3f, 35.4f);
                }
                else if (direction == Animal.Direction.SIDE)
                {
                    Vector3 cross = Vector3.Cross(biteToTargetVector, targetAnimal.transform.forward);
                    if (cross.y < 0) // 대상의 왼쪽
                        returnVector = new Vector3(-21.8f, 0, 4.4f);
                    else             // 대상의 오른쪽
                        returnVector = new Vector3(25, 0.0f, 6.8f);
                }
                else if (direction == Animal.Direction.BEHIND)
                {
                    returnVector = new Vector3(2.3f, 0f, -24.5f);
                }
                break;
            case "Impala":
                if (direction == Animal.Direction.FRONT)
                {
                    returnVector = new Vector3(-3.3f, 2.28f, 24.5f);
                }
                else if (direction == Animal.Direction.SIDE)
                {
                    Vector3 cross = Vector3.Cross(biteToTargetVector, targetAnimal.transform.forward);
                    if (cross.y < 0) // 대상의 왼쪽
                        returnVector = new Vector3(-22.3f, 4.7f, -16.5f);
                    else             // 대상의 오른쪽
                        returnVector = new Vector3(19.7f, 4.7f, 4.8f);
                }
                else if (direction == Animal.Direction.BEHIND)
                {
                    returnVector = new Vector3(4.6f, 4.7f, -34.9f);
                }
                break;  
            case "Jackal":
                if (direction == Animal.Direction.FRONT)
                {
                    returnVector = new Vector3(1.8f, -2.4f, 24.5f);
                }
                else if (direction == Animal.Direction.SIDE)
                {
                    Vector3 cross = Vector3.Cross(biteToTargetVector, targetAnimal.transform.forward);
                    if (cross.y < 0) // 대상의 왼쪽
                        returnVector = new Vector3(-20.6f, -2.5f, 5.8f);
                    else             // 대상의 오른쪽
                        returnVector = new Vector3(19.9f, -2.5f, 6.2f);
                }
                else if (direction == Animal.Direction.BEHIND)
                {
                    returnVector = new Vector3(0.49f, -2.5f, -26.22f);
                }
                break;  
            case "Leopard":
                if (direction == Animal.Direction.FRONT)
                {
                    returnVector = new Vector3(1.5f, 0, 30.3f);
                }
                else if (direction == Animal.Direction.SIDE)
                {
                    Vector3 cross = Vector3.Cross(biteToTargetVector, targetAnimal.transform.forward);
                    if (cross.y < 0) // 대상의 왼쪽
                        returnVector = new Vector3(-18.1f, 0, 1.9f);
                    else             // 대상의 오른쪽
                        returnVector = new Vector3(20f, 0, -7.2f);
                }
                else if (direction == Animal.Direction.BEHIND)
                {
                    returnVector = new Vector3(2.9f, 0, -27.2f);
                }
                break;
            case "Lycaon":
                if (direction == Animal.Direction.FRONT)
                {
                    returnVector = new Vector3(-2.9f, -8.3f, 50);
                }
                else if (direction == Animal.Direction.SIDE)
                {
                    Vector3 cross = Vector3.Cross(biteToTargetVector, targetAnimal.transform.forward);
                    if (cross.y < 0) // 대상의 왼쪽
                        returnVector = new Vector3(-38f, -10, 10.8f);
                    else             // 대상의 오른쪽
                        returnVector = new Vector3(38f, -10, 10.8f);
                }
                else if (direction == Animal.Direction.BEHIND)
                {
                    returnVector = new Vector3(0, -1, -48f);
                }
                break;
            default:
                returnVector = Vector3.zero;
                break;
        }
        return returnVector;
    }

    IEnumerator Shake(Animal animal, float shakeSize = 2f, float shakeSpeed = 40f, float attackedShakeDuration = 0.8f)
    {
        /// Shake 연출
        float elapsed = 0f;

        float direction = -1f;
        // 물기 직전의 위치로 돌아가기 위해서.
        Vector3 beforePosition = animal.transform.position;
        while (elapsed < attackedShakeDuration)
        {
            Vector3 destination = animal.transform.position;

            /// 동물이 바라보는 기준으로 양옆 흔들기 
            Vector3 transformRight = animal.transform.right * direction * shakeSize;
            destination += transformRight;

            float distance = Vector3.Distance(destination, animal.transform.position);
            while (distance > 0.1f && elapsed < attackedShakeDuration)
            {
                animal.transform.position = Vector3.Lerp(animal.transform.position, destination, Time.deltaTime * shakeSpeed);

                distance = Vector3.Distance(destination, animal.transform.position);

                elapsed += Time.deltaTime;

                yield return null;
            }

            direction *= -1f;
        }

        animal.transform.position = beforePosition;
    }
}
