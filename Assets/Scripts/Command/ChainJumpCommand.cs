﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using I2.Loc;

// 사정거리가 엄청 길어진다. 시야에 들어오는 동물을 하나 좇아 물을 수 있다. 녹턴 궁 같이 
public class ChainJumpCommand : Command
{
    readonly int RANGE = 20;
    readonly float SPEED = 2f;

    public static ChainJumpCommand instance;
    [SerializeField]
    AudioClip traceSound;
    Animal _ownAnimal;
    Animal ownAnimal
    {
        get { return _ownAnimal; }
        set
        {
            if (_ownAnimal != null)
            {
                _ownAnimal.OnFinishedMove -= OnFinishedMove;
                _ownAnimal.OnChangedCanAction -= OnChangedCanAction;
            }
            _ownAnimal = value;
            if (_ownAnimal != null)
            {
                _ownAnimal.OnFinishedMove += OnFinishedMove;
                _ownAnimal.OnChangedCanAction += OnChangedCanAction;
            }
        }
    }

    Animal _targetAnimal;
    Animal targetAnimal
    {
        get { return _targetAnimal; }
        set
        {
            _targetAnimal = value;
            if (value == null)
                UIManager.instance.ResetTargetUI();
            else
                UIManager.instance.SetTargetUi(value);
        }
    }

    Vectrosity.VectorLine _vectorline;
    Vectrosity.VectorLine vectorline
    {
        get
        {
            if (_vectorline == null)
                _vectorline = TileChunk.instance.lineCyan;
            return _vectorline;
        }
    }

    int targetIndex = 0;
    Tile targetTile;
    List<Animal> validTargetAnimals;
    bool canTrace { get { return ownAnimal != null && ownAnimal.animalData.skills.Contains("ChainJump") && ownAnimal.canAction; } }

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        GameManager.instance.OnSelectedAnimal += OnSelectedAnimal;
        GameManager.instance.OnSetTargetAnimal += OnSetTargetAnimal;
        //TileChunk.instance.OnClickedTile += OnClickedTile;
        Init();
    }

    public void Init()
    {
        validTargetAnimals = new List<Animal>();
    }

    int[] x = { 1, -1, 0, 0 };
    int[] y = { 0, 0, 1, -1 };
    
    void GetAvailableTargets(Animal ownAnimal)
    {
        if (ownAnimal == null)
            return;

        int tileX = ownAnimal.ownedTile.TileIndex_X;
        int tileY = ownAnimal.ownedTile.TileIndex_Y;
        validTargetAnimals.Clear();

        for(int i = 0; i < 4; i++)
        {
            if(TileChunk.instance.IsValidTileIndex(tileX + x[i], tileY + y[i]))
            if (TileChunk.instance.tiles[tileX + x[i], tileY + y[i]].owningAnimal != null)
            {
                Animal targetAnimal = TileChunk.instance.tiles[tileX + x[i], tileY + y[i]].owningAnimal;
                int indexX = targetAnimal.ownedTile.TileIndex_X * 2 - ownAnimal.ownedTile.TileIndex_X;
                int indexY = targetAnimal.ownedTile.TileIndex_Y * 2 - ownAnimal.ownedTile.TileIndex_Y;
                
                // 넘어가려는 타일이 맵 안에 들어오는지확인
                // 점프하고자 하는 타일의 시야가 확보가 안되어있으면 그냥 리스트에 넣는다. 많약 어떤 동물이있으면 느낌표처리로 다시 돌아오면된다.
                if (TileChunk.instance.IsValidTileIndex(indexX, indexY))
                if (!TileChunk.instance.tiles[indexX, indexY].isVisible)
                {
                    validTargetAnimals.Add(TileChunk.instance.tiles[tileX + x[i], tileY + y[i]].owningAnimal);
                }
                // 점프하고자하는 타일이 시야 화보가 된 상황이면, 그 타일에 동물이 없거나, 그 타일의 동물이 은신인경우에 리스트에 추가
                else if(TileChunk.instance.tiles[indexX, indexY].isVisible)
                {
                    // 동물이 은신중이면 validTarget에 추가
                    if(TileChunk.instance.tiles[indexX, indexY].owningAnimal != null && (TileChunk.instance.tiles[indexX, indexY].owningAnimal.isConcealed ||
                        TileChunk.instance.tiles[indexX, indexY].owningAnimal.isClimbedTree ||
                        TileChunk.instance.tiles[indexX, indexY].owningAnimal.isSubmerged))
                    {
                        validTargetAnimals.Add(TileChunk.instance.tiles[tileX + x[i], tileY + y[i]].owningAnimal);
                    }
                    // 동물이 정말 없다면 validTarget에 추가
                    if(TileChunk.instance.tiles[indexX, indexY].owningAnimal == null)
                    {
                        validTargetAnimals.Add(TileChunk.instance.tiles[tileX + x[i], tileY + y[i]].owningAnimal);
                    }
                }
            }
        }
    }
    

    protected virtual void SwitchState()
    {
        if (canTrace)
        {
            GetAvailableTargets(ownAnimal);
            state = ownAnimal.curRemainSkillCoolTime <= 0 && validTargetAnimals.Count > 0 ? CommandState.ACTIVE : CommandState.DISABLE;
        }
        else
        {
            state = CommandState.NONE;
        }
    }

    void OnFinishedMove(Animal animal)
    {
        SwitchState();
    }

    void OnChangedCanAction(bool canAction)
    {
        SwitchState();
    }

    // 이 동물이 선택되면 스킬버튼을 띄워야 한다.
    void OnSelectedAnimal(Animal animal)
    {
        ownAnimal = animal;

        SwitchState();
    }

    void OnSetTargetAnimal(Animal animal)
    {
        if (animal == null)
            return;

        if (state == CommandState.READY || state == CommandState.ACTIVATED)
        {
            foreach (var target in validTargetAnimals)
            {

            }
        }
    }

    public override void OnClickButton()
    {
        UIManager.instance.curCommand = this;
        GetAvailableTargets(ownAnimal);

        targetIndex++;  // 타겟 리스트에서 자동으로 타겟설정
        if (validTargetAnimals.Count <= targetIndex)
        {
            targetIndex = 0;
        }

        targetAnimal = validTargetAnimals[targetIndex];

        vectorline.Resize(0);
        foreach (var target in validTargetAnimals)
        {
            TileChunk.instance.AddVerticesIntoLinePointsByScale(target.ownedTile, 1f, vectorline);
        }
        vectorline.Draw3D();

        if (validTargetAnimals.Count > 0)
        {
            state = CommandState.ACTIVATED;
        }
        else
        {
            state = CommandState.READY;
        }

        UIManager.instance.OpenConfirmUI(this);
    }

    public override void Cancel()
    {
        vectorline.Resize(0);
        vectorline.Draw3D();
        SwitchState();
        targetAnimal = null;
    }  

    public override void Do()
    {
        vectorline.Resize(0);
        vectorline.Draw3D();

        GameManager.instance.OnAnimalDidAction(ownAnimal);
        StartCoroutine(Jump(ownAnimal, targetAnimal));
        state = CommandState.NONE;
        targetAnimal = null;
    }

    public override void Ready()
    {

    }

    protected override void OnStateChanged(CommandState changedState)
    {
        base.OnStateChanged(changedState);

        switch (changedState)
        {
            case CommandState.NONE:
                {
                }
                break;
            case CommandState.DISABLE:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(ownAnimal.curRemainSkillCoolTime > 0);
                    commandButton.cooltimeLabel.text = ownAnimal.curRemainSkillCoolTime.ToString();
                }
                break;
            case CommandState.READY:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVATED:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVE:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
        }
    }

    public IEnumerator Jump(Animal animal, Animal targetAnimal)
    {
        Tile tileToJump = null;
        int indexX = targetAnimal.ownedTile.TileIndex_X * 2 - animal.ownedTile.TileIndex_X;
        int indexY = targetAnimal.ownedTile.TileIndex_Y * 2 - animal.ownedTile.TileIndex_Y;
        if (TileChunk.instance.IsValidTileIndex(indexX, indexY))
        {
            tileToJump = TileChunk.instance.tiles[indexX, indexY]; 
        }
        if (tileToJump == null)
        {
            // 실패
        }
        else
        {
            /// 타겟 회전 
            var targetLookVector = targetAnimal.ownedTile.position - animal.ownedTile.position;
            var targetLookRotation = Quaternion.LookRotation(targetLookVector);
            animal.transform.rotation = targetLookRotation;

            UIManager.instance.ShowHUDText(I2.Loc.ScriptLocalization.Get(GetType().Name + "_name"), Color.yellow, targetAnimal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);
            yield return new WaitForSeconds(0.2f);

            animal.audioSource.PlayOneShot(animal.movingSound, 0.2f);

            if (tileToJump.owningAnimal != null)
            {
                var getBackCoroutine = GetBack(animal, targetAnimal, tileToJump);
                while (getBackCoroutine.MoveNext()) yield return getBackCoroutine.Current;
            }
            else
            {
                animal.ownedTile = tileToJump;
                animal.animator.SetTrigger("chainJumpTrigger");

                var moveCoroutine = animal.MoveTo(tileToJump.position, "chainJump");
                while (moveCoroutine.MoveNext()) yield return moveCoroutine.Current;

                animal.MoveToRapidly(tileToJump.position);

                /// 타일 시야 갱신
                TileChunk.instance.RefreshFieldOfView();
            }
        }
    }

    IEnumerator GetBack(Animal animal, Animal targetAnimal, Tile tileToJump)
    {
        animal.animator.SetTrigger("chainJumpTrigger");

        var moveCoroutine = animal.MoveToAnimDuration(tileToJump.position, "chainJump", animal.GetAnimationClip("chainJump").length / 3 );
        while (moveCoroutine.MoveNext()) yield return moveCoroutine.Current;

        animal.animator.speed = 0; // 애니메이션이 멈춤

        /// 느낌표를 띄운다. 뒷발차기 hud를 띄운다.
        /// 동물 놀라는 연출 
        animal.audioSource.PlayOneShot(GameManager.instance.surprisedSound, 0.4f);
        UIManager.instance.ShowHUDText("!", Color.yellow, animal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);

        yield return new WaitForSeconds(1.0f);

        animal.animator.speed = 1; // 애니메이션이 멈춤
        moveCoroutine = animal.MoveTo(animal.ownedTile.position, "chainJump");
        while (moveCoroutine.MoveNext()) yield return moveCoroutine.Current;
        
        animal.MoveToRapidly(animal.ownedTile.position);

        animal.canAction = false;
    }
}
