﻿using UnityEngine;
using System.Collections;


public class CommandButton : MonoBehaviour 
{
    public bool isUsing = false;

    public UIButton button;
    public UISprite iconSprite;
    public UILabel cooltimeLabel;
}
