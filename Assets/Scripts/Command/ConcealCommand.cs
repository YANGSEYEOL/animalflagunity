﻿using UnityEngine;
using System.Collections;

public class ConcealCommand : Command
{
    public static ConcealCommand instance;

    protected Animal _animal;
    protected Animal animal
    {
        get { return _animal; }
        set
        {
            if (_animal != null)
            {
                _animal.OnFinishedMove -= OnFinishedMove;
                _animal.OnFinishedAttack -= OnFinishedAttack;
                _animal.OnChangedCanAction -= OnChangedCanAction;
            }

            _animal = value;

            if (value != null)
            {
                value.OnFinishedMove += OnFinishedMove;
                value.OnFinishedAttack += OnFinishedAttack;
                value.OnChangedCanAction += OnChangedCanAction;
            }
        }
    }


    [SerializeField]
    public int skillCoolTime = 1;

    bool canConceal { get { return animal != null && animal.animalData.skills.Contains("Conceal") && animal.canAction; } }

    public virtual AudioClip concealSound { get { return GameManager.instance.concealSound; } }
    public virtual AudioClip unconcealSound { get { return GameManager.instance.unconcealSound; } }



    protected virtual void Awake()
    {
        instance = this;
    }


    private void Start()
    {
        GameManager.instance.OnSelectedAnimal += OnSelectedAnimal;
        GameManager.instance.OnChangedPlayerTurn += OnChangedPlayerTurn;

        state = CommandState.NONE;

        /// 쿨타임은 지정된 턴 수보다 +1해서 넣어줘야 한다. 스킬을 썼던 턴을 끝낼 때도 쿨타임이 -1 되기 때문
        /// 원래 쿨타임은 스킬을 사용하고 그 다음턴부터 쿨타임이 적용되어야 한다 
        skillCoolTime++;
    }


    private void OnChangedPlayerTurn(string turnUserId)
    {
        animal = null;
    }


    private void OnSelectedAnimal(Animal selectedAnimal)
    {
        animal = selectedAnimal;

        SwitchState();
    }


    public void OnChangedCanAction(bool canAction)
    {
        SwitchState();
    }


    protected virtual void SwitchState()
    {
        if (canConceal)
        {
            /// 은신 중일 때 Unconceal을 할 수 있다 
            if (animal.isConcealed)
            {
                state = CommandState.ACTIVATED;
            }
            /// 은신 중이 아닐 때, GRASS 타일이면 Conceal을 사용할 수 있다 
            /// 그 외에는 DISABLE
            else
            {
                state = animal.ownedTile.type == TileMaker.TileType.GRASS && animal.curRemainSkillCoolTime == 0 ? CommandState.ACTIVE : CommandState.DISABLE;
            }
        }
        else
        {
            state = CommandState.NONE;
        }
    }


    public override void OnClickButton()
    {
        state = CommandState.READY;
        UIManager.instance.OpenConfirmUI(this);
    }


    private void OnFinishedMove(Animal movedAnimal)
    {
        SwitchState();
    }


    private void OnFinishedAttack(Animal animal)
    {
        SwitchState();
    }


    protected override void OnStateChanged(CommandState changedState)
    {
        base.OnStateChanged(changedState);

        switch (changedState)
        {
            case CommandState.NONE:
                {
                }
                break;
            case CommandState.DISABLE:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(animal.curRemainSkillCoolTime > 0);
                    commandButton.cooltimeLabel.text = animal.curRemainSkillCoolTime.ToString();
                }
                break;
            case CommandState.READY:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVATED:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVE:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
        }
    }


    public override void Ready()
    {
    }


    public override void Do()
    {
        state = CommandState.NONE;
        GameManager.instance.OnAnimalDidAction(animal);
        animal.StartCoroutine(animal.Conceal(!animal.isConcealed));
    }


    public override void Cancel()
    {
        SwitchState();
    }
}
