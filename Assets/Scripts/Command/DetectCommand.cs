﻿using UnityEngine;


class DetectCommand : Command
{
    GameManager gameManager;
    TileChunk tileChunk;
    UIManager uiManager;

    Animal _animal;
    Animal animal
    {
        get { return _animal; }
        set
        {
            if (_animal != null)
            {
                _animal.OnChangedCanAction -= OnChangedCanAction;
            }

            _animal = value;

            if (value != null)
            {
                _animal.OnChangedCanAction += OnChangedCanAction;
            }
        }
    }

    public UIButton detectButton;

    const string LINE_NAME = "Detect";

    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        gameManager.OnChangedPlayerTurn += OnChangedPlayerTurn;
        gameManager.OnSelectedAnimal += OnSelectedAnimal;

        tileChunk = FindObjectOfType<TileChunk>();

        uiManager = FindObjectOfType<UIManager>();
    }

    private void OnSelectedAnimal(Animal selectedAnimal)
    {
        animal = selectedAnimal;

        state = selectedAnimal != null && selectedAnimal.canAction ? CommandState.ACTIVE : CommandState.NONE;
    }

    private void OnChangedPlayerTurn(string turnUserId)
    {
        animal = null;

        state = CommandState.NONE;
    }

    public void OnClickDetectButton()
    {
        state = animal != null && animal.canAction ? CommandState.READY : CommandState.NONE;

        uiManager.OpenConfirmUI(this);
    }

    public void OnChangedCanAction(bool canAction)
    {
        state = animal != null && animal.canAction ? CommandState.ACTIVE : CommandState.NONE;
    }

    protected override void OnStateChanged(CommandState changedState)
    {
        switch (changedState)
        {
            case CommandState.NONE:
                {
                    detectButton.gameObject.SetActive(false);

                    tileChunk.UndisplayLine(LINE_NAME);
                }
                break;
            case CommandState.ACTIVE:
                {
                    detectButton.gameObject.SetActive(true);

                    tileChunk.UndisplayLine(LINE_NAME);
                }
                break;
            case CommandState.READY:
                {
                    tileChunk.DisplayRange(animal.ownedTile.TileIndex_X, animal.ownedTile.TileIndex_Y, animal.curSmellRange, LINE_NAME, Color.cyan);
                }
                break;
        }
    }

    public override void Ready()
    {
        state = animal != null && animal.canAction ? CommandState.READY : CommandState.NONE;
    }

    public override void Do()
    {
        state = CommandState.NONE;
        GameManager.instance.OnAnimalDidAction(animal);
        //animal.Detect();
    }

    public override void Cancel()
    {
        state = animal != null && animal.canAction ? CommandState.ACTIVE : CommandState.NONE;
    }
}

