﻿using UnityEngine;
using System.Collections.Generic;

public class DoubleAttackCommand : AttackCommand
{
    public new static DoubleAttackCommand instance;


    [SerializeField]
    public int skillCoolTime = 1;

    
    bool canDoubleAttack { get { return animal != null && animal.animalData.skills.Contains("DoubleAttack") && animal.canAction; } }



    private void Awake()
    {
        instance = this;

        /// 쿨타임은 지정된 턴 수보다 +1해서 넣어줘야 한다. 스킬을 썼던 턴을 끝낼 때도 쿨타임이 -1 되기 때문
        /// 원래 쿨타임은 스킬을 사용하고 그 다음턴부터 쿨타임이 적용되어야 한다 
        skillCoolTime++;
    }
    

    protected override void OnSelectedAnimal(Animal selectedAnimal)
    {
        animal = selectedAnimal;

        /// 이전에 선택된 동물에 커맨드 초기화
        state = CommandState.NONE;

        SwitchState();
    }


    protected void SwitchState()
    {
        if (canDoubleAttack)
        {
            /// 공격할 대상이 있는지 서칭 
            GetAllAttackableTargetInfoList(animal, ref targetAnimalsInfoList);

            state = targetAnimalsInfoList.Count > 0 && animal.curRemainSkillCoolTime <= 0 ? CommandState.ACTIVE : CommandState.DISABLE;
        }
        else
        {
            state = CommandState.NONE;
        }
    }


    private void OnFinishedMove(Animal movedAnimal)
    {
        SwitchState();
    }


    private void OnFinishedAttack(Animal animal)
    {
        SwitchState();
    }


    protected override void OnStateChanged(CommandState changedState)
    {
        base.OnStateChanged(changedState);

        switch (changedState)
        {
            case CommandState.NONE:
                {
                    foreach (var targetInfo in targetAnimalsInfoList)
                    {
                        TileChunk.instance.UndisplayLine(LINE_NAME + targetInfo.animal.photonView.viewID);
                    }
                }
                break;
            case CommandState.DISABLE:
                {
                    foreach (var targetInfo in targetAnimalsInfoList)
                    {
                        TileChunk.instance.UndisplayLine(LINE_NAME + targetInfo.animal.photonView.viewID);
                    }

                    commandButton.cooltimeLabel.gameObject.SetActive(animal.curRemainSkillCoolTime > 0);
                    commandButton.cooltimeLabel.text = animal.curRemainSkillCoolTime.ToString();
                }
                break;
            case CommandState.ACTIVE:
                {
                    foreach (var targetInfo in targetAnimalsInfoList)
                    {
                        TileChunk.instance.UndisplayLine(LINE_NAME + targetInfo.animal.photonView.viewID);
                    }

                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
            case CommandState.READY:
                {
                    Color lineColor = Color.cyan;
                    foreach (var targetInfo in targetAnimalsInfoList)
                    {
                        lineColor = targetInfo.animal == targetAnimalInfo.animal ? Color.yellow : Color.cyan;
                        TileChunk.instance.DisplayTileHighlight(targetInfo.animal.ownedTile, LINE_NAME + targetInfo.animal.photonView.viewID, lineColor);
                    }

                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
        }
    }


    public override void Do()
    {
        HitResult[] hitResults;
        int[] damages;

        /// 판정 주사위 굴리기
        RollForHitTwice(animal, targetAnimalInfo, out hitResults, out damages);

        GameManager.instance.OnAnimalDidAction(animal);
        /// 공격 실행
        animal.DoubleAttack(targetAnimalInfo.animal, hitResults, damages);

        state = CommandState.NONE;
    }


    public void RollForHitTwice(Animal animal, AttackTargetInfo targetAnimalInfo, out HitResult[] hitResults, out int[] damages)
    {
        hitResults = new HitResult[2];
        damages = new int[2];

        /// 판정 주사위 굴리기
        hitResults[0] = RollForHit(targetAnimalInfo.hitResultTable, targetAnimalInfo.animal);
        hitResults[1] = RollForHit(targetAnimalInfo.hitResultTable, targetAnimalInfo.animal);

        /// 야매
        /// 카운터 판정은 원래 딱 한번만 나오는게 원칙이다 
        /// 이 주사위 판정은 서버로 넘어가기 전인 클라단계에서 하는거라 지금 단계에서 카운터 커맨드를 조정하는건 무리다 
        /// 그러므로 여기서 카운터 판정이 두번 난다면 찰과상 판정으로 야매적으로 바꾸는거다
        if (hitResults[0] == HitResult.COUNTER && hitResults[1] == HitResult.COUNTER)
        {
            hitResults[1] = HitResult.GRAZE;
        }

        /// 데미지 판정
        damages[0] = FinalizeDamage(animal, targetAnimalInfo.animal, hitResults[0]);
        damages[1] = FinalizeDamage(animal, targetAnimalInfo.animal, hitResults[1]);

        Debug.LogFormat("hitResults[0] : {0}, damages[0] : {1}", hitResults[0], damages[0]);
        Debug.LogFormat("hitResults[1] : {0}, damages[1] : {1}", hitResults[1], damages[1]);
    }


    public override void Cancel()
    {
        SwitchState();
    }


    protected override int GetHitChanceFactor(Animal animal, Animal targetAnimal)
    {
        int hitChanceFactor = base.GetHitChanceFactor(animal, targetAnimal);

        /// 더블 어택은 명중률 패널티가 적용된다 
        hitChanceFactor -= 20;

        return hitChanceFactor;
    }
}