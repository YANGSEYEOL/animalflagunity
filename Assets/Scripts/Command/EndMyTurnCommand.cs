﻿using UnityEngine;


class EndMyTurnCommand : Command
{
    GameManager gameManager;
    TileChunk tileChunk;
    UIManager uiManager;

    public UIButton endMyTurnButton;

    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        gameManager.OnChangedPlayerTurn += OnChangedPlayerTurn;

        tileChunk = FindObjectOfType<TileChunk>();

        uiManager = FindObjectOfType<UIManager>();
    }

    private void OnChangedPlayerTurn(string turnUserId)
    {
        state = gameManager.IsMyTurn ? CommandState.ACTIVE : CommandState.NONE;
    }

    public void OnClickEndMyTurnButton()
    {
        if(InputToEvent.canMove == false)
        {
            return;
        }
        gameManager.EndMyTurn();
    }

    protected override void OnStateChanged(CommandState changedState)
    {
        switch (changedState)
        {
            case CommandState.NONE:
                {
                    endMyTurnButton.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVE:
                {
                    endMyTurnButton.gameObject.SetActive(true);
                }
                break;
            case CommandState.READY:
                {
                }
                break;
        }
    }

    public override void Ready()
    {
    }

    public override void Do()
    {
    }

    public override void Cancel()
    {
    }
}


