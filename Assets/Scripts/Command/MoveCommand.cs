﻿using UnityEngine;

public class MoveCommand : Command
{
    Tile tile;

    Animal _animal;
    Animal animal
    {
        get { return _animal; }
        set
        {
            if (_animal != null)
            {
                _animal.OnFinishedMove -= OnFinishedMove;
                //_animal.OnFinishedAttack -= OnFinishedAttack;
            }

            _animal = value;

            if (value != null)
            {
                _animal.OnFinishedMove += OnFinishedMove;
                //_animal.OnFinishedAttack += OnFinishedAttack;
            }
        }
    }

    private void Start()
    {
        GameManager.instance.OnSelectedAnimal += OnSelectedAnimal;
        GameManager.instance.OnChangedPlayerTurn += OnChangedPlayerTurn;

        TileChunk.instance.OnClickedTile += OnClickedTile;
    }

    private void OnChangedPlayerTurn(string turnUserId)
    {
        animal = null;
        tile = null;

        state = CommandState.NONE;
    }

    private void OnSelectedAnimal(Animal selectedAnimal)
    {
        TileChunk.instance.UndisplayNavigationPath();

        animal = selectedAnimal;

        state = selectedAnimal != null && selectedAnimal.canMove ? CommandState.ACTIVE : CommandState.NONE;

        UIManager.instance.CloseConfirmUI();
    }

    private void OnClickedTile(Tile clickedTile)
    {
        if ((state == CommandState.ACTIVE || state == CommandState.READY) && TileChunk.instance.IsTileValidForMove(clickedTile))
        {
            tile = clickedTile;

            state = CommandState.READY;

            UIManager.instance.OpenConfirmUI(this);
        }
    }

    private void OnFinishedMove(Animal movedAnimal)
    {
        state = animal == movedAnimal && animal.canMove ? CommandState.ACTIVE : CommandState.NONE;
    }
    /*
    private void OnFinishedAttack(Animal animal)
    {
        state = animal != null && animal.canMove ? CommandState.ACTIVE : CommandState.NONE;
    }
    */
    protected override void OnStateChanged(CommandState changedState)
    {
        switch (changedState)
        {
            case CommandState.NONE:
                {
                    TileChunk.instance.UndisplayAllPathTiles();
                    TileChunk.instance.UndisplayNavigationPath();
                }
                break;
            case CommandState.ACTIVE:
                {
                    TileChunk.instance.DisplayAllPathTilesOf(animal, animal.curremainMovement, Color.white);
                }
                break;
            case CommandState.READY:
                {
                    TileChunk.instance.DisplayAllPathTilesOf(animal, animal.curremainMovement, Color.white);

                    if (tile != null)
                    { 
                        TileChunk.instance.DisplayNavigationPath(animal, tile);
                    }
                }
                break;
        }
    }

    public override void Ready()
    {
        state = animal != null && animal.canMove ? CommandState.READY : CommandState.NONE;
    }

    public override void Do()
    {
        animal.isDoingAction = true;

        state = CommandState.NONE;
        
        GameManager.instance.OnAnimalDidAction(animal);

        animal.MoveIfCanMove(tile);
    }

    public override void Cancel()
    {
        state = CommandState.NONE;

        tile = null;
    }
}