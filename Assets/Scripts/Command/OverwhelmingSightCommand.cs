﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using I2.Loc;

// 사정거리가 엄청 길어진다. 시야에 들어오는 동물을 하나 좇아 물을 수 있다. 녹턴 궁 같이 
public class OverwhelmingSightCommand : Command
{
    readonly int RANGE = 3;
    readonly float SPEED = 550f;
    readonly float FADING_SPEED = 1.3f;

    public static OverwhelmingSightCommand instance;
    [SerializeField]
    AudioClip useSound;

    Vectrosity.VectorLine _vectorline;
    Vectrosity.VectorLine vectorline
    {
        get
        {
            if (_vectorline == null)
                _vectorline = TileChunk.instance.lineCyan;
            return _vectorline;
        }
    }

    Animal _ownAnimal;
    Animal ownAnimal
    {
        get { return _ownAnimal; }
        set
        {
            if (_ownAnimal != null)
            {
                _ownAnimal.OnMoved -= OnMoved;
                _ownAnimal.OnFinishedMove -= OnFinishedMove;
                _ownAnimal.OnChangedCanAction -= OnChangedCanAction;
            }
            _ownAnimal = value;
            if (_ownAnimal != null)
            {
                _ownAnimal.OnMoved += OnMoved;
                _ownAnimal.OnFinishedMove += OnFinishedMove;
                _ownAnimal.OnChangedCanAction += OnChangedCanAction;
            }
        }
    }

    [SerializeField]
    GameObject useEffect;

    // 이 스킬을 쓰는 범위내의 타일인데 보이지 않는 타일
    List<Tile> invisibleTilesInRange;

    Dictionary<Animal, List<Tile>> absoluteSightTilesTable;

    bool canUse { get { return ownAnimal != null && ownAnimal.animalData.skills.Contains("OverwhelmingSight") && ownAnimal.canAction; } }

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        GameManager.instance.OnSelectedAnimal += OnSelectedAnimal;
        //GameManager.instance.OnChangedPlayerTurn += OnChangedPlayerTurn;
        GameManager.instance.OnAnimalDamaged += OnAnimalDamaged;
        Init();
    }

    public void Init()
    {        
        invisibleTilesInRange = new List<Tile>();
        absoluteSightTilesTable = new Dictionary<Animal, List<Tile>>();
        // 그림자를 끄면 렌더순서가 앞쪽으로 땡겨져서 효과가안보인다. 렌더큐를 수동으로정해주었다.
        // 이 효과의 그림자가 타일들에게 부자연스럽게 생겨서 그림자를 제거했다.
        useEffect.GetComponent<Renderer>().material.renderQueue = 3001;
    }
    

    // animal의 절대시야를 제거한다.
    void ResetAbsoluteSightTable(Animal animal)
    {
        // tilesInRangeTable에 유저키가 포함이안되어있으면 추가해줌
        if (!absoluteSightTilesTable.ContainsKey(animal))
        {
            absoluteSightTilesTable.Add(animal, new List<Tile>());
        }

        // 해당 동물의 절대시야 리스트를 테이블에서 제거
        TileChunk.instance.absoluteSightTileTable[animal].Clear();
        animal.hpBar.RemoveStatus("icon_overSight");

        // 바뀐 절대시야 리스트 테이블을 기준으로 디텍트를 갱신
        TileChunk.instance.RefreshDetect(animal.ownerUserId, TileChunk.instance.absoluteSightTileTable);
        TileChunk.instance.RefreshFieldOfView();
    }

    void OnAnimalDamaged(Animal damagedAnimal)
    {
        ResetAbsoluteSightTable(damagedAnimal);
    }

    // 동물이 destTile로 이동했을때
    void OnMoved(Animal movedAnimal, Tile destTile)
    {
        // tilesInRangeTable에 유저키가 포함이안되어있으면 추가해줌
        if (!absoluteSightTilesTable.ContainsKey(movedAnimal))
        {
            absoluteSightTilesTable.Add(movedAnimal, new List<Tile>());
        }
        // 이 동물의 절대시야가 없으면 무시
        if (absoluteSightTilesTable[movedAnimal].Count == 0)
            return;
        
        // 턴이시작되면 상대방이 detected 해놓은 타일들을 초기화하고 내 타일들의 detectedTiles로 함
        List<Tile> absoluteSightTiles = absoluteSightTilesTable[movedAnimal];
        
        // 이동물의 절대시야를 초기화해준다
        TileChunk.instance.absoluteSightTileTable[movedAnimal].Clear();
        
        // 새로운 범위만큼 절대시야 리스트를 업데이트시켜준다.
        AddAbsoluteSightTilesToTable(movedAnimal, destTile);
        
        // 은신동물을 발견하면 느낌표를 띄워줌
        CheckConcealedAnimals(TileChunk.instance.absoluteSightTileTable[movedAnimal]);

        // TileChunk의 절대시야 테이블대로 디텍트를 갱신함
        TileChunk.instance.RefreshDetect(movedAnimal.ownerUserId, TileChunk.instance.absoluteSightTileTable);

        // 새로 반영된 절대시야를 토대로 시야를 갱신함
        TileChunk.instance.RefreshFieldOfView();
    }
   

    // 내가 선택할 수 있는 타일을 모두 구해서 Vectorline으로 보여준다.
    void GetAvailableTargets(Animal ownAnimal)
    {
        if (ownAnimal == null)
            return;     
    }
    protected virtual void SwitchState()
    {
        if (canUse)
        {
            GetAvailableTargets(ownAnimal);
            state = ownAnimal.curRemainSkillCoolTime <= 0 ? CommandState.ACTIVE : CommandState.DISABLE;
        }
        else
        {
            state = CommandState.NONE;
        }
    }

    void OnFinishedMove(Animal animal)
    {
        SwitchState();
    }

    void OnChangedCanAction(bool canAction)
    {
        SwitchState();
    }

    // 이 동물이 선택되면 스킬버튼을 띄워야 한다.
    void OnSelectedAnimal(Animal animal)
    {
        ownAnimal = animal;

        SwitchState();
    }
    
 
    List<Tile> GetTilesInSkillRange(Tile tile)
    {
        int indexX = tile.TileIndex_X;
        int indexY = tile.TileIndex_Y;

        List<Tile> tilesInSkillRange = null;
        tilesInSkillRange = TileChunk.instance.GetTilesInRange(indexX, indexY, RANGE);
        
        return tilesInSkillRange;
    }

    public override void OnClickButton()
    {
        UIManager.instance.curCommand = this;

        List<Tile> tilesInSkillRange = GetTilesInSkillRange(ownAnimal.ownedTile);
        
        // 경계만 라인을 그림
        TileChunk.instance.DisplayTilesBoundary(tilesInSkillRange, vectorline);

        UIManager.instance.OpenConfirmUI(this);
    }

    public override void Cancel()
    {
        vectorline.Resize(0);
        vectorline.Draw3D();

        SwitchState();
    }
    
    public override void Do()
    {
        ownAnimal.canAction = false;
        vectorline.Resize(0);
        vectorline.Draw3D();

        GameManager.instance.OnAnimalDidAction(ownAnimal);
        StartCoroutine(OverwhelmingSight(ownAnimal));
        state = CommandState.NONE;
    }

    public override void Ready()
    {

    }

    protected override void OnStateChanged(CommandState changedState)
    {
        base.OnStateChanged(changedState);

        switch (changedState)
        {
            case CommandState.NONE:
                {
                }
                break;
            case CommandState.DISABLE:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(ownAnimal.curRemainSkillCoolTime > 0);
                    commandButton.cooltimeLabel.text = ownAnimal.curRemainSkillCoolTime.ToString();
                }
                break;
            case CommandState.READY:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVATED:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVE:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
        }
    }

    // 절대시야 리스트 수정(스킬을 시전했을경우, 시전중인 동물이 움직일경우 수행)
    void AddAbsoluteSightTilesToTable(Animal animal, Tile tile)
    {
        // 스킬 시전범위 
        List<Tile> tilesInSkillRange = GetTilesInSkillRange(tile);
       
        //// 스킬시전으로 인해서 새로 추가되는 타일들을 따로 기록해둬서
        //// 이 부분에 있는 동물들에게 느낌표를 띄울 수 있다. 이 기능이 필요없다면 필요가없는 리스트임
        //invisibleTilesInRange.Clear();
        //for (int i = 0; i < tilesInSkillRange.Count; i++)
        //{
        //    // 보이지 않는 타일만 invisibleTilesInRange에 추가해준다.
        //    if (!tilesInSkillRange[i].isVisible)
        //    {
        //        invisibleTilesInRange.Add(tilesInSkillRange[i]);
        //    }
        //}

        // 테이블에 등록
        absoluteSightTilesTable[animal] = tilesInSkillRange;

        // 이번스킬로 인해 드러난 동물들에게 !표시
        for (int i = 0; i < tilesInSkillRange.Count; i++)
        {
            // 디텍티드 되는 타일 == 절대시야 타일 == 스킬범위
            TileChunk.instance.absoluteSightTileTable[animal].Add(tilesInSkillRange[i]);
        }
    }

    public void CheckConcealedAnimals(List<Tile> tilesInSkillRange)
    {
        // 이번스킬로 인해 드러난 동물들에게 !표시
        for (int i = 0; i < tilesInSkillRange.Count; i++)
        {
            // 이 detected 타일에 있는 동물중에 은신인 적 동물에게 느낌표를 넣어준다.
            Animal targetAnimal = tilesInSkillRange[i].owningAnimal;
            if (targetAnimal != null
                && targetAnimal.isVisible == false
                && (targetAnimal.isConcealed || targetAnimal.isSubmerged || targetAnimal.isClimbedTree)
                && !targetAnimal.photonView.isMine)
            {
                UIManager.instance.ShowHUDText("!", Color.yellow, targetAnimal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);
            }
        }
    }

    public IEnumerator OverwhelmingSight(Animal animal)
    {
        animal.hpBar.AddStatus("icon_overSight");
        UIManager.instance.ShowHUDText(I2.Loc.ScriptLocalization.Get(GetType().Name + "_name"), Color.yellow, animal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);
        yield return new WaitForSeconds(0.5f);

        // 절대시야테이블 업데이트
        AddAbsoluteSightTilesToTable(animal, animal.ownedTile);

        // 은신동물을 발견하면 느낌표를 띄워줌
        CheckConcealedAnimals(TileChunk.instance.absoluteSightTileTable[animal]);

        // TileChunk의 절대시야 테이블대로 디텍트를 갱신함
        TileChunk.instance.RefreshDetect(animal.ownerUserId, TileChunk.instance.absoluteSightTileTable);

        // 시야 갱신
        TileChunk.instance.RefreshFieldOfView();

        // 이펙트사용 사운드
        animal.audioSource.PlayOneShot(useSound, 0.7f);

        // 쫙 퍼지는 이펙트 초기화
        useEffect.transform.position = animal.transform.position + Vector3.up*20;
        useEffect.transform.localScale = Vector3.forward;
        useEffect.GetComponent<MeshRenderer>().materials[0].color = Color.white - Color.black * 0.2f;

        // 쫙 퍼지는 이펙트
        while (useEffect.GetComponent<MeshRenderer>().materials[0].color.a > 0)
        {
            float speed = Time.deltaTime * SPEED;
            useEffect.transform.localScale += (Vector3.right + Vector3.up) * speed;

            speed = Time.deltaTime * FADING_SPEED;
            useEffect.GetComponent<MeshRenderer>().materials[0].color -= Color.black * speed;
            Color color = useEffect.GetComponent<MeshRenderer>().materials[0].color;

            if (color.a < 0){
                useEffect.GetComponent<MeshRenderer>().materials[0].color = Color.clear;
            }
            yield return null;
        }

        useEffect.transform.position = Vector3.one * 9999;
        useEffect.transform.localScale = Vector3.forward;
            
        yield return new WaitForSeconds(0.5f);

        // 투명유닛이 아닌 동물들도 !가 뜨게해야하나? 너무 TMI인가?
        for (int i = 0; i < invisibleTilesInRange.Count; i++)
        {
            /// 타겟 동물 놀라는 연출 
            //if (invisibleTilesInRange[i].owningAnimal != null)
            //{
            //    UIManager.instance.ShowHUDText("!", Color.yellow, invisibleTilesInRange[i].owningAnimal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);
            //    StartCoroutine(invisibleTilesInRange[i].owningAnimal.AdjustAlphaBlendingForConceal(1.0f));
            //    // 이 동물들의 타일을 보이게해야한다.
            //}
        }

    }
}
