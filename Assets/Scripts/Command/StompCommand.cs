﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using I2.Loc;

// 근접한 주변타일의 애들을 모두 띄운다.
public class StompCommand : Command
{
    readonly int DAMAGE = 10;

    public static StompCommand instance;
    
    [SerializeField]
    ParticleSystem partsEffect_dust;
    [SerializeField]
    ParticleSystem partsEffect_dirt;

    [SerializeField]
    AudioClip stompSound;
    [SerializeField]
    int yfactor;

    Vectrosity.VectorLine _vectorline;
    Vectrosity.VectorLine vectorline
    {
        get
        {
            if (_vectorline == null)
                _vectorline = TileChunk.instance.lineCyan;
            return _vectorline; }
    }

    Animal _ownAnimal;
    Animal ownAnimal
    {
        get { return _ownAnimal; }
        set
        {
            if(_ownAnimal !=null)
            {
                _ownAnimal.OnFinishedMove -= OnFinishedMove;
                _ownAnimal.OnChangedCanAction -= OnChangedCanAction;
            }

            _ownAnimal = value;
            if (value != null)
            {
                _ownAnimal.OnFinishedMove += OnFinishedMove;
                _ownAnimal.OnChangedCanAction += OnChangedCanAction;
            }
        }
    }

    List<Tile> targetTiles;
    List<Animal> validTargetAnimals;

    bool canStomp { get { return ownAnimal != null && ownAnimal.animalData.skills.Contains("Stomp") && ownAnimal.canAction; } }

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        GameManager.instance.OnSelectedAnimal += OnSelectedAnimal;
        Init();
    }

    public void Init()
    {
        validTargetAnimals = new List<Animal>();
        targetTiles = new List<Tile>();
    }

    int[] x = { 1,1,1,-1,-1, -1, 0, 0 };
    int[] y = { 0,1,-1, 0, 1, -1, 1, -1};

    void OnFinishedMove(Animal animal)
    {
        SwitchState();
    }

    public void GetTargetTiles(Animal ownAnimal)
    {
        targetTiles.Clear();

        Tile ownAnimalTile = ownAnimal.ownedTile;
        int tileX = ownAnimalTile.TileIndex_X;
        int tileY = ownAnimalTile.TileIndex_Y;
        for (int i = 0; i < 8; i++)
        {
            if (!TileChunk.instance.IsValidTileIndex(tileX + x[i], tileY + y[i])) continue;

            targetTiles.Add(TileChunk.instance.tiles[tileX + x[i], tileY + y[i]]);
        }
    }

    // 내가 선택할 수 있는 타일을 모두 구해서 Vectorline으로 보여준다.
    public void GetAvailableTargets(Animal ownAnimal)
    {
        if (ownAnimal == null)
            return;

        validTargetAnimals.Clear();
        if(ownAnimal.ownedTile.type == TileMaker.TileType.RIVER)
        {
            return;
        }
        Tile ownAnimalTile = ownAnimal.ownedTile;
        int tileX = ownAnimalTile.TileIndex_X;
        int tileY = ownAnimalTile.TileIndex_Y;
        for (int i = 0; i < 8; i++)
        {
            if (!TileChunk.instance.IsValidTileIndex(tileX + x[i], tileY + y[i])) continue;
            
            Animal animal = TileChunk.instance.tiles[tileX + x[i], tileY + y[i]].owningAnimal;
            if (animal != null)
            {
                if(animal.ownedTile.type != TileMaker.TileType.RIVER)
                    validTargetAnimals.Add(animal);             
            }
        }
    }

    void UndisplayIndicators()
    {
        vectorline.Resize(0);
        vectorline.Draw3D();
    }
    void DisplayIndicators()
    {
        vectorline.Resize(0);
        TileChunk.instance.DisplayTilesBoundary(targetTiles, vectorline);
    }

    // 이 동물이 선택되면 스킬버튼을 띄워야 한다.
    void OnSelectedAnimal(Animal animal)
    {
        ownAnimal = animal;

        SwitchState();
    }

    public override void OnClickButton()
    {
        GetAvailableTargets(ownAnimal);
        GetTargetTiles(ownAnimal);

        DisplayIndicators();
        state = CommandState.READY;
        UIManager.instance.OpenConfirmUI(this);
    }

    public void OnChangedCanAction(bool canAction)
    {
        SwitchState();
    }

    public override void Cancel()
    {
        UndisplayIndicators();
        SwitchState();
    }

    public override void Do()
    {
        ownAnimal.canAction = false;

        GameManager.instance.OnAnimalDidAction(ownAnimal);
        StartCoroutine(Stomp(ownAnimal));
        state = CommandState.NONE;
    }

    public override void Ready()
    {

    }

    protected virtual void SwitchState()
    {
        if (canStomp)
        {
            GetAvailableTargets(ownAnimal);
            state = ownAnimal.curRemainSkillCoolTime <= 0 && validTargetAnimals.Count > 0 ? CommandState.ACTIVE : CommandState.DISABLE;   
        }
        else
        {
            state = CommandState.NONE;
        }
    }

    protected override void OnStateChanged(CommandState changedState)
    {
        base.OnStateChanged(changedState);

        switch (changedState)
        {
            case CommandState.NONE:
                {
                }
                break;
            case CommandState.DISABLE:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(ownAnimal.curRemainSkillCoolTime > 0);
                    commandButton.cooltimeLabel.text = ownAnimal.curRemainSkillCoolTime.ToString();
                }
                break;
            case CommandState.READY:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVATED:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVE:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
        }
    }


    public IEnumerator Stomp(Animal ownAnimal)
    {
        GetAvailableTargets(ownAnimal);
        Animal[] tempValidTargetAnimals = new Animal[validTargetAnimals.Count];
        validTargetAnimals.CopyTo(tempValidTargetAnimals);
        UIManager.instance.ShowHUDText(I2.Loc.ScriptLocalization.Get(GetType().Name + "_name"), Color.yellow, ownAnimal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);
        yield return new WaitForSeconds(0.5f);

        AudioClip clickSound = ownAnimal.clickSound[Random.Range(0, ownAnimal.clickSound.Length)];
        ownAnimal.audioSource.PlayOneShot(clickSound, 0.2f);

        AnimationClip stompAnimationClip = ownAnimal.GetAnimationClip("stomp");
        
        ownAnimal.animator.SetTrigger("stompTrigger");

        yield return new WaitForSeconds(stompAnimationClip.length);

        CameraShake.instance.Shake(0.5f, 3.0f);
        ownAnimal.audioSource.PlayOneShot(stompSound, 0.1f);
        partsEffect_dust.transform.position = ownAnimal.transform.position;
        partsEffect_dust.Play();
        partsEffect_dirt.transform.position = ownAnimal.transform.position + Vector3.down * yfactor;
        partsEffect_dirt.Play();

        /// 맞는 사운드는 시끄러우니까 한번만 낸다. 대신 불륨 높여서
        AudioClip atkSound = GameManager.instance.attackSound_critical[Random.Range(0, GameManager.instance.attackSound_critical.Length)];
        ownAnimal.audioSource.PlayOneShot(atkSound, 0.8f);  

        foreach (var targetAnimal in tempValidTargetAnimals)
        {
            targetAnimal.animator.SetTrigger("levitateTrigger");
            GameManager.instance.OnAnimalLevitated(targetAnimal);

            AttackEffect.instance.Play(targetAnimal.transform.position, AttackCommand.HitResult.HIT);
            UIManager.instance.ShowHUDText(DAMAGE.ToString(), Color.grey, targetAnimal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);

            targetAnimal.curHealth -= DAMAGE;
            StartCoroutine(targetAnimal.Damaged(DAMAGE, AttackCommand.HitResult.GRAZE));
        }
        validTargetAnimals.Clear();
    }

    // 현재 이동범위에서 스코어 2이상의 타일이 존재하는가.
    const int AIMinimumTargetCount = 2;
    public void GetBestTileToStomp(Animal animal, List<Tile> targetAnimals)
    {
        targetAnimals.Clear();
        ownAnimal = animal;

        TileChunk.instance.dijkstra.FindPaths(animal, animal.animalData.mobility);
        List<int[]> validTileList = TileChunk.instance.dijkstra.LoadValidTiles();
        int highestScore = 0;

        for(int i = 0; i < validTileList.Count; i++)
        {   
            int score = GetStompScore(TileChunk.instance.GetTileWithIndex(validTileList[i][1]));
            if(score < AIMinimumTargetCount)
            {
                continue;
            }
            if (highestScore < score)
            {
                targetAnimals.Clear();
                highestScore = score;
                targetAnimals.Add(TileChunk.instance.GetTileWithIndex(validTileList[i][1]));
            }
            else if(highestScore == score)
            {
                targetAnimals.Add(TileChunk.instance.GetTileWithIndex(validTileList[i][1]));
            }            
        }   
    }

    int[] dirX = { -1, 0, 1, -1, 1, -1, 0, 1 };
    int[] dirY = { -1, -1, -1, 0, 0, 1, 1, 1 };
    int GetStompScore(Tile tile)
    {
        int x = tile.TileIndex_X;
        int y = tile.TileIndex_Y;
        int score = 0;

        //물타일에서는 stomp를 쓰지못하도록
        if (tile.type == TileMaker.TileType.RIVER)
            return score;

        for(int i = 0; i < 8; i++)
        {
            if (!TileChunk.instance.IsValidTileIndex(x + dirX[i], y + dirY[i])) continue;
            
            Animal animalOnTile = TileChunk.instance.tiles[x + dirX[i], y + dirY[i]].owningAnimal;
            if(animalOnTile != null)
            {
                // 물타일에는 stomp 영향이 안가므로 score계산에서 제외함
                if(animalOnTile.ownedTile.type != TileMaker.TileType.RIVER)
                {
                    score += GameManager.instance.enemyAnimals.Contains(animalOnTile) ? +1 : -1;
                }
            }
        }

        return score;
    }


}
