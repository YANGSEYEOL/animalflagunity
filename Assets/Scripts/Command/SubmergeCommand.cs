﻿using UnityEngine;
using System.Collections;

public class SubmergeCommand : ConcealCommand
{
    public static new SubmergeCommand instance;

    
    bool canSubmerge { get { return animal != null && animal.animalData.skills.Contains("Submerge") && animal.canAction; } }

    public override AudioClip concealSound { get { return GameManager.instance.submergeSound; } }
    public override AudioClip unconcealSound { get { return GameManager.instance.unsubmergedSound; } }



    protected override void Awake()
    {
        instance = this;
    }


    protected override void SwitchState()
    {
        if (canSubmerge)
        {
            /// 은신 중일 때 Unconceal을 할 수 있다 
            if (animal.isSubmerged)
            {
                state = CommandState.ACTIVATED;
            }
            /// 은신 중이 아닐 때, GRASS 타일이면 Conceal을 사용할 수 있다 
            /// 그 외에는 DISABLE
            else
            {
                state = animal.ownedTile.type == TileMaker.TileType.RIVER && animal.curRemainSkillCoolTime == 0 ? CommandState.ACTIVE : CommandState.DISABLE;
            }
        }
        else
        {
            state = CommandState.NONE;
        }
    }


    public override void Do()
    {
        state = CommandState.NONE;

        GameManager.instance.OnAnimalDidAction(animal);
        animal.StartCoroutine(animal.Conceal(!animal.isSubmerged));
    }
}
