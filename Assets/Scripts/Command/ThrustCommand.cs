﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ThrustCommand : Command {
    [SerializeField]
    int TARGET_RANGE = 4;
    [SerializeField]
    int THRUST_SPEED = 2;

    public static ThrustCommand instance;

    [SerializeField]
    ParticleSystem smoke;

    Animal _ownAnimal;
    public Animal ownAnimal
    {
        get { return _ownAnimal; }
        set
        {
            if (_ownAnimal != null)
            {
                _ownAnimal.OnMoved -= OnMoved;
                _ownAnimal.OnFinishedMove -= OnFinishedMove;
                _ownAnimal.OnChangedCanAction -= OnChangedCanAction;
            }
            _ownAnimal = value;
            if (_ownAnimal != null)
            {
                _ownAnimal.OnMoved += OnMoved;
                _ownAnimal.OnFinishedMove += OnFinishedMove;
                _ownAnimal.OnChangedCanAction += OnChangedCanAction;
            }
        }
    }

    int[] path;
    Tile _tileToThrust;
    public Tile tileToThrust {
        get { return _tileToThrust; }
        set
        {
            _tileToThrust = value;
        }
    }

    [SerializeField]
    public int skillCoolTime = 1;



    bool canThrust { get { return ownAnimal != null && ownAnimal.animalData.skills.Contains("Thrust") && ownAnimal.canAction; } }

    public enum ThrustLineDirection { NORTH, EAST, SOUTH, WEST, NONE }   /// 이 순서로 세팅돼있어야 SwitchLine할 때 시계방향으로 돌아간다  
    public ThrustLineDirection currentThrustLineDirection = ThrustLineDirection.NONE;
    public List<List<Tile>> thrustLineTileList = new List<List<Tile>>();

    

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        GameManager.instance.OnSelectedAnimal += OnSelectedAnimal;
        //GameManager.instance.OnChangedPlayerTurn += OnChangedPlayerTurn;
        
        TileChunk.instance.OnClickedTile += OnClickedTile;
        

        /// 쿨타임은 지정된 턴 수보다 +1해서 넣어줘야 한다. 스킬을 썼던 턴을 끝낼 때도 쿨타임이 -1 되기 때문
        /// 원래 쿨타임은 스킬을 사용하고 그 다음턴부터 쿨타임이 적용되어야 한다 
        skillCoolTime++;

        for (int lineIndex = 0; lineIndex < 4; lineIndex++)
        {
            thrustLineTileList.Add(new List<Tile>());
        }
    }

    // 내가 선택할 수 있는 타일을 모두 구해서 Vectorline으로 보여준다.
    public void GetRangeTiles(Animal animal)
    {
        currentThrustLineDirection = ThrustLineDirection.NONE;

        for (int lineIndex = 0; lineIndex < 4; lineIndex++)
        {
            thrustLineTileList[lineIndex].Clear();
        }

        /// 물 타일이면 바로 종료
        if (animal == null || animal.ownedTile.type == TileMaker.TileType.RIVER) return;

        int tileX = animal.ownedTile.TileIndex_X;
        int tileY = animal.ownedTile.TileIndex_Y;

        Tile tile;
        /// 동
        for (int i = 1; i <= TARGET_RANGE; ++i)
        {
            if (!IsTileThrustable(tileX + i, tileY, out tile)) break;

            thrustLineTileList[(int)ThrustLineDirection.EAST].Add(tile);
        }
        /// 서
        for (int i = 1; i <= TARGET_RANGE; ++i)
        {
            if (!IsTileThrustable(tileX - i, tileY, out tile)) break;

            thrustLineTileList[(int)ThrustLineDirection.WEST].Add(tile);
        }
        /// 남
        for (int i = 1; i <= TARGET_RANGE; ++i)
        {
            if (!IsTileThrustable(tileX, tileY + i, out tile)) break;

            thrustLineTileList[(int)ThrustLineDirection.SOUTH].Add(tile);
        }
        /// 북
        for (int i = 1; i <= TARGET_RANGE; ++i)
        {
            if (!IsTileThrustable(tileX, tileY - i, out tile)) break;

            thrustLineTileList[(int)ThrustLineDirection.NORTH].Add(tile);
        }

        /// 첫 라인 선택 
        currentThrustLineDirection = GetThrustLineDirectionUsable();
    }

    public ThrustLineDirection GetThrustLineDirectionUsable()
    {
        List<Animal> enemyListInMySight = GameManager.instance.enemyListInMySight;

        /// 각 돌진 방향안에 얼마의 적이 있는지 확인 
        int[] totalEnemyCountAtThrustLineDirection = new int[4];
        int allTotalEnemyCountInThrust = 0;
        for (int lineIndex = 0; lineIndex < 4; lineIndex++)
        {
            foreach (Tile tile in ThrustCommand.instance.thrustLineTileList[lineIndex])
            {
                /// 시야안에 들어오는 적 목록 카운팅을 라인별로 분류한다 
                if (tile.owningAnimal != null && enemyListInMySight.Contains(tile.owningAnimal))
                {
                    totalEnemyCountAtThrustLineDirection[lineIndex]++;
                    allTotalEnemyCountInThrust++;
                }
            }
        }

        /// 적이 가장 많이 있는 돌진 방향을 고른다 
        int thrustLineDirectionWithMostEnemies = 0;
        int mostEnemiesCountInThrustLine = 0;
        for (int lineIndex = 1; lineIndex < 4; lineIndex++)
        {
            if (totalEnemyCountAtThrustLineDirection[lineIndex] > mostEnemiesCountInThrustLine)
            {
                mostEnemiesCountInThrustLine = totalEnemyCountAtThrustLineDirection[lineIndex];
                thrustLineDirectionWithMostEnemies = lineIndex;
            }
        }

        /// 각 돌진 라인 방향안에 적 깃발 타일이 있는지 확인 
        int lineDirectionIncludingEnemyFlag = -1;
        Flag enemyFlag = PhotonNetwork.player.UserId == GameManager.instance.userIDForSimulation_origin ? GameManager.instance.flags[1] : GameManager.instance.flags[0];
        for (int lineIndex = 0; lineIndex < 4; lineIndex++)
        {
            if (ThrustCommand.instance.thrustLineTileList[lineIndex].Contains(enemyFlag.ownedTile))
            {
                lineDirectionIncludingEnemyFlag = lineIndex;
                break;
            }
        }

        ThrustLineDirection thrustLineDirectionUsable = ThrustLineDirection.NONE;
        /// 적이 있으면 적이 많이 있는 방향을 우선 지정
        if (mostEnemiesCountInThrustLine > 0)
        {
            thrustLineDirectionUsable = (ThrustLineDirection)thrustLineDirectionWithMostEnemies;
        }
        /// 적이 없으면 적 깃발이 있는 위치를 지정 
        else if(lineDirectionIncludingEnemyFlag >= 0)
        {
            thrustLineDirectionUsable = (ThrustLineDirection)lineDirectionIncludingEnemyFlag;
        }
        
        return thrustLineDirectionUsable;
    }
    
    bool IsTileThrustable(int x, int y, out Tile tile)
    {
        tile = null;

        if(!TileChunk.instance.IsValidTileIndex(x, y))
        {
            return false;
        }

        tile = TileChunk.instance.tiles[x, y];
        if (tile.isRock || tile.type == TileMaker.TileType.RIVER)
        {
            return false;
        }

        return true;
    }


    void OnFinishedMove(Animal animal)
    {
        SwitchState();
    }

    void OnChangedCanAction(bool canAction)
    {
        SwitchState();
    }

    // 이 동물이 선택되면 스킬버튼을 띄워야 한다.
    void OnSelectedAnimal(Animal animal)
    {
        ownAnimal = animal;
        
        SwitchState();
    }

    void OnClickedTile(Tile clickedTile)
    {
        if (state == CommandState.NONE)
            return;

        if (state == CommandState.READY || state == CommandState.ACTIVATED)
        {
            for (int lineIndex = 0; lineIndex < 4; ++lineIndex)
            {
                if (thrustLineTileList[lineIndex].Contains(clickedTile))
                {
                    currentThrustLineDirection = (ThrustLineDirection)lineIndex;

                    RefreshVectrosityLine();

                    break;
                }
            }
        }
    }

    void SwitchThrustLineDirection()
    {
        for (int loopCount = 0; loopCount < 4; ++loopCount) /// 4번 돌았는데 적합한 라인 방향이 없으면 없는거다 
        {
            currentThrustLineDirection++;
        
            if ((int)currentThrustLineDirection >= 4)
                currentThrustLineDirection = 0;

            if (thrustLineTileList[(int)currentThrustLineDirection].Count > 0)
                break;
        }
    }

    public override void OnClickButton()
    {
        if (state == CommandState.READY)
        {
            SwitchThrustLineDirection();
        }
        else
        {
            GetRangeTiles(ownAnimal);

            UIManager.instance.OpenConfirmUI(this);

            state = CommandState.READY;
        }

        RefreshVectrosityLine();
    }

    void RefreshVectrosityLine()
    {
        if (currentThrustLineDirection == ThrustLineDirection.NONE)
        {
            SwitchThrustLineDirection();
        }

        /// 선택된 라인 색깔 cyan으로 그려주기 
        TileChunk.instance.lineGreen.Resize(0);
        TileChunk.instance.lineCyan.Resize(0);
        for (int lineIndex = 0; lineIndex < 4; ++lineIndex)
        {
            Vectrosity.VectorLine vectorLine = lineIndex == (int)currentThrustLineDirection ? TileChunk.instance.lineCyan : TileChunk.instance.lineGreen;
            
            TileChunk.instance.DisplayTilesBoundary(thrustLineTileList[lineIndex], vectorLine);                        
        }

        /// 선택된 방향을 Navigation Path처럼 그려주기
        TileChunk.instance.UndisplayNavigationPath();
        List<Tile> currentThrustLineTileList = thrustLineTileList[(int)currentThrustLineDirection];
        if (currentThrustLineTileList.Count > 0) 
        {
            tileToThrust = currentThrustLineTileList[currentThrustLineTileList.Count - 1];

            RefreshThrustPath(ownAnimal.ownedTile._tileIndex);
            TileChunk.instance.DisplayNavigationPath(ownAnimal, tileToThrust, null, path); 
        }

        ///// Animal Line Circle
        //for (int lineIndex = 0; lineIndex < 4; ++lineIndex)
        //{
        //    foreach (Tile tile in thrustLineTileList[lineIndex])
        //    {
        //        bool showLineCircle = lineIndex == (int)currentThrustLineDirection && tile.isVisible;

        //        if (tile.owningAnimal != null) tile.owningAnimal.SetLineCircleVisible(showLineCircle);
        //    }
        //}
    }

    public void RefreshThrustPath(int tileIndexToThrust)
    {
        List<Tile> currentLineTileList = thrustLineTileList[(int)currentThrustLineDirection];
        path = new int[currentLineTileList.Count + 2];
        path[0] = currentLineTileList.Count + 1;
        for (int i = 1; i <= currentLineTileList.Count; i++)
        {
            path[i] = currentLineTileList[currentLineTileList.Count - i]._tileIndex;
        }
        path[currentLineTileList.Count + 1] = tileIndexToThrust;
    }

    // ownAnimal이 tile로 이동한다고치자.
    public IEnumerator MoveStaight(Animal animal)
    {        
        /// 회전 
        Vector3 lookVector = tileToThrust.position - animal.ownedTile.position;
        var lookRotation = Quaternion.LookRotation(lookVector);
        animal.transform.rotation = lookRotation;

        /// 스킬 이름 텍스트 이펙트
        UIManager.instance.ShowHUDText(I2.Loc.ScriptLocalization.Get(GetType().Name + "_name"), Color.yellow, animal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);

        /// 사운드 
        AudioClip sound = animal.clickSound[Random.Range(0, animal.clickSound.Length)];
        animal.audioSource.PlayOneShot(sound, 0.3f);

        /// 출발 전 딜레이
        yield return new WaitForSeconds(0.7f);

        animal.movingAnimSpeed = THRUST_SPEED;
        
        string movingAnim = "waddle";

        animal.isMoving = true;

        animal.animator.SetBool(movingAnim, true);
        animal.animator.SetTrigger(movingAnim + "Trigger");

        /// 내 위치 타일을 건너뛰기 위해 -1
        /// 0 = 경로 갯수
        /// path[경로 갯수] = 동물 현 위치 
        /// 1 = 도착 지점 
        for (int i = path[0] - 1; i >= 1; --i)
        {
            int nextTileIndex = path[i];
            int nextTileX = nextTileIndex % TileMaker.instance.tileSize;
            int nextTileY = nextTileIndex / TileMaker.instance.tileSize;
            Tile nextTile = TileChunk.instance.tiles[nextTileX, nextTileY];

            int currentTileIndex = path[i + 1];
            int currentTileX = currentTileIndex % TileMaker.instance.tileSize;
            int currentTileY = currentTileIndex / TileMaker.instance.tileSize;
            Tile currentTile = TileChunk.instance.tiles[currentTileX, currentTileY];

            yield return MoveForThrust(animal, currentTile, nextTile);
        }

        /// 이동 애니메이션 초기화
        /// 돌진 이동 애니메이션은 한번에 목표지점까지 쓰윽 갔다가 다시 돌아오는 것을 목표로 한다 
        /// 일반 타일 이동은 한타일 한타일, 타일의 타입을 체크해가면서 애니메이션을 바꾸지만, 돌진 애니메이션은 그렇게 하지 않는다 
        animal.animator.SetBool(movingAnim, false);
        animal.animator.ResetTrigger(movingAnim + "Trigger");

        /// 맞은 애들 데미징 처리 끝날 때까지 대기한다 
        for (int i = path[0] - 1; i >= 1; --i)
        {
            int nextTileIndex = path[i];
            int nextTileX = nextTileIndex % TileMaker.instance.tileSize;
            int nextTileY = nextTileIndex / TileMaker.instance.tileSize;
            Tile nextTile = TileChunk.instance.tiles[nextTileX, nextTileY];

            Animal tileAnimal = nextTile.owningAnimal;
            if (tileAnimal != null)
            {
                while (tileAnimal.isDodging || tileAnimal.isDamaging)
                {
                    yield return null;
                }    
            }
        }

        /// 사운드 
        sound = animal.clickSound[Random.Range(0, animal.clickSound.Length)];
        animal.audioSource.PlayOneShot(sound, 0.3f);

        /// 회전 
        lookVector = animal.ownedTile.position - tileToThrust.position;
        lookRotation = Quaternion.LookRotation(lookVector);
        animal.transform.rotation = lookRotation;

        animal.animator.SetBool(movingAnim, true);
        animal.animator.SetTrigger(movingAnim + "Trigger");

        /// 마지막 타일에 있던 애 한번 더 때리고 올라면, 수동으로 불러서 처리해줘야 한다 
        animal.OnMoved(animal, tileToThrust);

        for (int i = 2; i <= path[0]; ++i)
        {
            int nextTileIndex = path[i];
            int nextTileX = nextTileIndex % TileMaker.instance.tileSize;
            int nextTileY = nextTileIndex / TileMaker.instance.tileSize;
            Tile nextTile = TileChunk.instance.tiles[nextTileX, nextTileY];

            int currentTileIndex = path[i - 1];
            int currentTileX = currentTileIndex % TileMaker.instance.tileSize;
            int currentTileY = currentTileIndex / TileMaker.instance.tileSize;
            Tile currentTile = TileChunk.instance.tiles[currentTileX, currentTileY];

            yield return MoveForThrust(animal, currentTile, nextTile);
        }

        animal.animator.SetBool(movingAnim, false);
        animal.animator.ResetTrigger(movingAnim + "Trigger");

        animal.isMoving = false;
        
        /// 연출 끝. 마무리
        animal.movingAnimSpeed = 1;

        /// 동물 선택이 유지되어있다면 보여주기 
        if (GameManager.instance.SelectedAnimal == this)
        {
        }
    }

    public IEnumerator MoveForThrust(Animal animal, Tile currentTile, Tile nextTile)
    {
        /// 시야 갱신을 위한 무빙 타일 반영
        /// 이동 연출보다 빠르게 호출해줘야 한다 
        /// 연출하기 전에 movingTile을 참조하게 되면 null을 맛보게 된다 
        if (animal.movingTile != null) animal.movingTile.OnChangedTileVisible -= animal.OnChangedTileVisible;
        animal.movingTile = nextTile;
        if (animal.movingTile != null) animal.movingTile.OnChangedTileVisible += animal.OnChangedTileVisible;

        animal.PlayHopInGrassSound();
        animal.PlaySandWalkingSoundAndParticle();
        animal.PlayHopInForestSound();

        /// 움직일꺼라는 이벤트 알려주기 
        animal.OnStartMove(animal, nextTile);

        /// 이동할 위치 결정
        Vector3 tilePos = animal.GetRealPositionByTile(nextTile);

        yield return animal.MoveTo(tilePos, "waddle", 2f);

        /// 타일 시야 갱신
        TileChunk.instance.RefreshFieldOfView();

        /// 이벤트 알리기
        animal.OnMoved(animal, nextTile);
    }
    
    void OnMoved(Animal animal, Tile tile)
    {
        if(animal.ownedTile == tile)
            return;
        
        if (tile.owningAnimal != null)
        {
            Animal targetAnimal = tile.owningAnimal;

            /// 공격 판정 주사위 돌리기
            /// 카운터는 불가. 직선상에 여러마리가 주루룩 맞을 때 카운터 연출을 할 수 없다 
            int[] hitResultTable = AttackCommand.instance.GetHitChance(animal, targetAnimal);
            AttackCommand.HitResult hitResult = AttackCommand.instance.RollForHit(hitResultTable, targetAnimal, true);
            int damage = AttackCommand.instance.FinalizeDamage(animal, targetAnimal, hitResult);

            /// 모션 처리 
            /// 내 동물 혹은 MISS 판정 동물 피하는 연출. 그외는 전부 공중에 뜨는 연출 
            bool dodge = targetAnimal.photonView.isMine || hitResult == AttackCommand.HitResult.MISS;
            if (dodge)
            {                
                targetAnimal.StartCoroutine(targetAnimal.DirectingDodge(animal.ownedTile.position, true, true));
                targetAnimal.audioSource.PlayOneShot(GameManager.instance.missSound, 0.3f);   
            }
            else
            {
                GameManager.instance.OnAnimalLevitated(targetAnimal);
                targetAnimal.animator.SetTrigger("levitateTrigger");
            }

            /// 데미지 처리 (적 동물만)
            if (!targetAnimal.photonView.isMine)
            {
                /// 타격 사운드
                AudioClip atkSound = GameManager.instance.attackSound_hit[Random.Range(0, GameManager.instance.attackSound_hit.Length)];
                Color textColor = Color.red;
                if (hitResult == AttackCommand.HitResult.GRAZE )  
                {
                    atkSound = GameManager.instance.attackSound_graze[Random.Range(0, GameManager.instance.attackSound_graze.Length)];
                    textColor = Color.white;
                }
                else if(hitResult == AttackCommand.HitResult.MISS)
                {
                    atkSound = GameManager.instance.missSound;
                    textColor = Color.white;
                }
                else if (hitResult == AttackCommand.HitResult.CRITICAL) 
                {
                    atkSound = GameManager.instance.attackSound_critical[Random.Range(0, GameManager.instance.attackSound_critical.Length)];
                    textColor = Color.yellow;
                }
                targetAnimal.audioSource.PlayOneShot(atkSound, 0.4f);

                /// 타격 이펙트 
                AttackEffect.instance.Play(targetAnimal.transform.position, hitResult);

                /// 데미지 텍스트 
                UIManager.instance.ShowHUDText(I2.Loc.ScriptLocalization.Get(hitResult.ToString()), textColor, targetAnimal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR, false);
                if (hitResult != AttackCommand.HitResult.MISS)
                {
                    UIManager.instance.ShowHUDText(damage.ToString(), textColor, targetAnimal.transform.position + Animal.DAMAGE_TEXT_POS_FACTOR);
                    UIManager.instance.IncreaseHUDTextIndex();

                    /// 체력 차감
                    targetAnimal.curHealth -= damage;

                    /// 데미지 연출
                    targetAnimal.StartCoroutine(targetAnimal.Damaged(damage, AttackCommand.HitResult.GRAZE));
                }
            }
        }
    }
     

    void Thrust()
    {
        ownAnimal.canAction = false;

        ownAnimal.curRemainSkillCoolTime = skillCoolTime;

        StartCoroutine(MoveStaight(ownAnimal));
    }

    public override void Cancel()
    {
        SwitchState();
    }

    public override void Do()
    {
        Thrust();
        GameManager.instance.OnAnimalDidAction(ownAnimal);

        state = CommandState.NONE;
    }

    protected virtual void SwitchState()
    {
        /// AI에서 코드 일부분을 쓰기 때문에 AI턴일 때는 버튼 안 보여주도록 한다 
        bool isAITurn = GameManager.instance.isOnAI && PhotonNetwork.player.UserId != GameManager.instance.userIDForSimulation_origin;

        if (canThrust && !isAITurn)
        {
            GetRangeTiles(ownAnimal);

            int totalThrustableTileCount = 0;
            for (int lineIndex = 0; lineIndex < 4; ++lineIndex)
            {
                totalThrustableTileCount += thrustLineTileList[lineIndex].Count;
            }
            state = ownAnimal.curRemainSkillCoolTime <= 0 && totalThrustableTileCount > 0 ? CommandState.ACTIVE : CommandState.DISABLE;   
        }
        else
        {
            state = CommandState.NONE;
        }
    }

    public override void Ready()
    {

    }

    protected override void OnStateChanged(CommandState changedState)
    {
        base.OnStateChanged(changedState);

        switch (changedState)
        {
            case CommandState.NONE:
                {
                    for (int lineIndex = 0; lineIndex < 4; ++lineIndex) thrustLineTileList[lineIndex].Clear();
                    
                    RefreshVectrosityLine();
                }
                break;
            case CommandState.DISABLE:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(ownAnimal.curRemainSkillCoolTime > 0);
                    commandButton.cooltimeLabel.text = ownAnimal.curRemainSkillCoolTime.ToString();
                }
                break;
            case CommandState.READY:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVATED:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVE:
                {
                    commandButton.cooltimeLabel.gameObject.SetActive(false);

                    for (int lineIndex = 0; lineIndex < 4; ++lineIndex) thrustLineTileList[lineIndex].Clear();
                    
                    RefreshVectrosityLine();
                }
                break;
        }
    }
}
