﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Photon;
using ExitGames.Client.Photon;

public class DayAndNight : PunBehaviour
{
    public enum TimeOfNature
    {
        DAY, 
        NIGHT
    };

    int intervalTurnOfSwitchingTime = 0;

    [SerializeField]
    public int remainingTurnOfSwitchingTime = 0;

    [System.NonSerialized]
    public TimeOfNature currentTimeOfNature = TimeOfNature.DAY;

    GameManager gameManager;

    public static readonly string DAY_AND_NIGHT_PROP_KEY = "dayAndNight";
    public static readonly string INTERVAL_TURN_OF_SWITCHING_TIME_PROP_KEY = "intervalTurnOfSwitchingTime";
    public static readonly string REMAINING_TURN_OF_SWITCHING_TIME_PROP_KEY = "remainingTurnOfSwitchingTime";
    public static readonly string CURRENT_TIME_OF_NATURE_PROP_KEY = "currentTimeOfNature";



    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
    }


    public void OnTurnCompleted()
    {
        ///  이 함수는 마스터 클라이언트만 호출되어야 한다 
        if (!PhotonNetwork.isMasterClient)
        {
            return;
        }

        remainingTurnOfSwitchingTime -= 1;
        TimeOfNature toBeSentTime = TimeOfNature.DAY;
        if (remainingTurnOfSwitchingTime <= 0)
        {
            switch (currentTimeOfNature)
            {
                case TimeOfNature.DAY:
                    toBeSentTime = TimeOfNature.NIGHT;
                    break;
                case TimeOfNature.NIGHT:
                    toBeSentTime = TimeOfNature.DAY;
                    break;
            }

            remainingTurnOfSwitchingTime = intervalTurnOfSwitchingTime;
        }

        Hashtable dayAndNightProp = new Hashtable();
        Hashtable dayAndNight = new Hashtable();

        dayAndNight[INTERVAL_TURN_OF_SWITCHING_TIME_PROP_KEY] = intervalTurnOfSwitchingTime;
        dayAndNight[REMAINING_TURN_OF_SWITCHING_TIME_PROP_KEY] = remainingTurnOfSwitchingTime;
        dayAndNight[CURRENT_TIME_OF_NATURE_PROP_KEY] = (int)toBeSentTime;

        dayAndNightProp[DAY_AND_NIGHT_PROP_KEY] = dayAndNight;

        PhotonNetwork.room.SetCustomProperties(dayAndNightProp);
    }


    public override void OnPhotonCustomRoomPropertiesChanged(Hashtable propertiesThatChanged)
    {
        if (propertiesThatChanged.ContainsKey(DAY_AND_NIGHT_PROP_KEY))
        {
            Hashtable dayAndNightProp = (Hashtable)PhotonNetwork.room.CustomProperties[DAY_AND_NIGHT_PROP_KEY];
            TimeOfNature sentTimeOfNature = (TimeOfNature)dayAndNightProp[CURRENT_TIME_OF_NATURE_PROP_KEY];
            intervalTurnOfSwitchingTime = (int)dayAndNightProp[INTERVAL_TURN_OF_SWITCHING_TIME_PROP_KEY];
            remainingTurnOfSwitchingTime = (int)dayAndNightProp[REMAINING_TURN_OF_SWITCHING_TIME_PROP_KEY];

            if (currentTimeOfNature != sentTimeOfNature)
            {
                currentTimeOfNature = sentTimeOfNature;

                gameManager.OnSwitchedDayAndNight(sentTimeOfNature);
            }
        }
    }
}
