﻿using UnityEngine;
using System.Collections;

public class Detection : Skill
{
    public override void Init(Animal _ownAnimal)
    {
        ownAnimal = _ownAnimal;
        InitSkillDescription();
        InitSkillName();
    }

    public override void InitSkillDescription()
    {
        description = "This is Detection";
    }

    public override void InitSkillName()
    {
        skillName = "Detection";
    }

    public override void InitIsActive()
    {
        isActive = true;
    }

    public override void Do()
    {
        Debug.Log(ownAnimal.name + "used a Detection");
    }
}
