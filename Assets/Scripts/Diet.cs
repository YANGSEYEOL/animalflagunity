﻿using UnityEngine;
using System.Collections;

public class Diet : Skill
{
    public override void Init(Animal _ownAnimal)
    {
        ownAnimal = _ownAnimal;
        InitSkillDescription();
        InitSkillName();
    }

    public override void InitSkillDescription()
    {
        description = "This is Diet";
    }

    public override void InitSkillName()
    {
        skillName = "Diet";
    }

    public override void InitIsActive()
    {
        isActive = false;
    }

    public override void Do()
    {
        Debug.Log(ownAnimal.name + "used a Diet");
    }
}
