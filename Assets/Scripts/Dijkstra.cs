﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class Dijkstra{
    int mapWidth;
    int mapHeight;
    int vertexLength;
    int mapSize;
    int remainMoveCount = 0;
    int[,] costAtoB;
    public bool[,] tileArrayOccupied;
    Tile[,] tiles;
    // 각 정점들의 정보(전체코스트, 부모, 결과상태)
    Vertex[] vertexList;
    int[] directionX = new int[4] { 0, 0, 1, -1};
    int[] directionY = new int[4] { 1, -1, 0, 0};
    List<int[]> validTileList = new List<int[]>();
    private const int TEMPORARY = 1;
    private const int PERMANENT = 2;
    private const int NIL = -2;   // 맵 밖의 타일인덱스가 -1이기 때문에 혼동을 피하기 위함
    private readonly int INFINITY = 99999;


    public Dijkstra()
    {
    }

    public void Init(int mapWidth, int mapHeight, Tile[,] tiles)
    {
        this.mapWidth = mapWidth;
        this.mapHeight = mapHeight;
        this.tiles = tiles;

        int MAX_VERTICES = mapWidth * mapHeight;
        costAtoB = new int[MAX_VERTICES, MAX_VERTICES];  // a -> b 의 코스트
        vertexList = new Vertex[MAX_VERTICES];      // 각 정점들의 정보(전체코스트, 부모, 결과상태)

        mapSize = mapWidth * mapHeight;

        tileArrayOccupied = new bool[mapWidth, mapHeight];
        for (int i = 0; i < mapWidth; i++)
        {
            for (int j = 0; j < mapHeight; j++)
            {
                tileArrayOccupied[i,j] = false;

                /// 바위 타일이면 갈 수 없다 
                Tile tile = tiles[i, j];
                tileArrayOccupied[i, j] = tile.isRock;
            }
        }
    }

    // 타일의 정보를 받아서 다익스트라 알고리즘에 필요한 자료구조(vertexList, adj)를 초기화 하는 함수이다.
    // 유닛의 이동가능 타일을 계산할 때마다 TileMaker 클래스에서 호출된다.
    private void InputTileInfo(Animal animal)
    {
        validTileList.Clear();

        for (int i = 0; i < mapWidth ; i++)
        {
            for (int j = 0; j < mapHeight ; j++)
            {
                CalTileCost(i, j, tiles[i, j], animal);
            }
        }

        for(int i = 0; i < mapSize; i++)
        {
            vertexList[i] = new Vertex();
        }
    }

    // 해당 타일의 코스트를 계산해서 adj 행렬을 채우는 함수이다. 
    // InputTileInfo에서 한번호출된다.
    private void CalTileCost(int desti, int destj, Tile tile, Animal animal)
    {
        int srci;
        int srcj;

        for(int i = 0; i < 4; i++)
        {
            srci = desti + directionX[i];
            srcj = destj + directionY[i];

            int srcIndex  = srci + srcj  * mapWidth; // 조심
            int destIndex = desti+ destj * mapWidth; // 조심

            if (srcIndex < 0 || srcIndex > (mapSize - 1) || destIndex > (mapSize - 1) )
                continue;

            costAtoB[srcIndex, destIndex] = animal.GetMovementCostBy(tile);
        }
    }
    
    // 시작 정점의 인덱스가 주어지면 그곳에서 맵 전체의 타일로의 코스트가 계산된다.
    // vertexList가 업데이트가 된다.
    // s는 스타트 지점이다.
    private void DijkstraPathFinder(int startIndex, int remainMoveCount)
    {
        int v, c;
        int x = startIndex % mapHeight;
        int y = startIndex / mapHeight;

        for (v = 0; v < vertexLength; v++)
        {
            vertexList[v].status = TEMPORARY;
            vertexList[v].pathLength = INFINITY;
            vertexList[v].predecessor = NIL;
        }
        // remainMoveCount의 크기에 따라 두가지의 방법으로 vertexList를 채운다.
        // remainMoveCount가 맵의 크기보다 작을때는 더 작은 부위를 탐색하게 하였다.
        // 그러나 remainMoveCount가 맵의 크기보다 커질때는 이제 문제가 생긴다.
        // 이 작은 부위가 더이상 작은부위가아니고 맵보다 커지기 때문이다. 
        // 그러므로 이제 맵전체 (400) 을써야 한다.
        // remainMoveCount가 맵의 절반의크기보다 작다면 연산을 줄일 수 있는 방법(이 영역을 임시영역이라고 부를래)
        if (2 * remainMoveCount + 1 < mapHeight)
        {
            for (int i = 0; i < 2 * remainMoveCount + 1; i++)
            {
                for (int j = 0; j < 2 * remainMoveCount + 1; j++)
                {
                    int temp = i + j * (2 * remainMoveCount + 1);
                    // 범위가 바깥으로 넘어가면 -1로하고 PERMANENT라고 박는다.
                    if (x + i - remainMoveCount < 0 || x + i - remainMoveCount >= mapWidth || y + j - remainMoveCount < 0 || y + j - remainMoveCount >= mapHeight)
                    {
                        vertexList[temp].index = -1;
                        vertexList[temp].status = PERMANENT;
                    }
                    else
                    {
                        vertexList[temp].index = startIndex - remainMoveCount * (1 + mapHeight) + i + j * mapWidth;   // 임시영역의 내부만 인덱스를 구하면된다.
                    }
                }
            }
            // 이동할 동물이 있는 자리는 이동코스트가 0이겠지 자기 자리니까.
            vertexList[remainMoveCount * (2 * remainMoveCount + 1) + remainMoveCount].pathLength = 0;
        }
        else  // remainMoveCount가 맵의 절반의 크기보다 크다면 맵을 전체를 탐지하는게 더 효율적인 방법
        {
            for (int i = 0; i < mapHeight; i++)
            {
                for (int j = 0; j < mapWidth; j++)
                {                    
                    vertexList[i + j * mapHeight].index = TileChunk.instance.tiles[i,j]._tileIndex;   //index에 실제 타일의 인덱스를 부여함.   
                }
            }
            // 이동할 동물이 있는 자리는 이동코스트가 0이겠지 자기 자리니까.
            vertexList[startIndex].pathLength = 0;
        }
        while (true)
        {            
            // c : 최저코스트가 정해져있는 타일, c는 타일의 인덱스가 아닌 vertexList 배열의 인덱스이다.
            c = TempVertexMinPL();
            // 우선순위큐로 전환해야 하는 부분.

            if (c == NIL)
                return; 

            // c 는 최저코스트가 정해졌다고 선언한다. 
            vertexList[c].status = PERMANENT;

            for(v = 0; v < vertexLength; v++)
            {
                // c.index 에서 v.index로 가는 코스트가 기존의 v로가는 코스트보다 적은가... 
                if (vertexList[v].index == -1)
                    continue;

                if (IsAdjacent(vertexList[c].index, vertexList[v].index) &&                                   // c와 v가 인접해있는가?
                    !tileArrayOccupied[vertexList[v].index % mapHeight, vertexList[v].index / mapHeight] &&   // 해당 타일이 다른 동물이나 어떤것에의해서 점령당하고있는가...
                    vertexList[v].status == TEMPORARY)
                {
                    if (vertexList[c].pathLength + CalculateCost(c, v) < vertexList[v].pathLength)
                    {
                        vertexList[v].predecessor = c;
                        vertexList[v].pathLength = vertexList[c].pathLength + costAtoB[vertexList[c].index, vertexList[v].index];
                    }
                }
            }
        }
    }

    // 우선 c, v가 IsAdjacent라는 함수에서 인접해있다는 것으로 판명이 된상태에서 진행한다고 생각한다.
    // c, v 가 대각선인지 확인해본다.
    // 대각선이라면 둘이 인접해있고, index의 차이의 abs의 합이 2이다. 
    private int CalculateCost(int c, int v)
    {
        int IndexGap = Math.Abs(vertexList[c].index - vertexList[v].index);

        // 둘 차이의 절대값이 mapHeight + 1 이거나 mapHeight - 1 면 둘은 대각선이다. 
        if (IndexGap == mapHeight + 1 || IndexGap == mapHeight - 1)
        {
            return costAtoB[vertexList[c].index, vertexList[v].index] + 1;
        }
        else
            return costAtoB[vertexList[c].index, vertexList[v].index]; 
    }

    // 현재는 전수조사한다. 우선순위큐로 바꿔야한다.
    private int TempVertexMinPL()
    {
        int min = INFINITY;
        int x = NIL;
        
        for (int v = 0; v < vertexLength; v++)
        {
            if(min > vertexList[v].pathLength && vertexList[v].status == TEMPORARY)
            {
                min = vertexList[v].pathLength;
                x = v;
            }            
        }

        return x;
    }

    public void FindPaths(Animal animal, int moveRange)
    {
        InputTileInfo(animal);

        int startTileX = animal.ownedTile.TileIndex_X;
        int startTileY = animal.ownedTile.TileIndex_Y;
        int startTileIndex = startTileX + startTileY * mapWidth;
        remainMoveCount = moveRange;
        vertexLength = Mathf.Clamp((2 * remainMoveCount +1) * (2 * remainMoveCount +1), 1, mapSize) ;   // vertexLength가 최소 1이 보장됨 또한 최대 mapSize 보장됨
        DijkstraPathFinder(startTileIndex, remainMoveCount);

        for (int v=0; v < vertexLength; v++)
        {
            if(vertexList[v].pathLength == INFINITY)
            {
               // Debug.Log("There is no path from " + s + " to vertex ");
            }
            else
            {
                FindPath(startTileIndex, v);
            }
        }               
    }

    // 출발지점(s)에서 도착지점(v)의 최단거리를 출력한다.
    private void FindPath(int startIndex, int v)
    {
        // count가 vertexLenth보다 커지는 경우가 생기면 IndexOutOfRange 예외가 발생한다. 최소 2개는 만들도록 한다 
        int[] path = new int[vertexLength + 1];
        int accumulateCost = 0;
        int count = 0;

        while (vertexList[v].index != startIndex)
        {
            count++;
            path[count] = vertexList[v].index;
            int predecessorIndex = vertexList[v].predecessor;
            accumulateCost += costAtoB[vertexList[predecessorIndex].index, vertexList[v].index];
            v = predecessorIndex;
        }

        count++;    
        path[count] = startIndex;
        path[0] = count;
        if (accumulateCost <= remainMoveCount)
        {
            validTileList.Add(path);
        }
    } 

    public List<int[]> LoadValidTiles()
    {
        return validTileList;  
    }

    // 타일의 인덱스만으로 인접함을 알 수 있다. 
    // 가로 세로 차이의 합이 1이면 세로 인접 혹은 가로 인접이라고 말할 수 있다.
    private bool IsAdjacent(int c, int v)
    {
        int cx = c % mapWidth;  // 가로좌표
        int cy = c / mapWidth;  // 세로좌표
        int vx = v % mapWidth;
        int vy = v / mapWidth;

        if (Math.Abs(cx-vx)+ Math.Abs(cy-vy) == 1 )
        {
            return true;
        }
        return false;
    }

    public bool IsTileValidForMove(int tileIndex)
    {
        foreach (int[] path in validTileList)
        {
            if (path[1] == tileIndex) return true;
        }

        return false;
    }
}