﻿using UnityEngine;
using System.Collections;

public class EffectCamera : CacheMonobehavior 
{
    public static EffectCamera instance;

    bool stopFollowing;

    // unity functions ---------------------------------------------------
    void Awake()
	{
        instance = this;
    }

    void Start()
    {
        GetComponent<Camera>().depthTextureMode |= DepthTextureMode.Depth;
    }


    // public functions ---------------------------------------------------
    public void FollowMainCamera(float followingTime)
    {
        elapsed = 0f;
        StartCoroutine(FollowMainCameraCoroutine(followingTime));
    }

    bool isFollwingMainCamera = false;
    float elapsed = 0f;
    IEnumerator FollowMainCameraCoroutine(float followingTime)
    {
        if (isFollwingMainCamera) yield break;

        isFollwingMainCamera = true;

        while(true)
        //while (elapsed < followingTime)
        { 
            transform.position = Camera.main.transform.position;

            elapsed += Time.deltaTime;

            yield return null;
        }

        isFollwingMainCamera = false;
    }
}
