﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EllipseCollider : MonoBehaviour {

    [Range(1, 10)]
    public float radiusX = 1, radiusY = 2;

    [Range(10,90)]
    public int smoothness = 30;

    [Range(0, 180)]
    public int rotation = 0;
    
    Vector2 origin, center;
    float positionX = 0;
    float positionY = 0;

    public Vector2[] getPoints(Vector2 off)
    {
        List<Vector2> points = new List<Vector2>();

        origin.x = positionX;
        origin.y = positionY;
        center = origin + off;
 
        float ang = 0;
        float o = rotation * Mathf.Deg2Rad;

        for (int i = 0; i <= smoothness; i++)
        {
            float a = ang * Mathf.Deg2Rad;
            float x = center.x + 50 * radiusX * Mathf.Cos(a) * Mathf.Cos(o) - 50 * radiusY * Mathf.Sin(a) * Mathf.Sin(o);
            float y = center.y - 50 * radiusX * Mathf.Cos(a) * Mathf.Sin(o) - 50 * radiusY * Mathf.Sin(a) * Mathf.Cos(o);

            points.Add(new Vector2(x, y));
            ang += 360f/smoothness;
        }

        return points.ToArray();
    }

    public void SetPosition(float x, float y)
    {
        positionX = x;
        positionY = y;
    }

    public void SetProperties(float colliderRadiusX, float colliderRadiusY, int colliderRotation)
    {
        radiusX = colliderRadiusX;
        radiusY = colliderRadiusY;
        rotation = colliderRotation;
    }
}