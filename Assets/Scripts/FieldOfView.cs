﻿using System;
using System.Collections.Generic;

public class FieldOfView
{
    public TileChunk tileChunk;

    ShadowLine shadowLine = new ShadowLine();

    public void Refresh(Vec pos, int sight, List<Tile> tileListInSight = null, bool changeTileVisible = true)
    {
        // Sweep through the octants.
        for (var octant = 0; octant < 8; octant++)
        {
            RefreshOctant(pos, octant, sight, tileListInSight, changeTileVisible);
        }

        // The starting position is always visible.
        Tile currentTile = tileChunk.tiles[pos.x, pos.y];
        if (changeTileVisible) currentTile.isVisible = true;
        if (tileListInSight != null && !tileListInSight.Contains(currentTile)) tileListInSight.Add(currentTile);
    }


    public void RefreshOctant(Vec start, int octant, int maxRows = 999, List<Tile> tileListInSight = null, bool changeTileVisible = true) 
    {
        shadowLine.shadows.Clear();

        //var bounds = _demo.tiles.bounds;
        var fullShadow = false;

        // Sweep through the rows ('rows' may be vertical or horizontal based on
        // the incrementors). Start at row 1 to skip the center position.
        // 부등호가 < 으로 되어있으면 sight 0,1의 결과가 같아진다. sight가 1이라면 자신을 제외한 주위 타일 한 칸을 밝히는게 맞다. 그래서 <=로 바꿨다
        for (var row = 1; row<=maxRows; row++)  
        {
            // If we've gone out of bounds, bail.
            /// octant 검색에서 row는 기준점에서부터 한줄한줄 퍼져나가는 것을 의미한다
            /// row의 첫번째가 바은드에 걸린다면 그 줄 전체가 바운드에 걸린다는 것을 의미한다  
            Vec rowPos = start + Octant.TransformOctant(row, 0, octant);
            if (!tileChunk.IsValidTileIndex(rowPos.x, rowPos.y)) break;

            for (var col = 0; col <= row; col++)
            {
                var pos = start + Octant.TransformOctant(row, col, octant);

                // If we've traversed out of bounds, bail on this row.
                // note: this improves performance, but works on the assumption that
                // the starting tile of the FOV is in bounds.
                /// 위의 row 바운드 체크와 비슷하게 col을 진행하다가 바운드에 걸리면 그 이후에 col을 전부 무시한다 
                /// octants 예제를 보면 각 분할이 어떤 식으로 퍼져나가는지 확인해보면 이해하기 쉽다 
                /// 캐릭터 기준으로 매끄러운 선쪽이 row의 시작이며, col은 그곳에서부터 계단처럼 울퉁불퉁한 면으로 하나씩 퍼져나간다
                if (!tileChunk.IsValidTileIndex(pos.x, pos.y)) break;

                // If we know the entire row is in shadow, we don't need to be more
                // specific.
                if (fullShadow)
                {
                    /// 타일의 visible을 false할 수 있는 권한은 tileChunk가 가지고 있다 
                    /// RefreshFieldOfView를 할 때 모든 타일의 visible을 false 시키고 모든 애니멀들의 시야를 돌면서 true로만 바꿔주는 작업을 한다 
                    /// 어떤 동물에게는 보이는 타일이지만, 어떤 동물에게는 안 보이는 타일이라고 false시키지 않는다 
                    /// 어떤 동물에게 보이는 시야라면, 다른 동물에게도 보여져야 한다 
                    //tileChunk.tiles[pos.x, pos.y].isVisible = false;
                }
                else
                {
                    /// 타일 위에 씌울 그림자 타일을 만듬
                    var projection = GetProjectTile(row, col);

                    Tile tile = tileChunk.tiles[pos.x, pos.y];

                    // Set the visibility of this tile.
                    /// 해당 타일이 다른 그림자들안에 없다면, 보이는 타일이다
                    var visible = !shadowLine.IsInShadow(projection);

                    // Add any opaque tiles to the shadow map.
                    /// 엄폐물 설치. 엄폐물을 심으면 이거 너머에 있는 다른 타일들은 보이지 않게 된다 
                    if (visible && (tile.isRock || tile.type == TileMaker.TileType.FOREST))
                    {
                        shadowLine.Add(projection);

                        /// fullShadow가 되는 경우는 극히 드뭄 
                        /// 사방이 둘려쌓여서 그림자가 하나가 되고, 그 범위가 전체 범위일 때 가능함 
                        fullShadow = shadowLine.isFullShadow;
                    }

                    if (visible)
                    {
                        do
                        {
                            /// 숲 타일은 바로 옆에 있는 타일이 아니면 절대 볼 수 없게 만든다 
                            /// 이 처리를 안하면, 시야에서 가장 먼저 보이는 숲이 visible true가 되게 된다
                            /// 이 알고리즘이 그림자를 만들게 하는 엄폐물은 visible true로 판정하기 때문이다  
                            /// 숲 타일의 경우 그림자를 만드는 엄폐물 역할도 하면서도, visible은 false여야 한다 
                            if (tile.type == TileMaker.TileType.FOREST && !(row == 1 && col == 0)) break;

                            if (changeTileVisible) tile.isVisible = visible; /// Dart에서는 2차원 배열에 인자를 한번에 Vec으로 넘길 수 있군 
                            if (tileListInSight != null && !tileListInSight.Contains(tile)) tileListInSight.Add(tile);
                        }
                        while (false);
                    }
                }
            }
        }
    }

    /// Creates a [Shadow] that corresponds to the projected silhouette of the
    /// given tile. This is used both to determine visibility (if any of the
    /// projection is visible, the tile is) and to add the tile to the shadow map.
    ///
    /// The maximal projection of a square is always from the two opposing
    /// corners. From the perspective of octant zero, we know the square is
    /// above and to the right of the viewpoint, so it will be the top left and
    /// bottom right corners.
    Shadow GetProjectTile(int row, int col)
    {
        // The top edge of row 0 is 2 wide.
        var topLeft = (float)col / (float)(row + 2);

        // The bottom edge of row 0 is 1 wide.
        var bottomRight = (float)(col + 1) / (float)(row + 1);

        /// 그림자끼리의 contains를 체크할 때, 위 두 값만을 가지고 체크한다
        /// 단지 row, col 순서 하나만으로 이를 예측한다는 것이다
        /// octant의 참조도 없고, 캐릭터의 시야 기준점을 전혀 주지도 않았는데도 이 계산값을 바로 사용한다 
        /// octant의 모형을 보면 캐릭터 기준으로 마치 시야가 뻗어나가는 것처럼 생겼다 
        /// 시야 거리(row) 20까지 위의 두 값이 어떻게 매겨지나 확인해봤다.
        /// row:1, col:0             start:0.00, end:0.50
        /// row:1, col:1             start:0.33, end:1.00
        /// row:2, col:0             start:0.00, end:0.33
        /// row:2, col:1             start:0.25, end:0.67
        /// row:2, col:2             start:0.50, end:1.00
        /// row:3, col:0             start:0.00, end:0.25
        /// row:3, col:1             start:0.20, end:0.50
        /// row:3, col:2             start:0.40, end:0.75
        /// row:3, col:3             start:0.60, end:1.00
        /// 위 값들을 보고 딱 드는 생각이 '시야의 영향력'이라는 느낌이 들었다
        /// 0,0 기준점에 가까울수록 범위가 굉장히 컸으며, 멀수록 각 타일이 가지는 영향력이 점점 작아졌다 
        /// 그 수만큼 각 위치(row,col)들이 시야 영향력을 나눠 가지는 느낌이었다
        /// 내가 만약 1,0에 벽을 세웠다면, 이 벽에 영향력은 0.0~0.5다
        /// 이 범위안에 완벽히 들어가는 모든 위치는 이 벽에 가려진다고 설명할 수 있다 
        /// 2,0 타일은 가려지고, 3,1도 가려진다. 
        /// 어떻게 이런 간단한 수식으로 시야의 영향력을 수식화한건지 참 대단하다

        return new Shadow(topLeft, bottomRight,
            new Vec(col, row + 2), new Vec(col + 1, row + 1));
    }
}


/// --------------------------------------------------------------------------------------------------------------------------------------------
class ShadowLine
{
    public List<Shadow> shadows = new List<Shadow>();

    public bool IsInShadow(Shadow projection)
    {
        // Check the shadow list.
        foreach (var shadow in shadows)
        {
            if (shadow.Contains(projection)) return true;
        }

        return false;
    }

    /// Add [shadow] to the list of non-overlapping shadows. May merge one or
    /// more shadows.
    public void Add(Shadow shadow)
    {
        // Figure out where to slot the new shadow in the sorted list.
        int index = 0;
        for (; index < shadows.Count; index++)
        {
            // Stop when we hit the insertion point.
            /// index그림자의 start가 신입그림자의 start보다 크다면
            if (shadows[index].start >= shadow.start) break;
        }

        // The new shadow is going here. See if it overlaps the previous or next.
        Shadow overlappingPrevious = null;
        /// index그림자의 이전 그림자의 end가 신입그림자보다 크다면 저장해놓음 
        if (index > 0 && shadows[index - 1].end > shadow.start)
        {
            overlappingPrevious = shadows[index - 1];
        }

        Shadow overlappingNext = null;
        /// 이번엔 index그림자의 start가 신입그림자의 end보다 작으면 저장해놓음
        if (index < shadows.Count && shadows[index].start < shadow.end)
        {
            overlappingNext = shadows[index];
        }

        /// 그림자가 unity되는 상황은 어떻게 해야 가능할까
        /// 기존 그림자의 영향력 안에 완벽히 들어가는 타일은 그림자로 판단되고 이 함수를 호출하지 않는다
        /// 기존 그림자들의 영향력 안에 들어있지 않으면서 새로운 그림자일 때만 들어온다 
        /// 먼저 _shadows의 순서는 0,0에 가까울수록 index가 낮다. 0,0에 가까울수록 먼저 타일을 검사하기 때문이다
        /// 즉, 처음 판정된 index그림자보다 이전의 그림자라는 것은 0,0에 더 가까운 그림자라는 뜻이다
        /// 이 예제는 렌더링할 때마다 FOV를 새로 new하기 때문에 _shadows가 유지되지 않는다 
        

        // Insert and unify with overlapping shadows.
        if (overlappingNext != null)
        {
            if (overlappingPrevious != null)
            {
                // Overlaps both, so unify one and delete the other.
                overlappingPrevious.end = overlappingNext.end;
                overlappingPrevious.endPos = overlappingNext.endPos;
                shadows.RemoveAt(index);
            }
            else
            {
                // Only overlaps the next shadow, so unify it with that.
                overlappingNext.start = shadow.start;
                overlappingNext.startPos = shadow.startPos;
            }
        }
        else
        {
            if (overlappingPrevious != null)
            {
                // Only overlaps the previous shadow, so unify it with that.
                overlappingPrevious.end = shadow.end;
                overlappingPrevious.endPos = shadow.endPos;
            }
            else
            {
                // Does not overlap anything, so insert.
                shadows.Insert(index, shadow);
            }
        }
    }

    public bool isFullShadow { get { return shadows.Count == 1 && shadows[0].start == 0 && shadows[0].end == 1; } }
}


/// --------------------------------------------------------------------------------------------------------------------------------------------
/// Represents the 1D projection of a 2D shadow onto a normalized line. 
/// In other words, a range from 0.0 to 1.0.
class Shadow
{
    public float start;
    public float end;

    public Vec startPos;
    public Vec endPos;

    public Shadow(float start, float end, Vec startPos, Vec endPos)
    {
        this.start = start;
        this.end = end;
        this.startPos = startPos;
        this.endPos = endPos;
    }

    /// Returns `true` if [other] is completely covered by this shadow.
    public bool Contains(Shadow other)
    {
        /// 다른 그림자녀석이 이 그림자에 완벽히 가려진다면 true로 반환
        /// 그렇다면 start, end는 그림자의 범위를 나타낸다는 얘기가 된다 
        return start <= other.start && other.end <= end;
    }
}


/// --------------------------------------------------------------------------------------------------------------------------------------------
public struct Vec
{
    public int x, y;

    public static Vec operator +(Vec a, Vec b)
    {
        return new Vec(a.x + b.x, a.y + b.y);
    }

    public Vec(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
}


/// --------------------------------------------------------------------------------------------------------------------------------------------
class Octant 
{
    static public Vec TransformOctant(int row, int col, int octant)
    {
        switch (octant)
        {
            case 0: return new Vec(col, -row);
            case 1: return new Vec(row, -col);
            case 2: return new Vec(row, col);
            case 3: return new Vec(col, row);
            case 4: return new Vec(-col, row);
            case 5: return new Vec(-row, col);
            case 6: return new Vec(-row, -col);
            case 7: return new Vec(-col, -row);
            default: return new Vec(-1, -1);
        }
    }
}
