﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FishManager : MonoBehaviour {

    public static FishManager instance;
 
    public GameObject fishPrefab;

    [SerializeField]
    GameObject fishGroup;

    public static int tankSize = 350;
    public static Vector3 mapCenterPos;

    static int numFish = 20;

    public static GameObject[] allFish = new GameObject[numFish];
    WaitForSeconds goalUpdate = new WaitForSeconds(10.0f);

    public static Vector3 goalPos = Vector3.zero;

    public List<Vector3> waterTilesPos;

    private void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start () {
        mapCenterPos = new Vector3(Tile.TILE_WORLD_WIDTH * TileChunk.instance.tileSizeWidth / 2,
                            -25,
                            -Tile.TILE_WORLD_HEIGHT * TileChunk.instance.tileSizeHeight / 2);
        // StartCoroutine(ResetGoal());

        TileChunk.instance.OnCreatedTiles += CreateFishes;
    }

    public void AddGoal(int x, int y)
    {
        waterTilesPos.Add(new Vector3(x * Tile.TILE_WORLD_HEIGHT - Tile.TILE_WORLD_HEIGHT/2, -25, -y * Tile.TILE_WORLD_WIDTH + Tile.TILE_WORLD_WIDTH / 2));
    }

    void CreateFishes()
    {
        for(int i = 0; i < TileMaker.instance.tileSize; i++)
        {
            for(int j = 0; j < TileMaker.instance.tileSize; j++)
            {
                if (TileChunk.instance.tiles[i, j].type == TileMaker.TileType.RIVER) {
                    allFish[i] = (GameObject)Instantiate(fishPrefab, TileChunk.instance.tiles[i, j].position, Quaternion.identity);
                    allFish[i].transform.rotation = Quaternion.AngleAxis(Random.Range(0,90), Vector3.up);
                    allFish[i].transform.position = new Vector3(allFish[i].transform.position.x, -25, allFish[i].transform.position.z);
                    allFish[i].transform.SetParent(fishGroup.transform);
                }
            }
        }        
    }

    IEnumerator ResetGoal()
    {
        while (true)
        {
            if (waterTilesPos.Count > 0)
            {
                goalPos = waterTilesPos[Random.Range(0, waterTilesPos.Count)];
            }
            yield return goalUpdate;
        }
    }
}
