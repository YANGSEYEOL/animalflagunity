﻿using UnityEngine;
using System.Collections;

public class Flock : MonoBehaviour {

    public float speed = 5f;
    float rotationSpeed = 5;
    Vector3 averageHeading;
    Vector3 averagePosition;
    float neighbourDistance = 20f;

    MeshRenderer meshRenderer;
    bool isVisible
    {
        get
        {
            return meshRenderer.enabled;
        }

        set
        {
            meshRenderer.enabled = value;
        }
    }

    // Use this for initialization
    void Start () {
        meshRenderer = GetComponentInChildren<MeshRenderer>();
        speed = Random.Range(10, 20);
        //StartCoroutine(ApplyRuleRandomly());
        StartCoroutine(RotateFish());
    }

    bool turning =false;
    float timeStep = 0;
    void Update()
    {
        if (TileChunk.instance.IsValidTilePos(transform.position.x, transform.position.z))
        {
            // 안개에 위치하면 렌더러를꺼줌
            bool tileVisible = TileChunk.instance.GetTileWithPosition(transform.position).isVisible;
            if (isVisible != tileVisible) isVisible = tileVisible;
        }

        if (transform.position.x <= 0 || transform.position.z >= 0 ||
            transform.position.x >= Tile.TILE_WORLD_WIDTH  * TileChunk.instance.tileSizeWidth ||
            transform.position.z <= -Tile.TILE_WORLD_HEIGHT * TileChunk.instance.tileSizeHeight ||
            TileChunk.instance.GetTileWithPosition(transform.position).type != TileMaker.TileType.RIVER || 
            TileChunk.instance.IsValidTilePos(transform.position.x, transform.position.z))
        {
            turning = true;
        }
        else
            turning = false;

        // 벽에 부딪혔을때
        if (turning)
        {
            transform.Translate(0, 0, -10 * Time.deltaTime * speed);
            Vector3 direction = TileChunk.instance.GetTileWithPositionForFish(transform.position).position - transform.position;
            direction.y = 0;
            transform.rotation = Quaternion.LookRotation(direction);
            speed = Random.Range(5, 10);
        }
        // 벽에 부딪히지않고있을 경우
        else
        {
            if (Random.Range(0, 5) < 1)
                ApplyRules();
        }
        
        transform.Translate(0,0, Time.deltaTime * speed);
    }

    WaitForSeconds RotateFishWait = new WaitForSeconds(1.0f);
    IEnumerator RotateFish()
    {
        while (true)
        {
            if (!turning)
            {
                float angle = Random.Range(0, 2) < 1 ? 15 : -15;
                if (transform.position.y >= -5)
                {
                    angle = 15;   
                }
                else if(transform.position.y <= -40)
                {
                    angle = -15;
                }
                iTween.RotateTo(gameObject, iTween.Hash("x", angle, "easeType", iTween.EaseType.linear, "time", 0.5f));

            }
            yield return RotateFishWait;
        }
    }

    WaitForSeconds flockRuleInterval = new WaitForSeconds(1);

    IEnumerator ApplyRuleRandomly()
    {
        while (true)
        {
            ApplyRules();
            yield return flockRuleInterval;
        }
    }

    void ApplyRules()
    {
        GameObject[] fishes;
        fishes = FishManager.allFish;

        Vector3 vcentre = Vector3.zero;
        Vector3 vavoid = Vector3.zero;

        float groupSpeed = 0.1f;
        Vector3 goalPos = FishManager.goalPos;

        float dist;

        int groupSize = 0;
        foreach(GameObject fish in fishes)
        {
            if (fish == null)
                continue;
            if(fish != this.gameObject)
            {
                dist = Vector3.Distance(fish.transform.position, this.transform.position);
                if(dist <= neighbourDistance)
                {
                    vcentre += fish.transform.position;
                    groupSize++;

                    // 이거리가 클수록 무리끼리 떨어져있는다..
                    if(dist < 10f)
                    {
                        vavoid = vavoid + (this.transform.position - fish.transform.position);
                    }

                    Flock anotherFlock = fish.GetComponent<Flock>();
                    groupSpeed = groupSpeed + anotherFlock.speed;
                }
            }
        }

        if(groupSize > 0)
        {            
            vcentre = vcentre / groupSize + (TileChunk.instance.GetTileWithPositionForFish(transform.position).position - this.transform.position);
            speed = groupSpeed / groupSize;

            Vector3 direction = (vcentre + vavoid) - transform.position;
            direction.y = 0;
            if (direction != Vector3.zero)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
            }
        }

    }
}
