using System;
using System.Linq;
using UnityEngine;

/// <summary>
/// Manages our Fog of War data and targets (projector, image effect, or overlay plane), tracking player units, 
/// and providing callback services to the game.
/// </summary>
[Serializable]
public class FogOfWar : MonoBehaviour
{
    #region INSPECTOR FIELDS

    public FoWType FogType = FoWType.Projector;

    [System.NonSerialized]
    public Vector3 WorldMin = new Vector3(0, -50, 0);         // minimum world coordinates that should be "fogged"
    [System.NonSerialized]
    public Vector3 WorldMax = new Vector3(256, 32, 256);    // maximum world coordinates that should be "fogged"
    [System.NonSerialized]
    public float WorldUnitsPerTileSide = 1f;                // number of Unity world units per tile

    public int TextureResolution = 1;                       // 1 - 4, pixels per tile (lower = less memory, higher = less blurry)
    public bool TexturePointFilter = false;                 // if not set, texture is Bilinear filtered

    // light values --> 0.0 = black, 1.0 = white
    public float VisibleLightStrength = 1f;              // maximum light used in a "Visible" tile
    public float NightVisibleLightStrength = 0.5f;
    public float ExploredLightStrength = 0.5f;              // light value used in an "Explored" tile
    public float HiddenLightStrength = 0f;                  // light value used in a "Hidden" tile

    public bool FadeVisibility = true;                      // if set, lighting per tile will be based on distance from player units
    public bool ShowExplored = true;                        // if not set, no tiles will be made "Explored"

    public Material ProMaterialReference;

    public enum FoWType
    {
        Projector = 0,
        ProShader = 1,
        Overlay = 2,
    }

    #endregion

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    #region PRIVATE FIELDS

    [SerializeField, HideInInspector]
    private int _numTilesX;             // calculated play field size in tiles (width)

    [SerializeField, HideInInspector]
    private int _numTilesZ;             // calculated play field size in tiles (height)

    [SerializeField, HideInInspector]
    private Projector _projector;       // cache of our projector instance

    [SerializeField, HideInInspector]   // cache our plane
    private GameObject _plane;

    [SerializeField, HideInInspector]
    private Texture2D _fogTexture;    // the texture we're passing to projector

    [SerializeField, HideInInspector]
    private readonly Color32 _white = new Color32(255, 255, 255, 0);

    [SerializeField, HideInInspector]
    private readonly Color32 _black = new Color32(0, 0, 0, 0);

    [SerializeField, HideInInspector]
    private readonly Color32 _blue = new Color32(0, 0, 120, 0);

    [SerializeField, HideInInspector]
    private Color32 _colorHidden;       // calculated color (lighting) to use for hidden tiles

    [SerializeField, HideInInspector]
    private Color32 _colorExplored;     // calculated color (lighting) to use for explored tiles

    [SerializeField, HideInInspector]
    private Color32 _colorVisible;   // calculated color (lighting) to use for visible tiles (max)

    private FoWCallbacks _callbacks;    // cache of our callback instance to avoid multiple Finds

    private static FogOfWar _instance;    // stored instance of this class (to avoid multiple "FindObject" calls)

    private TileChunk tileChunk;

    #endregion

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    #region UNITY STANDARD METHODS

    /// <summary>
    /// Standard Unity method.
    /// </summary>
    public void Reset()
    {
        FogType = FoWType.Projector;
        WorldMin = new Vector3(0, 0, 0);
        WorldMax = new Vector3(256, 32, 256);
        WorldUnitsPerTileSide = 1f;
        TextureResolution = 1;
        VisibleLightStrength = 1f;
        ExploredLightStrength = 0.5f;
        HiddenLightStrength = 0f;
        FadeVisibility = true;
        ShowExplored = true;
    }

    /// <summary>
    /// Standard Unity method.
    /// </summary>
    public void Start()
    {
        tileChunk = FindObjectOfType<TileChunk>();
    }

    #endregion

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    #region PUBLIC METHODS

    /// <summary>
    /// Convenience method to find the *single* instance of FoWManager in the game scene.
    /// </summary>
    /// <returns>Instance of FoWManager if found</returns>
    public static FogOfWar FindInstance()
    {
        return _instance ?? (_instance = FindObjectOfType<FogOfWar>());
    }

    public Texture2D GetTexture()
    {
        Initialize();

        return _fogTexture;
    }

    /// <summary>
    /// Enables or disables fog of war projector.
    /// </summary>
    /// <param name="enableFoW">true to enable, false to disable</param>
    public void EnableFogOfWar(bool enableFoW)
    {
        _projector.enabled = enableFoW;
    }

    public void RefreshTexture()
    {
        ///Initialize();

        for (var x = 0; x < _numTilesX; x++)
        {
            for (var z = 0; z < _numTilesZ; z++)
            {
                UpdateTile(x, z);
            }
        }

        _fogTexture.Apply();
    }

    public void OnSwitchedDayAndNight(DayAndNight.TimeOfNature switchedTime)
    {
        switch (switchedTime)
        {
            case DayAndNight.TimeOfNature.DAY:
                _colorExplored = Color32.Lerp(_black, _white, ExploredLightStrength);
                _colorVisible = Color32.Lerp(_black, _white, VisibleLightStrength);
                break;
            case DayAndNight.TimeOfNature.NIGHT:
                _colorExplored = Color32.Lerp(_black, _white, HiddenLightStrength);
                _colorVisible = Color32.Lerp(_blue, _white, NightVisibleLightStrength);
                break;
        }

        RefreshTexture();
    }

    #endregion

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    #region SETUP METHODS

    /// <summary>
    /// Initializes our instance if not already initialized
    /// </summary>
    public void Initialize()
    {
        //

        _callbacks = GetComponent<FoWCallbacks>();

        InitializeSizes();

        InitializeColors();

        SetupFogTarget();

        InitializeTexture();

        //
    }

    private void SetupFogTarget()
    {
        var projector = transform.FindChild("FoWProjector");
        if (projector != null && FogType == FoWType.Projector)
        {
            projector.gameObject.SetActive(true);

            _projector = transform.GetComponentInChildren<Projector>();
            if (_projector != null)
            {
                _projector.enabled = true;

                _projector.orthographic = true;
                _projector.orthographicSize = (_numTilesZ * WorldUnitsPerTileSide * 0.5f);
                _projector.aspectRatio = _numTilesX / (float)_numTilesZ;

                _projector.farClipPlane = (WorldMax.y - WorldMin.y) + 10;
                
                transform.position = new Vector3(
                    WorldMin.x + ((WorldMax.x - WorldMin.x) * 0.5f),
                    WorldMax.y + 5,
                    -(WorldMin.z + ((WorldMax.z - WorldMin.z) * 0.5f))
                    );
            }
        }

        //

        var planeTransform = transform.FindChild("FoWOverlay");
        if (planeTransform != null)
        {
            planeTransform.position = new Vector3(
                WorldMin.x + ((WorldMax.x - WorldMin.x) * 0.5f),
                WorldMax.y,
                -(WorldMin.z + ((WorldMax.z - WorldMin.z) * 0.5f)) 
                );

            planeTransform.localScale = new Vector3((WorldMax.x - WorldMin.x) * 0.1f, 1f, -(WorldMax.z - WorldMin.z) * 0.1f);

            _plane = planeTransform.gameObject;

            if (FogType == FoWType.Overlay)
            {
                _plane.SetActive(true);
            }
        }

        //

        var fogProScripts = FindObjectsOfType(typeof(FoWPro));
        if (FogType == FoWType.ProShader)
        {
            if (fogProScripts.Length == 0)
            {
                // auto add the script
                var cam = Camera.main;
                if (cam != null)
                {
                    var fowp = cam.gameObject.AddComponent<FoWPro>();
                    fowp.Material = ProMaterialReference;
                }
            }
        }
        else
        {
            foreach (var scr in fogProScripts)
            {
                var fpscr = scr as FoWPro;
                if (fpscr != null)
                {
                    fpscr.enabled = false;
                }
            }
        }
    }

    private void InitializeSizes()
    {
        var worldSizeX = WorldMax.x - WorldMin.x;
        var worldSizeZ = WorldMax.z - WorldMin.z;
        _numTilesX = (int)(worldSizeX / WorldUnitsPerTileSide);
        _numTilesZ = (int)(worldSizeZ / WorldUnitsPerTileSide);
    }

    private void InitializeColors()
    {
        _colorHidden = Color32.Lerp(_black, _white, HiddenLightStrength);
        _colorExplored = Color32.Lerp(_black, _white, ExploredLightStrength);
        _colorVisible = Color32.Lerp(_black, _white, VisibleLightStrength);
    }

    private void InitializeTexture()
    {
        ClampTextureResolution();

        var textureSizeX = _numTilesX * TextureResolution;
        var textureSizeZ = _numTilesZ * TextureResolution;

        if (_fogTexture == null || _fogTexture.filterMode != (TexturePointFilter ? FilterMode.Point : FilterMode.Trilinear))
        {
            _fogTexture = new Texture2D(textureSizeX, textureSizeZ, TextureFormat.ARGB32, false)
            {
                wrapMode = TextureWrapMode.Clamp,
                filterMode = TexturePointFilter ? FilterMode.Point : FilterMode.Trilinear
            };
        }
        else
        {
            if (_fogTexture.width != textureSizeX || _fogTexture.height != textureSizeZ)
            {
                _fogTexture.Resize(textureSizeX, textureSizeZ);
            }
        }

        for (var x = 0; x < textureSizeX; x++)
        {
            for (var z = 0; z < textureSizeZ; z++)
            {
                _fogTexture.SetPixel(x, z, _colorHidden);
            }
        }
        _fogTexture.Apply();

        SetFogTargetTexture();
    }

    private void SetFogTargetTexture()
    {
        //
        // Projector - set texture
        //

        if (_projector != null)
        {
            _projector.material.SetTexture("_ShadowTex", _fogTexture);
        }

        //
        // FoWPro shader - set texture
        //

        var shaders = FindObjectsOfType(typeof(FoWPro));
        foreach (var shader in shaders)
        {
            var fowProShader = shader as FoWPro;
            if (fowProShader != null)
            {
                var hf = WorldUnitsPerTileSide * 0.5f;
                fowProShader.SetValues(_fogTexture, WorldMin.x - hf, WorldMin.z - hf, WorldMax.x - hf, WorldMax.z - hf);
            }
        }

        //
        // Plan overlay - set texture
        //

        if (_plane != null)
        {
            _plane.GetComponent<Renderer>().material.SetTexture("_MainTex", _fogTexture);
        }
    }

    #endregion

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    #region PRIVATE METHODS
        
    private void UpdateTile(int x, int z)
    {
        if (!tileChunk.IsValidTileIndex(x, z))
            return;

        var tile = tileChunk.tiles[x, z];
        if (tile == null)
            return;

        var lightColor = _colorHidden;

        if (tile.isVisible)
        {
            lightColor = _colorVisible;
            tileChunk.SetFogMaskTexture(tile, Color.white);
        }
        else if (ShowExplored && tile.isExplored)
        {
            lightColor = _colorExplored;
            tileChunk.SetFogMaskTexture(tile, Color.black);
        }

        /// 일반적인 텍스쳐 좌표 인덱스와 우리 타일 인덱스 개념이 다르기 때문에 z,x를 반대로 넣고, 프로젝터를 y축으로 90회전까지 해줘야 한다 
        SetTexturePixel(z, x, lightColor);
    }

    private void SetTexturePixel(int x, int z, Color32 lightColor)
    {
        ClampTextureResolution();

        for (var dx = 0; dx < TextureResolution; dx++)
        {
            var px = (x * TextureResolution);
            for (var dz = 0; dz < TextureResolution; dz++)
            {
                var pz = (z * TextureResolution);
                _fogTexture.SetPixel(px + dx, pz + dz, lightColor);
            }
        }
    }

    private void ClampTextureResolution()
    {
        if (TextureResolution < 1)
            TextureResolution = 1;
        else if (TextureResolution > 4)
            TextureResolution = 4;
    }

    #endregion

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    #region CALLBACK UTILITY METHODS

    /// <summary>
    /// Callback utility method
    /// </summary>
    /// <param name="x">x position</param>
    /// <param name="z">z position</param>
    private void HandleTileBecomesVisible(int x, int z)
    {
        if (_callbacks == null)
            return;

        if (_callbacks.OnTileBecomesVisible != null)
            _callbacks.OnTileBecomesVisible(x, z);
    }

    /// <summary>
    /// Callback utility method
    /// </summary>
    /// <param name="x">x position</param>
    /// <param name="z">z position</param>
    private void HandleTileFirstVisible(int x, int z)
    {
        if (_callbacks == null)
            return;

        if (_callbacks.OnTileFirstVisible != null)
            _callbacks.OnTileFirstVisible(x, z);
    }

    /// <summary>
    /// Callback utility method
    /// </summary>
    /// <param name="x">x position</param>
    /// <param name="z">z position</param>
    private void HandleTileBecomesExplored(int x, int z)
    {
        if (_callbacks == null)
            return;

        if (_callbacks.OnTileBecomesExplored != null)
            _callbacks.OnTileBecomesExplored(x, z);
    }

    /// <summary>
    /// Callback utility method
    /// </summary>
    /// <param name="x">x position</param>
    /// <param name="z">z position</param>
    private void HandleTileBecomesHidden(int x, int z)
    {
        if (_callbacks == null)
            return;

        if (_callbacks.OnTileBecomesHidden != null)
            _callbacks.OnTileBecomesHidden(x, z);
    }

    public void HandleNonPlayerUnitBecomesVisible(GameObject unit)
    {
        if (_callbacks == null)
            return;

        if (_callbacks.OnNonPlayerUnitBecomesVisible != null)
            _callbacks.OnNonPlayerUnitBecomesVisible(unit);
    }

    public void HandleNonPlayerUnitBecomesExplored(GameObject unit)
    {
        if (_callbacks == null)
            return;

        if (_callbacks.OnNonPlayerUnitBecomesExplored != null)
            _callbacks.OnNonPlayerUnitBecomesExplored(unit);
    }

    public void HandleNonPlayerUnitBecomesHidden(GameObject unit)
    {
        if (_callbacks == null)
            return;

        if (_callbacks.OnNonPlayerUnitBecomesHidden != null)
            _callbacks.OnNonPlayerUnitBecomesHidden(unit);
    }

    public void HandleRemoveGhost(GameObject unit, GameObject ghost)
    {
        if (_callbacks == null)
            return;

        if (_callbacks.OnRemoveGhost != null)
            _callbacks.OnRemoveGhost(unit, ghost);
    }

    public GameObject HandleAddGhost(GameObject unit)
    {
        if (_callbacks == null)
            return null;

        if (_callbacks.OnAddGhost != null)
            return _callbacks.OnAddGhost(unit);

        return null;
    }

    #endregion

    ////////////////////////////////////////////////////////////////////////////////////////////////////
}