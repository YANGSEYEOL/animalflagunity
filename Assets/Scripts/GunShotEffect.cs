﻿using UnityEngine;
using System.Collections;

public class GunShotEffect : CacheMonobehavior 
{
    public static GunShotEffect instance;
    


    // unity functions ---------------------------------------------------
    void Awake()
	{
        instance = this;
    }

    void Start()
    {
        
    }


    // public functions ---------------------------------------------------
    public void RotateParticle (float angle)
	{
        ParticleSystem[] systems = GetComponentsInChildren<ParticleSystem>();

        foreach (ParticleSystem system in systems)
        {
            system.startRotation += angle * Mathf.Deg2Rad;
        }
	}


    public void Play(Vector3 position)
    {
        
        transform.position = position;

        EffectCamera.instance.FollowMainCamera(4f);


        ParticleSystem[] systems = GetComponentsInChildren<ParticleSystem>();

        foreach (ParticleSystem system in systems)
        {
            system.Play();
        }
    }


    public void Stop()
    {
        ParticleSystem[] systems = GetComponentsInChildren<ParticleSystem>();

        foreach (ParticleSystem system in systems)
        {
            system.Stop();
        }
    }
    
}
