﻿using UnityEngine;
using System.Collections;

public class HardSkin : Skill
{
    public override void Init(Animal _ownAnimal)
    {
        ownAnimal = _ownAnimal;
        InitSkillDescription();
        InitSkillName();
    }

    public override void InitSkillDescription()
    {
        description = "This is HardSkin";
    }

    public override void InitSkillName()
    {
        skillName = "HardSkin";
    }

    public override void InitIsActive()
    {
        isActive = true;
    }

    public override void Do()
    {
        Debug.Log(ownAnimal.name + "used a HardSkin");
    }
}
