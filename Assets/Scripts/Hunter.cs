﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using GameDataEditor;
using Vectrosity;

public class Hunter : Animal
{
    [System.NonSerialized]
    public MeshRenderer[] meshRenderers;
    
    static GunShotEffect gunShotEffect;

    [SerializeField]
    Transform gunShotEffectPosition;

    [SerializeField]
    AudioClip weaponSound;

    [SerializeField]
    AudioClip reloadSound;

    
    AttackCommand.AttackTargetInfo targetAnimalInfo;

    public enum AttackType { AWP, AK47, SHOTGUN, AXE }
    [SerializeField]
    public AttackType attackType;

    List<AttackCommand.AttackTargetInfo> attackableAnimals = new List<AttackCommand.AttackTargetInfo>();



    protected override void Awake()
    {
        base.Awake();

        meshRenderers = GetComponentsInChildren<MeshRenderer>();
    }


    protected override void Start()
    {
        base.Start();

        /// 샷 이펙트 찾기 
        if (gunShotEffect == null)
        {
            gunShotEffect = GameObject.FindWithTag("GunShotEffect").GetComponent<GunShotEffect>();
        }

        /// 애니메이터 어택 타입 넣어주기
        animator.SetInteger("attackType", (int)attackType);

        /// 이벤트 
        OnStartMove += OnStartMoveFunc;

        /// showSightOfAI가 false면 RefreshFieldOfView를 막아놓기 때문에 헌터 생성 후, 헌터가 있는 타일의 Visible 갱신이 되지 않아 OnChangedTileVisible이 발동되지 않는다
        /// 그래서 직접 헌터의 Visible을 바꿔 놓는다 
        if (!GameManager.instance.showSightOfAI && ownedTile != null)
        {
            SetVisible(ownedTile.isVisible);
        }

        /// AI는 선택되지 못하지만, 타겟으로는 지정될 수 있다 
        GameManager.instance.OnSelectedAnimal -= OnSelectedAnimal;
        GameManager.instance.OnSetTargetAnimal += OnSetTargetAnimal;
    }


    void OnSetTargetAnimal(Animal targetAnimal)
    {
        SetHPBarVisible(targetAnimal == this);
    }


    public IEnumerator DoAI()
    {
        /// 카메라 패닝
        CameraMovement.instance.followingTarget = isVisible ? transform : null;

        /// HPBar on
        SetHPBarVisible(ownedTile.isVisible);
        
        /// AI 
        var comebackCoroutine = DisplayAllPath();
        while (comebackCoroutine.MoveNext()) yield return comebackCoroutine.Current;

        CameraMovement.instance.followingTarget = null;

        /// HPBar off
        SetHPBarVisible(false);
    }


    public IEnumerator DisplayAllPath()
    {
        if (!canAction)
            yield break;

        if (isVisible) yield return new WaitForSeconds(1.0f);

        // 공격범위 내에 들어오는 동물 리스트를 갱신한다
        AttackCommand.instance.GetAllAttackableTargetInfoList(this, ref attackableAnimals);
    
        if (attackableAnimals.Count > 0)
        {
            var attackCoroutine = DirectingAttackInMyRange();
            while (attackCoroutine.MoveNext()) yield return attackCoroutine.Current;
        }
        else
        {
            /// 이동은 공격할 대상이 없을 때, 동물들을 찾기 위해서 한다 
            TileChunk.instance.dijkstra.FindPaths(this, curremainMovement);
            List<int[]> moveableTileList = TileChunk.instance.dijkstra.LoadValidTiles();

            /// 시야 범위 안에 있는 동물이 있는지 체크 
            List<Animal> animalListInMySight = new List<Animal>();
            foreach (Animal animal in GameManager.instance.animals1ForSimulation)
            {
                /// 공격할 수 있는 대상인가 
                if (!canAttackableTarget(animal)) continue;

                /// 시야 범위 안에 들어와있는가
                bool isInMySight = false;
                foreach (Tile tileInMySight in GameManager.instance.sightOfUser[ownerUserId])
                {
                    if (tileInMySight._tileIndex == animal.ownedTile._tileIndex)
                    {
                        isInMySight = true;
                        break;
                    }
                }
                if (!isInMySight) continue;
                
                /// 추가
                animalListInMySight.Add(animal);
            }

            /// 시야안에 동물이 있는 경우
            Tile targetTile = null;
            if (animalListInMySight.Count > 0)
            {
                /// 가장 가까운 녀석 골라내기 
                Animal closeAnimal = animalListInMySight[0];
                float curDistance = 9999f; 
                foreach (Animal animal in animalListInMySight)
                {
                    float distance = Vector3.Distance(closeAnimal.ownedTile.position, ownedTile.position);
                    if (distance < curDistance)
                    {
                        curDistance = distance;
                        closeAnimal = animal;
                    }
                }

                /// 그 녀석과 가장 가까운 이동 가능한 타일 골라내기 
                float curTileDistance = 9999f;
                foreach (int[] path in moveableTileList)
                {
                    Tile tile = TileChunk.instance.tiles[path[1] % TileChunk.instance.tileSizeWidth, path[1] / TileChunk.instance.tileSizeHeight];

                    float distance = Vector3.Distance(tile.position, closeAnimal.ownedTile.position);
                    if (distance < curTileDistance)
                    {
                        curTileDistance = distance;
                        targetTile = tile;
                    }
                }
            }
            /// 시야 범위안에도 동물이 없다면 그냥 아래로 이동시키기 
            else
            {
                List<Tile> advanceTileList = new List<Tile>();
                foreach (int[] path in moveableTileList)
                {
                    int tileIndex_X = path[1] % TileChunk.instance.tileSizeWidth;
                    int tileIndex_Y = path[1] / TileChunk.instance.tileSizeHeight;

                    /// Y 인덱스가 내가 위치한 타일의 Y 인덱스보다 크면 아래에 위치한 타일이다
                    if (tileIndex_Y > ownedTile.TileIndex_Y)
                    {
                        advanceTileList.Add(TileChunk.instance.tiles[tileIndex_X, tileIndex_Y]);
                    }
                }

                /// 아래에 있는 타일들 중 하나 랜덤으로 골라서 선택 
                if (advanceTileList.Count > 0) 
                {
                    targetTile = advanceTileList[Random.Range(0, advanceTileList.Count)];
                }
            }

            // 이동
            if (targetTile != null)
            {
                int[] shortestPath = TileChunk.instance.GetShortestPathForMovement(this, targetTile._tileIndex, curremainMovement);
                
                /// 이동 path들이 전부 안개에 가려진다면 순간이동시킨다
                /// 하지만, path 중 하나라도 시야에 보여지는 타일이 있으면 무빙 연출을 보여준다 
                bool foundVisibleTileInPath = isVisible;
                if (!isVisible)
                {
                    for (int i = shortestPath[0] - 1; i >= 1; --i)
                    {
                        int pathTileIndex = shortestPath[i];

                        int tileX = pathTileIndex % TileMaker.instance.tileSize;
                        int tileY = pathTileIndex / TileMaker.instance.tileSize;
                        Tile tile = TileChunk.instance.tiles[tileX, tileY];
                        
                        if (tile.isVisible) 
                        {
                            foundVisibleTileInPath = true;
                            break;
                        }
                    }
                }

                if (!GameManager.instance.showSightOfAI && !foundVisibleTileInPath)
                {
                    ownedTile = targetTile;
                    transform.position = GetRealPositionByTile(targetTile);
                }
                else
                {
                    IEnumerator moveCoroutine = MoveIfCanMove(targetTile);
                    while (IsTrackedCoroutineRunning(moveCoroutine))
                    {
                        CameraMovement.instance.followingTarget = movingTile != null && movingTile.isVisible ? transform : null;
                        SetHPBarVisible(movingTile != null && movingTile.isVisible);

                        yield return null;
                    }
                }
            }

            if (isVisible) yield return new WaitForSeconds(1.0f);

            /// 다시 한번 공격가능한 동물이 있는지 서칭해서 공격 시도해본다 
            if (curHealth > 0)
            {
                var attackCoroutine = DirectingAttackInMyRange();
                while (attackCoroutine.MoveNext()) yield return attackCoroutine.Current;
            }
        }
    }


    IEnumerator DirectingAttackInMyRange()
    {
        // 공격범위 내에 들어오는 동물 리스트를 갱신한다
        AttackCommand.instance.GetAllAttackableTargetInfoList(this, ref attackableAnimals);
        
        // 헌터가 아닌 동물만 사냥한다.
        if (attackableAnimals.Count > 0)
        {
            targetAnimalInfo = attackableAnimals[Random.Range(0, attackableAnimals.Count)];

            /// 공격할 때는 동물 시야에 안 보이더라도 무조건 보여지게 한다 
            SetVisible(true);
            SetHPBarVisible(true);
            ownedTile.isVisible = true;
            TileChunk.instance.fogOfWar.RefreshTexture();

            // 공격대상을 표시
            TileChunk.instance.DisplayTileHighlight(targetAnimalInfo.animal.ownedTile, "TARGET_" + targetAnimalInfo.animal.photonView.viewID, Color.yellow);
            targetAnimalInfo.animal.SetHPBarVisible(true);

            /// 공격 판정 주사위 돌리기 
            AttackCommand.HitResult hitResult = AttackCommand.instance.RollForHit(targetAnimalInfo.hitResultTable, targetAnimalInfo.animal);
            int damage = AttackCommand.instance.FinalizeDamage(this, targetAnimalInfo.animal, hitResult);

            /// 타겟과 헌터 사이로 카메라를 위치시켜 둘다 보이게 만든다 
            CameraMovement.instance.followingTarget = null; /// 카메라 팔로잉 풀기 
            Camera.main.MoveTo((ownedTile.position + targetAnimalInfo.animal.ownedTile.position) / 2f);

            var attackCoroutine = Attack(targetAnimalInfo.animal.photonView.viewID, (int)hitResult, damage);
            while (attackCoroutine.MoveNext()) yield return attackCoroutine.Current;
            
            if (isVisible) yield return new WaitForSeconds(1.5f);

            /// 공격대상표시를 제거
            TileChunk.instance.UndisplayLine("TARGET_" + targetAnimalInfo.animal.photonView.viewID);
            targetAnimalInfo.animal.SetHPBarVisible(false);

            /// 강제로 보여지게 했던거 해제
            TileChunk.instance.RefreshFieldOfView();
            SetVisible(ownedTile.isVisible);
            SetHPBarVisible(ownedTile.isVisible);
        }
    }


    [PunRPC]
    public override IEnumerator Attack(int targetAnimalViewId, int hitResultInt, int damage)
    {
        if (isAttacking)
        {
            yield break;
        }

        PhotonView targetAnimalView = PhotonView.Find(targetAnimalViewId);
        if (targetAnimalView != null)
        {
            Animal targetAnimal = targetAnimalView.GetComponent<Animal>();
            canAction = false;

            /// 연출하기 전 체력 차감을 먼저 해놓는다 
            AttackCommand.HitResult hitResult = (AttackCommand.HitResult)hitResultInt;
            switch (hitResult)
            {
                case AttackCommand.HitResult.COUNTER:
                    {
                        curHealth -= damage;
                    }
                    break;
                case AttackCommand.HitResult.HIT:
                case AttackCommand.HitResult.GRAZE:
                case AttackCommand.HitResult.CRITICAL:
                    {
                        targetAnimal.curHealth -= damage;
                    }
                    break;
            }

            /// 회전 
            var lookVector = targetAnimal.ownedTile.position - ownedTile.position;
            var lookRotation = Quaternion.LookRotation(lookVector);
            transform.rotation = lookRotation;

            /// 근접공격 하는 애들은 따로 Ready 애니메이션을 만들지 않아서 코드상에서 약간 딜레이를 준다 
            if (attackType == AttackType.AXE)
            {
                yield return new WaitForSeconds(0.5f);
            }

            /// 때리는 연출 
            animator.SetBool(animalData.attackAnim, true);
            animator.SetTrigger(animalData.attackAnim + "Trigger");

            /// 타겟 때리러 접근 (근접공격이 가능한 녀석만)
            IEnumerator moveToTargetCoroutine = null;
            if (attackType == AttackType.AXE)
            {
                Vector3 destination = transform.position + (targetAnimal.transform.position - transform.position) / 2f;
                moveToTargetCoroutine = MoveTo(destination, "chop");

                /// 이 코루틴을 MoveNext()방식으로 실행하게 되면 도끼질 애니메이션이 다 끝나고 나서 타격 이펙트가 나오기 때문에 타격감이 안 산다 
                StartTrackedCoroutine(moveToTargetCoroutine);
            }

            switch (hitResult)
            {
                case AttackCommand.HitResult.COUNTER:
                    {
                        string counterAnimationName = "attackWithForepaw";
                        var targetLookVector = ownedTile.position - targetAnimal.ownedTile.position;

                        /// 뒷발로 카운터치는 동물들은 상대가 SIDE, BEHIND에 있다면 뒷발로 칠 수 있도록 반대로 회전
                        /// FRONT에 있다면 반대로 회전 안해도 된다 
                        Direction direction = targetAnimal.GetDirectionOfTarget(this);

                        if (targetAnimal.animalData.canKickingWithHeel && (direction == Direction.SIDE || direction == Direction.BEHIND))
                        {
                            counterAnimationName = "kickingWithHeel";
                            targetLookVector *= -1;
                        }

                        /// 타겟 회전 
                        var targetLookRotation = Quaternion.LookRotation(targetLookVector);
                        targetAnimal.transform.rotation = targetLookRotation;

                        /// 카운터 연출
                        AnimationClip counterAnimationClip = null;
                        foreach (var animationClip in targetAnimal.animator.runtimeAnimatorController.animationClips)
                        {
                            if (animationClip.name == counterAnimationName)
                            {
                                counterAnimationClip = animationClip;

                                break;
                            }
                        }

                        targetAnimal.animator.SetBool(counterAnimationName, true);
                        targetAnimal.animator.SetTrigger(counterAnimationName + "Trigger");

                        yield return new WaitForSeconds(counterAnimationClip.length);

                        targetAnimal.animator.SetBool(counterAnimationName, false);
                        targetAnimal.animator.ResetTrigger(counterAnimationName + "Trigger");

                        /// 타격 사운드 
                        AudioClip atkSound = GameManager.instance.attackSound_critical[Random.Range(0, GameManager.instance.attackSound_critical.Length)];
                        audioSource.PlayOneShot(atkSound, 0.3f);

                        /// 텍스트 이펙트
                        UIManager.instance.ShowHUDText(I2.Loc.ScriptLocalization.Get("COUNTER"), Color.grey, transform.position + DAMAGE_TEXT_POS_FACTOR, false);
                        UIManager.instance.ShowHUDText(damage.ToString(), Color.grey, transform.position + DAMAGE_TEXT_POS_FACTOR);

                        /// 내가 맞는 연출
                        StartCoroutine(Damaged(damage, hitResult));

                        /// 다시 내 제자리로 돌아온다 
                        if (attackType == AttackType.AXE)
                        {
                            while (IsTrackedCoroutineRunning(moveToTargetCoroutine)) yield return null; /// 가는 이동이 다 끝날 때까지 대기 

                            var comebackCoroutine = MoveToRapidly(GetRealPositionByTile(ownedTile));
                            while (comebackCoroutine.MoveNext()) yield return comebackCoroutine.Current;
                        }
                    }
                    break;

                case AttackCommand.HitResult.HIT:
                case AttackCommand.HitResult.GRAZE:
                case AttackCommand.HitResult.CRITICAL:
                    {
                        // 이펙트 시작하라는 신호 올때까지 대기
                        while (!startAttackEffect) yield return null;

                        /// 카메라 셰이크
                        float cameraShakeMagnitude = 0.8f;
                        if (hitResult == AttackCommand.HitResult.GRAZE)
                        {
                            cameraShakeMagnitude = 0.3f;
                        }
                        else if (hitResult == AttackCommand.HitResult.CRITICAL)
                        {
                            cameraShakeMagnitude = 1.5f;
                        }
                        CameraShake.instance.Shake(attakcedShakeDuration, cameraShakeMagnitude);

                        /// 타격 사운드 
                        if(attackType == AttackType.AXE)
                        { 
                            AudioClip atkSound = GameManager.instance.attackSound_hit[Random.Range(0, GameManager.instance.attackSound_hit.Length)];
                            float volume = 0.3f;
                            if (hitResult == AttackCommand.HitResult.GRAZE)
                            {
                                atkSound = GameManager.instance.attackSound_graze[Random.Range(0, GameManager.instance.attackSound_graze.Length)];
                                volume = 0.2f;
                            }
                            else if (hitResult == AttackCommand.HitResult.CRITICAL)
                            {
                                atkSound = GameManager.instance.attackSound_critical[Random.Range(0, GameManager.instance.attackSound_critical.Length)];
                                volume = 0.4f;
                            }
                            audioSource.PlayOneShot(atkSound, volume);
                        }

                        /// 이펙트
                        AttackEffect.instance.Stop();
                        AttackEffect.instance.Play(targetAnimal.ownedTile.position, hitResult);

                        /// 텍스트 이펙트
                        Color textColor = Color.red;
                        if (hitResult == AttackCommand.HitResult.GRAZE)
                        {
                            textColor = Color.white;
                            UIManager.instance.ShowHUDText(I2.Loc.ScriptLocalization.Get("GRAZE"), textColor, targetAnimal.transform.position + DAMAGE_TEXT_POS_FACTOR, false);
                        }
                        else if (hitResult == AttackCommand.HitResult.CRITICAL)
                        {
                            textColor = Color.yellow;
                            UIManager.instance.ShowHUDText(I2.Loc.ScriptLocalization.Get("CRITICAL"), textColor, targetAnimal.transform.position + DAMAGE_TEXT_POS_FACTOR, false);
                        }
                        UIManager.instance.ShowHUDText(damage.ToString(), textColor, targetAnimal.transform.position + DAMAGE_TEXT_POS_FACTOR);

                        /// 맞는 연출
                        targetAnimal.StartCoroutine(targetAnimal.Damaged(damage, hitResult));

                        /// 다시 내 제자리로 돌아온다 
                        if (attackType == AttackType.AXE)
                        {
                            while (IsTrackedCoroutineRunning(moveToTargetCoroutine)) yield return null; /// 가는 이동이 다 끝날 때까지 대기 

                            var comebackCoroutine = MoveToRapidly(GetRealPositionByTile(ownedTile));
                            while (comebackCoroutine.MoveNext()) yield return comebackCoroutine.Current;
                        }

                        /// 타겟 동물의 데미징 코루틴이 종료되어야만 빠져나간다
                        while (targetAnimal.isDamaging) yield return null;

                        startAttackEffect = false;
                    }
                    break;

                case AttackCommand.HitResult.MISS:
                    {
                        // 이펙트 시작하라는 신호 올때까지 대기
                        while (!startAttackEffect) yield return null;
                        
                        var targetLookVector = transform.position - targetAnimal.transform.position;
                        var targetLookRotation = Quaternion.LookRotation(targetLookVector);
                        targetAnimal.transform.rotation = targetLookRotation;

                        /// 어느 방향으로 피할 것인지 결정
                        Vector3 dodgeDestination = Vector3.zero;

                        int dodgeDirection = Random.Range(0, 2);
                        switch (dodgeDirection)
                        {
                            case 0: /// 왼쪽
                                dodgeDestination = targetAnimal.transform.position + ((targetAnimal.transform.right * -1f) * (Tile.TILE_WORLD_WIDTH / 2f));
                                break;
                            case 1: /// 오른쪽
                                dodgeDestination = targetAnimal.transform.position + (targetAnimal.transform.right * (Tile.TILE_WORLD_WIDTH / 2.0f));
                                break;/* /// 총으로 쏘는데 뒤로 피한다고 안 맞는게 이상하잖아. 
                            case 2: /// 뒤
                                dodgeDestination = targetAnimal.transform.position + ((targetAnimal.transform.forward * -1f) * (Tile.TILE_WORLD_WIDTH / 2.0f));
                                break;*/
                        }

                        Vector3 originPos = targetAnimal.transform.position;

                        /// 피하는 소리 
                        audioSource.PlayOneShot(GameManager.instance.missSound, 0.3f);

                        /// 텍스트 이펙트
                        UIManager.instance.ShowHUDText("MISS", Color.white, targetAnimal.transform.position + DAMAGE_TEXT_POS_FACTOR);

                        /// 피하는 연출 
                        var targetMoveToTileCoroutine = targetAnimal.MoveToRapidly(dodgeDestination);
                        while (targetMoveToTileCoroutine.MoveNext()) yield return targetMoveToTileCoroutine.Current;

                        /// 때릴려고 했던 녀석 다시 내 제자리로 돌아온다 
                        if (attackType == AttackType.AXE)
                        {
                            while (IsTrackedCoroutineRunning(moveToTargetCoroutine)) yield return null; /// 가는 이동이 다 끝날 때까지 대기 

                            var comebackCoroutine = MoveToRapidly(GetRealPositionByTile(ownedTile));
                            while (comebackCoroutine.MoveNext()) yield return comebackCoroutine.Current;
                        }

                        /// 피한 녀석 다시 돌아오기 
                        var targetReturnToTileCoroutine = targetAnimal.MoveToRapidly(originPos);
                        while (targetReturnToTileCoroutine.MoveNext()) yield return targetReturnToTileCoroutine.Current;

                        startAttackEffect = false;
                    }
                    break;
            }
            
             animator.SetBool(animalData.attackAnim, false);
             animator.ResetTrigger(animalData.attackAnim + "Trigger");

            /// 동기화 
            SetCustomPropertiesByMe();

            /// 끝났다고 알려주기
            OnFinishedAttack(this);
        }

        isAttacking = false;
    }


    void SetTrap()
    {

    }


    public void ShootBulletAndStartEffect()
    {
        StartCoroutine(ShootBullet(true));
    }


    public void ShootBulletAndNothing()
    {
        StartCoroutine(ShootBullet(false));
    }


    public void Chop()
    {
        /// 공격 연출 시작하라고 하기
        startAttackEffect = true;
    }


    public void Reload()
    {
        audioSource.PlayOneShot(reloadSound, 0.5f);
    }


    IEnumerator ShootBullet(bool andThenStartAttackEffect)
    {
        if (targetAnimalInfo.animal == null) yield break;

        /// 발사 사운드 
        audioSource.PlayOneShot(weaponSound, 0.3f);

        /// 발사 이펙트 
        gunShotEffect.Stop();
        gunShotEffect.Play(gunShotEffectPosition.position);

        /// 안 쓰고 있는 총알 찾기
        const int SHOTGUN_BULLET = 5;
        int shootingBulletCount = 0;
        foreach (var restBullet in GameManager.instance.bullets)
        {
            if (!restBullet.isUsing)
            { 
                /// 탄퍼짐 
                Vector3 destination = targetAnimalInfo.animal.ownedTile.position;
                float bulletSpread_X = 0f;
                if (attackType == AttackType.AK47)   /// AK47는 랜덤 탄퍼짐
                {
                    bulletSpread_X = Random.Range(-20f, 20f);
                }
                else if (attackType == AttackType.SHOTGUN)   /// Shotgun은 균일하게 퍼지는 탄퍼짐
                {
                    bulletSpread_X = Mathf.Lerp(-20, 20, 1f / (float)SHOTGUN_BULLET * shootingBulletCount);
                }
                destination.x += bulletSpread_X;
                
                restBullet.StartCoroutine(restBullet.Shoot(gunShotEffectPosition.position, transform.rotation, destination));
                
                shootingBulletCount++;

                /// 샷건은 5발을 한번에 쏠꺼다. 나머지 총은 한발씩 쏜다
                if ((attackType == AttackType.SHOTGUN && shootingBulletCount >= SHOTGUN_BULLET) 
                || (attackType == AttackType.AWP && shootingBulletCount >= 1) 
                || (attackType == AttackType.AK47 && shootingBulletCount >= 1))
                {
                    /// 딱 한 발한테만 이벤트를 등록시킨다. 여러발에 등록시켰다가 이벤트 여러개 들어오면 처리에 혼란이 생길 수 있다 
                    if (andThenStartAttackEffect)
                    {
                        restBullet.OnFinishedShoot += OnFinishedShoot;
                    }
                    
                    break;
                }
            }
        }
    }


    void OnFinishedShoot(Bullet bullet)
    {
        bullet.OnFinishedShoot -= OnFinishedShoot;

        /// 공격 연출 시작하라고 하기
        startAttackEffect = true;
    }


    public override void SetVisible(bool visible)
    {
        base.SetVisible(visible);

        foreach (var meshRenderer in meshRenderers)
        {
            meshRenderer.enabled = visible;
        }
    }


    void OnStartMoveFunc(Animal animal, Tile movingTile)
    {
        if (!GameManager.instance.showSightOfAI)
        {
            SetVisible(movingTile.isVisible);
        }
    }
}
