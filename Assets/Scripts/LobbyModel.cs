﻿using UnityEngine;
using System.Collections;

public class LobbyModel : MonoBehaviour 
{
	[SerializeField]
	UIPanel scrollViewPanel;

	[SerializeField]
	float panelOriginOffset = 1000f;

	[SerializeField]
	float worldScrollSize = 4f;

	Vector3 originPosition;

	void Start()
	{
		originPosition = transform.localPosition;

		scrollViewPanel.onClipMove += OnClippingMoved;
	}


	void OnDestroy()
	{
		scrollViewPanel.onClipMove -= OnClippingMoved;
	}


	void OnClippingMoved(UIPanel panel)
	{
		/// 기준 offset에서 얼마나 움직였는가
		float offsetDiff = panelOriginOffset - panel.clipOffset.x;

		/// UI 기준에서 스크롤되는 offest 값을 world에 그대로 적용하면 좌표계가 달라 확확 움직인다
		/// 그렇게 되지 않도록 world상에서 자연스럽게 스크롤되는 값을 찾아내어 그 값에 들어맞는 factor를 계산한다
		float worldOffsetFactor = worldScrollSize / panel.GetViewSize().x; 

		Vector3 newPosition = originPosition;
		newPosition.x += offsetDiff * worldOffsetFactor;
		transform.localPosition = newPosition;
	}
}
