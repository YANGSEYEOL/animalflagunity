﻿using UnityEngine;
using System.Collections;

public class MonopolyHippo : Skill
{
    public override void Init(Animal _ownAnimal)
    {
        ownAnimal = _ownAnimal;
        InitSkillDescription();
        InitSkillName();
    }

    public override void InitSkillDescription()
    {
        description = "This is MonopolyHippo";
    }

    public override void InitSkillName()
    {
        skillName = "MonopolyHippo";
    }

    public override void InitIsActive()
    {
        isActive = true;
    }

    public override void Do()
    {
        Debug.Log(ownAnimal.name + "used a MonopolyHippo");
    }
}
