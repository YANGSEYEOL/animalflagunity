﻿using UnityEngine;
using System.Collections;

public class PopUpLabel : MonoBehaviour
{
    public UISprite popUpSprite;
    public UILabel popUpLabel;
    Animal animal;

    public void Init(Animal animal, string spriteName, string message)
    {
        this.animal = animal;
        popUpSprite.spriteName = spriteName;
        popUpLabel.text = message;
    }

    public void PopUptoSky()
    {
        transform.position = animal.transform.position + Vector3.up *30;
        int heartDest = Random.Range(20, 30);
       iTween.MoveBy(this.gameObject, iTween.Hash("time", 1.0f, "islocal", true, "y" , heartDest, "oncomplete", "OnPopUpComplete", "oncompletetarget", gameObject));
    }

    void OnPopUpComplete()
    {
        gameObject.SetActive(false);
        Destroy(gameObject);
    }
}