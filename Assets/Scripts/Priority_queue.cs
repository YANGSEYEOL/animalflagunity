﻿using UnityEngine;
using System.Collections;

public class Priority_queue : MonoBehaviour {
    int curArrSize = 20;

     public Node[] nodeArray;
    int endIndex = 0;
    
    Priority_queue() {
       nodeArray = new Node[curArrSize];
    }
    
    // 현재는 node에 key밖엔 없지만 key이외에도 x,y값이나 다른 
    // 애트리뷰트를 추가할거양~
    public void Push(int x)
    {
        Node temp = new Node(x);
        nodeArray[++endIndex] = temp;
        UpHeap(endIndex);
    }

    void UpHeap(int i)
    {
        while (nodeArray[i].key > nodeArray[i/2].key)
        {
            Node temp = nodeArray[i/2];

            nodeArray[i/2] = nodeArray[i];
            nodeArray[i] = temp;
            i = i / 2;
        }
    }

    void DownHeap(int i)
    {
        int min = 0;
        int minIndex = 0;
        while (nodeArray[i].key >= min)
        {
            if (nodeArray[2*i].key > nodeArray[2*i + 1].key)
            {
                min = nodeArray[2 * i + 1].key;
                minIndex = 2 * i + 1;
            }
            else
            {
                min = nodeArray[2 * i].key;
                minIndex = 2 * i;
            }

            Node temp = nodeArray[minIndex];
            nodeArray[minIndex] = nodeArray[i];
            nodeArray[i] = temp;
            i = minIndex;

        }

    }
    public Node Pop()
    {
        Node ret = nodeArray[1];
        nodeArray[1] = nodeArray[endIndex];
        DownHeap(1);
        return ret;
    }
	
}

public class Node
{
//    Node parent = null;
//    Node Lchild = null;
//    Node Rchild = null;
   public int key = 0;

    public Node(int _key) {
    //    parent = _parent;
      //  Lchild = _Lchild;
  //      Rchild = _Rchild;
        key = _key;
    }
}