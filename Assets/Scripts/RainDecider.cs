﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Photon;
using Hashtable = ExitGames.Client.Photon.Hashtable;

// 우기 지역을 결정하는 클래스이다. 
public class RainDecider : PunBehaviour {
    // RD의 랜덤한 중심좌표
    float rainDeciderPosX;
    float rainDeciderPosY;
    int colliderAngle;
    // RainDecider가 생성spawn되는 반경(radius)을 조절할 수 있도록 두었다.
    float rainDeciderSpawnRadius = 10;
    float colliderRadiusX;
    float colliderRadiusY;
    float mapScaleXSize;
    public List<Tile> rainyTileList;
    bool[,] visited;
    public TileChunk tileChunk;
    EllipseCollider ellipseCollider;
    bool isSeasonInfoReceived = false;
    public TileMaker.TileType typeToChange;
    public bool isRainySomewhere = false;
    public static readonly string TILE_SEASONS_PROP_KEY = "TileSeasons";
    public static readonly string TILE_SEASONS_RAINY_LOCATION_PROP_KEY = "TileSeasons_Rainy_Locations";
    public static readonly string TILE_SEASONS_RAINY_TYPETOCHANGE_PROP_KEY = "TileSeasons_Rainy_TypeToChange";

    void Start ()
    {
        rainDeciderPosX = 0;
        rainDeciderPosY = 0;
        colliderAngle = 0;
        colliderRadiusX = 3;
        colliderRadiusY = 5;
        mapScaleXSize = TileMaker.instance.tileSize * TileMaker.instance.tileScale;
        rainyTileList = new List<Tile>();
        ellipseCollider = GetComponent<EllipseCollider>();
        if (PhotonNetwork.isMasterClient)
        {
            visited = new bool[TileMaker.instance.tileSize, TileMaker.instance.tileSize];
            for (int i = 0; i < TileMaker.instance.tileSize; i++)
            {
                for (int j = 0; j < TileMaker.instance.tileSize; j++)
                {
                    visited[i, j] = false;
                }
            }
        }
    }
	
    void SetTypeToChangeRandomly()
    {
        System.Random tileDice = new System.Random();
        do
        {
            typeToChange = (TileMaker.TileType)tileDice.Next(1, 6);
        }
        while (typeToChange == TileMaker.TileType.DESSERT);
    }

    public void SetRainyLand()
    {
        isRainySomewhere = true;
        // 마스터가 처리하는 함수들
        if (PhotonNetwork.isMasterClient)
        {
            SetTypeToChangeRandomly();
            SetRainDeciderAttributeRadomly();
            CollectRainyTileContours();
            FillRainyTilesArea();
            SendTileSeasonsInfo();
        }
    }

    public void ResetRainyLand()
    {
        isRainySomewhere = false;
        // 마스터가 처리하는 함수들
        if (PhotonNetwork.isMasterClient)
        {
            rainyTileList.Clear();
            SendTileSeasonsInfo();
        }
    }

    // RD가 생성되는 위치의 반경을 미리설정해서 이 범위내부에 랜덤하게 좌표가 생성되도록하였다.
    void SetRainDeciderAttributeRadomly()
    {
        rainDeciderSpawnRadius = 5;
        rainDeciderPosX = Random.Range(TileMaker.instance.tileSize / 2 - rainDeciderSpawnRadius, TileMaker.instance.tileSize / 2 + rainDeciderSpawnRadius);
        rainDeciderPosY = Random.Range(TileMaker.instance.tileSize / 2 - rainDeciderSpawnRadius, TileMaker.instance.tileSize / 2 + rainDeciderSpawnRadius);
        ellipseCollider.SetPosition(rainDeciderPosX * TileMaker.instance.tileScale, rainDeciderPosY * TileMaker.instance.tileScale);

        colliderRadiusX = Random.Range(2, 5);
        colliderRadiusY = Random.Range(2, 5);
        colliderAngle   = Random.Range(0, 90);
        ellipseCollider.SetProperties(colliderRadiusX, colliderRadiusY, colliderAngle);
    }

    // 우선 ellipseCollider에서 반환하는 벡터들의좌표 -> 그 좌표에 해당하는 타일들의 정보를 체크 -> 그 타일의 settorainyseason함수 접근
    // 비가오는 타일들의 테두리타일들만 구한다.
    void CollectRainyTileContours()
    {
        // 여기서 반환되는 배열들은 타원들의 테두리일것이다.
        Vector2[] tilesCoords = ellipseCollider.getPoints(Vector2.zero);
        foreach(var tileCoord in tilesCoords)
        {
            if (tileCoord.x < 0 || tileCoord.y < 0 || tileCoord.x > mapScaleXSize || tileCoord.y > mapScaleXSize)
            {
                continue;
            }

            int x = (int)tileCoord.x / TileMaker.instance.tileScale;
            int y = (int)tileCoord.y / TileMaker.instance.tileScale;

            Tile tile = tileChunk.tiles[x, y];
            if (!rainyTileList.Contains(tile))
            {
                rainyTileList.Add(tile);
                visited[x, y] = true;
            }
        }
    }
    
    // 내가 하려는것 dfs, bfs를 이용해서 비구름 안을 휘져으면서 채워넣는다.
    void FillRainyTilesArea()
    {
        Queue<Tile> queue = new Queue<Tile>();
        int coreX = (int)rainDeciderPosX;
        int coreY = (int)rainDeciderPosY;
        queue.Enqueue(tileChunk.tiles[coreX, coreY]);

        while(queue.Count != 0)
        {
            Tile tempTile = queue.Dequeue();
            coreX = tempTile.TileIndex_X;
            coreY = tempTile.TileIndex_Y;
            if (!visited[coreX, coreY])
            {
                if (!rainyTileList.Contains(tempTile))
                {
                    rainyTileList.Add(tempTile);
                    visited[coreX, coreY] = true;
                }
            }

            if(!visited[coreX - 1 , coreY])
            {
                queue.Enqueue(tileChunk.tiles[coreX - 1, coreY]);
            }
            if (!visited[coreX, coreY - 1])
            {
                queue.Enqueue(tileChunk.tiles[coreX, coreY - 1]);
            }
            if (!visited[coreX + 1, coreY])
            {
                queue.Enqueue(tileChunk.tiles[coreX + 1, coreY]);
            }
            if (!visited[coreX, coreY + 1])
            {
                queue.Enqueue(tileChunk.tiles[coreX, coreY + 1]);
            }
        }
        for (int i = 0; i < TileMaker.instance.tileSize; i++)
        {
            for (int j = 0; j < TileMaker.instance.tileSize; j++)
            {
                if(visited[i, j]) visited[i, j] = false;
            }
        }
    }
    
    void UpdateEachTileSeasonInfo()
    {
        if (!isRainySomewhere)
        {
            foreach (Tile tile in tileChunk.tiles)
            {
                //tile.SetToNormalSeason();
            }
        }
        else
        {
            foreach (Tile tile in tileChunk.tiles)
            {
                //tile.SetToDraughtSeason();
            }
            foreach (var rainyTile in rainyTileList)
            {
                //rainyTile.SetToRainySeason();
            }
        }
    }

    // 포톤 프로퍼티에 정보가 저장되어있어야 플레이어가 재접속하더라도 정보를 다시 받아 게임을 이어갈 수 있다.
    // RPC를 이용하면 프로퍼티에 저장하지 않고 바로 함수호출을 할 수 있다. 그래서 우린 프로퍼티를 이용해야한다. RPC가아니고.
    // 우선 계절이 비밖에없지만 나중에 어떻게 될지모르므로 일단 비 타일의 인덱스를 해쉬로 집어넣는다.
    // 이 해쉬에들어있지않는 인덱스는 건기지역이다.
    void SendTileSeasonsInfo()
    {
        Hashtable tileSeasonsProp = new Hashtable();

        Hashtable tileSeasons = new Hashtable();

        int rainyCount = 0;
        Hashtable rainyTilesHash = new Hashtable();

        // 자기가 가지고있는 모든 비타일의 정보를 보내줌 -> 우기지역이 여러곳이더라도 문제가 없겠넹
        foreach (Tile tile in rainyTileList)
        {
            rainyCount++;
            int tileIndex = tile._tileIndex;
            rainyTilesHash[rainyCount] = tileIndex;
        }
        rainyTilesHash[0] = rainyCount;

        tileSeasons[TILE_SEASONS_RAINY_LOCATION_PROP_KEY] = rainyTilesHash;
        tileSeasons[TILE_SEASONS_RAINY_TYPETOCHANGE_PROP_KEY] = typeToChange;
        tileSeasonsProp[TILE_SEASONS_PROP_KEY] = tileSeasons;
        PhotonNetwork.room.SetCustomProperties(tileSeasonsProp);
    }

    public override void OnPhotonCustomRoomPropertiesChanged(Hashtable propertiesThatChanged)
    {
        if (propertiesThatChanged.ContainsKey(TILE_SEASONS_PROP_KEY))
        {
            //SyncTileSeasonsByUsingTilesProperties();
        }
    }
    /*
    void SyncTileSeasonsByUsingTilesProperties() {
        if (!isSeasonInfoReceived)
        {
            // 기존의 우기타일을 건기로 바꾸는작업 기존의 우기지역을 다 초기화일단 시킨다.
            if (!PhotonNetwork.isMasterClient)
            {
                Hashtable tileSeasonProp = (Hashtable)PhotonNetwork.room.CustomProperties[TILE_SEASONS_PROP_KEY];
                Hashtable tileSeasonRainyLocationProp = (Hashtable)tileSeasonProp[TILE_SEASONS_RAINY_LOCATION_PROP_KEY];
                typeToChange = (TileMaker.TileType)tileSeasonProp[TILE_SEASONS_RAINY_TYPETOCHANGE_PROP_KEY];
                int rainyTileCount = (int)tileSeasonRainyLocationProp[0];

                if (rainyTileCount == 0) // 건기우기가 아닌상황
                {
                    isRainySomewhere = false;
                    foreach (Tile tile in tileChunk.tiles)
                    {
                        tile.SetToNormalSeason();
                    }
                    rainyTileList.Clear();
                }
                else // 건기우기인상황
                {
                    isRainySomewhere = true;
                    for (int i = 1; i <= rainyTileCount; i++)
                    {
                        int tileIndexX = (int)tileSeasonRainyLocationProp[i] % TileMaker.instance.tileSize;
                        int tileIndexY = (int)tileSeasonRainyLocationProp[i] / TileMaker.instance.tileSize;

                        rainyTileList.Add(tileChunk.tiles[tileIndexX, tileIndexY]);
                    }
                }                
            }
            UpdateEachTileSeasonInfo();
            //tileChunk.UpdateRainyAreaVectorline();
            //tileChunk.DisplayRainyArea();
            // 주구장창 계절정보를 업데이트할 수없으니까 나중에 플래그로 통제를 해둘 필요가 있다. -> 턴이 끝났을때 이것을 false로 풀어줌으로써 다시 계절업데이트가 되도록
            // 근데 아직 어떻게 업데이트를 통제할지 감이안잡혀서 우선 주석처리~!
            // isSeasonInfoReceived = true;
        }
    }*/
}
