﻿using UnityEngine;


class RetrieveFlagCommand : Command
{
    GameManager gameManager;
    TileChunk tileChunk;
    UIManager uiManager;

    Animal _animal;
    Animal animal
    {
        get { return _animal; }
        set
        {
            if (_animal != null)
            {
                _animal.OnChangedCanRetrieveFlag -= OnChangedCanRetrieveFlag;
            }

            _animal = value;

            if (value != null)
            {
                _animal.OnChangedCanRetrieveFlag += OnChangedCanRetrieveFlag;
            }
        }
    }

    public UIButton retrieveFlagButton;


    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        gameManager.OnChangedPlayerTurn += OnChangedPlayerTurn;
        gameManager.OnSelectedAnimal += OnSelectedAnimal;

        tileChunk = FindObjectOfType<TileChunk>();

        uiManager = FindObjectOfType<UIManager>();
    }

    private void OnSelectedAnimal(Animal selectedAnimal)
    {
        animal = selectedAnimal;

        state = selectedAnimal != null && selectedAnimal.canRetrieveFlag ? CommandState.ACTIVE : CommandState.NONE;
    }

    private void OnChangedPlayerTurn(string turnUserId)
    {
        animal = null;

        state = CommandState.NONE;
    }

    public void OnClickRetrieveFlagButton()
    {
        uiManager.OpenConfirmUI(this);
    }

    public void OnChangedCanRetrieveFlag(bool canRetrieveFlag)
    {
        state = animal != null && animal.canRetrieveFlag ? CommandState.ACTIVE : CommandState.NONE;
    }

    protected override void OnStateChanged(CommandState changedState)
    {
        switch (changedState)
        {
            case CommandState.NONE:
                {
                    retrieveFlagButton.gameObject.SetActive(false);
                }
                break;
            case CommandState.ACTIVE:
                {
                    retrieveFlagButton.gameObject.SetActive(true);
                }
                break;
            case CommandState.READY:
                {
                }
                break;
        }
    }

    public override void Ready()
    {
        state = animal != null && animal.canRetrieveFlag ? CommandState.READY : CommandState.NONE;
    }

    public override void Do()
    {
        state = CommandState.NONE;

        animal.RetrieveFlag();
    }

    public override void Cancel()
    {
        state = animal != null && animal.canRetrieveFlag ? CommandState.ACTIVE : CommandState.NONE;
    }
}

