﻿using UnityEngine;
using System.Collections;

public class ScorpionManager : MonoBehaviour {

    public static ScorpionManager instance;

    public GameObject scorpionPrefab;

    public int smokeIndex = 0;

    public ParticleSystem[] digSmoke;
    public ParticleSystem[] sandRipple;

    [SerializeField]
    GameObject scorpionGroup;

    GameObject[] scorpions;

    int scorpionCount = 3;
    
    void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start () {
        scorpions = new GameObject[scorpionCount];
        TileChunk.instance.OnCreatedTiles += CreateScorpions;
    }

    public Tile GetRandomSandTile()
    {
        Tile tile = TileChunk.instance.GetTileWithIndex(Random.Range(0, TileMaker.instance.tileSize * TileMaker.instance.tileSize));
        while(tile.type != TileMaker.TileType.DESSERT)
        {
            tile = TileChunk.instance.GetTileWithIndex(Random.Range(0, TileMaker.instance.tileSize * TileMaker.instance.tileSize));
        }

        return tile;
    }

    void CreateScorpions()
    {
        if (!TileChunk.instance.desertExisting)
            return;

        for (int i = 0; i < scorpionCount; i++)
        {
            scorpions[i] = (GameObject)Instantiate(scorpionPrefab, GetRandomSandTile().position_AdjustedTileYFactor, Quaternion.identity);
            scorpions[i].transform.rotation = Quaternion.AngleAxis(Random.Range(0, 90), Vector3.up);
            scorpions[i].transform.SetParent(scorpionGroup.transform);
        }
    }
}
