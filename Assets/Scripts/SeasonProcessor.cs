﻿using UnityEngine;
using System.Collections;

public abstract class SeasonProcessor {

   public  abstract void SeasonProcess(Tile tile);
    
}
