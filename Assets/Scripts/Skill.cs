﻿using UnityEngine;
using System.Collections;

public abstract class Skill : MonoBehaviour
{
    public string skillName;
    public string description;
    public Animal ownAnimal;
    public bool isActive;

    public abstract void Do();
    public abstract void Init(Animal _ownAnimal);
    public abstract void InitSkillName();
    public abstract void InitSkillDescription();
    public abstract void InitIsActive();
}
