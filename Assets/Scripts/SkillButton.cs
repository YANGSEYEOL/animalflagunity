﻿using UnityEngine;
using System.Collections;

public class SkillButton : MonoBehaviour
{
    public Skill skill;
    public int buttonIndex;

    public void SetButtonColorAndSprite(Animal animal)
    {
        skill = animal.skills[buttonIndex];
        GetComponent<UIButton>().normalSprite = skill.skillName + "_icon";

        if (IsLearnedSkill(animal))
        {
            GetComponent<UIButton>().defaultColor = Color.blue;
        }
        else
        {
            GetComponent<UIButton>().defaultColor = Color.white;
        }



    }

    bool IsLearnedSkill(Animal animal)
    {
        if (animal.tier > buttonIndex / 2)
        {
            if (skill.skillName == animal.learnedSkill[buttonIndex / 2].skillName)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
            return false;
    }
    
}
