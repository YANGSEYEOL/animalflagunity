﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Photon;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Vectrosity;

public delegate void OnCreatedTilesDelegate();
public delegate void OnClickTileDelegate(Tile tile);
public delegate void OnChangedAnimalIsVisibleDelegate(Animal animal, bool isVisible);

public class TileChunk : Photon.PunBehaviour
{
    public static TileChunk instance;

    //public BreedManager breedManager;
    //public RainDecider rainDecider;


    // 동물이 있는 타일은 나무나 풀이 알파블렌딩이되어 동물을 가리지 않도록해야한다.
    // 각 나무와 풀은 뱃칭이 되어있기 때문에 각 오브젝트의 알파값만 조절하는 것은 어렵다
    // 그러므로 나무, 풀 쉐이더에 알파텍스처를 곱해서 동물이 있는 위치만 알파값을 조절한다.
    [System.NonSerialized]
    public Texture2D alphaTex;

    // 나무와 풀 쉐이더 곱해질 텍스처이다.
    // 쉐이더를 이용해서 바람에 의한 흔들림을 표시하는데 안개에 가려진 나무나 풀은 바람에 영향을 안받도록할것이다.
    // 그러므로 타일의 시야가 밝혀지지 않은부분은 fogMask텍스처를 검정색으로해서 흔들리지 않도록설정한다.
    [System.NonSerialized]
    public Texture2D fogMaskTex;

    public Material grassMaterial;
    public Material forestMaterial;
    //public Material targetHighlightMaterial;

    public Tile[,] tiles;

    private RainySeasonProcessor rainySeasonProcessor;
    private NormalSeasonProcessor normalSeasonProcessor;
    private DraughtSeasonProcessor draughtSeasonProcessor;
    
    public Dictionary<Animal, List<Tile>> absoluteSightTileTable = new Dictionary<Animal, List<Tile>>();

    //List<GameObject> HighLights;

    [System.NonSerialized]
    public int tileSizeWidth = 20;
    [System.NonSerialized]
    public int tileSizeHeight = 20;

    public Dijkstra dijkstra = new Dijkstra();

    /// 라인 그리기용 객체들
    [System.NonSerialized]
    public VectorLine lineMoveableRange = null;
    [System.NonSerialized]
    public VectorLine lineNavigation = null;
    [System.NonSerialized]
    public VectorLine lineGrey = null;
    [System.NonSerialized]
    public VectorLine lineMagenta = null;
    [System.NonSerialized]
    public VectorLine lineGreen = null;
    [System.NonSerialized]
    public VectorLine lineCyan = null;

    [SerializeField]
    public Material lineMaterial = null;
    [SerializeField]
    public float intensityOfGlowLine = 20f;
    [SerializeField]
    public Material lineColorMaterial = null;
    [SerializeField]
    public float thicknessOfColorLine = 5f;
    [SerializeField]
    public Material dotMaterial = null;
    [SerializeField]
    float dotSize = 50f;

    VectorPoints dot = null;


    /// 시야
    public FieldOfView fieldOfView = new FieldOfView();

    /// 안개
    [System.NonSerialized]
    public FogOfWar fogOfWar;

    /// 밤낮 관리자
    DayAndNight dayAndNight;

    /// 동물 이동 자취
    List<AnimalTrail> animalTrails = new List<AnimalTrail>();

    bool isCreatedTiles = false;

    /// 타일 메쉬 생성을 위한 변수들
    [SerializeField]
    Material[] tileTypeMaterials;

    [SerializeField]
    GameObject tileMeshPrefab;

    //public GameObject highLightPrefab;

    [SerializeField]
    GameObject chunkPrefab;

    [SerializeField]
    public int[] yFactorOfTileType;

    [SerializeField]
    GameObject planeForRiver;

    public bool desertExisting = false;
    /// Key : Material Name
    Dictionary<string, List<CombineInstance>> combineListDic = new Dictionary<string, List<CombineInstance>>();
    Dictionary<string, Material> materialDic = new Dictionary<string, Material>();

    public OnCreatedTilesDelegate OnCreatedTiles = delegate {};
    public OnClickTileDelegate OnClickedTile = delegate {};



    private void Awake()
    {
        instance = this;
    }


    private void Start()
    {/*
        rainySeasonProcessor = new RainySeasonProcessor(rainDecider);
        normalSeasonProcessor = new NormalSeasonProcessor();
        draughtSeasonProcessor = new DraughtSeasonProcessor();*/
        InitObjectAlphaTexture();
        InitObjectFogMaskTexture();

        InitGrassMaterial();
        InitForestMaterial();

        InitPlaneForRiver();

        lineMoveableRange = new VectorLine("lineMoveableRange", new List<Vector3>(), lineColorMaterial, thicknessOfColorLine, LineType.Discrete, Joins.Weld);
        lineMoveableRange.color = Color.yellow;

        lineNavigation = new VectorLine("lineNavigation", new List<Vector3>(), lineColorMaterial, thicknessOfColorLine, LineType.Discrete, Joins.Weld);
        lineNavigation.color = Color.white;

        lineGrey = new VectorLine("lineGrey", new List<Vector3>(), lineColorMaterial, thicknessOfColorLine, LineType.Discrete, Joins.Weld);
        lineGrey.color = Color.grey;

        lineMagenta = new VectorLine("lineMagenta", new List<Vector3>(), lineColorMaterial, thicknessOfColorLine, LineType.Discrete, Joins.Weld);
        lineMagenta.color = Color.magenta;

        lineGreen = new VectorLine("lineGreen", new List<Vector3>(), lineColorMaterial, thicknessOfColorLine, LineType.Discrete, Joins.Weld);
        lineGreen.color = Color.green;

        lineCyan = new VectorLine("lineCyan", new List<Vector3>(), lineColorMaterial, thicknessOfColorLine, LineType.Discrete, Joins.Weld);
        lineCyan.color = Color.cyan;

        dot = new VectorPoints("Dot", new List<Vector3>(), dotMaterial, dotSize);
        
        fieldOfView.tileChunk = this;

        fogOfWar = FogOfWar.FindInstance();

        dayAndNight = FindObjectOfType<DayAndNight>();
    }


    void OnDestroy()
    {
    }

    [SerializeField]
    int planeOffset = 0;
    void InitPlaneForRiver()
    {
        float smallPlaneWidth = Tile.TILE_WORLD_WIDTH / 3.5f;
        float scaledPlaneWidth = Tile.TILE_WORLD_WIDTH * TileMaker.instance.tileSize;
        float scaledPlaneWidthPlusOffset = scaledPlaneWidth + planeOffset * 2; 
        float planeScale = scaledPlaneWidthPlusOffset / smallPlaneWidth;

        planeForRiver.transform.position = new Vector3(scaledPlaneWidth / 2, -40 , -scaledPlaneWidth / 2);
        planeForRiver.transform.localScale = new Vector3(planeScale, 1, planeScale);
    }

    void InitObjectAlphaTexture()
    {
        int tileSize = TileMaker.instance.tileSize;

        alphaTex = new Texture2D(tileSize, tileSize);
        for (int i = 0; i < tileSize; i++)
        {
            for (int j = 0; j < tileSize; j++)
            {
                alphaTex.SetPixel(i, j, Color.white);
            }
        }
    }

    void InitObjectFogMaskTexture()
    {
        int tileSize = TileMaker.instance.tileSize;
        fogMaskTex = new Texture2D(tileSize, tileSize);
        for (int i = 0; i < tileSize; i++)
        {
            for (int j = 0; j < tileSize; j++)
            {
                fogMaskTex.SetPixel(i, j, Color.black);
            }
        }
    }

    void InitGrassMaterial()
    {
        grassMaterial.SetVector("_WorldSize", new Vector4(Tile.TILE_WORLD_HEIGHT * TileMaker.instance.tileSize, 0, Tile.TILE_WORLD_WIDTH * TileMaker.instance.tileSize, 0));

        grassMaterial.SetTexture("_AlphaTex", alphaTex);
        grassMaterial.GetTexture("_AlphaTex").anisoLevel = 16;
        grassMaterial.GetTexture("_AlphaTex").filterMode = FilterMode.Point;

        grassMaterial.SetTexture("_FogMaskTex", fogMaskTex);
        grassMaterial.GetTexture("_FogMaskTex").anisoLevel = 16;
        grassMaterial.GetTexture("_FogMaskTex").filterMode = FilterMode.Point;
        
        alphaTex.Apply();
        fogMaskTex.Apply();
    }

    void InitForestMaterial()
    {
        forestMaterial.SetVector("_WorldSize", new Vector4(Tile.TILE_WORLD_HEIGHT * TileMaker.instance.tileSize, 0, Tile.TILE_WORLD_HEIGHT * TileMaker.instance.tileSize, 0));

        forestMaterial.SetTexture("_AlphaTex", alphaTex);
        forestMaterial.GetTexture("_AlphaTex").anisoLevel = 16;
        forestMaterial.GetTexture("_AlphaTex").filterMode = FilterMode.Point;

        forestMaterial.SetTexture("_FogMaskTex", fogMaskTex);
        forestMaterial.GetTexture("_FogMaskTex").anisoLevel = 16;
        forestMaterial.GetTexture("_FogMaskTex").filterMode = FilterMode.Point;

        alphaTex.Apply();
        fogMaskTex.Apply();
    }

    public int[] GetShortestPathForMovement(Animal animal, int tileIndex, int moveRange)
    {
        dijkstra.FindPaths(animal, moveRange);

        // 검색
        int[] shortestPathList = null;
        foreach (int[] pathList in dijkstra.LoadValidTiles())
        {
            if (pathList[1] == tileIndex)
            {
                shortestPathList = pathList;
                break;
            }
        }

        return shortestPathList;
    }


    /// Called by InputToEvent
	void OnClick(Vector3 hitPos)
    {
        if (GameManager.instance.IsCanClick)
        {
            /// 어떤 타일을 클릭했는지 알아본다 
            Tile clickedTile = GetTileWithPosition(hitPos);

            OnClickedTile(clickedTile);
        }
    }


    ///  타일청크에 처음 눌린 위치와 드래그하는 위치의 방향을 기반으로 카메라 패닝을 구현 
    ///  기존 방식은 카메라의 Y축이 45도 방향으로 회전되어있을 때, 2D 처럼 상하좌우로만 패닝되기 때문에 이 방식으로 수정함 
    bool isPress = false;
    Vector3 firstPressHitPos = Vector3.zero;
    void OnPress(Vector3 hitPos)
    {
        isPress = true;

        firstPressHitPos = hitPos;

        /// 카메라 무빙 중이라면 무빙 캔슬시키기
        /// 유저에 의지대로 화면을 조종하지 못하면 답답함을 느끼더라
        iTween.Stop(Camera.main.gameObject);
    }


    void OnDrag(Vector3 hitPos)
    {
        if (isPress)
        {
            Vector3 diff = firstPressHitPos - hitPos;

            Camera.main.transform.Translate(diff, Space.World);
        }
    }


    void OnRelease()
    {
        isPress = false;

        /// 화면 가두기 범위를 벗어나는지 체크해서 화면 가둠 코루틴 실행 
        Vector3 position = Camera.main.transform.position;
        if (!(confinedCamera_minX <= position.x && position.x <= confinedCamera_maxX && confinedCamera_minZ <= position.z && position.z <= confinedCamera_maxZ))
        {
            StartCoroutine(ConfinedCamera());
        }
    }


    [SerializeField]
    public float confinedCamera_minX = -70f;
    [SerializeField]
    public float confinedCamera_maxX = 550f;
    [SerializeField]
    public float confinedCamera_minZ = -770f;
    [SerializeField]
    public float confinedCamera_maxZ = -140f;
    IEnumerator ConfinedCamera()
    {
        Vector3 confinedPosition = Camera.main.transform.position;

        bool isConfinedCamera = false;
        while (!isPress && !isConfinedCamera)
        {
            float confiningSpeed = 10f * Time.deltaTime;

            isConfinedCamera = true;
            
            Vector3 cameraPosition = Camera.main.transform.position;
            if (cameraPosition.x > confinedCamera_maxX)
            {
                confinedPosition.x = confinedCamera_maxX;

                isConfinedCamera = false;
            }
            else if (cameraPosition.x < confinedCamera_minX)
            {
                confinedPosition.x = confinedCamera_minX;

                isConfinedCamera = false;
            }

            if (cameraPosition.z > confinedCamera_maxZ)
            {
                confinedPosition.z = confinedCamera_maxZ;

                isConfinedCamera = false;
            }
            else if (cameraPosition.z < confinedCamera_minZ)
            {
                confinedPosition.z = confinedCamera_minZ;

                isConfinedCamera = false;
            }

            Camera.main.transform.position = Vector3.Lerp(cameraPosition, confinedPosition, confiningSpeed);

            yield return null;
        }       
    }


    public Tile GetTileWithIndex(int tileIndex)
    {
        int tileIndex_X = tileIndex % tileSizeWidth;
        int tileIndex_Y = tileIndex / tileSizeWidth;

        return tiles[tileIndex_X, tileIndex_Y];
    }


    public Tile GetTileWithPosition(Vector3 pos)
    {
        /// 0,0 타일(좌상단) 기준으로 영점을 맞추고 인덱스를 계산한다
        Vector3 modifiedPos = pos - tiles[0, 0].GetVertexPositionByScale(Tile.TileVertex.LEFT_TOP, 1.0f);
        int tileIndex_X = Mathf.Abs((int)(modifiedPos.x / Tile.TILE_WORLD_WIDTH));
        int tileIndex_Y = Mathf.Abs((int)(modifiedPos.z / Tile.TILE_WORLD_HEIGHT));

        return tiles[tileIndex_X, tileIndex_Y];
    }

    public Tile GetTileWithPositionForFish(Vector3 pos)
    {
        /// 0,0 타일(좌상단) 기준으로 영점을 맞추고 인덱스를 계산한다
        Vector3 modifiedPos = pos - tiles[0, 0].GetVertexPositionByScale(Tile.TileVertex.LEFT_TOP, 1.0f);
        int tileIndex_X = Mathf.Abs((int)(modifiedPos.x / Tile.TILE_WORLD_WIDTH));
        int tileIndex_Y = Mathf.Abs((int)(modifiedPos.z / Tile.TILE_WORLD_HEIGHT));

        if(tileIndex_X >= tileSizeWidth)
        {
            tileIndex_X = tileSizeWidth - 1;
        }
        if(tileIndex_Y >= tileSizeHeight)
        {
            tileIndex_Y = tileSizeWidth - 1;
        }
        return tiles[tileIndex_X, tileIndex_Y];
    }

    public void UndisplayAllPathTiles()
    {
        lineMoveableRange.Resize(0);
        lineMoveableRange.Draw3D();

        UndisplayNavigationPath();
    }


    public void DisplayAllAttackableTile(Animal animal)
    {
        List<AttackCommand.AttackTargetInfo> attakableTargetList = new List<AttackCommand.AttackTargetInfo>();
        AttackCommand.instance.GetAllAttackableTargetInfoList(animal, ref attakableTargetList);

        lineMagenta.Resize(0);

        foreach (var attackableAnimal in attakableTargetList)
        {
            AddVerticesIntoLinePointsByScale(attackableAnimal.animal.ownedTile, 1.0f, lineMagenta);
        }

        lineMagenta.Draw3D();
    }


    /// path 계산할 때 occupation 전부 false로 만들어서 동물이 있거나 바위가 있어도 일단 라인 체크에 포함시키기 위함
	/// 이래야 진짜 경계라인만 걸러낼 수 있다
    public bool[,] emptyTileArrayOccupied = null;

    enum TileDirection { TOP, BOTTOM, LEFT, RIGHT }
    
    struct TileInfoForDisplay
    {
        public Tile tile;
        public bool[] isDirectionBounding;
        public int[] boundingTileIndex;

        public TileInfoForDisplay(Tile tile, bool[] isDirectionBounding)
        {
            this.tile = tile;
            this.isDirectionBounding = isDirectionBounding;
            boundingTileIndex = new int[4];
        }
    }
    List<TileInfoForDisplay> tileListForDisplay = new List<TileInfoForDisplay>();
    public List<int[]> DisplayAllPathTilesOf(Animal animal, int moveRange, Color color, float lineScale = 0.5f)
    {
        if (animal == null)
        {
            return null;
        }
        
        if (emptyTileArrayOccupied == null)
        {
            emptyTileArrayOccupied = new bool[TileMaker.instance.tileSize, TileMaker.instance.tileSize];
        }
        bool[,] tempTileArrayOccupied = dijkstra.tileArrayOccupied;
        
        tileListForDisplay.Clear();

        UndisplayNavigationPath();

        lineMoveableRange.Resize(0);

        /// occupation 계산이 포함되지 않은 path
        dijkstra.tileArrayOccupied = emptyTileArrayOccupied;
        dijkstra.FindPaths(animal, moveRange);
        List<int[]> validTileListWithoutOccupation = new List<int[]>(dijkstra.LoadValidTiles());

        /// occupation 계산이 포함된 진짜 path
        dijkstra.tileArrayOccupied = tempTileArrayOccupied;
        dijkstra.FindPaths(animal, moveRange);
        List<int[]> validTileList = dijkstra.LoadValidTiles();

        /// 1. 정확한 경계처리를 위한 사전작업 
        /// tileOccupation을 제외하고 path를 돌리면 원래는 막혀서 갈 수 없던 곳도 갈 수 있다고 계산한다
        /// 그런 타일들은 지워줘야 한다 
        SiftOccupation(validTileListWithoutOccupation, validTileList);
        
        /// 2. path에서 경계 라인만 골라내기 
        GetBoundaryOnly(validTileListWithoutOccupation);

        /// 3. 라인 그리기 세팅
        DisplayTileList(lineMoveableRange, tileListForDisplay, color);

        return validTileList;
    }
 
    public void DisplayTilesBoundary(List<Tile> tiles, VectorLine vectorLine)
    {
        tileListForDisplay.Clear();

        SiftOccupation(tiles);
        GetBoundaryOnly(tiles);
        DisplayTileList(vectorLine, tileListForDisplay);
    }

    // occupation된 타일을 구분해서 리스트에 담음
    public void SiftOccupation(List<int[]> validTileListWithoutOccupation, List<int[]> validTileList)
    {
        for (int i = 0; i < validTileListWithoutOccupation.Count;)
        {
            int[] path = validTileListWithoutOccupation[i];
            int tileIndex = path[1];
            int tileIndex_X = tileIndex % tileSizeWidth;
            int tileIndex_Y = tileIndex / tileSizeHeight;
            Tile tile = tiles[tileIndex_X, tileIndex_Y];

            bool foundSameTileInRealPath = false;
            foreach (int[] path2 in validTileList)
            {
                int tileIndex2 = path2[1];
                int tileIndex2_X = tileIndex2 % tileSizeWidth;
                int tileIndex2_Y = tileIndex2 / tileSizeHeight;
                Tile tile2 = tiles[tileIndex2_X, tileIndex2_Y];

                if (tile == tile2)
                {
                    foundSameTileInRealPath = true;
                    break;
                }
            }

            /// 진짜 경로에는 없는데 withoutOccupation에만 있다면 뺸다 
            /// 하지만, 바위타일이나 동물이 있는 타일은 뺴지 않는다 
            if (!foundSameTileInRealPath && (tile.owningAnimal == null || !tile.owningAnimal.photonView.isMine) /*&& !tile.isRock*/)
            {
                validTileListWithoutOccupation.RemoveAt(i);
            }
            else
            {
                i++;
            }
        }

    }
    public void SiftOccupation(List<Tile> tiles)
    {
        List<Tile> tileList = new List<Tile>();

        for (int i = 0; i < tiles.Count; i++)
        {
            bool foundSameTileInRealPath = false;
            for(int j = 0; j < tiles.Count; j++)
            { 
                if (tiles[i] == tiles[j])
                {
                    foundSameTileInRealPath = true;
                    break;
                }
            }

            /// 진짜 경로에는 없는데 withoutOccupation에만 있다면 뺸다 
            /// 하지만, 바위타일이나 동물이 있는 타일은 뺴지 않는다 
            if (!foundSameTileInRealPath && (tiles[i].owningAnimal == null || !tiles[i].owningAnimal.photonView.isMine) /*&& !tile.isRock*/)
            {
            }
            else
            {
                tileList.Add(tiles[i]);
                i++;
            }
        }

        tiles = tileList;
    }

    void GetBoundaryOnly(List<Tile> tileList)
    {
        foreach (Tile tile1 in tileList)
        {
            int tileIndex = tile1._tileIndex;
            int tileIndex_X = tileIndex % tileSizeWidth;
            int tileIndex_Y = tileIndex / tileSizeHeight;
            
            bool[] isDirectionBounding = new bool[4];
            for (int i = 0; i < 4; ++i)
            {
                isDirectionBounding[i] = true;
            }

            /// 해당 타일 상하좌우를 검색
            /// 상하좌우에 이동가능경로 타일이 있다면 그 방향은 경계처리하지 않아도 된다 
            /// 만약 없다면, 그 방향은 경계처리를 해야 한다 
            foreach (Tile tile2 in tileList)
            {
                int tileIndex2 = tile2._tileIndex;
                int tileIndex2_X = tileIndex2 % tileSizeWidth;
                int tileIndex2_Y = tileIndex2 / tileSizeHeight;

                /// 상
                if (tileIndex2_X == tileIndex_X && tileIndex2_Y == tileIndex_Y - 1)
                    isDirectionBounding[(int)TileDirection.TOP] = false;

                /// 하
                if (tileIndex2_X == tileIndex_X && tileIndex2_Y == tileIndex_Y + 1)
                    isDirectionBounding[(int)TileDirection.BOTTOM] = false;

                /// 좌
                if (tileIndex2_Y == tileIndex_Y && tileIndex2_X == tileIndex_X - 1)
                    isDirectionBounding[(int)TileDirection.LEFT] = false;

                /// 우
                if (tileIndex2_Y == tileIndex_Y && tileIndex2_X == tileIndex_X + 1)
                    isDirectionBounding[(int)TileDirection.RIGHT] = false;
            }

            /// 허나 4방향이 전부 경계라면 혼자 저 멀리 낙오된 타일이므로 경계에서 제외한다 
            int countOfDirectionBounding = 0;
            for (int i = 0; i < 4; i++)
            {
                if (isDirectionBounding[i]) countOfDirectionBounding++;
            }
            if (countOfDirectionBounding == 4)
            {
                for (int i = 0; i < 4; i++) isDirectionBounding[i] = false;
            }

            TileInfoForDisplay newTileInfo = new TileInfoForDisplay(tile1, isDirectionBounding);

            /// 경계라고 판단되는 타일들을 인덱싱한다 
            for (int i = 0; i < 4; ++i)
            {
                if (isDirectionBounding[i])
                {
                    int boundingTileIndex = -1;

                    switch ((TileDirection)i)
                    {
                        case TileDirection.TOP:
                            boundingTileIndex = tileIndex - tileSizeWidth;
                            break;

                        case TileDirection.BOTTOM:
                            boundingTileIndex = tileIndex + tileSizeWidth;
                            break;

                        case TileDirection.LEFT:
                            boundingTileIndex = tileIndex - 1;
                            break;

                        case TileDirection.RIGHT:
                            boundingTileIndex = tileIndex + 1;
                            break;
                    }

                    newTileInfo.boundingTileIndex[i] = boundingTileIndex;
                }
            }

            tileListForDisplay.Add(newTileInfo);
        }

    }
    void GetBoundaryOnly(List<int[]> tileList)
    {
        foreach (int[] path in tileList)
        {
            int tileIndex = path[1];
            int tileIndex_X = path[1] % tileSizeWidth;
            int tileIndex_Y = path[1] / tileSizeHeight;
            Tile tile = tiles[tileIndex_X, tileIndex_Y];

            bool[] isDirectionBounding = new bool[4];
            for (int i = 0; i < 4; ++i)
            {
                isDirectionBounding[i] = true;
            }

            /// 해당 타일 상하좌우를 검색
            /// 상하좌우에 이동가능경로 타일이 있다면 그 방향은 경계처리하지 않아도 된다 
            /// 만약 없다면, 그 방향은 경계처리를 해야 한다 
            foreach (int[] path2 in tileList)
            {
                int tileIndex2 = path2[1];
                int tileIndex2_X = path2[1] % tileSizeWidth;
                int tileIndex2_Y = path2[1] / tileSizeHeight;

                /// 상
                if (tileIndex2_X == tileIndex_X && tileIndex2_Y == tileIndex_Y - 1)
                    isDirectionBounding[(int)TileDirection.TOP] = false;

                /// 하
                if (tileIndex2_X == tileIndex_X && tileIndex2_Y == tileIndex_Y + 1)
                    isDirectionBounding[(int)TileDirection.BOTTOM] = false;

                /// 좌
                if (tileIndex2_Y == tileIndex_Y && tileIndex2_X == tileIndex_X - 1)
                    isDirectionBounding[(int)TileDirection.LEFT] = false;

                /// 우
                if (tileIndex2_Y == tileIndex_Y && tileIndex2_X == tileIndex_X + 1)
                    isDirectionBounding[(int)TileDirection.RIGHT] = false;
            }

            /// 허나 4방향이 전부 경계라면 혼자 저 멀리 낙오된 타일이므로 경계에서 제외한다 
            int countOfDirectionBounding = 0;
            for (int i = 0; i < 4; i++)
            {
                if (isDirectionBounding[i]) countOfDirectionBounding++;
            }
            if (countOfDirectionBounding == 4)
            {
                for (int i = 0; i < 4; i++) isDirectionBounding[i] = false;
            }

            TileInfoForDisplay newTileInfo = new TileInfoForDisplay(tile, isDirectionBounding);

            /// 경계라고 판단되는 타일들을 인덱싱한다 
            for (int i = 0; i < 4; ++i)
            {
                if (isDirectionBounding[i])
                {
                    int boundingTileIndex = -1;

                    switch ((TileDirection)i)
                    {
                        case TileDirection.TOP:
                            boundingTileIndex = tileIndex - tileSizeWidth;
                            break;

                        case TileDirection.BOTTOM:
                            boundingTileIndex = tileIndex + tileSizeWidth;
                            break;

                        case TileDirection.LEFT:
                            boundingTileIndex = tileIndex - 1;
                            break;

                        case TileDirection.RIGHT:
                            boundingTileIndex = tileIndex + 1;
                            break;
                    }

                    newTileInfo.boundingTileIndex[i] = boundingTileIndex;
                }
            }

            tileListForDisplay.Add(newTileInfo);
        }
    }

    void DisplayTileList(VectorLine vectorline, List<TileInfoForDisplay> tileListForDisplay)
    {
        foreach (var tileInfo in tileListForDisplay)
        {
            Vector3 leftTop = tileInfo.tile.GetVertexPositionByScale(Tile.TileVertex.LEFT_TOP, 1);
            Vector3 leftBottom = tileInfo.tile.GetVertexPositionByScale(Tile.TileVertex.LEFT_BOTTOM, 1);
            Vector3 rightTop = tileInfo.tile.GetVertexPositionByScale(Tile.TileVertex.RIGHT_TOP, 1);
            Vector3 rightBottom = tileInfo.tile.GetVertexPositionByScale(Tile.TileVertex.RIGHT_BOTTOM, 1);

            /// newLT, newRT, newLB, newRB를 쓰는 이유
            /// 단색 라인 메테리얼을 쓰게 될 시, 라인이 포인트의 끝과 끝을 정확하게 긋고 렌더링해서 모서리가 파인 것처럼 표시된다
            /// 사각형을 그릴 때 윗줄과 아랫줄만 x를 좀 더 width/2 만큼 늘려줘서 그리면 모서리가 자연스럽게 연결된 것처럼 그려진다 
            /// 사실 이런 작업을 하지 않아도, navigation처럼 포인트와 포인트를 계속 이어지게 세팅해놓으면 모서리가 파인 것처럼 표시 안되긴 하지만 
            /// 그렇게 하면 이번엔 glow 메테리얼을 쓸때 또 이상하게 렌더링된다 
            /// 즉, glow 메테리얼일 때랑, 단색 메테리얼일 때 각각 포인트 세팅법을 다르게 해줘야 한다
            if (tileInfo.isDirectionBounding[(int)TileDirection.TOP])
            {
                Vector3 newLT = leftTop;
                newLT.x -= thicknessOfColorLine / 2f;
                Vector3 newRT = rightTop;
                newRT.x += thicknessOfColorLine / 2f;
                vectorline.points3.Add(newLT);
                vectorline.points3.Add(newRT);
            }

            if (tileInfo.isDirectionBounding[(int)TileDirection.BOTTOM])
            {
                Vector3 newLB = leftBottom;
                newLB.x -= thicknessOfColorLine / 2f;
                Vector3 newRB = rightBottom;
                newRB.x += thicknessOfColorLine / 2f;
                vectorline.points3.Add(newLB);
                vectorline.points3.Add(newRB);
            }

            if (tileInfo.isDirectionBounding[(int)TileDirection.LEFT])
            {
                vectorline.points3.Add(leftTop);
                vectorline.points3.Add(leftBottom);
            }

            if (tileInfo.isDirectionBounding[(int)TileDirection.RIGHT])
            {
                vectorline.points3.Add(rightTop);
                vectorline.points3.Add(rightBottom);
            }
        }

        vectorline.Draw3D();
    }
    void DisplayTileList(VectorLine vectorline, List<TileInfoForDisplay> tileListForDisplay, Color color)
    {
        foreach (var tileInfo in tileListForDisplay)
        {
            Vector3 leftTop = tileInfo.tile.GetVertexPositionByScale(Tile.TileVertex.LEFT_TOP, 1);
            Vector3 leftBottom = tileInfo.tile.GetVertexPositionByScale(Tile.TileVertex.LEFT_BOTTOM, 1);
            Vector3 rightTop = tileInfo.tile.GetVertexPositionByScale(Tile.TileVertex.RIGHT_TOP, 1);
            Vector3 rightBottom = tileInfo.tile.GetVertexPositionByScale(Tile.TileVertex.RIGHT_BOTTOM, 1);

            /// newLT, newRT, newLB, newRB를 쓰는 이유
            /// 단색 라인 메테리얼을 쓰게 될 시, 라인이 포인트의 끝과 끝을 정확하게 긋고 렌더링해서 모서리가 파인 것처럼 표시된다
            /// 사각형을 그릴 때 윗줄과 아랫줄만 x를 좀 더 width/2 만큼 늘려줘서 그리면 모서리가 자연스럽게 연결된 것처럼 그려진다 
            /// 사실 이런 작업을 하지 않아도, navigation처럼 포인트와 포인트를 계속 이어지게 세팅해놓으면 모서리가 파인 것처럼 표시 안되긴 하지만 
            /// 그렇게 하면 이번엔 glow 메테리얼을 쓸때 또 이상하게 렌더링된다 
            /// 즉, glow 메테리얼일 때랑, 단색 메테리얼일 때 각각 포인트 세팅법을 다르게 해줘야 한다
            if(tileInfo.isDirectionBounding[(int)TileDirection.TOP])
            {
                Vector3 newLT = leftTop;
                newLT.x -= thicknessOfColorLine/2f;
                Vector3 newRT = rightTop;
                newRT.x += thicknessOfColorLine/2f;
                vectorline.points3.Add(newLT);
                vectorline.points3.Add(newRT);
            }

            if (tileInfo.isDirectionBounding[(int)TileDirection.BOTTOM])
            {
                Vector3 newLB = leftBottom;
                newLB.x -= thicknessOfColorLine/2f;
                Vector3 newRB = rightBottom;
                newRB.x += thicknessOfColorLine/2f;
                vectorline.points3.Add(newLB);
                vectorline.points3.Add(newRB);
            } 

            if (tileInfo.isDirectionBounding[(int)TileDirection.LEFT])
            {
                vectorline.points3.Add(leftTop);
                vectorline.points3.Add(leftBottom);
            }

            if (tileInfo.isDirectionBounding[(int)TileDirection.RIGHT])
            {
                vectorline.points3.Add(rightTop);
                vectorline.points3.Add(rightBottom);
            }
        }

        vectorline.Draw3D();
        vectorline.color = color;
    }


    // 실제로 닿을 수 없는 대상까지 navigation으로 닿기 위해서 추가한 additionalTarget 매개변수
    // 다익스트라로 구할수 없는 경로또한 강제로 입력해서 네비게이션을 그릴 수 있게한다.
    // additionalTarget을 배열이나 리스트로 담아서 여러 타일의 경로를 추가시킬 수 있어야하지만 
    // 우선... trace스킬에서만 필요하니 간단하게짜보자.
    public void DisplayNavigationPath(Animal animal, Tile target, Tile additionalTarget = null, int[] precalculatedRoute = null)
    {
        if (animal == null || target == null)
        {
            return;
        }

        lineNavigation.Resize(0);
        int[] shortestPath = null;
        if (precalculatedRoute == null)
        {
            shortestPath = GetShortestPathForMovement(animal, target._tileIndex, animal.curremainMovement);
        }
        else
        {
            shortestPath = precalculatedRoute;
        }

        if (shortestPath != null)
        {
            // 마지막 추가타일까지의 경로도 그려줘야함
            if (additionalTarget != null)
            {
                lineNavigation.points3.Add(additionalTarget.position);
                lineNavigation.points3.Add(tiles[ shortestPath[1] % tileSizeWidth, shortestPath[1] / tileSizeHeight].position);
            }
            for (int i = 1; i < shortestPath[0]; i++)
            {
                lineNavigation.points3.Add(tiles[shortestPath[i] % tileSizeWidth, shortestPath[i] / tileSizeHeight].position);
                lineNavigation.points3.Add(tiles[shortestPath[i+1] % tileSizeWidth, shortestPath[i+1] / tileSizeHeight].position);
            }

            lineNavigation.Draw3D();

            Tile goalTile = null;
            if (additionalTarget != null)
            {
                goalTile = additionalTarget;
            }
            else
            {
                goalTile = tiles[shortestPath[1] % tileSizeWidth, shortestPath[1] / tileSizeHeight];
            }

            dot.Resize(0);
            dot.points3.Add(goalTile.position);
            dot.Draw3D();
        }
    }


    public void UndisplayNavigationPath()
    {
        lineNavigation.Resize(0);
        lineNavigation.Draw3D();

        dot.Resize(0);
        dot.Draw3D();
    }

    /*
    public void UpdateRainyAreaVectorline()
    {
        lineGrey.Resize(0);
        foreach (var rainyTile in rainDecider.rainyTileList)
        {
            AddVerticesIntoLinePointsByScale(rainyTile, 0.7f, lineGrey);
        }
    }

    public void DisplayRainyArea()
    {
        lineGrey.Draw3D();
    }

    public void DisplayAreasToBreed()
    {
        lineMagenta.Draw3D();
    }
    */
    public void UndisplayAreasToBreed()
    {
        lineMagenta.Resize(0);
        lineMagenta.Draw3D();
    }

    public void DisplaySelectedTile()
    {
        lineGreen.Draw3D();
    }

    public void UndisplaySelectedTile()
    {
        lineGreen.Resize(0);
        lineGreen.Draw3D();
    }


    public void AddVerticesIntoLinePointsByScale(Tile tile, float scale, VectorLine line)
    {
        Vector3 leftTop = tile.GetVertexPositionByScale(Tile.TileVertex.LEFT_TOP, scale);
        Vector3 leftBottom = tile.GetVertexPositionByScale(Tile.TileVertex.LEFT_BOTTOM, scale);
        Vector3 rightTop = tile.GetVertexPositionByScale(Tile.TileVertex.RIGHT_TOP, scale);
        Vector3 rightBottom = tile.GetVertexPositionByScale(Tile.TileVertex.RIGHT_BOTTOM, scale);

        Vector3 newLT = leftTop;
        newLT.x -= thicknessOfColorLine/2f;
        Vector3 newRT = rightTop;
        newRT.x += thicknessOfColorLine/2f;
        
        Vector3 newLB = leftBottom;
        newLB.x -= thicknessOfColorLine/2f;
        Vector3 newRB = rightBottom;
        newRB.x += thicknessOfColorLine/2f;

        line.points3.Add(newLT);
        line.points3.Add(newRT);

        line.points3.Add(rightTop);
        line.points3.Add(rightBottom);

        line.points3.Add(leftTop);
        line.points3.Add(leftBottom);

        line.points3.Add(newLB);
        line.points3.Add(newRB);        
    }


    public List<Tile> GetTilesInRange(int centerX, int centerY, int range)
    {
        List<Tile> tilesInRange = new List<Tile>();

        var dist = range * range;

        for (var dx = -(range); dx <= range; dx++)
        {
            for (var dz = -(range); dz <= range; dz++)
            {
                var px = centerX + dx;
                var pz = centerY + dz;

                if (!IsValidTileIndex(px, pz))
                {
                    continue;
                }

                /// 원 모양
                Vector2 v = new Vector2(dx, dz);
                float sqrmag = v.sqrMagnitude;
                float degreeOfCircle = 4f; // 이 값에 따라 원의 둥근 정도가 결정된다. 클수록 사각형에 가까워짐 
                float modsqrmag = sqrmag - (degreeOfCircle * 0.5f);
                if (modsqrmag <= dist)
                {
                    Tile tile = tiles[px, pz];

                    tilesInRange.Add(tile);
                }
            }
        }

        return tilesInRange;
    }


    Dictionary<string, VectorLine> lineDictionary = new Dictionary<string, VectorLine>();
    public void DisplayRange(int centerX, int centerY, int range, string lineName, Color color)
    {
        VectorLine line;
        lineDictionary.TryGetValue(lineName, out line);
        if(line == null)
        {
            line = new VectorLine(lineName, new List<Vector3>(), lineMaterial, 20.0f, LineType.Discrete, Joins.Weld);
            lineDictionary.Add(lineName, line);
        }

        line.Resize(0);

        foreach (var tile in GetTilesInRange(centerX, centerY, range))
        {
            AddVerticesIntoLinePointsByScale(tile, 1f, line);
        }

        line.Draw3D();
        line.color = color;
    }


    public void UndisplayLine(string lineName)
    {
        VectorLine line;
        lineDictionary.TryGetValue(lineName, out line);
        if(line != null)
        {
            line.Resize(0);
            line.Draw3D();
        }
    }

    //[SerializeField]
    //GameObject targetHighLights;
    //public GameObject CreateTileTargetHighlight()
    //{
    //    GameObject targetHighlight = targetHighLights.gameObject.AddChild(highLightPrefab);
    //    targetHighlight.transform.Rotate(Vector3.right * 90);
    //    targetHighlight.transform.localScale = new Vector3(35, 35, 1);
    //    targetHighlight.transform.localPosition = Vector3.up * 1;
    //    targetHighlight.SetActive(false);

    //    return targetHighlight;
    //}

    public void DisplayTileHighlight(Tile tile, string lineName, Color color, float scale = 1f)
    {
        VectorLine line;
        lineDictionary.TryGetValue(lineName, out line);
        if (line == null)
        {
            line = new VectorLine(lineName, new List<Vector3>(), lineColorMaterial, thicknessOfColorLine, LineType.Discrete, Joins.Weld);
            lineDictionary.Add(lineName, line);
        }

        line.Resize(0);

        AddVerticesIntoLinePointsByScale(tile, scale, line);

        line.Draw3D();
        line.color = color;
    }


    public void DisplayAnimalTrailsWithinRange(int centerX, int centerY, int range)
    {
        foreach (var tile in GetTilesInRange(centerX, centerY, range))
        {
            for (int i = 0; i < animalTrails.Count; ++i)
            {
                AnimalTrail animalTrail = animalTrails[i];
                if(animalTrail.ownerUserId != PhotonNetwork.player.UserId && !animalTrail.isDirecting && animalTrail.IsInPath(tile))
                {
                    animalTrail.DisplayTrail();
                }
            }
        }
    }

    
    public void UndisplayAnimalTrails()
    {
        for (int i = 0; i < animalTrails.Count; ++i)
        {
            AnimalTrail animalTrail = animalTrails[i];
            animalTrail.UndisplayTrail();
        }
    }


    public bool IsTileValidForMove(Tile tile)
    {
        return tile != null && dijkstra.IsTileValidForMove(tile._tileIndex);
    }


    public void ChnangeTileOccupation(int tileIndex, bool isOccupied)
    {
        dijkstra.tileArrayOccupied[tileIndex % tileSizeWidth, tileIndex / tileSizeHeight] = isOccupied;
    }


    public bool IsValidTileIndex(int tile_x, int tile_y)
    {
        return !(tile_x < 0 || tile_x >= TileMaker.instance.tileSize || tile_y < 0 || tile_y >= TileMaker.instance.tileSize);
    }

    public bool IsValidTilePos(float x, float z)
    {
        return x <= Tile.TILE_WORLD_HEIGHT * TileChunk.instance.tileSizeHeight && x >= 0 && z <= Tile.TILE_WORLD_WIDTH * tileSizeWidth && z >= 0;
    }

    public Tile tempTile = new Tile() { type = TileMaker.TileType.NA_TILE };
    public Tile GetTile(int tile_x, int tile_y)
    {
        tempTile._tileIndex = tile_x * TileMaker.instance.tileSize + tile_y;

        if (IsValidTileIndex(tile_x, tile_y))
        {
            Tile tile = tiles[tile_x,tile_y];
            return (tile == null) ? tempTile : tile;
        }
        else
        {
            return tempTile;
        }
    }

    // materialName 매터리얼에 관련된 CombineInstance 리스트를 반환함 (각 Combineinstance는 2^16개의 버텍스만 저장가능)
    public List<CombineInstance> GetCombineList(string materialName)
    {
        List<CombineInstance> combineList;
        // 매터리얼 : combineInstance들의 리스트
        // 기존에 있는 메터리얼이라면 combineList에 할당
        if (combineListDic.ContainsKey(materialName))
        {
            combineList = combineListDic[materialName];
        }
        // 기존에 없는 메터리얼이라면 새로 combineInstance
        else
        {
            combineList = new List<CombineInstance>();
            combineListDic.Add(materialName, combineList);
        }

        return combineList;
    }
    
    public void LoadAndPushMeshResource(string resourceName, Vector3 posFactor)
    {
        GameObject resource = (GameObject)Resources.Load(resourceName);

        /// 리소스 위치를 0,0,0으로 맞춰놔야 자식으로 있는 파츠의 위치가 제대로 0,0,0 기준으로 세팅된다
        resource.transform.position = new Vector3(0, 0, 0);

        MeshFilter[] meshFilters = resource.GetComponentsInChildren<MeshFilter>();
        MeshRenderer[] meshRenderers = resource.GetComponentsInChildren<MeshRenderer>();
        for (int i = 0; i < meshFilters.Length; ++i)
        {
            GameObject childPart = meshFilters[i].gameObject;
            MeshFilter meshFilter = meshFilters[i];
            MeshRenderer meshRenderer = meshRenderers[i];

            Quaternion rot = childPart.transform.rotation;
            Vector3 scale = Vector3.Scale(resource.transform.localScale, childPart.transform.localScale);

            CombineInstance newCombineInstance = new CombineInstance();
            newCombineInstance.mesh = meshFilter.sharedMesh;
            newCombineInstance.transform = Matrix4x4.TRS(posFactor + childPart.transform.position, rot, scale);

            // 같은 material을 사용하는 mesh끼리 리스트로 묶는구나.
            // 하나의 object당 하나의 CombineInstance라고 생각하면되는건가.
            foreach (var material in meshRenderer.sharedMaterials)
            {
                List<CombineInstance> combineList = GetCombineList(material.name);
                combineList.Add(newCombineInstance);
            }
        }
    }


    void SetupTileData()
    {
        /// 타일 바닥 메쉬 정보 세팅
        MeshFilter filter = tileMeshPrefab.GetComponentInChildren<MeshFilter>();
        Mesh polyTileMesh = filter.sharedMesh ?? filter.mesh;

        Quaternion rot = tileMeshPrefab.transform.rotation;
        Vector3 scale = tileMeshPrefab.transform.localScale;

        // 타입이 결정된 타일들의 타입에 따라 매터리얼 해쉬에 저장함
        for (int tileIndex = 0; tileIndex < tileSizeWidth * tileSizeHeight;)
        {
            CombineInstance newCombineInstance = new CombineInstance();

            int x = tileIndex % tileSizeWidth;
            int y = tileIndex / tileSizeHeight;

            Tile tile = tiles[x, y];

            newCombineInstance.mesh = polyTileMesh;

            ///          되고자 하는 사이즈   /   해당 Mesh의 실제 월드 사이즈 
            scale.x *= Tile.TILE_WORLD_WIDTH / (polyTileMesh.bounds.size.x * scale.x);
            scale.y *= Tile.TILE_WORLD_HEIGHT / (polyTileMesh.bounds.size.y * scale.y);
            scale.z *= Tile.TILE_WORLD_WIDTH / (polyTileMesh.bounds.size.z * scale.z);
            newCombineInstance.transform = Matrix4x4.TRS(
                new Vector3(
                    x * Tile.TILE_WORLD_WIDTH + Tile.TILE_WORLD_WIDTH / 2f,
                -Tile.TILE_WORLD_HEIGHT / 2f - 1f + yFactorOfTileType[(int)tile.type],
                -y * Tile.TILE_WORLD_HEIGHT - Tile.TILE_WORLD_HEIGHT / 2f),
                rot, scale);

            ++tileIndex;

            List<CombineInstance> combineList = GetCombineList(tileTypeMaterials[(int)tile.type].name);
            combineList.Add(newCombineInstance);
        }

        CombineMesh();
    }
    
    void CombineMesh()
    {
        /// 모아놓은 모든 타일들의 메쉬 정보들을 이용해서 Chunk Mesh로 Combine 시킨다
        foreach (var combineList in combineListDic)
        {
            /// 일반적인 정사각형 큐브를 Mesh로 썼다 가정했을 때, CombineInstance가 감당할 수 있는 오브젝트의 갯수는 
            /// 65534(하나의 Combineinstance, mesh의 최대 버텍스) / 한 오브젝트의 개수(정사각형 버텍스 갯수) = 2730개이다
            
            // 같은 매터리얼을 사용하면 같은 버텍스의 수를 쓸 것이라는 가정하에 하였음
            // 이 매터리얼을 사용하는 매쉬의 버텍스 수를 가지고 한 콤바인매쉬에 몇개의 매쉬를 넣을 수 있는지 계산
            // 나중에 풀, 나무, 바위, 꽃을 모두 묶어버릴 수있는데(메터리얼을 하나로 통일해서) 한 컴바인메쉬에 들어갈 수 있는 매쉬의 개수를 잘 정해야할 것이다.
            int MESH_VERTICES = combineList.Value[0].mesh.vertexCount;
            int DIVIDE_COUNT = 65534 / MESH_VERTICES;

            for (int i = 0; i < combineList.Value.Count; i += DIVIDE_COUNT)
            {
                var divideCombineList = combineList.Value.GetRange(i, Mathf.Min(DIVIDE_COUNT, combineList.Value.Count - i));

                CombineInstance[] combines = divideCombineList.ToArray();

                Mesh chunkMesh = new Mesh();
                chunkMesh.CombineMeshes(combines);

                GameObject newChunk = Instantiate(chunkPrefab);
                newChunk.name = combineList.Key;
                if (combineList.Key == string.Format("RIVER"))
                {
                    newChunk.layer = 4; // WATER 레이어
                }
                newChunk.transform.SetParent(transform);
                // 여기에 자식 오브젝트로 LOD를 넣어야함
                // 매쉬의 리스트가 아니라 매쉬의 리스트(LOD 0 1 2 ..)의 리스트로 자료구조를 바꿔야 하고 
                // newChunk는 LOD0 1 2 3 을 자식으로 가지고 있어야 한다.
                newChunk.GetComponent<MeshFilter>().sharedMesh = chunkMesh;

                Material material;
                if (materialDic.ContainsKey(combineList.Key))
                {
                    material = materialDic[combineList.Key];
                }
                else
                {
                    material = GameObject.Instantiate(Resources.Load("Materials/" + combineList.Key)) as Material;
                    materialDic.Add(combineList.Key, material);
                }

                newChunk.GetComponent<MeshRenderer>().sharedMaterial = material;
            }
        }
    }

    public void SetAlphaTexture(Tile tile, Color color)
    {
        alphaTex.SetPixel(tile.TileIndex_X, TileMaker.instance.tileSize - tile.TileIndex_Y - 1, color);
        alphaTex.Apply();
    }

    public void SetFogMaskTexture(Tile tile, Color color)
    {
        fogMaskTex.SetPixel(tile.TileIndex_X, TileMaker.instance.tileSize - tile.TileIndex_Y - 1, color);
        fogMaskTex.Apply();
    }


    public void RefreshTileOccupation()
    {
        /// 먼저 모든 동물의 TileOccupation 해제
        List<Tile> tileListInSight = GameManager.instance.sightOfUser[PhotonNetwork.player.UserId];

        foreach (Animal animal in GameManager.instance.animals1ForSimulation) TileChunk.instance.ChnangeTileOccupation(animal.ownedTile._tileIndex, false);
        foreach (Animal animal in GameManager.instance.animals2ForSimulation) TileChunk.instance.ChnangeTileOccupation(animal.ownedTile._tileIndex, false);
        foreach (Animal animal in GameManager.instance.huntersForSimulation) TileChunk.instance.ChnangeTileOccupation(animal.ownedTile._tileIndex, false);
        foreach (Tile tile in tileListInSight)
        {
            Animal owningAnimal = tile.owningAnimal;
            if (owningAnimal == null) continue;

            /// 내 동물이거나
            /// 적의 동물인데 은신은 아닌 경우 
            /// TileOccupation 할당 
            if (owningAnimal.photonView.isMine || (!owningAnimal.photonView.isMine && !owningAnimal.isConcealed && !owningAnimal.isSubmerged && !owningAnimal.isClimbedTree)) 
            {
                TileChunk.instance.ChnangeTileOccupation(owningAnimal.ownedTile._tileIndex, true);
            }
        }
    }

    // 맵의 디텍트여부 갱신
	// UserID의 isDetected 갱신
    public void RefreshDetect(string userID, Dictionary<Animal, List<Tile>> detectTable)
    {
        List<Animal> animalListToRefresh = null;

        // 어느동물의 시야를 업데이트할지 결정한다.
        if (userID == GameManager.instance.userIDForSimulation_origin)
        {
            animalListToRefresh = GameManager.instance.animals1ForSimulation;
        }
        else
        {
            animalListToRefresh = GameManager.instance.animals2ForSimulation;
        }

        // 일단 true인것을 false로 다 고쳐줌
        for (int i = 0; i < tileSizeHeight; i++)
        {
            for(int j = 0; j < tileSizeWidth; j++)
            {
                if(tiles[i, j].isDetected[userID] == true) tiles[i, j].isDetected[userID] = false;
            }
        }
        for(int i = 0; i < animalListToRefresh.Count ; i++)
        {
            Animal animal = animalListToRefresh[i];
            List<Tile> detectedTiles = detectTable[animal];

            for(int j = 0; j < detectedTiles.Count; j++)
            {
                detectedTiles[j].isDetected[userID] = true;
            }
        }
    }

    // animalToRefresh가 소속된 동물 리스트의 시야를 기준으로 리프레쉬한다.
    // animalToRefresh가 null이면 기본적으로 myAnimals 리스트를 기준으로 리프레쉬한다.
    public void RefreshFieldOfView(Animal animalToRefresh = null)
    {
        List<Animal> animalListToRefresh = null;

        // 어느동물의 시야를 업데이트할지 결정한다.
        if (animalToRefresh != null)
        {
            animalListToRefresh = animalToRefresh.animalListBelongTo;
        }       
        if (animalListToRefresh == null)
        {
            animalListToRefresh = GameManager.instance.myAnimals;
        }

        /// 시야 리스트 갱신
        List<Tile> tileListInSight = GameManager.instance.sightOfUser[PhotonNetwork.player.UserId];
        tileListInSight.Clear();
        foreach (Animal animal in animalListToRefresh)
        {
            Tile animalTile = animal.isMoving ? animal.movingTile : animal.ownedTile;
            if (animalTile != null)
                TileChunk.instance.fieldOfView.Refresh(new Vec(animalTile.TileIndex_X, animalTile.TileIndex_Y), animal.curSight, tileListInSight, false);
        }

        // 내 동물의 모든 절대시야 리스트를 시야에 추가해주어야 한다
        for (int i = 0; i < animalListToRefresh.Count ; i++)
        {
            if (absoluteSightTileTable.ContainsKey(animalListToRefresh[i]))
            {
                List<Tile> absoluteSightTileList = absoluteSightTileTable[animalListToRefresh[i]];
                /// 절대시야도 추가
                for (int j = 0; j < absoluteSightTileList.Count; j++)
                {
                    tileListInSight.Add(absoluteSightTileList[j]);
                }
            }
        }

        /// 시야에 안보이는 적은 TileOccupation falae 시켜서, 클릭만으로 유닛 위치를 알수없게 만들고
        /// 시야에 보이는 적은 TileOccupation true 시킨다 
        RefreshTileOccupation();

        // refresh할 동물이 정해지지 않았다면
        // 단순히 AI시야를 보이지 않게 했을경우 AI차례일때는 시야refresh을 하지않는다.
        if (animalToRefresh == null) {
            if (!GameManager.instance.showSightOfAI
                && PhotonNetwork.player.UserId != GameManager.instance.userIDForSimulation_origin)
                return;
        }
        // refresh할 동물이 정해진경우는 상대방의경우(AI포함)에도 플레이어의 동물이 움직일경우이다.
        // 이때는 플레이어의 동물이 움직이면 시야refresh을해야하고 ai동물이 움직이면 refresh하지 말아야한다.
        else
        {
            if (!GameManager.instance.showSightOfAI
            && PhotonNetwork.player.UserId != GameManager.instance.userIDForSimulation_origin
            && animalToRefresh.ownerUserId != GameManager.instance.userIDForSimulation_origin) return;
        }

        /// 먼저, 모든 타일의 visible을 false로 바꿔놓는다 
        for (int tile_x = 0; tile_x < tileSizeWidth; ++tile_x)
        {
            for (int tile_y = 0; tile_y < tileSizeHeight; ++tile_y)
            {
                tiles[tile_x, tile_y].isVisible = false;
            }
        }

        /// 시야 타일 visible 
        for(int i = 0; i < tileListInSight.Count; i++) { 
            tileListInSight[i].isVisible = true;
        }

        /// 안개 갱신
        fogOfWar.RefreshTexture();
    }


    public void OnSwitchedDayAndNight(DayAndNight.TimeOfNature switchedTime)
    {
        fogOfWar.OnSwitchedDayAndNight(switchedTime);

        RefreshFieldOfView();
    }


    void CreateTile(TileMaker.TileType tileType, int tileIndex, bool isRock)
    {
        Tile newTile = new Tile();
        newTile._tileIndex = tileIndex;
        newTile.type = tileType;
        //newTile.SetInitialResourcesByType();

        newTile.tileChunk = this;/*
        newTile.rainySeasonProcessor = rainySeasonProcessor;
        newTile.normalSeasonProcessor = normalSeasonProcessor;
        newTile.draughtSeasonProcessor = draughtSeasonProcessor;*/

        int tileIndex_x = tileIndex % tileSizeWidth;
        int tileIndex_y = tileIndex / tileSizeHeight;
        tiles[tileIndex_x, tileIndex_y] = newTile;

        newTile.isRock = isRock;

        /// 순서 중요
        newTile.Init(); 
    }


    public override void OnJoinedRoom()
    {
        SyncTilesByUsingTilesProperties();
    }


    public override void OnPhotonCustomRoomPropertiesChanged(Hashtable propertiesThatChanged)
    {
        if (propertiesThatChanged.ContainsKey(TileMaker.TILES_PROP_KEY))
        {
            SyncTilesByUsingTilesProperties();
        }
        else if (propertiesThatChanged.ContainsKey(AnimalTrail.TOTAL_INDEX_PROP_KEY))
        {
            /// 클라에 생성되어있는 animalTrails를 전부 뒤져서 해당하는 index가 없다면 새로 만들어준다 
            int animalTrailsTotalIndex = (int)PhotonNetwork.room.CustomProperties[AnimalTrail.TOTAL_INDEX_PROP_KEY];
            int animalTrailsStartIndex = (int)PhotonNetwork.room.CustomProperties[AnimalTrail.START_INDEX_PROP_KEY];
            for (int trailIndex = animalTrailsStartIndex; trailIndex < animalTrailsTotalIndex; ++trailIndex)
            {
                bool foundAnimalTrail = false;
                for (int i = 0; i < animalTrails.Count; ++i)
                {
                    if (animalTrails[i].index == trailIndex)
                    {
                        foundAnimalTrail = true;

                        break;
                    }
                }

                if (!foundAnimalTrail)
                {
                    GameObject newGameObject;
                    newGameObject = new GameObject("AnimalTrail");
                    AnimalTrail comp = newGameObject.AddComponent<AnimalTrail>();

                    comp.Init(trailIndex);
                    animalTrails.Add(comp);
                }
            }
        }
        else if (propertiesThatChanged.ContainsKey(AnimalTrail.START_INDEX_PROP_KEY))
        {
            /// animalTrailsStartIndex 이하 전부 삭제 
            int animalTrailsStartIndex = (int)PhotonNetwork.room.CustomProperties[AnimalTrail.START_INDEX_PROP_KEY];
            for (int i = animalTrails.Count - 1; i >= 0; i--)
            {
                AnimalTrail animalTrail = animalTrails[i];
                if (animalTrail.index < animalTrailsStartIndex)
                {
                    animalTrails.RemoveAt(i);
                    Destroy(animalTrail.gameObject);
                }
            }
        }
    }


    void SyncTilesByUsingTilesProperties()
    {
        if (!PhotonNetwork.room.CustomProperties.ContainsKey(TileMaker.TILES_PROP_KEY))
        {
            return;
        }

        if (!isCreatedTiles)
        {
            Hashtable tilesProp = (Hashtable)PhotonNetwork.room.CustomProperties[TileMaker.TILES_PROP_KEY];

            tileSizeWidth = (int)tilesProp[TileMaker.TILES_SIZE_WIDTH_PROP_KEY];
            tileSizeHeight = (int)tilesProp[TileMaker.TILES_SIZE_HEIGHT_PROP_KEY];

            tiles = new Tile[tileSizeWidth, tileSizeHeight];

            for (int tileIndex = 0; tileIndex < tileSizeWidth * tileSizeHeight; ++tileIndex)
            {
                Hashtable tileProp = (Hashtable)tilesProp[tileIndex];
                TileMaker.TileType tileType = (TileMaker.TileType)tileProp[Tile.TILE_TYPE_PROP_KEY];
                int tileTypeint = (int)tileProp[Tile.TILE_TYPE_PROP_KEY];
                bool isRock = (bool)tileProp[Tile.TILE_IS_WALL_PROP_KEY];

                CreateTile(tileType, tileIndex, isRock);
            }

            isCreatedTiles = true;

            /// 생성하려는 Mesh 크기에 맞춰 Colider 생성. OnClick 이벤트를 받기 위한 작업 
            /// 콜라이더는 Center와 Size 개념이 있는데, Size를 늘리면 Center를 기준으로 늘어나는 특징이 있다 
            /// 그렇게 늘어난 Size를 진짜 Mesh에 딱 맞추는 작업이 필요함 
            BoxCollider collider = GetComponent<BoxCollider>();
            collider.size = new Vector3(tileSizeWidth * Tile.TILE_WORLD_WIDTH, 0, tileSizeHeight * Tile.TILE_WORLD_HEIGHT);
            collider.center = new Vector3(tileSizeWidth * Tile.TILE_WORLD_WIDTH * 0.5f, 0, -tileSizeHeight * Tile.TILE_WORLD_HEIGHT * 0.5f);

            /// Chunk Mesh 생성
            SetupTileData();

            /// 다익스트라 
            dijkstra.Init(tileSizeWidth, tileSizeHeight, tiles);

            /// 안개 
            fogOfWar.WorldMax = new Vector3(tileSizeWidth * Tile.TILE_WORLD_WIDTH, 100, tileSizeHeight * Tile.TILE_WORLD_HEIGHT);
            fogOfWar.WorldUnitsPerTileSide = Tile.TILE_WORLD_WIDTH;
            fogOfWar.Initialize();

            /// 타일 생성되었다고 알리기 
            OnCreatedTiles();
        }
    }
    /*
    public void ChangeTileTypeAndTextureFromDessert(Tile tile)
    {
        ChangeTileTypeAndTexture(tile, rainDecider.typeToChange);
    }

    public void ChangeTileTypeAndTexture(Tile tile, TileMaker.TileType type)
    {
        tile.type = type;
    }
    */

}
