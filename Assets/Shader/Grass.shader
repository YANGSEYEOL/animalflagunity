﻿Shader "Custom/Grass"
{
	Properties
	{
		_RampTex("Ramp", 2D) = "white" {}
		_Color("Color", Color) = (1, 1, 1, 1)
		_WaveSpeed("Wave Speed", float) = 1.0
		_WaveAmp1("Wave Amp1", float) = 1.0
		_WaveAmp2("Wave Amp2", float) = 1.0
		_HeightFactor("Height Factor", float) = 1.0
		_HeightCutoff("Height Cutoff", float) = 1.2
		_WindTex("Wind Texture", 2D) = "white" {}
		_WorldSize("World Size", vector) = (1, 1, 1, 1)
		_WindSpeed("Wind Speed", vector) = (1, 1, 1, 1)
		_AlphaTex("AlphaTex", 2D) = "white" {}
		_FogMaskTex("FogMaskTex", 2D) = "white" {}
	}

	SubShader
	{
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			Tags
			{ 
				"RenderType" = "Transparent"
				"Queue" = "Transparent"
				"DisableBatching" = "True"
			}

			CGPROGRAM
			#pragma fragment frag
			#pragma vertex vert
			#pragma multi_compile_fwdbase // shadows
			#include "UnityCG.cginc"
			#include "AutoLight.cginc"

			// Properties
			sampler2D _RampTex;
			sampler2D _WindTex;
			sampler2D _AlphaTex;
			sampler2D _FogMaskTex;
			float4 _WindTex_ST;
			float4 _Color;
			float4 _LightColor0; // provided by Unity
			float4 _WorldSize;
			float _WaveSpeed;
			float _WaveAmp1;
			float _WaveAmp2;
			float _HeightFactor;
			float _HeightCutoff;
			float4 _WindSpeed;
			float _Time1;
			float _Time2;

			struct vertexInput
			{
				float4 vertex : POSITION;
				float3 color : COLOR;
				float3 lightDir : TEXCOORD1;
				float3 normal : NORMAL;
				LIGHTING_COORDS(3, 4)
			};

			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float3 normal : NORMAL;	
				float3 lightDir : TEXCOORD1;
				float3 color : COLOR;
				float alpha : COLOR1;
				float fog : float;
				//float2 sp : TEXCOORD0; // test sample position
			};

			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;

				// convert input to clip & world space
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);

				float4 normal4 = float4(input.normal, 0.0);
				output.normal = normalize(mul(normal4, _Object2World).xyz);
				//output.lightDir = normalize(ObjSpaceLightDir(.vertex));

				// get vertex world position
				float4 worldPos = mul(input.vertex, _Object2World);
				// normalize position based on world size
				float2 samplePos = worldPos.xz / _WorldSize.xz;
				// scroll sample position based on time

				output.alpha = tex2Dlod(_AlphaTex, float4(samplePos, 0, 0)).x;
				float fog = tex2Dlod(_FogMaskTex, float4(samplePos, 0, 0));
				output.fog = fog.r;
				output.color = input.color;
				output.color = fog * output.color;

				samplePos += _Time1 * _WindSpeed.xz;
				float windSample1 = tex2Dlod(_WindTex, float4(samplePos, 0, 0));

				samplePos = _Time2 * _WindSpeed.xz;
				float windSample2 = tex2Dlod(_WindTex, float4(samplePos.x, 0.5, 0, 0));

				// 0 animation below _HeightCutoff
				float heightFactor = input.vertex.y > _HeightCutoff;
				heightFactor = input.vertex.y;
				// make animation stronger with height
				//heightFactor = heightFactor * pow(input.vertex.y, _HeightFactor);
				
				// apply wave animation
				output.pos.z += sin(_WaveSpeed*windSample1)*_WaveAmp1 * heightFactor * fog * windSample2;
				output.pos.x += cos(_WaveSpeed*windSample1)*_WaveAmp1 * heightFactor * fog * windSample2;

				TRANSFER_VERTEX_TO_FRAGMENT(output);
				return output;
			}

			float4 frag(vertexOutput input) : COLOR
			{
				// normalize light dir
				float3 lightDir = normalize(_WorldSpaceLightPos0.xyz);

				// apply lighting
				float ramp = clamp(dot(input.normal, lightDir), 0.001, 1.0);
				float3 lighting = tex2D(_RampTex, float2(ramp, 0.5)).rgb;

				//return float4(frac(input.sp.x), 0, 0, 1); // test sample position
				float atten = LIGHT_ATTENUATION(input);
				float3 rgb = _LightColor0.rgb * lighting * _Color.rgb;// *atten;

				return float4(rgb, input.alpha);
			}

			ENDCG
		}

		Pass
		{
			Tags
			{
				"LightMode" = "ShadowCaster"
			}

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase // shadows
			#include "UnityCG.cginc"
			#include "AutoLight.cginc"
				
			// Properties
			sampler2D _RampTex;
			sampler2D _WindTex;
			sampler2D _FogMaskTex;
			float4 _WindTex_ST;
			float4 _Color;
			float4 _LightColor0; // provided by Unity
			float4 _WorldSize;
			float _WaveSpeed;
			float _WaveAmp1;
			float _HeightFactor;
			float _HeightCutoff;
			float4 _WindSpeed;
			float _Time1;
			float _Time2;

			struct vertexInput
			{
				float4 vertex : POSITION;
				float3 vlight : COLOR;
				float3 lightDir : TEXCOORD1;
				float3 normal : NORMAL;
				LIGHTING_COORDS(3, 4)
			};

			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float3 normal : NORMAL;
				float3 lightDir : TEXCOORD1;
				float3 vlight : COLOR;
				float fog : float;
				//float2 sp : TEXCOORD0; // test sample position
			};

			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;
				
				// convert input to clip & world space
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);

				float4 normal4 = float4(input.normal, 0.0);
				output.normal = normalize(mul(normal4, _Object2World).xyz);

				// get vertex world position
				float4 worldPos = mul(input.vertex, _Object2World);
				// normalize position based on world size
				float2 samplePos = worldPos.xz / _WorldSize.xz;
				float fog = tex2Dlod(_FogMaskTex, float4(samplePos, 0, 0));
				output.fog = fog.r;

				samplePos += _Time1 * _WindSpeed.xz;
				float windSample1 = tex2Dlod(_WindTex, float4(samplePos, 0, 0));

				samplePos = _Time2 * _WindSpeed.xz;
				float windSample2 = tex2Dlod(_WindTex, float4(samplePos.x, 0.5, 0, 0));
				//output.sp = samplePos; // test sample position

				// 0 animation below _HeightCutoff
				float heightFactor = input.vertex.y > _HeightCutoff;
				heightFactor = input.vertex.y;
				// make animation stronger with height
				//heightFactor = heightFactor * pow(input.vertex.y, _HeightFactor);

				// apply wave animation
				output.pos.z += sin(_WaveSpeed*windSample1)*_WaveAmp1 * heightFactor * fog * windSample2;
				output.pos.x += cos(_WaveSpeed*windSample1)*_WaveAmp1 * heightFactor * fog * windSample2;
				
				if (fog == 0) {
					output.pos.xz = worldPos.xz;
					output.pos.y = -1;
				}
				//TRANSFER_VERTEX_TO_FRAGMENT(output);
				return output;
			}

			float4 frag(vertexOutput input) : COLOR
			{
				// normalize light dir
				float3 lightDir = normalize(_WorldSpaceLightPos0.xyz);

				// apply lighting
				float ramp = clamp(dot(input.normal, lightDir), 0.001, 1.0);
				float3 lighting = tex2D(_RampTex, float2(ramp, 0.5)).rgb;

				//return float4(frac(input.sp.x), 0, 0, 1); // test sample position
				//float atten = LIGHT_ATTENUATION(input);
				float3 rgb = _LightColor0.rgb * lighting * _Color.rgb;// *atten;

				return float4(rgb, 1);
			}

				ENDCG
		}
	
	}
}