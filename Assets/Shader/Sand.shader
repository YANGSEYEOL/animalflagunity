﻿Shader "Custom/Sand" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		//_BumpMap ("Albedo (RGB)", 2D) = "white" {}
		_WorldSize("World Size", vector) = (700, 1,700, 1)

	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		//sampler2D _BumpMap;

		struct Input {
			float3 worldPos;
			float2 uv_BumpMap;
		};

		fixed4 _Color;
		float4 _WorldSize;

		void surf (Input IN, inout SurfaceOutputStandard o) {
			float3 worldPos = mul(float4(IN.worldPos,1), _Object2World);
			worldPos.z *= -1;
			// normalize position based on world size
			float2 samplePos = worldPos.xz / _WorldSize.xz;

			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, samplePos) * _Color;
			o.Albedo = c.rgb;
			//o.Normal = UnpackNormal(tex2D(_BumpMap, samplePos));

			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
