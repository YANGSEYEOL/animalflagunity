﻿Shader "Custom/targetHighlight" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("MainTex (RGB)", 2D) = "white" {}
		_GlowTex("GlowTex (RGB)", 2D) = "white" {}
		_ChunkSize("Highlight Chunk Size", vector) = (700, 1,700, 1)
		_ChunkPos("Chunk WorldPos ", vector) = (0,0,0,0)  // Chunk의 중심 좌표 물결이 퍼져나가는 위치
	}
	SubShader
	{
		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha
			Tags
			{
				"Queue" = "Transparent"
			}

			CGPROGRAM
			#include "UnityCG.cginc"

			#pragma vertex vert
			#pragma fragment frag

			//Properties
			float4 _Color;
			float4 _ChunkSize;
			float3 _ChunkPos;
			sampler2D _GlowTex;
			sampler2D _MainTex;
			float _ChunkRippleSize;

			struct vertexInput
			{
				float4 vertex : POSITION;
				float2 texCoord : TEXCOORD1;
			};

			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float2 texCoord : TEXCOORD0;
				float2 globalTexCoord : TEXCOORD1;
			};

			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;

				// convert to world space
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);

				// 월드좌표  
				float3 worldPos = mul(_Object2World, input.vertex);
				// worldPos z좌표가 마이너스인데 uv좌표는 양수이어야 하므로 -1를 곱해줌
				worldPos.z *= -1;
				
				// 시간에 따라 텍스처 좌표를 이동시켜서 물결처럼 _ChunkRippleSize는 GameManager update에서 계속 바뀜
				_ChunkSize.x = _ChunkRippleSize;
				_ChunkSize.z = _ChunkRippleSize;

				// samplePos는 chunk기준 uv좌표임
				float2 samplePos = (worldPos.xz - _ChunkPos.xz + _ChunkSize.xz) / (2 * _ChunkSize.xz);

				// texture coordinates 
				output.texCoord = input.texCoord;
				output.globalTexCoord = samplePos;
				return output;
			}

			float4 frag(vertexOutput input) : COLOR
			{				
				// 자체 uv좌표대로 대각텍스처 곱함
				float4 albedo = tex2D(_MainTex, input.texCoord.xy);
			
				// 글로벌 uv좌표대로 glow 텍스처 곱함
				albedo += tex2D(_GlowTex, input.globalTexCoord.xy);
			
				albedo.rgb *= _Color.rgb;
				
				return albedo;
			}

			ENDCG	
		}
	}
}