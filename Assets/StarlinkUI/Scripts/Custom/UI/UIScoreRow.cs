using UnityEngine;

public class UIScoreRow : MonoBehaviour
{
	public UILabel rankLabel;
	public UILabel nameLabel;
	public UILabel titleLabel;
	public UILabel labelA;
	public UILabel labelB;
	public UILabel labelC;
	public UISprite background;
}
